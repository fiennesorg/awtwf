drop table if exists GntmlZoomedImage;
create table GntmlZoomedImage (
  id int not null auto_increment, primary key (id),
  gntmlImageInstance_id int not null, index (gntmlImageInstance_id),
  style_id int not null,
  webpath varchar(128) not null,
  width int not null,
  height int not null,
  alt varchar(255) not null,
  format_id int not null, 
  sizeKb int not null,
  css varchar(32) not null
);

insert into GntmlZoomedImage 
    (gntmlImageInstance_id, style_id, webpath, width, height, alt, format_id, sizeKb, css)
  select
     controllingImage_id,   style_id, webpath, width, height, alt, format_id, sizeKb, css                    
  from GntmlImageInstance where controllingImage_id != 0;
  
delete from GntmlImageInstance where controllingImage_id != 0;