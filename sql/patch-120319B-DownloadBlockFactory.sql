DROP PROCEDURE IF EXISTS patchRecords;

DELIMITER //
CREATE PROCEDURE patchRecords()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE recordId int;
	DECLARE recordIds CURSOR FOR SELECT id from GntmlDownloadInstance;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	OPEN recordIds;
	read_loop: LOOP
		FETCH recordIds into recordId;
		IF (done) THEN
			LEAVE read_loop;
		END IF;
		update GntmlInstance, GntmlDownloadInstance
			set GntmlInstance.gntml = replace(GntmlInstance.gntml, 
			                                  concat(':',GntmlDownloadInstance.blockName,':'),
			                                  concat('|file:',GntmlDownloadInstance.blockName,'|'))
			where GntmlInstance.id = GntmlDownloadInstance.gntmlInstance_id
				and GntmlDownloadInstance.id = recordId;
	END LOOP;
	CLOSE recordIds;
END//

DELIMITER ;;

CALL patchRecords();	
DROP PROCEDURE patchRecords;
