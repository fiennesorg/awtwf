alter table GntmlEmbeddedLink 
  add titleMode int not null 
  after backLinkTitle;

alter table GntmlEmbeddedLink
  add explicitTitle varchar(255) not null
  after titleMode;