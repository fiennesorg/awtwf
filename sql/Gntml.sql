-- MySQL dump 10.9
--
-- Host: localhost    Database: Gntml
-- ------------------------------------------------------
-- Server version	4.1.10a

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

--
-- Table structure for table `AbstractStyle`
--

DROP TABLE IF EXISTS `AbstractStyle`;
CREATE TABLE `AbstractStyle` (
  `id` int(11) NOT NULL auto_increment,
  `name` char(32) default NULL,
  `isOptional` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AbstractStyle`
--


/*!40000 ALTER TABLE `AbstractStyle` DISABLE KEYS */;
LOCK TABLES `AbstractStyle` WRITE;
INSERT INTO `AbstractStyle` VALUES (1,'default',0,1),(2,'optional',1,0),(3,'compulsory',0,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `AbstractStyle` ENABLE KEYS */;

--
-- Table structure for table `Acl`
--

DROP TABLE IF EXISTS `Acl`;
CREATE TABLE `Acl` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) NOT NULL default '',
  `isInclusiveAcl` int(11) NOT NULL default '0',
  `includeOwner` int(11) NOT NULL default '0',
  `editAcl_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Acl`
--


/*!40000 ALTER TABLE `Acl` DISABLE KEYS */;
LOCK TABLES `Acl` WRITE;
INSERT INTO `Acl` VALUES (1,'rootOnly',1,0,1),(2,'public',0,0,1),(3,'loggedInOnly',0,0,1),(4,'coreAdminOnly',1,0,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `Acl` ENABLE KEYS */;

--
-- Table structure for table `BinaryAssetFormat`
--

DROP TABLE IF EXISTS `BinaryAssetFormat`;
CREATE TABLE `BinaryAssetFormat` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) NOT NULL default '',
  `extension` varchar(255) NOT NULL default '',
  `mimetype` varchar(255) NOT NULL default '',
  `iconPath` varchar(255) NOT NULL default '',
  `iconWidth` int(11) NOT NULL default '0',
  `iconHeight` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `BinaryAssetFormat`
--


/*!40000 ALTER TABLE `BinaryAssetFormat` DISABLE KEYS */;
LOCK TABLES `BinaryAssetFormat` WRITE;
INSERT INTO `BinaryAssetFormat` VALUES (1,'Adobe PDF','.pdf','application/pdf','/node/icons/pdf.gif',24,24),(2,'JPEG Image','.jpeg','image/jpeg','/node/icons/jpg.gif',24,24),(3,'JPG Image','.jpg','image/jpeg','/node/icons/jpg.gif',24,24),(4,'GIF Image','.gif','image/gif','/node/icons/gif.gif',24,24),(5,'PNG Image','.png','image/png','/node/icons/png.gif',24,24),(6,'TIFF Image','.tiff','image/tiff','/node/icons/tif.gif',24,24),(7,'TIF Image','.tif','image/tiff','/node/icons/tif.gif',24,24),(8,'PSD Image','.psd','image/x-photoshop','/node/icons/psd.gif',24,24),(9,'Shockwave Flash','.swf','application/x-shockwave-flash','/node/icons/swf.gif',24,24),(10,'Quicktime Movie','.mov','video/quicktime','/node/icons/mov.gif',24,24),(11,'MPEG Movie','.mpg','video/mpeg','/node/icons/mpg.gif',24,24),(12,'MP3 Audio','.mp3','audio/mpeg','/node/icons/mp3.gif',24,24),(13,'MS Word Document','.doc','application/msword','/node/icons/doc.gif',24,24),(14,'MS Excel Spreadsheet','.xls','application/excel','/node/icons/xls.gif',24,24),(15,'MS Powerpoint Presentation','.ppt','application/vnd.ms-powerpoint','/node/icons/ppt.gif',24,24),(16,'Comma Separated Values','.csv','text/plain','/node/icons/csv.gif',24,24),(17,'Plain Text Document','.txt','text/plain','/node/icons/txt.gif',24,24),(18,'Rich Text Format Document','.rtf','text/rtf','/node/icons/rtf.gif',24,24),(19,'Stuffit Archive','.sit','application/x-stuffit','/node/icons/sit.gif',24,24),(20,'Zip Archive','.zip','application/zip','/node/icons/zip.gif',24,24),(21,'TGZ Archive','.tgz','application/x-gtar','/node/icons/tgz.gif',24,24),(22,'GZIP Archive','.gz','application/x-gzip','/node/icons/gzip.gif',24,24),(23,'TAR Archive','.gz','application/x-tar','/node/icons/tar.gif',24,24);
UNLOCK TABLES;
/*!40000 ALTER TABLE `BinaryAssetFormat` ENABLE KEYS */;

--
-- Table structure for table `BinaryAssetInstance`
--

DROP TABLE IF EXISTS `BinaryAssetInstance`;
CREATE TABLE `BinaryAssetInstance` (
  `id` int(11) NOT NULL auto_increment,
  `region_id` int(11) NOT NULL default '0',
  `style_id` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `isUploaded` int(11) NOT NULL default '0',
  `path` varchar(255) NOT NULL default '',
  `folder` varchar(255) NOT NULL default '',
  `filename` varchar(255) NOT NULL default '',
  `format_id` int(11) NOT NULL default '0',
  `sizeKb` int(11) NOT NULL default '0',
  `title` varchar(255) default NULL,
  `description` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `region_id` (`region_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `BinaryAssetInstance`
--


/*!40000 ALTER TABLE `BinaryAssetInstance` DISABLE KEYS */;
LOCK TABLES `BinaryAssetInstance` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `BinaryAssetInstance` ENABLE KEYS */;

--
-- Table structure for table `BinaryAssetStyle`
--

DROP TABLE IF EXISTS `BinaryAssetStyle`;
CREATE TABLE `BinaryAssetStyle` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) NOT NULL default '',
  `isOptional` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `folder` varchar(255) NOT NULL default '',
  `format_id` int(11) NOT NULL default '0',
  `isFixedFormat` int(11) NOT NULL default '0',
  `hasTitle` int(11) NOT NULL default '0',
  `defaultTitle` varchar(255) NOT NULL default '',
  `hasDescription` int(11) NOT NULL default '0',
  `defaultDescription` varchar(255) NOT NULL default '',
  `maxSizeKb` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `name` (`name`),
  KEY `format_id` (`format_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `BinaryAssetStyle`
--


/*!40000 ALTER TABLE `BinaryAssetStyle` DISABLE KEYS */;
LOCK TABLES `BinaryAssetStyle` WRITE;
INSERT INTO `BinaryAssetStyle` VALUES (1,'gntmlAsset',1,0,'/assets_awtwf/dynamic',1,0,1,'',1,'',1024);
UNLOCK TABLES;
/*!40000 ALTER TABLE `BinaryAssetStyle` ENABLE KEYS */;

--
-- Table structure for table `DayIndex`
--

DROP TABLE IF EXISTS `DayIndex`;
CREATE TABLE `DayIndex` (
  `id` int(11) NOT NULL auto_increment,
  `day` int(11) NOT NULL default '0',
  `month_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `day` (`day`),
  KEY `month_id` (`month_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `DayIndex`
--


/*!40000 ALTER TABLE `DayIndex` DISABLE KEYS */;
LOCK TABLES `DayIndex` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `DayIndex` ENABLE KEYS */;

--
-- Table structure for table `ExtendedImageInstance`
--

DROP TABLE IF EXISTS `ExtendedImageInstance`;
CREATE TABLE `ExtendedImageInstance` (
  `id` int(11) NOT NULL auto_increment,
  `region_id` int(11) NOT NULL default '0',
  `style_id` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `thumbnailImage_id` int(11) NOT NULL default '0',
  `detailImage_id` int(11) NOT NULL default '0',
  `backImage_id` int(11) NOT NULL default '0',
  `title` char(255) NOT NULL default '',
  `clickImageUrl` char(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `region_id` (`region_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ExtendedImageInstance`
--


/*!40000 ALTER TABLE `ExtendedImageInstance` DISABLE KEYS */;
LOCK TABLES `ExtendedImageInstance` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `ExtendedImageInstance` ENABLE KEYS */;

--
-- Table structure for table `ExtendedImageStyle`
--

DROP TABLE IF EXISTS `ExtendedImageStyle`;
CREATE TABLE `ExtendedImageStyle` (
  `id` int(11) NOT NULL auto_increment,
  `name` char(32) default NULL,
  `isOptional` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `thumbnailStyle_id` int(11) NOT NULL default '0',
  `detailStyle_id` int(11) NOT NULL default '0',
  `detailWindowName` char(32) NOT NULL default '',
  `hasLinkedDetail` int(11) NOT NULL default '0',
  `detailTemplate` char(255) NOT NULL default '',
  `detailIsRenderReset` int(11) NOT NULL default '0',
  `backStyle_id` int(11) NOT NULL default '0',
  `backInvokesClose` int(11) NOT NULL default '0',
  `backText` char(255) NOT NULL default '',
  `backCss` char(32) NOT NULL default '',
  `titleCss` char(32) NOT NULL default '',
  `defaultClickImageUrl` char(255) default NULL,
  `isEditableClickImageUrl` int(11) NOT NULL default '0',
  `urlWindowName` char(32) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ExtendedImageStyle`
--


/*!40000 ALTER TABLE `ExtendedImageStyle` DISABLE KEYS */;
LOCK TABLES `ExtendedImageStyle` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `ExtendedImageStyle` ENABLE KEYS */;

--
-- Table structure for table `GntmlClass`
--

DROP TABLE IF EXISTS `GntmlClass`;
CREATE TABLE `GntmlClass` (
  `id` int(11) NOT NULL auto_increment,
  `englishName` varchar(255) default NULL,
  `cssName` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `GntmlClass`
--


/*!40000 ALTER TABLE `GntmlClass` DISABLE KEYS */;
LOCK TABLES `GntmlClass` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `GntmlClass` ENABLE KEYS */;

--
-- Table structure for table `GntmlDownloadInstance`
--

DROP TABLE IF EXISTS `GntmlDownloadInstance`;
CREATE TABLE `GntmlDownloadInstance` (
  `id` int(11) NOT NULL auto_increment,
  `gntmlInstance_id` int(11) NOT NULL default '0',
  `downloadName` varchar(255) NOT NULL default '',
  `isReferenced` int(11) NOT NULL default '0',
  `style_id` int(11) NOT NULL default '0',
  `isUploaded` int(11) NOT NULL default '0',
  `path` varchar(255) NOT NULL default '',
  `folder` varchar(255) NOT NULL default '',
  `filename` varchar(255) NOT NULL default '',
  `format_id` int(11) NOT NULL default '0',
  `sizeKb` int(11) NOT NULL default '0',
  `title` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `GntmlDownloadInstance`
--


/*!40000 ALTER TABLE `GntmlDownloadInstance` DISABLE KEYS */;
LOCK TABLES `GntmlDownloadInstance` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `GntmlDownloadInstance` ENABLE KEYS */;

--
-- Table structure for table `GntmlImageInstance`
--

DROP TABLE IF EXISTS `GntmlImageInstance`;
CREATE TABLE `GntmlImageInstance` (
  `id` int(11) NOT NULL auto_increment,
  `gntmlInstance_id` int(11) NOT NULL default '0',
  `imageName` varchar(255) NOT NULL default '',
  `isReferenced` int(11) NOT NULL default '0',
  `style_id` int(11) NOT NULL default '0',
  `title` varchar(255) NOT NULL default '',
  `webpath` varchar(128) NOT NULL default '',
  `width` int(11) NOT NULL default '0',
  `height` int(11) NOT NULL default '0',
  `alt` varchar(255) NOT NULL default '',
  `format_id` int(11) NOT NULL default '0',
  `sizeKb` int(11) NOT NULL default '0',
  `css` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `GntmlImageInstance`
--


/*!40000 ALTER TABLE `GntmlImageInstance` DISABLE KEYS */;
LOCK TABLES `GntmlImageInstance` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `GntmlImageInstance` ENABLE KEYS */;

--
-- Table structure for table `GntmlInstance`
--

DROP TABLE IF EXISTS `GntmlInstance`;
CREATE TABLE `GntmlInstance` (
  `id` int(11) NOT NULL auto_increment,
  `region_id` int(11) default NULL,
  `style_id` int(11) default NULL,
  `isDefined` int(11) NOT NULL default '0',
  `gntml` text NOT NULL,
  `html` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `region_id` (`region_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `GntmlInstance`
--


/*!40000 ALTER TABLE `GntmlInstance` DISABLE KEYS */;
LOCK TABLES `GntmlInstance` WRITE;
INSERT INTO `GntmlInstance` VALUES (1,1,1,1,'','');
UNLOCK TABLES;
/*!40000 ALTER TABLE `GntmlInstance` ENABLE KEYS */;

--
-- Table structure for table `GntmlStyle`
--

DROP TABLE IF EXISTS `GntmlStyle`;
CREATE TABLE `GntmlStyle` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) NOT NULL default '',
  `isOptional` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `GntmlStyle`
--


/*!40000 ALTER TABLE `GntmlStyle` DISABLE KEYS */;
LOCK TABLES `GntmlStyle` WRITE;
INSERT INTO `GntmlStyle` VALUES (1,'compulsory',0,1),(2,'optional',1,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `GntmlStyle` ENABLE KEYS */;

--
-- Table structure for table `HtmlInstance`
--

DROP TABLE IF EXISTS `HtmlInstance`;
CREATE TABLE `HtmlInstance` (
  `id` int(11) NOT NULL auto_increment,
  `region_id` int(11) NOT NULL default '0',
  `style_id` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `html` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `region_id` (`region_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `HtmlInstance`
--


/*!40000 ALTER TABLE `HtmlInstance` DISABLE KEYS */;
LOCK TABLES `HtmlInstance` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `HtmlInstance` ENABLE KEYS */;

--
-- Table structure for table `HtmlMetaTag`
--

DROP TABLE IF EXISTS `HtmlMetaTag`;
CREATE TABLE `HtmlMetaTag` (
  `id` int(11) NOT NULL auto_increment,
  `region_id` int(11) NOT NULL default '0',
  `style_id` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `pageKeywords` varchar(255) NOT NULL default '',
  `cascadingKeywords` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `region_id` (`region_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `HtmlMetaTag`
--


/*!40000 ALTER TABLE `HtmlMetaTag` DISABLE KEYS */;
LOCK TABLES `HtmlMetaTag` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `HtmlMetaTag` ENABLE KEYS */;

--
-- Table structure for table `HtmlStyle`
--

DROP TABLE IF EXISTS `HtmlStyle`;
CREATE TABLE `HtmlStyle` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) default NULL,
  `isOptional` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `textareaCss` varchar(255) NOT NULL default '',
  `textareaCols` int(11) default NULL,
  `textareaRows` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `HtmlStyle`
--


/*!40000 ALTER TABLE `HtmlStyle` DISABLE KEYS */;
LOCK TABLES `HtmlStyle` WRITE;
INSERT INTO `HtmlStyle` VALUES (1,'default',0,1,'defaultFont',40,8),(2,'optional',1,0,'defaultFont',40,8);
UNLOCK TABLES;
/*!40000 ALTER TABLE `HtmlStyle` ENABLE KEYS */;

--
-- Table structure for table `MonthIndex`
--

DROP TABLE IF EXISTS `MonthIndex`;
CREATE TABLE `MonthIndex` (
  `id` int(11) NOT NULL auto_increment,
  `month` int(11) NOT NULL default '0',
  `name` varchar(16) NOT NULL default '',
  `year_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `month` (`month`),
  KEY `year_id` (`year_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `MonthIndex`
--


/*!40000 ALTER TABLE `MonthIndex` DISABLE KEYS */;
LOCK TABLES `MonthIndex` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `MonthIndex` ENABLE KEYS */;

--
-- Table structure for table `Node`
--

DROP TABLE IF EXISTS `Node`;
CREATE TABLE `Node` (
  `id` int(11) NOT NULL auto_increment,
  `parentNode_id` int(11) NOT NULL default '0',
  `parentNodeGroup_id` int(11) NOT NULL default '0',
  `number` int(11) NOT NULL default '0',
  `title` varchar(255) NOT NULL default '',
  `htmlTitle` varchar(255) NOT NULL default '',
  `filename` varchar(64) NOT NULL default '',
  `isPublished` int(11) NOT NULL default '0',
  `creationTime` bigint(20) NOT NULL default '0',
  `modificationTime` bigint(20) NOT NULL default '0',
  `nodeStyle_id` int(11) NOT NULL default '0',
  `owner_id` int(11) NOT NULL default '0',
  `viewAcl_id` int(11) NOT NULL default '0',
  `editAcl_id` int(11) NOT NULL default '0',
  `publishAcl_id` int(11) NOT NULL default '0',
  `deleteAcl_id` int(11) NOT NULL default '0',
  `generation` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `parentNodeGroup_id` (`parentNodeGroup_id`,`number`),
  KEY `isPublished` (`isPublished`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Node`
--


/*!40000 ALTER TABLE `Node` DISABLE KEYS */;
LOCK TABLES `Node` WRITE;
INSERT INTO `Node` VALUES (1,0,1,1,'','','',1,1118320718470,1118320718483,1,1,2,4,1,1,0),(2,1,2,1,'feedbackError','feedbackError','feedbackError',1,1118320718493,1118320718495,1002,1,2,1,1,1,1),(3,1,3,1,'404','404','404',1,1118320718500,1118320718502,1001,1,2,1,1,1,1),(4,1,4,1,'Login','Login','Login',1,1118320718506,1118320718508,1000,1,2,1,1,1,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `Node` ENABLE KEYS */;

--
-- Table structure for table `NodeGroup`
--

DROP TABLE IF EXISTS `NodeGroup`;
CREATE TABLE `NodeGroup` (
  `id` int(11) NOT NULL auto_increment,
  `parentNode_id` int(11) NOT NULL default '0',
  `createChildAcl_id` int(11) NOT NULL default '0',
  `number` int(11) NOT NULL default '0',
  `nodeGroupStyle_id` int(11) NOT NULL default '0',
  `name` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `parentNode_id` (`parentNode_id`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `NodeGroup`
--


/*!40000 ALTER TABLE `NodeGroup` DISABLE KEYS */;
LOCK TABLES `NodeGroup` WRITE;
INSERT INTO `NodeGroup` VALUES (1,0,1,1,1,'Home'),(2,1,1,10,1002,'FeedbackErrorGroup'),(3,1,1,10,1001,'FourOhFourGroup'),(4,1,1,10,1000,'LoginGroup');
UNLOCK TABLES;
/*!40000 ALTER TABLE `NodeGroup` ENABLE KEYS */;

--
-- Table structure for table `NodeGroupStyle`
--

DROP TABLE IF EXISTS `NodeGroupStyle`;
CREATE TABLE `NodeGroupStyle` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) NOT NULL default '',
  `parentNodeStyle_id` int(11) NOT NULL default '0',
  `childNodeStyle_id` int(11) NOT NULL default '0',
  `createChildAcl_id` int(11) NOT NULL default '0',
  `updateCreateChildAcl_id` int(11) NOT NULL default '0',
  `number` int(11) NOT NULL default '0',
  `minimum` int(11) NOT NULL default '0',
  `maximum` int(11) NOT NULL default '0',
  `isUpdateLinked` int(11) NOT NULL default '0',
  `isDeleteLinked` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `parentNodeStyle_id` (`parentNodeStyle_id`,`name`,`number`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `NodeGroupStyle`
--


/*!40000 ALTER TABLE `NodeGroupStyle` DISABLE KEYS */;
LOCK TABLES `NodeGroupStyle` WRITE;
INSERT INTO `NodeGroupStyle` VALUES (1,'Home',0,1,1,1,1,1,1,0,0),(1000,'LoginGroup',1,1000,1,1,10,1,1,0,0),(1001,'FourOhFourGroup',1,1001,1,1,10,1,1,0,0),(1002,'FeedbackErrorGroup',1,1002,1,1,10,1,1,0,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `NodeGroupStyle` ENABLE KEYS */;

--
-- Table structure for table `NodeStyle`
--

DROP TABLE IF EXISTS `NodeStyle`;
CREATE TABLE `NodeStyle` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) NOT NULL default '',
  `filename` varchar(255) NOT NULL default '',
  `isEditableFilename` int(11) NOT NULL default '0',
  `owner_id` int(11) NOT NULL default '0',
  `viewAcl_id` int(11) NOT NULL default '0',
  `updateViewAcl_id` int(11) NOT NULL default '0',
  `editAcl_id` int(11) NOT NULL default '0',
  `updateEditAcl_id` int(11) NOT NULL default '0',
  `publishAcl_id` int(11) NOT NULL default '0',
  `updatePublishAcl_id` int(11) NOT NULL default '0',
  `deleteAcl_id` int(11) NOT NULL default '0',
  `updateDeleteAcl_id` int(11) NOT NULL default '0',
  `cssPath` varchar(255) NOT NULL default '',
  `rootTemplatePath` varchar(255) NOT NULL default '',
  `viewTemplatePath` varchar(255) NOT NULL default '',
  `editTemplatePath` varchar(255) NOT NULL default '',
  `isRenderReset` int(11) NOT NULL default '0',
  `defaultIsPublished` int(11) NOT NULL default '0',
  `mayEditPublished` int(11) NOT NULL default '0',
  `isMenuItem` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `NodeStyle`
--


/*!40000 ALTER TABLE `NodeStyle` DISABLE KEYS */;
LOCK TABLES `NodeStyle` WRITE;
INSERT INTO `NodeStyle` VALUES (1,'Home','',0,1,2,1,4,1,1,1,1,1,'digest.css','gntml/home.wm.html','gntml/home.view.wm.html','gntml/home.edit.wm.html',0,1,1,1),(1000,'Login','Login',0,1,2,1,1,1,1,1,1,1,'','','node/login.view.wm.html','mccp/node.edit.wm.html',0,1,1,1),(1001,'FourOhFour','404',0,1,2,1,1,1,1,1,1,1,'','','node/404.view.wm.html','node/404.edit.wm.html',0,1,1,0),(1002,'FeedbackError','feedbackError',0,1,2,1,1,1,1,1,1,1,'','','node/feedbackError.view.wm.html','node/feedbackError.edit.wm.html',0,1,1,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `NodeStyle` ENABLE KEYS */;

--
-- Table structure for table `NtmlInstance`
--

DROP TABLE IF EXISTS `NtmlInstance`;
CREATE TABLE `NtmlInstance` (
  `id` int(11) NOT NULL auto_increment,
  `region_id` int(11) NOT NULL default '0',
  `style_id` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `ntml` text NOT NULL,
  `html` text NOT NULL,
  `flowAlgorithmName` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `region_id` (`region_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `NtmlInstance`
--


/*!40000 ALTER TABLE `NtmlInstance` DISABLE KEYS */;
LOCK TABLES `NtmlInstance` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `NtmlInstance` ENABLE KEYS */;

--
-- Table structure for table `NtmlStyle`
--

DROP TABLE IF EXISTS `NtmlStyle`;
CREATE TABLE `NtmlStyle` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) default NULL,
  `isOptional` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `textareaCss` varchar(255) NOT NULL default '',
  `textareaCols` int(11) NOT NULL default '0',
  `textareaRows` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `NtmlStyle`
--


/*!40000 ALTER TABLE `NtmlStyle` DISABLE KEYS */;
LOCK TABLES `NtmlStyle` WRITE;
INSERT INTO `NtmlStyle` VALUES (1,'default',0,1,'defaultFont',40,8),(2,'optional',1,0,'defaultFont',40,8);
UNLOCK TABLES;
/*!40000 ALTER TABLE `NtmlStyle` ENABLE KEYS */;

--
-- Table structure for table `Region`
--

DROP TABLE IF EXISTS `Region`;
CREATE TABLE `Region` (
  `id` int(11) NOT NULL auto_increment,
  `node_id` int(11) NOT NULL default '0',
  `regionStyle_id` int(11) NOT NULL default '0',
  `name` varchar(32) NOT NULL default '',
  `number` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `node_id` (`node_id`,`name`,`number`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Region`
--


/*!40000 ALTER TABLE `Region` DISABLE KEYS */;
LOCK TABLES `Region` WRITE;
INSERT INTO `Region` VALUES (1,1,1,'pageDocument',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `Region` ENABLE KEYS */;

--
-- Table structure for table `RegionStyle`
--

DROP TABLE IF EXISTS `RegionStyle`;
CREATE TABLE `RegionStyle` (
  `id` int(11) NOT NULL auto_increment,
  `nodeStyle_id` int(11) NOT NULL default '0',
  `number` int(11) NOT NULL default '0',
  `name` varchar(32) NOT NULL default '',
  `aliasName` varchar(32) NOT NULL default '',
  `title` varchar(255) NOT NULL default '',
  `compilerName` varchar(255) NOT NULL default '',
  `compilerStyleName` varchar(255) NOT NULL default '',
  `isFull` int(11) NOT NULL default '0',
  `isSummary` int(11) NOT NULL default '0',
  `minimum` int(11) NOT NULL default '0',
  `maximum` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `nodeStyle_id` (`nodeStyle_id`,`name`,`number`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `RegionStyle`
--


/*!40000 ALTER TABLE `RegionStyle` DISABLE KEYS */;
LOCK TABLES `RegionStyle` WRITE;
INSERT INTO `RegionStyle` VALUES (1,1,2,'pageDocument','','Page Document','GNTML','compulsory',1,0,1,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `RegionStyle` ENABLE KEYS */;

--
-- Table structure for table `SettableValue`
--

DROP TABLE IF EXISTS `SettableValue`;
CREATE TABLE `SettableValue` (
  `id` int(11) NOT NULL auto_increment,
  `namespace` varchar(32) NOT NULL default '',
  `valueKey` varchar(255) NOT NULL default '',
  `value` varchar(255) default NULL,
  `originalValue` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `namespace` (`namespace`,`valueKey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SettableValue`
--


/*!40000 ALTER TABLE `SettableValue` DISABLE KEYS */;
LOCK TABLES `SettableValue` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `SettableValue` ENABLE KEYS */;

--
-- Table structure for table `SimpleTextInstance`
--

DROP TABLE IF EXISTS `SimpleTextInstance`;
CREATE TABLE `SimpleTextInstance` (
  `id` int(11) NOT NULL auto_increment,
  `region_id` int(11) NOT NULL default '0',
  `style_id` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `text` text NOT NULL,
  `html` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `region_id` (`region_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SimpleTextInstance`
--


/*!40000 ALTER TABLE `SimpleTextInstance` DISABLE KEYS */;
LOCK TABLES `SimpleTextInstance` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `SimpleTextInstance` ENABLE KEYS */;

--
-- Table structure for table `SimpleTextStyle`
--

DROP TABLE IF EXISTS `SimpleTextStyle`;
CREATE TABLE `SimpleTextStyle` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) NOT NULL default '',
  `isOptional` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `maxLength` int(11) NOT NULL default '0',
  `supportsLinebreaks` int(11) NOT NULL default '0',
  `editorRows` int(11) NOT NULL default '0',
  `editorColumns` int(11) NOT NULL default '0',
  `editCss` varchar(255) NOT NULL default '',
  `viewCss` varchar(255) NOT NULL default '',
  `isBlock` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SimpleTextStyle`
--


/*!40000 ALTER TABLE `SimpleTextStyle` DISABLE KEYS */;
LOCK TABLES `SimpleTextStyle` WRITE;
INSERT INTO `SimpleTextStyle` VALUES (1,'default',0,1,255,0,1,40,'defaultFont','defaultFont',0),(2,'optional',1,0,255,0,1,40,'defaultFont','defaultFont',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `SimpleTextStyle` ENABLE KEYS */;

--
-- Table structure for table `SingleExtendedImage`
--

DROP TABLE IF EXISTS `SingleExtendedImage`;
CREATE TABLE `SingleExtendedImage` (
  `id` int(11) NOT NULL auto_increment,
  `style_id` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `webpath` char(255) NOT NULL default '',
  `width` int(11) NOT NULL default '0',
  `height` int(11) NOT NULL default '0',
  `alt` char(255) NOT NULL default '',
  `format_id` int(11) NOT NULL default '0',
  `sizeKb` int(11) NOT NULL default '0',
  `css` char(32) NOT NULL default '',
  `extendedImageInstance_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `extendedImageInstance_id` (`extendedImageInstance_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SingleExtendedImage`
--


/*!40000 ALTER TABLE `SingleExtendedImage` DISABLE KEYS */;
LOCK TABLES `SingleExtendedImage` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `SingleExtendedImage` ENABLE KEYS */;

--
-- Table structure for table `SingleImageFormat`
--

DROP TABLE IF EXISTS `SingleImageFormat`;
CREATE TABLE `SingleImageFormat` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) NOT NULL default '',
  `imParameter` varchar(255) NOT NULL default '',
  `imFormat` varchar(255) NOT NULL default '',
  `extension` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SingleImageFormat`
--


/*!40000 ALTER TABLE `SingleImageFormat` DISABLE KEYS */;
LOCK TABLES `SingleImageFormat` WRITE;
INSERT INTO `SingleImageFormat` VALUES (1,'PNG: full colour','','PNG','.png'),(2,'GIF: 256 colour (dithered)','-dither','GIF','.gif'),(3,'GIF: 256 colour (no-dither)','+dither','GIF','.gif'),(4,'JPEG: (low quality)','-quality 50','JPEG','.jpeg'),(5,'JPEG: (medium quality)','-quality 75','JPEG','.jpeg'),(6,'JPEG: (high quality)','-quality 95','JPEG','.jpeg');
UNLOCK TABLES;
/*!40000 ALTER TABLE `SingleImageFormat` ENABLE KEYS */;

--
-- Table structure for table `SingleImageInstance`
--

DROP TABLE IF EXISTS `SingleImageInstance`;
CREATE TABLE `SingleImageInstance` (
  `id` int(11) NOT NULL auto_increment,
  `region_id` int(11) NOT NULL default '0',
  `style_id` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `webpath` varchar(128) NOT NULL default '',
  `width` int(11) NOT NULL default '0',
  `height` int(11) NOT NULL default '0',
  `alt` varchar(255) NOT NULL default '',
  `format_id` int(11) NOT NULL default '0',
  `sizeKb` int(11) NOT NULL default '0',
  `css` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `region_id` (`region_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SingleImageInstance`
--


/*!40000 ALTER TABLE `SingleImageInstance` DISABLE KEYS */;
LOCK TABLES `SingleImageInstance` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `SingleImageInstance` ENABLE KEYS */;

--
-- Table structure for table `SingleImageStyle`
--

DROP TABLE IF EXISTS `SingleImageStyle`;
CREATE TABLE `SingleImageStyle` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) NOT NULL default '',
  `isOptional` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `defaultFilename` varchar(255) NOT NULL default '',
  `defaultWidth` int(11) NOT NULL default '0',
  `defaultHeight` int(11) NOT NULL default '0',
  `targetWidth` int(11) NOT NULL default '0',
  `targetHeight` int(11) NOT NULL default '0',
  `imageScalingMode` int(11) NOT NULL default '0',
  `webpathRoot` varchar(255) default NULL,
  `defaultAlt` varchar(255) default NULL,
  `hasEditableAlt` int(11) NOT NULL default '0',
  `format_id` int(11) NOT NULL default '0',
  `isFixedFormat` int(11) NOT NULL default '0',
  `css` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SingleImageStyle`
--


/*!40000 ALTER TABLE `SingleImageStyle` DISABLE KEYS */;
LOCK TABLES `SingleImageStyle` WRITE;
INSERT INTO `SingleImageStyle` VALUES (1,'gntmlThumbnail',1,0,'gntmlThumbnail.jpg',100,200,100,200,3,'assets_awtwf/dynamic/','Thumbnail Image',1,5,0,'gntmlThumbnail'),(2,'gntmlPage',1,0,'gntmlPage.jpg',200,400,200,400,3,'assets_awtwf/dynamic/','Page Image',1,5,0,'gntmlPage'),(3,'gntmlZoom',1,0,'gntmlZoom.jpg',600,600,600,600,3,'assets_awtwf/dynamic/','Zoomed Image',1,5,0,'gntmlZoom');
UNLOCK TABLES;
/*!40000 ALTER TABLE `SingleImageStyle` ENABLE KEYS */;

--
-- Table structure for table `TemporalInstance`
--

DROP TABLE IF EXISTS `TemporalInstance`;
CREATE TABLE `TemporalInstance` (
  `id` int(11) NOT NULL auto_increment,
  `region_id` int(11) NOT NULL default '0',
  `style_id` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `eventDate` bigint(20) NOT NULL default '0',
  `eventDate_year_id` int(11) NOT NULL default '0',
  `eventDate_month_id` int(11) NOT NULL default '0',
  `eventDate_day_id` int(11) NOT NULL default '0',
  `publishDateStart` bigint(20) NOT NULL default '0',
  `publishDateStart_year_id` int(11) NOT NULL default '0',
  `publishDateStart_month_id` int(11) NOT NULL default '0',
  `publishDateStart_day_id` int(11) NOT NULL default '0',
  `publishDateEnd` bigint(20) NOT NULL default '0',
  `publishDateEnd_year_id` int(11) NOT NULL default '0',
  `publishDateEnd_month_id` int(11) NOT NULL default '0',
  `publishDateEnd_day_id` int(11) NOT NULL default '0',
  `isPublished` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `region_id` (`region_id`),
  KEY `eventDate_year_id` (`eventDate_year_id`),
  KEY `eventDate_month_id` (`eventDate_month_id`),
  KEY `eventDate_day_id` (`eventDate_day_id`),
  KEY `publishDateStart_year_id` (`publishDateStart_year_id`),
  KEY `publishDateStart_month_id` (`publishDateStart_month_id`),
  KEY `publishDateStart_day_id` (`publishDateStart_day_id`),
  KEY `publishDateEnd_year_id` (`publishDateEnd_year_id`),
  KEY `publishDateEnd_month_id` (`publishDateEnd_month_id`),
  KEY `publishDateEnd_day_id` (`publishDateEnd_day_id`),
  KEY `isPublished` (`isPublished`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TemporalInstance`
--


/*!40000 ALTER TABLE `TemporalInstance` DISABLE KEYS */;
LOCK TABLES `TemporalInstance` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `TemporalInstance` ENABLE KEYS */;

--
-- Table structure for table `TemporalStyle`
--

DROP TABLE IF EXISTS `TemporalStyle`;
CREATE TABLE `TemporalStyle` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) NOT NULL default '',
  `isOptional` int(11) NOT NULL default '0',
  `isDefined` int(11) NOT NULL default '0',
  `hasEventDate` int(11) NOT NULL default '0',
  `eventDate` bigint(20) NOT NULL default '0',
  `eventDateLabel` varchar(255) NOT NULL default '',
  `hasPublishDate` int(11) NOT NULL default '0',
  `publishDateStart` bigint(20) NOT NULL default '0',
  `publishDateLength` bigint(20) NOT NULL default '0',
  `publishDateLabel` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TemporalStyle`
--


/*!40000 ALTER TABLE `TemporalStyle` DISABLE KEYS */;
LOCK TABLES `TemporalStyle` WRITE;
INSERT INTO `TemporalStyle` VALUES (1,'default',0,1,1,-1,'Event Date',1,-1,28,'Publish'),(2,'optional',1,0,1,-1,'Event Date',1,-1,28,'Publish');
UNLOCK TABLES;
/*!40000 ALTER TABLE `TemporalStyle` ENABLE KEYS */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `email` varchar(255) NOT NULL default '',
  `login` varchar(255) NOT NULL default '',
  `password` varchar(255) NOT NULL default '',
  `creationDate` bigint(20) NOT NULL default '0',
  `ipFilter` varchar(255) NOT NULL default '',
  `status` int(11) NOT NULL default '0',
  `secretCode` bigint(20) NOT NULL default '0',
  `plainTextPassword` varchar(255) NOT NULL default '',
  `passwordClue` varchar(255) NOT NULL default '',
  `userStyle_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `login` (`login`),
  KEY `userStyle_id` (`userStyle_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `User`
--


/*!40000 ALTER TABLE `User` DISABLE KEYS */;
LOCK TABLES `User` WRITE;
INSERT INTO `User` VALUES (1,'root','webmaster@localhost','root','85074f3169e436e3556a0be9ee45487b01aaf1c4',0,'127.0.0.1/32',1,0,'','',1),(2,'anonymous','webmaster@localhost','anonymous','da39a3ee5e6b4b0d3255bfef95601890afd80709',0,'',1,0,'','',1),(3,'Core Administrator','','core','94a0426e8d3203da5468ccf0c624f93cb37601e2',0,'',1,0,'','',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `User` ENABLE KEYS */;

--
-- Table structure for table `UserAcl`
--

DROP TABLE IF EXISTS `UserAcl`;
CREATE TABLE `UserAcl` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL default '0',
  `acl_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `user_id` (`user_id`),
  KEY `acl_id` (`acl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `UserAcl`
--


/*!40000 ALTER TABLE `UserAcl` DISABLE KEYS */;
LOCK TABLES `UserAcl` WRITE;
INSERT INTO `UserAcl` VALUES (1,1,1),(2,2,3),(3,1,4),(4,3,4);
UNLOCK TABLES;
/*!40000 ALTER TABLE `UserAcl` ENABLE KEYS */;

--
-- Table structure for table `UserStyle`
--

DROP TABLE IF EXISTS `UserStyle`;
CREATE TABLE `UserStyle` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(32) NOT NULL default '',
  `hasPlainTextPassword` int(11) NOT NULL default '0',
  `hasPasswordClue` int(11) NOT NULL default '0',
  `mayDelete` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `UserStyle`
--


/*!40000 ALTER TABLE `UserStyle` DISABLE KEYS */;
LOCK TABLES `UserStyle` WRITE;
INSERT INTO `UserStyle` VALUES (1,'Paranoid',0,0,0),(2,'HighSecurity',0,0,1),(3,'MediumSecurity',0,1,1),(4,'LowSecurity',1,1,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `UserStyle` ENABLE KEYS */;

--
-- Table structure for table `WordIndex`
--

DROP TABLE IF EXISTS `WordIndex`;
CREATE TABLE `WordIndex` (
  `id` int(11) NOT NULL auto_increment,
  `wordHash` int(11) NOT NULL default '0',
  `node_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `wordHash` (`wordHash`),
  KEY `node_id` (`node_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `WordIndex`
--


/*!40000 ALTER TABLE `WordIndex` DISABLE KEYS */;
LOCK TABLES `WordIndex` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `WordIndex` ENABLE KEYS */;

--
-- Table structure for table `YearIndex`
--

DROP TABLE IF EXISTS `YearIndex`;
CREATE TABLE `YearIndex` (
  `id` int(11) NOT NULL auto_increment,
  `year` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `year` (`year`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `YearIndex`
--


/*!40000 ALTER TABLE `YearIndex` DISABLE KEYS */;
LOCK TABLES `YearIndex` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `YearIndex` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

