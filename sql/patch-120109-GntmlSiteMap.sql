drop table if exists GntmlSiteMap;
create table GntmlSiteMap (
        id int not null auto_increment, primary key (id),
        gntmlInstance_id int not null, index (gntmlInstance_id),
        blockName varchar(255) not null,
        isReferenced int not null,
        title varchar(255),
        rootNode_id int,
        navigationStyle varchar(255) not null,
        validChildren varchar(255) not null,
        maxDepth int not null
        );
