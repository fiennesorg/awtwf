-- script to migrate to blockName for all GNTML blocks

alter table GntmlDownloadInstance
  change downloadName
  blockName varchar(255) not null;

alter table GntmlImageInstance
  change imageName
  blockName varchar(255) not null;

alter table GntmlStyle
  add
  defaultDownloadStyle_id int not null default 1;
