drop table if exists GntmlPageList;
create table GntmlPageList (
	id int not null auto_increment, primary key (id),
	gntmlInstance_id int not null, index (gntmlInstance_id),
	blockName varchar(255) not null,
	isReferenced int not null,
	listTitle varchar(255) not null,
	hasExplicitOrdering int not null,
	isPaginated int not null,
	listingView varchar(255) not null,
	hasBackLinks int not null,
	backLinkTitle varchar(255) not null
);

drop table if exists GntmlPageListJoin;
create table GntmlPageListJoin (
  id int not null auto_increment, primary key (id),
  gntmlPageList_id int not null, index(gntmlPageList_id),
  node_id int not null, index(node_id),
  isDefined int not null,
  number int not null
);
