alter table GntmlImageInstance
  add hasNodeTarget int not null
  after imageTarget;
  
alter table GntmlImageInstance
  add targetNode_id int not null
  after hasNodeTarget;
  
alter table GntmlImageInstance
  add index (targetNode_id);
  
alter table GntmlImageInstance
  add hasBackLink int not null
  after targetNode_id;
  
alter table GntmlImageInstance
  add backLinkTitle varchar(255) not null
  after hasBackLink;
  
