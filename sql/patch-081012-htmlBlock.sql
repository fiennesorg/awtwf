drop table if exists HtmlBlock;
create table HtmlBlock (
	id int not null auto_increment, primary key (id),
	gntmlInstance_id int not null,
	blockName varchar(255) not null,
	isReferenced int not null,
	html text not null
);
