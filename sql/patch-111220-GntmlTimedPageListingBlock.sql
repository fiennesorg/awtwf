drop table if exists GntmlTimedPageListingBlock;
create table GntmlTimedPageListingBlock (
        id int not null auto_increment,
                primary key (id),
        gntmlInstance_id int not null,
        blockName varchar(255) not null,
        isReferenced int not null,
        timedPageListing_id int not null,
        timedPageCount int not null,
        templatePath varchar(255) not null,
        title varchar(255) not null
);
