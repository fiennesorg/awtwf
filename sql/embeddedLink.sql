drop table if exists GntmlEmbeddedLink;
create table GntmlEmbeddedLink (
  id int not null auto_increment, primary key (id),
  gntmlInstance_id int not null, index (gntmlInstance_id),
  name varchar(255) not null,
  isReferenced int not null,
  node_id int not null, index (node_id),
  contents varchar(255),
  hasBackLinks int not null,
  backLinkTitle varchar(255) not null
);