drop table if exists GntmlIndexListingBlock;
create table GntmlIndexListingBlock (
        id int not null auto_increment,
                primary key (id),
        gntmlInstance_id int not null,
        blockName varchar(255) not null,
        isReferenced int not null,
        title varchar(255),
        indexListing_id int not null,
        templatePath varchar(255) not null
);
