insert into SingleImageStyle values (0,'gntmlThumbnail',1,0,'gntmlThumbnail.jpg',100,200,100,200,3,'assets_awtwf/dynamic/','Thumbnail Image',1,5,0,'gntmlThumbnail');
insert into SingleImageStyle values (0,'gntmlPage',1,0,'gntmlPage.jpg',200,400,200,400,3,'assets_awtwf/dynamic/','Page Image',1,5,0,'gntmlPage');
insert into SingleImageStyle values (0,'gntmlZoom',1,0,'gntmlZoom.jpg',600,600,600,600,3,'assets_awtwf/dynamic/','Zoomed Image',1,5,0,'gntmlZoom');

insert into BinaryAssetStyle values (0, 'gntmlAsset',1,0,'/assets_awtwf/dynamic',1,0,1,'',1,'',1024);

# Home
insert into NodeGroupStyle values (1,'Home', 0,1, 1,1, 1, 1,1, 0,0);
insert into NodeStyle values (1,'Home', '',0, 1, 2,1, 4,1, 1,1, 1,1, 'digest.css', 'gntml/home.wm.html', 'gntml/home.view.wm.html','gntml/home.edit.wm.html',0,1,1,1);
insert into RegionStyle values (0,1,2,'pageDocument','','Page Document','GNTML','compulsory',1,0,1,1);


  # Login
  insert into NodeGroupStyle values (1000,'LoginGroup', 1,1000, 1,1, 10, 1,1, 0,0);
  insert into NodeStyle values (1000,'Login','Login',0, 1, 2,1, 1,1, 1,1, 1,1, '','','node/login.view.wm.html','mccp/node.edit.wm.html', 0,1,1,1);

  # 404
  insert into NodeGroupStyle values (1001,'FourOhFourGroup',1,1001, 1,1, 10, 1,1,0,0);
  insert into NodeStyle values (1001,'FourOhFour','404',0, 1, 2,1, 1,1, 1,1, 1,1, '','','node/404.view.wm.html','node/404.edit.wm.html',0,1,1,0);

  # feedback error
  insert into NodeGroupStyle values (1002,'FeedbackErrorGroup',1,1002, 1,1, 10, 1,1, 0,0);
  insert into NodeStyle values (1002,'FeedbackError','feedbackError',0, 1, 2,1, 1,1, 1,1, 1,1, '','','node/feedbackError.view.wm.html','node/feedbackError.edit.wm.html',0,1,1,0);


# Booting seed...
insert into NodeGroup values (1,0,1,1,1,'Home');
