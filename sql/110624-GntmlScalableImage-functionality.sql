alter table GntmlScalableImage add functionality varchar(255) not null after title;

update GntmlScalableImage set functionality='NONE';

update GntmlScalableImage set functionality='ZOOM' where hasZoomedView = 1;
alter table GntmlScalableImage drop hasZoomedView;

update GntmlScalableImage set functionality='NODE' where functionality='NONE' and hasNodeTarget=1;
alter table GntmlScalableImage drop hasNodeTarget;

update GntmlScalableImage set functionality='EXTERNAL' where functionality='NONE' and imageTarget != '';
