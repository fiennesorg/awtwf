-- Copy all of the information in the NtmlInstance Table to GntmlInstance

-- the html will not be ideal yet, because it will still have been
-- parsed as Ntml as opposed to Gntml but as long as we leave the legacy
-- CSS in place then there will be no visible change.  Obviously when we
-- edit the GNTML then it will become updated...

-- Gntml.id = Ntml.id
-- Gtml.node_id = Ntml.node_id
-- Gntml.regionStyle_id = Ntml.regionStyle_id
-- Gntml.style_id = Ntml.style_id
-- Gntml.isDefined = Ntml.isDefined
-- Gntml.gntml = Ntml.ntml
-- Gntml.html = Ntml.html

insert into GntmlInstance 
        (id,node_id, regionStyle_id,style_id,isDefined,gntml,html,outputsWebmacro) 
    select 
        id,node_id,regionStyle_id,style_id,isDefined,ntml,html,0
    from NtmlInstance;



-- Try and build a sensible set of GntmlStyle definitions

-- This will not define any image zoom definitions because this is domain
-- specific.  It is expected that the admin should go in and perform
-- dedicated updates after the automated import to provide this functionality....

-- (we auto-generate the id as the coupling is loose and by name)
-- GntmlStyle.name = NtmlStyle.name
-- GntmlStyle.isOptional = NtmlStyle.isOptional
-- GntmlStyle.isDefined = NtmlStyle.isDefined
-- GntmlStyle.editCss = 'gntml_edit'
-- GntmlStyle.viewCss = 'gntml_view'
-- GntmlStyle.defaultImageStyle_id = 0
-- GntmlStyle.defaultZoomedImageStyle_id = 0

insert into GntmlStyle 
        (name,isOptional,isDefined,editCss,viewCss,defaultImageStyle_id,defaultZoomedImageStyle_id)
    select 
        name,isOptional,isDefined,'gntml_edit','gntml_view',0,0
    from NtmlStyle;



-- now we update all of our RegionStyle records from NtmlDocument to
-- GNTML

update RegionStyle set compilerName = 'GNTML' where compilerName = 'NtmlDocument';