-- this file includes the table definitions for legacy Gntml Image
-- handling.  It should be included if you are re-initialising a
- site that still uses the old style of rendering images which is
- unlikely...

drop table if exists GntmlImageInstance;
create table GntmlImageInstance (
        id int not null auto_increment,
                primary key (id),
        gntmlInstance_id int not null,
        blockName varchar(255) not null,
        isReferenced int not null,
        style_id int not null, 
        title varchar(255) not null,
        caption varchar(255) not null,
        controllingImage_id int not null,
        hasZoomedView int not null,
        imageTarget varchar(255) not null,
        hasNodeTarget int not null,
        targetNode_id int not null, index (targetNode_id),
        hasBackLink int not null,
        backLinkTitle varchar(255) not null,
        gntmlImageStyle_id int not null,
        webpath varchar(128) not null,
        width int not null,
        height int not null,
        alt varchar(255) not null,
        format_id int not null, 
        sizeKb int not null,
        css varchar(32) not null
);

drop table if exists GntmlZoomedImage;
create table GntmlZoomedImage (
  id int not null auto_increment, primary key (id),
  gntmlImageInstance_id int not null, index (gntmlImageInstance_id),
  style_id int not null,
  webpath varchar(128) not null,
  width int not null,
  height int not null,
  alt varchar(255) not null,
  format_id int not null, 
  sizeKb int not null,
  css varchar(32) not null
);
