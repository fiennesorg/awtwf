alter table GntmlInstance add node_id int not null after region_id;
alter table GntmlInstance add index(node_id);
alter table GntmlInstance add regionStyle_id int not null after node_id;
alter table GntmlInstance add index(regionStyle_id);
update GntmlInstance,Region,Node set GntmlInstance.node_id=Node.id,GntmlInstance.regionStyle_id=Region.regionStyle_id where GntmlInstance.region_id = Region.id and Region.node_id = Node.id;
alter table GntmlInstance drop region_id;

