drop table if exists GntmlPlusBlockStyle;
create table GntmlPlusBlockStyle (
  id int not null auto_increment, primary key (id),
  gntmlStyle_id int not null,
  name varchar(255) not null,
  horizontalBorder int not null,
  verticalBorder int not null
);
