-- script to migrate to mediumtext for GNTML fields

alter table GntmlInstance change gntml gntml mediumtext;
alter table GntmlInstance change html html mediumtext;
