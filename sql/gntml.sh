#!/bin/sh
# build script for initialising the GNTML project

echo gntml-db.sql
cat gntml-db.sql | mysql -u root -pseeekwell mysql

echo node.create.sql
cat $CVSCHECKOUT/org.cord/sql/node.create.sql | mysql -u root -pseeekwell Gntml

echo gntml-tables.sql
cat gntml-tables.sql | mysql -u root -pseeekwell Gntml

echo gntml-pop.sql
cat gntml-pop.sql | mysql -u root -pseeekwell Gntml

echo TestNodeManagerFactory.java
java org.awtwf.test.TestNodeManagerFactory $CVSCHECKOUT/awtwf/www/WEB-INF/web.xml

mysqldump --add-drop-table -u root -pseeekwell Gntml > Gntml.sql

./rebuild.sh
