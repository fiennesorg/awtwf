drop table if exists GntmlRssBlock;
create table GntmlRssBlock (
        id int not null auto_increment,
                primary key (id),
        gntmlInstance_id int not null,
        blockName varchar(255) not null,
        isReferenced int not null,
        configSrc mediumtext not null,
        timedPageListing_id int not null
);