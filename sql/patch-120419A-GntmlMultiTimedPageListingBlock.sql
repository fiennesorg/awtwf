drop table if exists GntmlMultiTimedPageListingBlockTimedPageListing;
create table GntmlMultiTimedPageListingBlockTimedPageListing (
	id int not null auto_increment,
		primary key (id),
	gntmlMultiTimedPageListingBlock_id int not null,
		index (gntmlMultiTimedPageListingBlock_id),
	timedPageListing_id int not null,
		index (timedPageListing_id),
	isDefined int not null
);

drop table if exists GntmlMultiTimedPageListingBlock;
create table GntmlMultiTimedPageListingBlock (
        id int not null auto_increment,
                primary key (id),
        gntmlInstance_id int not null,
        blockName varchar(255) not null,
        isReferenced int not null,
        timedPageCount int not null,
        templatePath varchar(255) not null,
        title varchar(255) not null,
        listingStyle varchar(255) not null
);
