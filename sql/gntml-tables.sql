-- table bootup script for the Gntml ContentCompiler
-- Alex Twisleton-Wykeham-Fiennes

drop table if exists GntmlClass;
create table GntmlClass (
        id int not null auto_increment,
                primary key (id),
        englishName varchar(255),
        cssName varchar(255)
);

drop table if exists GntmlDownloadInstance;
create table GntmlDownloadInstance (
        id int not null auto_increment,
                primary key (id),
        gntmlInstance_id int not null,
        blockName varchar(255) not null,
        isReferenced int not null,
        style_id int not null,
        isUploaded int not null,
        path varchar(255) not null,
        folder varchar(255) not null,
        filename varchar(255) not null,
        format_id int not null,
        sizeKb int not null,
        title varchar(255) not null,
        description varchar(255) not null
);

drop table if exists GntmlEmbeddedLink;
create table GntmlEmbeddedLink (
  id int not null auto_increment, primary key (id),
  gntmlInstance_id int not null, index (gntmlInstance_id),
  name varchar(255) not null,
  isReferenced int not null,
  node_id int not null, index (node_id),
  contents varchar(255) not null,
  hasBackLinks int not null,
  backLinkTitle varchar(255) not null,
  titleMode int not null,
  explicitTitle varchar(255) not null
);


drop table if exists GntmlScalableImage;
create table GntmlScalableImage (
        id int not null auto_increment,
                primary key (id),
        gntmlInstance_id int not null,
        blockName varchar(255) not null,
        isReferenced int not null,
        title varchar(255) not null,
        functionality varchar(255) not null,
        zoomCaption varchar(255) not null,
        imageTarget varchar(255) not null,
        targetNode_id int not null, index (targetNode_id),
        hasBackLink int not null,
        backLinkTitle varchar(255) not null,
        alt varchar(255) not null,
        imgFormat_id int not null, 
        imgOriginal_id int not null,
        isDownloadable int not null
);

drop table if exists GntmlTimedPageListingBlock;
create table GntmlTimedPageListingBlock (
        id int not null auto_increment,
                primary key (id),
        gntmlInstance_id int not null,
        blockName varchar(255) not null,
        isReferenced int not null,
        timedPageListing_id int not null,
        timedPageCount int not null,
        templatePath varchar(255) not null,
        title varchar(255) not null
);

drop table if exists GntmlMultiTimedPageListingBlockTimedPageListing;
create table GntmlMultiTimedPageListingBlockTimedPageListing (
	id int not null auto_increment,
		primary key (id),
	gntmlMultiTimedPageListingBlock_id int not null,
		index (gntmlMultiTimedPageListingBlock_id),
	timedPageListing_id int not null,
		index (timedPageListing_id),
	isDefined int not null
);

drop table if exists GntmlMultiTimedPageListingBlock;
create table GntmlMultiTimedPageListingBlock (
        id int not null auto_increment,
                primary key (id),
        gntmlInstance_id int not null,
        blockName varchar(255) not null,
        isReferenced int not null,
        timedPageCount int not null,
        templatePath varchar(255) not null,
        title varchar(255) not null,
        listingStyle varchar(255) not null
);

drop table if exists GntmlIndexListingBlock;
create table GntmlIndexListingBlock (
        id int not null auto_increment,
                primary key (id),
        gntmlInstance_id int not null,
        blockName varchar(255) not null,
        isReferenced int not null,
        title varchar(255),
        indexListing_id int not null,
        templatePath varchar(255) not null
);

drop table if exists GntmlNavigation;
create table GntmlNavigation (
        id int not null auto_increment,
                primary key (id),
        gntmlInstance_id int not null,
        blockName varchar(255) not null,
        isReferenced int not null,
        title varchar(255) not null,
        validChildren varchar(255) not null,
        navigationStyle varchar(255) not null,
        paginationSize int not null,
        itemName varchar(255) not null,
        itemsName varchar(255) not null
        );
        
drop table if exists GntmlRssBlock;
create table GntmlRssBlock (
	id int not null auto_increment, primary key (id),
	gntmlInstance_id int not null,
	blockName varchar(255) not null,
	isReferenced int not null,
	configSrc mediumtext not null,
	timedPageListing_id int not null
);
        
drop table if exists GntmlSiteMap;
create table GntmlSiteMap (
        id int not null auto_increment, primary key (id),
        gntmlInstance_id int not null, index (gntmlInstance_id),
        blockName varchar(255) not null,
        isReferenced int not null,
        title varchar(255),
        rootNode_id int not null,
        rootNodePolicy varchar(255) not null,
        navigationStyle varchar(255) not null,
        validChildren varchar(255) not null,
        maxDepth int not null
        );

-- drop table if exists GntmlImageStyle;
-- create table GntmlImageStyle (
--         id int not null auto_increment,
--                 primary key (id),
--         gntmlStyle_id int not null,
--         name varchar(255) not null,
--         singleImageStyle_id int not null,
--         zoomedSingleImageStyle_id int not null
-- );

drop table if exists GntmlPlusBlockStyle;
create table GntmlPlusBlockStyle (
  id int not null auto_increment, primary key (id),
  gntmlStyle_id int not null,
  name varchar(255) not null,
  horizontalBorder int not null,
  verticalBorder int not null
);


drop table if exists GntmlInstance;
create table GntmlInstance (
        id int not null auto_increment,
                primary key (id),
        node_id int not null,
                index (node_id),
        regionStyle_id int not null,
                index (regionStyle_id),
        style_id int not null,
        isDefined int not null,
        gntml mediumtext not null,
        html mediumtext not null,
        outputsWebmacro int not null
);

drop table if exists GntmlStyle;
create table GntmlStyle (
        id int not null auto_increment,
                primary key (id),
        name varchar(32) not null, 
                index (name),
        isOptional INT not null,
        isDefined INT not null,
        editCss varchar(255) not null,
        viewCss varchar(255) not null,
        defaultImageStyle_id int not null,
        defaultZoomedImageStyle_id int not null,
        defaultDownloadStyle_id int not null,
	configValue mediumtext not null
);

-- insert into GntmlStyle values (0,'compulsory',0,1,'gntml_edit','')
-- insert into GntmlStyle values (0,'optional',1,0,'gntml_edit','')

drop table if exists HtmlBlock;
create table HtmlBlock (
	id int not null auto_increment, primary key (id),
	gntmlInstance_id int not null,
	blockName varchar(255) not null,
	isReferenced int not null,
	html text not null
);

drop table if exists GntmlEmbedVideo;
create table GntmlEmbedVideo (
	id int not null auto_increment, primary key (id),
	gntmlInstance_id int not null,
	blockName varchar(255) not null,
	isReferenced int not null,
	title varchar(255) not null,
	html text not null,
	width int not null,
	height int not null
);

drop table if exists GntmlPageList;
create table GntmlPageList (
	id int not null auto_increment, primary key (id),
	gntmlInstance_id int not null, index (gntmlInstance_id),
	blockName varchar(255) not null,
	isReferenced int not null,
	listTitle varchar(255),
	hasExplicitOrdering int not null,
	isPaginated int not null,
	listingView varchar(255) not null,
	hasBackLinks int not null,
	backLinkTitle varchar(255) not null
);

drop table if exists GntmlPageListJoin;
create table GntmlPageListJoin (
  id int not null auto_increment, primary key (id),
  gntmlPageList_id int not null, index(gntmlPageList_id),
  node_id int not null, index(node_id),
  isDefined int not null,
  number int not null
);

drop table if exists GntmlCyclingImage;
create table GntmlCyclingImage (
	id int not null auto_increment, primary key (id),
	gntmlInstance_id int not null, index (gntmlInstance_id),
	blockName varchar(255) not null,
	isReferenced int not null,
	configSrc mediumtext not null
);

drop table if exists GntmlCyclingImage_ImgOriginal;
create table GntmlCyclingImage_ImgOriginal (
	id int not null auto_increment, primary key (id),
	gntmlCyclingImage_id int not null,
	imgOriginal_id int not null,
	isDefined int not null,
	rank int not null
);