DROP PROCEDURE IF EXISTS patchRecords;

DELIMITER //
CREATE PROCEDURE patchRecords()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE recordId int;
	DECLARE recordIds CURSOR FOR SELECT id from GntmlScalableImage;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	OPEN recordIds;
	read_loop: LOOP
		FETCH recordIds into recordId;
		IF (done) THEN
			LEAVE read_loop;
		END IF;
		update GntmlScalableImage, GntmlInstance
			set GntmlInstance.gntml = replace(GntmlInstance.gntml, 
        			                          concat('[[',GntmlScalableImage.blockName,']]'),
		    	                              concat('|image:',GntmlScalableImage.blockName,'|'))
			where GntmlInstance.id = GntmlScalableImage.gntmlInstance_id 
				and GntmlScalableImage.id = recordId;
	END LOOP;
	CLOSE recordIds;
END//

DELIMITER ;;

CALL patchRecords();	
DROP PROCEDURE patchRecords;
