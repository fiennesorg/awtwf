drop table if exists GntmlCyclingImage;
create table GntmlCyclingImage (
	id int not null auto_increment, primary key (id),
	gntmlInstance_id int not null, index (gntmlInstance_id),
	blockName varchar(255) not null,
	isReferenced int not null,
	configSrc mediumtext not null
);

drop table if exists GntmlCyclingImage_ImgOriginal;
create table GntmlCyclingImage_ImgOriginal (
	id int not null auto_increment, primary key (id),
	gntmlCyclingImage_id int not null,
	imgOriginal_id int not null,
	isDefined int not null,
	rank int not null
);
