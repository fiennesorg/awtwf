-- This only partially fixes any problems: we still need to fix the
-- {hN.style} syntax, which maps to "{style}\n|hN|", and usages of
-- {blockquote}, which will map to \"\"

update GntmlInstance set gntml=replace(gntml, "{h1}", "|h1|");
update GntmlInstance set gntml=replace(gntml, "{h2}", "|h2|");
update GntmlInstance set gntml=replace(gntml, "{h3}", "|h3|");
update GntmlInstance set gntml=replace(gntml, "{h4}", "|h4|");
update GntmlInstance set gntml=replace(gntml, "{h5}", "|h5|");
update GntmlInstance set gntml=replace(gntml, "{h6}", "|h6|");
update GntmlInstance set gntml=replace(gntml, "{H1}", "|h1|");
update GntmlInstance set gntml=replace(gntml, "{H2}", "|h2|");
update GntmlInstance set gntml=replace(gntml, "{H3}", "|h3|");
update GntmlInstance set gntml=replace(gntml, "{H4}", "|h4|");
update GntmlInstance set gntml=replace(gntml, "{H5}", "|h5|");
update GntmlInstance set gntml=replace(gntml, "{H6}", "|h6|");
