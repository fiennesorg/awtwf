package org.awtwf.node;

import org.awtwf.gntml.GntmlStyle;
import org.cord.image.ImageStyleTable;
import org.cord.mirror.AbstractTableWrapper;
import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.sql.SqlUtil;

import com.google.common.base.Strings;

@Deprecated
public class GntmlImageStyle
  extends AbstractTableWrapper
{
  public final static String TABLENAME = "GntmlImageStyle";

  public final static FieldKey<String> NAME = FieldKey.create("name", String.class);

  public static final FieldKey<Integer> GNTMLSTYLE_ID =
      LinkField.getLinkFieldName(GntmlStyle.TABLENAME);

  public final static FieldKey<Integer> SINGLEIMAGESTYLE_ID =
      LinkField.getLinkFieldName(ImageStyleTable.SINGLEIMAGESTYLE);

  public final static RecordOperationKey<PersistentRecord> SINGLEIMAGESTYLE =
      LinkField.getLinkName(SINGLEIMAGESTYLE_ID);

  public final static FieldKey<Integer> ZOOMEDSINGLEIMAGESTYLE_ID =
      FieldKey.create("zoomedSingleImageStyle_id", Integer.class);

  public final static RecordOperationKey<PersistentRecord> ZOOMEDSINGLEIMAGESTYLE =
      LinkField.getLinkName(ZOOMEDSINGLEIMAGESTYLE_ID);

  public final static RecordOperationKey<RecordSource> SINGLEIMAGESTYLE_ZOOMEDIMAGESTYLES =
      RecordOperationKey.create("zoomedImageStyles", RecordSource.class);

  private GntmlImageStyle()
  {
    super(TABLENAME);
  }

  public PersistentRecord getOptionalInstance(PersistentRecord gntmlStyle,
                                              String name,
                                              Viewpoint viewpoint)
  {
    TransientRecord.assertIsTableNamed(gntmlStyle, GntmlStyle.TABLENAME);
    if (Strings.isNullOrEmpty(name)) {
      return null;
    }
    StringBuilder filter = new StringBuilder();
    filter.append(TABLENAME + '.' + GNTMLSTYLE_ID + '=').append(gntmlStyle.getId());
    filter.append(" and " + TABLENAME + '.' + NAME + '=');
    SqlUtil.toSafeSqlString(filter, name);
    return RecordSources.getOptFirstRecord(getTable().getQuery(null, filter.toString(), null),
                                           viewpoint);
  }

  @Override
  protected void initTable(Db db,
                           String pwd,
                           Table table)
      throws MirrorTableLockedException
  {
    table.addField(new LinkField(GntmlStyle.TABLENAME,
                                 "Containing GNTML style",
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new StringField(NAME, "Name"));
    table.addField(new LinkField(ImageStyleTable.SINGLEIMAGESTYLE,
                                 "Image style",
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new LinkField(ZOOMEDSINGLEIMAGESTYLE_ID,
                                 "Zoomed image style",
                                 null,
                                 ZOOMEDSINGLEIMAGESTYLE,
                                 ImageStyleTable.SINGLEIMAGESTYLE,
                                 SINGLEIMAGESTYLE_ZOOMEDIMAGESTYLES,
                                 null,
                                 pwd,
                                 Dependencies.LINK_ENFORCED | Dependencies.BIT_OPTIONAL));
  }
}
