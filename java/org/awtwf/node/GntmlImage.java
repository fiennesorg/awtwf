package org.awtwf.node;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.awtwf.gntml.Gntml;
import org.awtwf.gntml.GntmlStyle;
import org.awtwf.gntml.ImageBlockFactory;
import org.cord.image.ImageMetaData;
import org.cord.image.ImageMetaDataProvider;
import org.cord.image.ImageStyleTable;
import org.cord.image.ImageTable;
import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.Dependencies;
import org.cord.mirror.DirectCachingSimpleRecordOperation;
import org.cord.mirror.FieldKey;
import org.cord.mirror.HtmlGuiSupplier;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.AbstractGuiSupplierRecordOperation;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.trigger.PostTransactionDeleteTrigger;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.RegionStyle;
import org.cord.node.cc.TableContentCompiler;
import org.cord.sql.SqlUtil;
import org.cord.util.Casters;
import org.cord.util.LogicException;
import org.cord.util.NamedImpl;
import org.cord.util.NumberUtil;
import org.cord.webmacro.AppendableParametricMacro;
import org.webmacro.Macro;

import com.google.common.base.Strings;

/**
 * Object that represents the behaviour of the GntmlImage Table which holds meta-information about a
 * single nested image inside a GntmlDocument. regionName-img-<imageName>-param
 */
@Deprecated
public class GntmlImage
  extends NamedImpl
  implements DbInitialiser, ImageMetaDataProvider
{
  public final static String NAME = "org.awtwf.node.GntmlImage";

  public final static String TABLENAME = "GntmlImageInstance";

  public final static RecordOperationKey<RecordSource> REFERENCES =
      RecordOperationKey.create("gntmlImageInstances", RecordSource.class);

  public final static FieldKey<Integer> GNTMLINSTANCE_ID =
      LinkField.getLinkFieldName(Gntml.TABLENAME);

  public final static RecordOperationKey<PersistentRecord> GNTMLINSTANCE =
      LinkField.getLinkName(GNTMLINSTANCE_ID);

  public final static FieldKey<String> IMAGENAME = FieldKey.create("imageName", String.class);

  public final static FieldKey<Boolean> ISREFERENCED = FieldKey.create("isReferenced",
                                                                       Boolean.class);

  public final static FieldKey<Integer> STYLE_ID = TableContentCompiler.STYLE_ID;

  public final static RecordOperationKey<PersistentRecord> STYLE = TableContentCompiler.STYLE;

  public final static FieldKey<String> TITLE = FieldKey.create("title", String.class);

  public final static FieldKey<String> CAPTION = FieldKey.create("caption", String.class);

  public final static FieldKey<Integer> CONTROLLINGIMAGE_ID =
      FieldKey.create("controllingImage_id", Integer.class);

  public final static RecordOperationKey<PersistentRecord> CONTROLLINGIMAGE =
      LinkField.getLinkName(CONTROLLINGIMAGE_ID);

  public final static RecordOperationKey<RecordSource> CONTROLLEDIMAGES =
      RecordOperationKey.create("controlledImages", RecordSource.class);

  public final static FieldKey<Boolean> HASZOOMEDVIEW = FieldKey.create("hasZoomedView",
                                                                        Boolean.class);

  public final static FieldKey<String> IMAGETARGET = FieldKey.create("imageTarget", String.class);

  public final static FieldKey<Integer> GNTMLIMAGESTYLE_ID =
      LinkField.getLinkFieldName(GntmlImageStyle.TABLENAME);

  public final static RecordOperationKey<PersistentRecord> GNTMLIMAGESTYLE =
      LinkField.getLinkName(GNTMLIMAGESTYLE_ID);

  public final static FieldKey<String> WEBPATH = ImageTable.WEBPATH;

  public final static FieldKey<Integer> WIDTH = ImageTable.WIDTH;

  public final static FieldKey<Integer> HEIGHT = ImageTable.HEIGHT;

  public final static FieldKey<String> ALT = ImageTable.ALT;

  public final static FieldKey<Integer> FORMAT_ID = ImageTable.FORMAT_ID;

  public final static RecordOperationKey<PersistentRecord> FORMAT = ImageTable.FORMAT;

  public final static FieldKey<Integer> SIZEKB = ImageTable.SIZEKB;

  public final static FieldKey<String> CSS = ImageTable.CSS;

  public final static RecordOperationKey<String> NAMEPREFIX =
      RecordOperationKey.create("namePrefix", String.class);

  public final static RecordOperationKey<String> IMAGEPREVIEW =
      RecordOperationKey.create("imagePreview", String.class);

  public final static RecordOperationKey<Boolean> MAYHAVEZOOM =
      RecordOperationKey.create("mayHaveZoom", Boolean.class);

  private NodeManager _nodeManager;

  public GntmlImage(NodeManager nodeManager)
  {
    super(NAME);
    _nodeManager = nodeManager;
  }

  @Override
  public void shutdown()
  {
    _nodeManager = null;
  }

  public NodeManager getNodeManager()
  {
    return _nodeManager;
  }

  @Override
  public void init(final Db db,
                   final String transactionPassword)
      throws MirrorTableLockedException
  {
    Table gntmlImageInstances = db.addTable(null, TABLENAME, REFERENCES, null, null);
    gntmlImageInstances.addField(new LinkField(Gntml.TABLENAME,
                                               null,
                                               transactionPassword,
                                               Dependencies.LINK_ENFORCED
                                                   | Dependencies.BIT_S_ON_T_UPDATE));
    gntmlImageInstances.addField(new StringField(IMAGENAME, null));
    gntmlImageInstances.addField(new BooleanField(ISREFERENCED, null));
    // custom fields
    gntmlImageInstances.addField(new LinkField(STYLE_ID,
                                               null,
                                               null,
                                               STYLE,
                                               ImageStyleTable.SINGLEIMAGESTYLE,
                                               REFERENCES,
                                               null,
                                               transactionPassword,
                                               Dependencies.LINK_ENFORCED));
    gntmlImageInstances.addField(new StringField(TITLE, "Title"));
    gntmlImageInstances.addField(new StringField(CAPTION, "Zoomed caption").setMayBeTextArea(true));
    gntmlImageInstances.addField(new LinkField(CONTROLLINGIMAGE_ID,
                                               null,
                                               null,
                                               CONTROLLINGIMAGE,
                                               TABLENAME,
                                               CONTROLLEDIMAGES,
                                               null,
                                               transactionPassword,
                                               Dependencies.LINK_ENFORCED
                                                   | Dependencies.BIT_OPTIONAL));
    gntmlImageInstances.addField(new BooleanField(HASZOOMEDVIEW,
                                                  "Has zoomed view?") {
      @Override
      public boolean shouldReloadOnChange()
      {
        return true;
      }

      @Override
      public boolean hasHtmlWidget(TransientRecord gntmlImageInstance)
      {
        return ((Boolean) gntmlImageInstance.get("mayHaveZoom")).booleanValue();
      }
    });
    gntmlImageInstances.addField(new StringField(IMAGETARGET, "Image link target"));
    gntmlImageInstances.addField(new LinkField(GntmlImageStyle.TABLENAME,
                                               "Image style",
                                               transactionPassword,
                                               Dependencies.LINK_ENFORCED
                                                   | Dependencies.BIT_OPTIONAL));
    ImageTable.initialiseInstanceTable(_nodeManager,
                                       transactionPassword,
                                       gntmlImageInstances,
                                       REFERENCES);
    PostTransactionDeleteTrigger.register(gntmlImageInstances,
                                          ISREFERENCED,
                                          transactionPassword,
                                          "GntmlImage.isReferenced cleanup");
    gntmlImageInstances.addRecordOperation(new DirectCachingSimpleRecordOperation<String>(NAMEPREFIX) {
      @Override
      public String calculate(TransientRecord gntmlImageInstance,
                              Viewpoint viewpoint)
      {
        StringBuilder buf = new StringBuilder();
        PersistentRecord gntmlInstance = gntmlImageInstance.followEnforcedLink(GNTMLINSTANCE);
        buf.append(gntmlInstance.comp(Gntml.NAMEPREFIX))
           .append(ImageBlockFactory.NAMEPREFIX_VALUE)
           .append(gntmlImageInstance.comp(IMAGENAME))
           .append("-");
        return buf.toString();
      }
    });
    gntmlImageInstances.addRecordOperation(new MonoRecordOperation<String, Macro>(HtmlGuiSupplier.ASFORMELEMENT,
                                                                                  Casters.TOSTRING) {
      @Override
      public Macro calculate(final TransientRecord gntmlImageInstance,
                             final Viewpoint viewpoint,
                             final String name)
      {
        return new AppendableParametricMacro() {
          @Override
          public void append(Appendable out,
                             Map<Object, Object> context)
              throws IOException
          {
            gntmlImageInstance.appendFormElement(gntmlImageInstance.getString(NAMEPREFIX),
                                                 name,
                                                 out,
                                                 NodeRequest.asGettable(context));
          }
        };
      }
    });
    gntmlImageInstances.addRecordOperation(new AbstractGuiSupplierRecordOperation(IMAGEPREVIEW,
                                                                                  "Image preview") {
      @Override
      public String getOperation(TransientRecord targetRecord,
                                 Viewpoint viewpoint)
      {
        return "<img class=\"imagePreview\" src=\"" + targetRecord.comp(WEBPATH) + "\" />";
      }
    });
    gntmlImageInstances.addRecordOperation(new SimpleRecordOperation<Boolean>(MAYHAVEZOOM) {
      @Override
      public Boolean getOperation(TransientRecord gntmlImageInstance,
                                  Viewpoint viewpoint)
      {
        if (gntmlImageInstance.comp(STYLE_ID) == 0) {
          PersistentRecord gntmlStyle = gntmlImageInstance.comp(GNTMLINSTANCE).comp(Gntml.STYLE);
          if (gntmlStyle.comp(GntmlStyle.DEFAULTZOOMEDIMAGESTYLE_ID) != 0) {
            return Boolean.TRUE;
          }
        } else {
          PersistentRecord gntmlImageStyle = gntmlImageInstance.comp(GNTMLIMAGESTYLE);
          if (gntmlImageStyle.comp(GntmlImageStyle.ZOOMEDSINGLEIMAGESTYLE_ID) != 0) {
            return Boolean.TRUE;
          }
        }
        return Boolean.FALSE;
      }
    });
  }

  /**
   * Get the Query the reolves to the named image on a given Gntml document.
   * 
   * @return A Query that should be of maximum length 1 in a well formed database
   */
  public static Query getInstanceQuery(Table gntmlImageInstanceTable,
                                       Integer gntmlInstance_id,
                                       String imageName)
  {
    StringBuilder filter = new StringBuilder();
    filter.append("(" + TABLENAME + ".gntmlInstance_id=")
          .append(gntmlInstance_id.intValue())
          .append(") and (" + TABLENAME + ".blockName=");
    SqlUtil.toSafeSqlString(filter, imageName);
    filter.append(')');
    Query query = gntmlImageInstanceTable.getQuery(null, filter.toString(), null);
    return query;
  }

  public static PersistentRecord getOrCreateInstance(Db db,
                                                     Integer gntmlInstance_id,
                                                     String imageName,
                                                     Transaction transaction,
                                                     boolean autoCreateRecord,
                                                     int style_id)
      throws MirrorFieldException, MirrorInstantiationException, MirrorNoSuchRecordException
  {
    Table gntmlImageInstanceTable = db.getTable(TABLENAME);
    Query query = getInstanceQuery(gntmlImageInstanceTable, gntmlInstance_id, imageName);
    PersistentRecord instance = RecordSources.getOptOnlyRecord(query, transaction);
    if (instance != null) {
      return instance;
    }
    if (autoCreateRecord) {
      TransientRecord newInstance = gntmlImageInstanceTable.createTransientRecord();
      newInstance.setField(GNTMLINSTANCE_ID, gntmlInstance_id);
      newInstance.setField(IMAGENAME, imageName);
      Table gntmlInstanceTable = db.getTable(Gntml.TABLENAME);
      PersistentRecord gntmlInstance = gntmlInstanceTable.getRecord(gntmlInstance_id, transaction);
      PersistentRecord gntmlStyle = gntmlInstance.followEnforcedLink(Gntml.STYLE_ID);
      newInstance.setField(STYLE_ID,
                           style_id > 0
                               ? style_id
                               : gntmlStyle.getIntField(GntmlStyle.DEFAULTIMAGESTYLE_ID));
      return newInstance.makePersistent(null);
    } else {
      throw new MirrorNoSuchRecordException("No image named " + imageName
                                                + " found on GntmlInstance#" + gntmlInstance_id,
                                            gntmlImageInstanceTable,
                                            null,
                                            null);
    }
  }

  public PersistentRecord getOrCreateInstance(Integer gntmlInstance_id,
                                              String imageName,
                                              Transaction transaction,
                                              boolean autoCreateRecord,
                                              int style_id)
      throws MirrorNoSuchRecordException, MirrorFieldException, MirrorInstantiationException
  {
    return getOrCreateInstance(_nodeManager.getDb(),
                               gntmlInstance_id,
                               imageName,
                               transaction,
                               autoCreateRecord,
                               style_id);
  }

  public PersistentRecord getOrCreateInstance(Integer gntmlInstance_id,
                                              String imageName,
                                              Transaction transaction,
                                              int style_id)
      throws MirrorFieldException, MirrorInstantiationException
  {
    try {
      return getOrCreateInstance(gntmlInstance_id, imageName, transaction, true, style_id);
    } catch (MirrorNoSuchRecordException mnsrEx) {
      throw new LogicException("impossible if creating record", mnsrEx);
    }
  }

  public static PersistentRecord getZoomedInstance(PersistentRecord controllingImage,
                                                   Transaction transaction)
      throws MirrorNoSuchRecordException
  {
    return RecordSources.getFirstRecord(controllingImage.comp(CONTROLLEDIMAGES), transaction);
  }

  public PersistentRecord getOrCreateZoomedInstance(PersistentRecord controllingImage,
                                                    Transaction transaction)
      throws MirrorFieldException, MirrorInstantiationException
  {
    PersistentRecord controlledImage =
        RecordSources.getOptOnlyRecord(controllingImage.comp(CONTROLLEDIMAGES), transaction);
    if (controlledImage != null) {
      return controlledImage;
    }
    TransientRecord newControlledImage = controllingImage.getTable().createTransientRecord();
    newControlledImage.setField(GNTMLINSTANCE_ID, controllingImage.comp(GNTMLINSTANCE_ID));
    newControlledImage.setField(IMAGENAME, controllingImage.comp(IMAGENAME) + "-Zoom");
    newControlledImage.setField(ISREFERENCED, true);
    if (controllingImage.comp(STYLE_ID) == 0) {
      PersistentRecord gntmlInstance = controllingImage.comp(GNTMLINSTANCE);
      PersistentRecord gntmlStyle = gntmlInstance.comp(Gntml.STYLE);
      newControlledImage.setField(STYLE_ID, gntmlStyle.comp(GntmlStyle.DEFAULTZOOMEDIMAGESTYLE_ID));
    } else {
      PersistentRecord gntmlImageStyle = (PersistentRecord) controllingImage.get("gntmlImageStyle");
      newControlledImage.setField(STYLE_ID,
                                  gntmlImageStyle.comp(GntmlImageStyle.ZOOMEDSINGLEIMAGESTYLE_ID));
    }
    newControlledImage.setField(TITLE, controllingImage.comp(TITLE));
    newControlledImage.setField(CONTROLLINGIMAGE_ID, controllingImage.getId());
    return newControlledImage.makePersistent(null);
  }

  public PersistentRecord getInstance(Integer gntmlInstance_id,
                                      String imageName,
                                      Transaction transaction)
      throws MirrorNoSuchRecordException
  {
    try {
      return getOrCreateInstance(gntmlInstance_id, imageName, transaction, false, 0);
    } catch (MirrorFieldException mfEx) {
      throw new LogicException("impossible if not creating record", mfEx);
    } catch (MirrorInstantiationException miEx) {
      throw new LogicException("impossible if not creating record", miEx);
    }
  }

  /**
   * Convert the namespace into the Integer id of the GntmlInstance that the image is embedded in
   * and then resolve the existing image file.
   * 
   * @see #getInstance(Integer, String, Transaction)
   */
  public PersistentRecord getInstance(String namespace,
                                      String imageName,
                                      Transaction transaction)
      throws MirrorNoSuchRecordException
  {
    Integer docId =
        NumberUtil.toInteger(namespace, NumberUtil.INTEGER_MINUSONE, NumberUtil.INTEGER_MINUSONE);
    if (docId.intValue() == -1 || Strings.isNullOrEmpty(imageName)) {
      return null;
    }
    return getInstance(docId, imageName, transaction);
  }

  @Override
  public ImageMetaData getImageMetaData(String namespace,
                                        String imageName,
                                        Transaction transaction)
  {
    Integer docId =
        NumberUtil.toInteger(namespace, NumberUtil.INTEGER_MINUSONE, NumberUtil.INTEGER_MINUSONE);
    if (docId.intValue() == -1 || Strings.isNullOrEmpty(imageName)) {
      return null;
    }
    try {
      PersistentRecord gntmlImage = getOrCreateInstance(docId, imageName, transaction, 0);
      String imageAction = null;
      if (gntmlImage.comp(HASZOOMEDVIEW)) {
        PersistentRecord gntmlInstance = gntmlImage.comp(GNTMLINSTANCE);
        PersistentRecord regionStyle = gntmlInstance.comp(Gntml.REGIONSTYLE);
        imageAction =
            "$node.url" + "?view=zoomGntmlImage" + "&amp;iName=" + gntmlImage.get("imageName")
                + "&amp;rName=" + regionStyle.comp(RegionStyle.NAME);
      } else {
        imageAction = gntmlImage.opt(IMAGETARGET);
      }
      return new ImageMetaData(gntmlImage.comp(WIDTH),
                               gntmlImage.comp(HEIGHT),
                               new File(getNodeManager().getWebRoot(), gntmlImage.comp(WEBPATH)),
                               gntmlImage.comp(WEBPATH),
                               gntmlImage.comp(FORMAT).toString(),
                               gntmlImage.comp(STYLE).comp(CSS),
                               gntmlImage.comp(TITLE),
                               gntmlImage.comp(CAPTION),
                               gntmlImage.comp(ALT),
                               imageAction);
    } catch (MirrorException mnsrEx) {
      mnsrEx.printStackTrace();
    }
    return null;
  }
}