package org.awtwf.node;

import org.cord.mirror.MirrorException;
import org.cord.mirror.PersistentRecord;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.ReloadException;
import org.cord.node.TemplateSuccessOperation;
import org.cord.node.view.DynamicAclAuthenticatedFactory;
import org.webmacro.Context;

import com.google.common.base.Strings;

/**
 * View that provides a zoomed interface to a single GntmlImage. The small version of the GntmlImage
 * will be placed in the as $controllingImage and the large zoomed version will be $controlledImage.
 */
@Deprecated
public class ZoomGntmlImage
  extends DynamicAclAuthenticatedFactory
{
  public ZoomGntmlImage(String name,
                        NodeManager nodeManager,
                        String templatePath,
                        String authenticationError)
  {
    super(name,
          nodeManager,
          new TemplateSuccessOperation(nodeManager, templatePath),
          Node.VIEWACL,
          authenticationError);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    String regionName = nodeRequest.getString("rName");
    if (Strings.isNullOrEmpty(regionName)) {
      throw new ReloadException(node, null);
    }
    String imageName = nodeRequest.getString("iName");
    if (Strings.isNullOrEmpty(imageName)) {
      throw new ReloadException(node, null);
    }
    try {
      PersistentRecord gntmlInstance =
          (PersistentRecord) (getNodeManager().getNode().compile(node,
                                                                 regionName,
                                                                 nodeRequest,
                                                                 null));
      PersistentRecord controllingImage =
          GntmlImage.getOrCreateInstance(getNodeManager().getDb(),
                                         gntmlInstance.getId(),
                                         imageName,
                                         null,
                                         false,
                                         0);
      PersistentRecord controlledImage = GntmlImage.getZoomedInstance(controllingImage, null);
      context.put("controllingImage", controllingImage);
      context.put("controlledImage", controlledImage);
    } catch (MirrorException mEx) {
      throw new ReloadException(node, mEx);
    }
    return handleSuccess(nodeRequest, node, context, null);
  }
}