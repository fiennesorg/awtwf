package org.awtwf.gntml;

import java.io.File;
import java.io.IOException;
import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.IdList;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.operation.ManyToManyReference;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.mirror.recordsource.ConstantRecordSource;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.UnmodifiableIdList;
import org.cord.node.Copyables;
import org.cord.node.NodeManager;
import org.cord.node.img.ImgOriginal;
import org.cord.util.DebugConstants;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONArray;
import org.json.WritableJSONObject;

import com.google.common.base.Strings;
import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableMap;

public class CyclingImage
  extends JsonTableBlockFactory
{
  public static final String NAME = "CyclingImage";
  public static final String TABLENAME = "GntmlCyclingImage";

  public static final RecordOperationKey<String> TITLE = RecordOperationKey.create("title",
                                                                                   String.class);

  public static final RecordOperationKey<String> FX = RecordOperationKey.create("fx", String.class);
  public static final RecordOperationKey<Integer> SPEED = RecordOperationKey.create("speed",
                                                                                    Integer.class);
  public static final RecordOperationKey<Integer> TIMEOUT =
      RecordOperationKey.create("timeout", Integer.class);
  public static final RecordOperationKey<JSONArray> JSONIMAGES =
      RecordOperationKey.create("jsonImages", JSONArray.class);

  public static final String IN_ADD_ID = "add-id";

  public static final String IN_ADD_JSON = "add-json";
  public static final String IN_ADD_JSON_ID = "id";
  public static final String IN_ADD_JSON_RANK = "rank";

  public static final String IN_HASSELECTEDIMAGES = "hasSelectedImages";

  public static final RecordOperationKey<RecordSource> IMGORIGINAL__GNTMLCYCLINGIMAGES =
      RecordOperationKey.create("gntmlCyclingImages", RecordSource.class);
  public static final RecordOperationKey<RecordSource> IMGORIGINAL__GNTMLCYCLINGIMAGENODES =
      RecordOperationKey.create("gntmlCyclingImageNodes", RecordSource.class);

  public static final RecordOperationKey<RecordSource> IMGORIGINALSLINKS =
      CyclingImage_ImgOriginal.GNTMLCYCLINGIMAGE_IMGORIGINALSLINKS;

  private final static ImmutableMap<String, String> __types;

  private static void put(ImmutableMap.Builder<String, String> builder,
                          String... values)
  {
    for (String value : values) {
      builder.put(value, value);
    }
  }

  static {
    ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
    put(builder,
        "blindX",
        "blindY",
        "blindZ",
        "cover",
        "curtainX",
        "curtainY",
        "fade",
        "fadeZoom",
        "growX",
        "growY",
        "none",
        "scrollUp",
        "scrollDown",
        "scrollLeft",
        "scrollRight",
        "scrollHorz",
        "scrollVert",
        "shuffle",
        "slideX",
        "slideY",
        "toss",
        "turnUp",
        "turnDown",
        "turnLeft",
        "turnRight",
        "uncover",
        "wipe",
        "zoom");
    __types = builder.build();
  }

  public CyclingImage(NodeManager nodeMgr)
  {
    super(nodeMgr,
          NAME,
          "Cycling Scalable Images",
          "|images:",
          "|",
          TABLENAME,
          "Gntml/CyclingImage/CyclingImage.view.wm.html",
          NAME,
          NAME + "_caption",
          "Gntml/CyclingImage/CyclingImage.edit.wm.html",
          null);
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table table)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    super.init(db, pwd, table);
    db.add(_cyclingImage_imgOriginal = new CyclingImage_ImgOriginal());
    register(new TableBlockJsonStringCC(this,
                                        getConfigSrc(),
                                        "Title",
                                        TITLE,
                                        Suppliers.ofInstance("")),
             pwd,
             table);
    register(new TableBlockJsonMapCC<String>(this,
                                             getConfigSrc(),
                                             "Transition type",
                                             FX,
                                             __types,
                                             "fade"), pwd, table);
    register(new TableBlockJsonIntegerCC(this,
                                         getConfigSrc(),
                                         "Transition Speed (ms)",
                                         SPEED,
                                         Suppliers.ofInstance(1000)), pwd, table);
    register(new TableBlockJsonIntegerCC(this,
                                         getConfigSrc(),
                                         "Pause Speed (ms)",
                                         TIMEOUT,
                                         Suppliers.ofInstance(4000)), pwd, table);
    ImgOriginal ImgOriginal_ = getNodeManager().getImgMgr().getImgOriginal();
    ImgOriginal_.getTable()
                .addRecordOperation(new SimpleRecordOperation<RecordSource>(IMGORIGINAL__GNTMLCYCLINGIMAGENODES) {
                  @Override
                  public RecordSource getOperation(TransientRecord imgOriginal,
                                                   Viewpoint viewpoint)
                  {
                    IdList nodeIds = new ArrayIdList(getNodeManager().getNode());
                    for (PersistentRecord gntmlCyclingImage : imgOriginal.comp(IMGORIGINAL__GNTMLCYCLINGIMAGES,
                                                                               viewpoint)) {
                      Integer nodeId =
                          gntmlCyclingImage.comp(CyclingImage.GNTMLINSTANCE, viewpoint)
                                           .comp(Gntml.NODE, viewpoint)
                                           .getId();
                      if (!nodeIds.contains(nodeId)) {
                        nodeIds.add(nodeId);
                      }
                    }
                    return RecordSources.viewedFrom(new ConstantRecordSource(new UnmodifiableIdList(nodeIds),
                                                                             null),
                                                    viewpoint);
                  }
                });
    ImgOriginal_.registerNodeResolver(IMGORIGINAL__GNTMLCYCLINGIMAGENODES);
    table.addRecordOperation(new SimpleRecordOperation<JSONArray>(JSONIMAGES) {
      @Override
      public JSONArray getOperation(TransientRecord cyclingImage,
                                    Viewpoint viewpoint)
      {
        JSONArray jArr = new WritableJSONArray();
        for (PersistentRecord imgOriginalLink : cyclingImage.comp(IMGORIGINALSLINKS, viewpoint)) {
          try {
            jArr.put(new WritableJSONObject().put(IN_ADD_JSON_ID,
                                                  imgOriginalLink.comp(CyclingImage_ImgOriginal.IMGORIGINAL_ID))
                                             .put(IN_ADD_JSON_RANK,
                                                  imgOriginalLink.comp(CyclingImage_ImgOriginal.RANK)));
          } catch (JSONException e) {
            throw new LogicException(String.format("Error adding %s to %s", imgOriginalLink, jArr),
                                     e);
          }
        }
        return jArr;
      }
    });
  }
  @Override
  protected TableBlock buildTableBlock(ListIterator<String> sourceLines,
                                       GntmlBlockStyle blockStyle,
                                       GntmlDocument gntmlDocument,
                                       int blockId)
      throws GntmlException
  {
    return new TableBlock(NAME,
                          sourceLines,
                          blockStyle,
                          gntmlDocument,
                          this,
                          blockId) {
      @Override
      public void copyGntmlBlock(Settable settable,
                                 PersistentRecord fromNode,
                                 PersistentRecord fromRegionStyle,
                                 PersistentRecord toRegionStyle,
                                 PersistentRecord fromGntmlInstance,
                                 GntmlDocument gntmlDocument,
                                 PersistentRecord instance)
          throws SettableException
      {
        settable.set(ISACTIVE, Boolean.TRUE);
        Copyables.copy(instance, settable, TITLE, FX, SPEED, TIMEOUT);
        settable.set(IN_ADD_JSON, instance.comp(JSONIMAGES));
      }

      @Override
      protected void updateInstance(WritableRecord instance,
                                    Gettable params,
                                    Map<Object, Object> outputs)
          throws GntmlException, MirrorException
      {
        if (Boolean.TRUE.equals(params.getBoolean(ISACTIVE))) {
          final ImgOriginal ImgOriginal_ = getNodeManager().getImgMgr().getImgOriginal();
          final Table imgOriginalTable = ImgOriginal_.getTable();
          if (Boolean.TRUE.equals(params.getBoolean(IN_HASSELECTEDIMAGES))) {
            for (PersistentRecord gntmlCyclingImage_imgOriginal : instance.comp(CyclingImage_ImgOriginal.GNTMLCYCLINGIMAGE_IMGORIGINALSLINKS)) {
              if ("true".equals(params.get(gntmlCyclingImage_imgOriginal.getId() + "-isDefined"))) {
                gntmlCyclingImage_imgOriginal.setField(CyclingImage_ImgOriginal.RANK,
                                                       params.get(gntmlCyclingImage_imgOriginal.getId()
                                                                  + "-rank"));
              } else {
                gntmlCyclingImage_imgOriginal.setField(CyclingImage_ImgOriginal.ISDEFINED, false);
              }
            }
          }
          String add_id = params.getString(IN_ADD_ID);
          DebugConstants.variable("add_id", add_id);
          if (!Strings.isNullOrEmpty(add_id)) {
            PersistentRecord imgOriginal = imgOriginalTable.getOptRecord(add_id);
            DebugConstants.variable("imgOriginal", imgOriginal);
            if (imgOriginal != null) {
              ManyToManyReference.defineLink(params,
                                             _cyclingImage_imgOriginal.getTable(),
                                             instance,
                                             imgOriginal,
                                             true,
                                             null,
                                             getTransaction(),
                                             Db.DEFAULT_TIMEOUT,
                                             null);
            }
          }
          String add_json = params.getString(IN_ADD_JSON);
          if (!Strings.isNullOrEmpty(add_json)) {
            JSONArray jArr;
            try {
              jArr = new WritableJSONArray(add_json);
            } catch (JSONException e) {
              throw new MirrorFieldException(String.format("Unable to parse %s value of %s",
                                                           IN_ADD_JSON,
                                                           add_json), e, instance, null);
            }
            for (int i = 0; i < jArr.length(); i++) {
              JSONObject jObj = jArr.optJSONObject(i);
              PersistentRecord imgOriginal =
                  imgOriginalTable.getOptRecord(jObj.opt(IN_ADD_JSON_ID));
              if (imgOriginal != null) {
                ManyToManyReference.defineLink(params,
                                               _cyclingImage_imgOriginal.getTable(),
                                               instance,
                                               imgOriginal,
                                               true,
                                               null,
                                               getTransaction(),
                                               Db.DEFAULT_TIMEOUT,
                                               null).setField(CyclingImage_ImgOriginal.RANK,
                                                              jObj.opt(IN_ADD_JSON_RANK));
              }
            }
          }
        }
      }
    };
  }

  private CyclingImage_ImgOriginal _cyclingImage_imgOriginal;

  public final CyclingImage_ImgOriginal getCyclingImage_ImgOriginal()
  {
    return _cyclingImage_imgOriginal;
  }

  @Override
  public void shutdown()
  {
    // TODO Auto-generated method stub
  }

  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
      throws GntmlException
  {
    // TODO Auto-generated method stub
  }
}
