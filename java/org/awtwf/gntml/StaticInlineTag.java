package org.awtwf.gntml;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.node.json.JSONLoader;
import org.cord.node.json.JSONSaver;
import org.cord.node.json.UnresolvedRecordException;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.cord.util.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class StaticInlineTag
  extends EnglishNamedImpl
  implements InlineTag
{
  /**
   * Create a new StaticInlineTag that gives the ability to utilise an HTML tag directly in the
   * GNTML source code. This means that for less common tags which it is hard to find a sensible way
   * of notating it in GNTML then we can just let people use the tag directly. No point in
   * re-inventing the wheel unless it is sensible to do so...
   * 
   * @param tag
   *          The HTML tag without the enclosing quotes
   * @param englishName
   *          The readable description of what the tag does
   * @return The appropriate StaticInlineTag
   */
  public static StaticInlineTag createHtmlTag(String tag,
                                              String englishName)
  {
    return new StaticInlineTag(tag,
                               englishName,
                               "&lt;" + tag + "&gt;",
                               "&lt;" + tag + "&gt;(.*?)&lt;/" + tag + "&gt;",
                               "<" + tag + ">$1</" + tag + ">",
                               null,
                               null,
                               null,
                               null,
                               false);
  }

  private final String __targetSubstring;

  private final Pattern __pattern;

  private final String[] __targetValues = new String[5];

  private final boolean __outputsWebmacro;

  public StaticInlineTag(String name,
                         String englishName,
                         String targetSubstring,
                         String targetRegex,
                         String targetValue0,
                         String targetValue1,
                         String targetValue2,
                         String targetValue3,
                         String targetValue4,
                         boolean outputsWebmacro)
  {
    super(name,
          englishName);
    __targetSubstring = targetSubstring;
    __pattern = Pattern.compile(targetRegex);
    __targetValues[0] = StringUtils.padEmpty(targetValue0, "");
    __targetValues[1] = StringUtils.padEmpty(targetValue1, __targetValues[0]);
    __targetValues[2] = StringUtils.padEmpty(targetValue2, __targetValues[1]);
    __targetValues[3] = StringUtils.padEmpty(targetValue3, __targetValues[2]);
    __targetValues[4] = StringUtils.padEmpty(targetValue4, __targetValues[3]);
    __outputsWebmacro = outputsWebmacro;
  }

  private Matcher matcher(String source)
  {
    if (__targetSubstring != null && source.indexOf(__targetSubstring) == -1) {
      return null;
    }
    return __pattern.matcher(source);
  }

  @Override
  public boolean outputsWebmacro(String source)
  {
    if (!__outputsWebmacro) {
      return false;
    }
    Matcher matcher = matcher(source);
    return matcher == null ? false : matcher.find();
  }

  @Override
  public String parse(GntmlDocument document,
                      Transaction transaction,
                      String source,
                      Gettable params)
  {
    Matcher matcher = matcher(source);
    if (matcher == null || !matcher.find()) {
      return source;
    }
    matcher.reset();
    StringBuffer result = new StringBuffer(source.length() + 32);
    while (matcher.find()) {
      for (int i = Math.min(matcher.groupCount(), __targetValues.length); i >= 0; i--) {
        if (matcher.group(i) != null) {
          matcher.appendReplacement(result, __targetValues[i]);
          break;
        }
      }
    }
    matcher.appendTail(result);
    return result.toString();
  }

  @Override
  public void preParse(GntmlDocument gntmlDocument,
                       Transaction transaction)
      throws GntmlException
  {
  }

  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
      throws GntmlException
  {
  }

  @Override
  public boolean hasEditWidget()
  {
    return false;
  }

  @Override
  public String getEditTemplatePath()
  {
    return null;
  }

  /**
   * Do nothing as StaticInlineTags are self-sufficient and don't require external input.
   */
  @Override
  public void copyInlineTag(Settable settable,
                            PersistentRecord fromNode,
                            PersistentRecord fromRegionStyle,
                            PersistentRecord toRegionStyle,
                            PersistentRecord fromInstance,
                            GntmlDocument gntmlDocument)
      throws SettableException
  {
  }

  /**
   * @return false
   */
  @Override
  public boolean isParametric()
  {
    return false;
  }

  /**
   * Do nothing as the information is contained within the GNTML
   */
  @Override
  public void loadJSON(JSONLoader jLoader,
                       PersistentRecord node,
                       PersistentRecord regionStyle,
                       JSONObject jObj,
                       Settable settable)
      throws SettableException, UnresolvedRecordException
  {
  }

  /**
   * Do nothing as the information is contained within the GNTML
   */
  @Override
  public void saveJSON(JSONSaver jsonSaver,
                       PersistentRecord node,
                       PersistentRecord regionStyle,
                       PersistentRecord gntmlInstance,
                       GntmlDocument gntmlDocument,
                       JSONObject jGntml)
      throws JSONException
  {
  }

}
