package org.awtwf.gntml;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.DbInitialisers;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorLogicException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.node.ContentCompilerFactory;
import org.cord.node.FeedbackErrorException;
import org.cord.node.MoveableContentCompiler;
import org.cord.node.NodeManager;
import org.cord.node.NodeMoveException;
import org.cord.node.NodeRequestSegmentManager;
import org.cord.node.cc.CopyableTableContentCompiler;
import org.cord.node.cc.TableContentCompilerDb;
import org.cord.node.json.HandlesJSON;
import org.cord.node.json.JSONLoader;
import org.cord.node.json.JSONSaver;
import org.cord.node.json.UnresolvedRecordException;
import org.cord.node.search.AdvancedWordSearchable;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.NamespaceSettable;
import org.cord.util.NoSuchNamedException;
import org.cord.util.NumberUtil;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.cord.util.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONObject;
import org.webmacro.NotFoundException;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

public class Gntml
  extends CopyableTableContentCompiler
  implements AdvancedWordSearchable, MoveableContentCompiler, HandlesJSON
{
  public final static String NAME = "GNTML";

  public final static String TABLENAME = "GntmlInstance";

  public final static FieldKey<String> GNTML = FieldKey.create("gntml", String.class);

  public final static FieldKey<String> HTML = FieldKey.create("html", String.class);

  public final static FieldKey<Boolean> OUTPUTSWEBMACRO = FieldKey.create("outputsWebmacro",
                                                                          Boolean.class);

  public final static RecordOperationKey<RecordSource> REFERENCES =
      RecordOperationKey.create("gntmlDocuments", RecordSource.class);

  public final static String VIEWTEMPLATEPATH = "Gntml/gntml.view.wm.html";

  public final static String EDITTEMPLATEPATH = "Gntml/gntml.edit.wm.html";

  public static final RecordOperationKey<GntmlDocument> GNTMLDOCUMENT =
      RecordOperationKey.create("gntmlDocument", GntmlDocument.class);
  public static final RecordOperationKey<String> VIEWCSS = GntmlStyle.VIEWCSS;

  public static final String CONF_GNTML = "Gntml";

  /**
   * Resolve the istance of Gntml that has been registered with the NodeManager.
   * 
   * @return never null
   * @throws NoSuchNamedException
   *           if there isn't a ContentCompiler named NAME registered on the NodeManager
   * @throws ClassCastException
   *           if there is a ContentCompiler named NAME but it isn't a Gntml ContentCompiler. This
   *           should never happen in a normal well configured system.
   * @see ContentCompilerFactory#getContentCompiler(String)
   */
  public static Gntml getGntmlManager(NodeManager nodeManager)
      throws NoSuchNamedException
  {
    Preconditions.checkNotNull(nodeManager, "nodeManager");
    return (Gntml) nodeManager.getContentCompilerFactory().getContentCompiler(NAME);
  }

  // protected final GntmlParserFactory __gntmlParserFactory;

  private final GntmlMgr __gntmlMgr;

  @Deprecated
  public Gntml(final GntmlMgr gntmlMgr,
               final String transactionPassword,
               GntmlStyle gntmlStyle,
               // final GntmlParserFactory gntmlParserFactory,
               String zoomTemplatePath,
               String zoomAuthenticationError) throws MirrorTableLockedException
  {
    this(gntmlMgr,
         transactionPassword,
         gntmlStyle);
    // gntmlParserFactory);
  }

  public Gntml(final GntmlMgr gntmlMgr,
               final String transactionPassword,
               final GntmlStyle gntmlStyle)
  // final GntmlParserFactory gntmlParserFactory)
  {
    super(NAME,
          gntmlMgr.getNodeMgr(),
          transactionPassword,
          new TableContentCompilerDb("org.awtwf.node.GntmlDocument",
                                     Gntml.TABLENAME,
                                     Gntml.REFERENCES,
                                     3,
                                     gntmlStyle,
                                     VIEWTEMPLATEPATH,
                                     EDITTEMPLATEPATH) {
            @Override
            protected void internalInit(final Db db,
                                        final String pwd,
                                        final Table instanceTable)
                throws MirrorTableLockedException
            {
              instanceTable.addField(new StringField(GNTML,
                                                     "GNTML source document",
                                                     StringField.LENGTH_MEDIUMTEXT,
                                                     ""));
              instanceTable.addField(new StringField(HTML,
                                                     "Compiled XHTML document",
                                                     StringField.LENGTH_MEDIUMTEXT,
                                                     ""));
              instanceTable.addField(new BooleanField(OUTPUTSWEBMACRO,
                                                      "XHTML is webmacro template?"));
              instanceTable.addRecordOperation(new SimpleRecordOperation<GntmlDocument>(GNTMLDOCUMENT) {
                @Override
                public GntmlDocument getOperation(TransientRecord gntmlInstance,
                                                  Viewpoint viewpoint)
                {
                  Transaction transaction =
                      viewpoint != null && viewpoint instanceof Transaction
                          ? (Transaction) viewpoint
                          : null;
                  try {
                    return getGntmlDocument(gntmlMgr, (PersistentRecord) gntmlInstance, transaction);
                  } catch (GntmlException e) {
                    throw new LogicException("Unable to build GntmlDocument", e);
                  }
                }
              });
              instanceTable.addRecordOperation(new SimpleRecordOperation<String>(VIEWCSS) {
                @Override
                public String getOperation(TransientRecord gntmlInstance,
                                           Viewpoint viewpoint)
                {
                  final PersistentRecord gntmlStyle = gntmlInstance.comp(STYLE, viewpoint);
                  StringBuilder buf = new StringBuilder();
                  buf.append("gntml");
                  buf.append(" left_")
                     .append(NumberUtil.toString(GntmlStyle.getGridMarginLeft(gntmlStyle)));
                  buf.append(" body_")
                     .append(NumberUtil.toString(GntmlStyle.getGridWidth(gntmlStyle)));
                  buf.append(" right_")
                     .append(NumberUtil.toString(GntmlStyle.getGridMarginRight(gntmlStyle)));
                  buf.append(" ").append(gntmlStyle.opt(GntmlStyle.VIEWCSS));
                  return buf.toString();
                }
              });
              try {
                GntmlTemplateLoader.init(gntmlMgr.getNodeMgr().getWebMacro(), db);
              } catch (NotFoundException e) {
                throw new LogicException("Unable to resolve GntmlTemplateLoader", e);
              }
            }
          },
          null,
          true,
          null,
          null);
    __gntmlMgr = gntmlMgr;
    // __gntmlParserFactory = gntmlParserFactory;
    getNodeManager().getContentCompilerFactory()
                    .getContentCompiler(ContentCompilerFactory.NAME_SINGLEIMAGE);
    getNodeManager().getContentCompilerFactory()
                    .getContentCompiler(ContentCompilerFactory.NAME_BINARYASSET);
    NodeRequestSegmentManager nrsm = getNodeManager().getNodeRequestSegmentManager();
    // nrsm.register(new ZoomGntmlImage("zoomGntmlImage",
    // nodeManager,
    // zoomTemplatePath,
    // zoomAuthenticationError));
    // nrsm.register(new ZoomImageBlock(nodeManager,
    // zoomTemplatePath,
    // zoomAuthenticationError));
    nrsm.register(ZoomScalableImage.getInstance(getNodeManager()));
  }

  protected final GntmlMgr getGntmlMgr()
  {
    return __gntmlMgr;
  }

  @Override
  public DbInitialisers getDbInitialisers()
  {
    final List<DbInitialiser> ldi = getDefaultDbInitialisers();
    _gntmlStyle = (GntmlStyle) getTableContentCompilerDb().getStyleTableWrapper();
    // ldi.add(_gntmlImageStyle = new GntmlImageStyle());
    ldi.add(_gntmlPlusBlockStyle = new GntmlPlusBlockStyle());
    return getDbInitialisers(ldi);
  }

  // private GntmlImageStyle _gntmlImageStyle;

  // @Deprecated
  // public GntmlImageStyle getGntmlImageStyle()
  // {
  // return _gntmlImageStyle;
  // }

  private GntmlStyle _gntmlStyle;

  public GntmlStyle getGntmlStyle()
  {
    return _gntmlStyle;
  }

  private GntmlPlusBlockStyle _gntmlPlusBlockStyle;

  public GntmlPlusBlockStyle getGntmlPlusBlockStyle()
  {
    return _gntmlPlusBlockStyle;
  }

  protected GntmlDocument getGntmlDocument(PersistentRecord gntmlInstance,
                                           Transaction transaction)
      throws GntmlException
  {
    return getGntmlDocument(getGntmlMgr(),
    // __gntmlDownload,
                            gntmlInstance,
                            transaction);
  }

  // protected static GntmlParser getGntmlParser(GntmlParserFactory gntmlParserFactory,
  // PersistentRecord gntmlInstance)
  // throws MirrorNoSuchRecordException
  // {
  // return gntmlParserFactory.getParser((Integer) gntmlInstance.getField(STYLE_ID));
  // }

  protected static GntmlDocument getGntmlDocument(GntmlMgr gntmlMgr,
                                                  PersistentRecord gntmlInstance,
                                                  Transaction transaction)
      throws GntmlException
  {
    String sourceGntml = gntmlInstance.opt(GNTML);
    // GntmlParser gntmlParser = null;
    // try {
    // gntmlParser = getGntmlParser(parserFactory, gntmlInstance);
    // } catch (MirrorNoSuchRecordException mnsrEx) {
    // throw new LogicException("Missing GntmlParser for " + gntmlInstance, mnsrEx);
    // }
    // PersistentRecord gntmlStyle = gntmlInstance.followEnforcedLink(STYLE);
    // boolean permitsImages = gntmlStyle.mayFollowLink(GntmlStyle.DEFAULTIMAGESTYLE);
    GntmlDocument gntmlDocument = gntmlMgr.getDocument(gntmlInstance.getId(), sourceGntml,
    // permitsImages ? gntmlInstance.getId().toString(): null,
                                                       gntmlInstance.getId().toString(),
                                                       transaction);
    return gntmlDocument;
  }

  public boolean updateDocument(WritableRecord gntmlInstance,
                                Gettable params,
                                Transaction transaction,
                                Map<Object, Object> outputs)
      throws MirrorFieldException, GntmlException
  {
    String source =
        StringUtils.toString(uploadStringField(params, gntmlInstance, null, GNTML, false));
    if (Strings.isNullOrEmpty(source)) {
      gntmlInstance.setField(HTML, "");
      gntmlInstance.setField(OUTPUTSWEBMACRO, false);
    } else {
      GntmlDocument gntmlDocument;
      gntmlDocument = getGntmlDocument(gntmlInstance, transaction);
      gntmlInstance.setField(HTML, gntmlDocument.parse(params, transaction, outputs));
      gntmlInstance.setField(OUTPUTSWEBMACRO, gntmlDocument.outputsWebmacro());
    }
    return gntmlInstance.getIsUpdated();
  }

  /**
   * @see #updateDocument(WritableRecord, Gettable, Transaction)
   */
  @Override
  public final boolean updateTransientInstance(PersistentRecord node,
                                               PersistentRecord regionStyle,
                                               TransientRecord trGntmlInstance,
                                               PersistentRecord gntmlStyle,
                                               Transaction transaction,
                                               Gettable inputs,
                                               Map<Object, Object> outputs,
                                               PersistentRecord user)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException,
      FeedbackErrorException
  {
    if (!(trGntmlInstance instanceof WritableRecord)) {
      return false;
    }
    WritableRecord gntmlInstance = (WritableRecord) trGntmlInstance;
    try {
      return updateDocument(gntmlInstance, inputs, transaction, outputs);
    } catch (GntmlException e) {
      throw new FeedbackErrorException("Parse GNTML document",
                                       node,
                                       e,
                                       false,
                                       true,
                                       "Unable to parse document: %s",
                                       e.getMessage());
    }
  }

  private final List<RecordOperationKey<?>> AWS_FIELDS =
      ImmutableList.<RecordOperationKey<?>> builder().add(GNTML).build();

  @Override
  public Iterable<RecordOperationKey<?>> getAwsIndexableFields()
  {
    return AWS_FIELDS;
  }

  @Override
  public Iterable<RecordOperationKey<?>> getAwsSearchableFields()
  {
    return AWS_FIELDS;
  }

  @Override
  public Gettable getAwsIndexableValues(PersistentRecord node,
                                        PersistentRecord regionStyle,
                                        Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    PersistentRecord instance = getInstance(node, regionStyle, null, viewpoint);
    return instance.comp(Gntml.ISDEFINED) ? instance : null;
  }

  @Override
  protected void copyContentCompiler(Settable settable,
                                     PersistentRecord fromNode,
                                     PersistentRecord fromRegionStyle,
                                     PersistentRecord toRegionStyle,
                                     PersistentRecord fromGntmlInstance)
      throws MirrorNoSuchRecordException, SettableException
  {
    GntmlDocument gntmlDocument = fromGntmlInstance.opt(GNTMLDOCUMENT);
    settable.set(GNTML, fromGntmlInstance.opt(GNTML));
    // GntmlParser parser = getGntmlParser(__gntmlParserFactory, fromGntmlInstance);
    for (InlineTag inlineTag : getGntmlMgr().getInlineTags().values()) {
      inlineTag.copyInlineTag(settable,
                              fromNode,
                              fromRegionStyle,
                              toRegionStyle,
                              fromGntmlInstance,
                              gntmlDocument);
    }
    for (GntmlBlock gntmlBlock : gntmlDocument.getBlocks()) {
      String xhtmlId = gntmlBlock.getXhtmlId();
      Settable blockParams =
          Strings.isNullOrEmpty(xhtmlId)
              ? settable
              : new NamespaceSettable(null, settable, gntmlBlock.getGntmlBlockFactory().getName()
                                                      + "-" + xhtmlId + "-");
      gntmlBlock.copyGntmlBlock(blockParams,
                                fromNode,
                                fromRegionStyle,
                                toRegionStyle,
                                fromGntmlInstance,
                                gntmlDocument);
    }
  }

  @Override
  public void moveContentCompiler(Transaction transaction,
                                  WritableRecord fromNode,
                                  PersistentRecord regionStyle,
                                  PersistentRecord fromParentNode,
                                  PersistentRecord toParentNode)
      throws NodeMoveException
  {
    // TODO: Validate if GNTML is really moveable
  }

  @Override
  public void moveChildContentCompiler(Transaction transaction,
                                       WritableRecord fromNode,
                                       PersistentRecord fromParentNode,
                                       PersistentRecord toParentNode,
                                       WritableRecord fromChildNode,
                                       PersistentRecord regionStyle)
      throws NodeMoveException
  {
    // TODO: Validate if GNTML is really moveable
  }

  @Override
  public void loadJSON(JSONLoader jLoader,
                       PersistentRecord node,
                       PersistentRecord regionStyle,
                       JSONObject jGntml,
                       Settable settable)
      throws SettableException, UnresolvedRecordException, JSONException,
      MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException, IOException, FeedbackErrorException
  {
    PersistentRecord gntmlInstance;
    try {
      gntmlInstance = getInstance(node, regionStyle, null, null);
    } catch (MirrorNoSuchRecordException e) {
      throw new MirrorLogicException("Cannot resolve GNTML", e);
    }
    // GntmlParser parser = null;
    // try {
    // parser = getGntmlParser(__gntmlParserFactory, gntmlInstance);
    // } catch (MirrorNoSuchRecordException e) {
    // throw new MirrorLogicException("Cannot resolve parser for " + gntmlInstance, e);
    // }
    settable.set(GNTML, jGntml.opt(GNTML.getKeyName()));
    GntmlDocument gntmlDocument = gntmlInstance.opt(GNTMLDOCUMENT);
    for (InlineTag inlineTag : getGntmlMgr().getInlineTags().values()) {
      inlineTag.loadJSON(jLoader, node, regionStyle, jGntml, settable);
    }
    for (GntmlBlock gntmlBlock : gntmlDocument.getBlocks()) {
      gntmlBlock.loadJSON(jLoader, node, regionStyle, jGntml, settable);
    }
    System.err.println(settable);
  }

  @Override
  public JSONObject saveJSON(JSONSaver jSaver,
                             PersistentRecord node,
                             PersistentRecord regionStyle)
      throws JSONException, IOException
  {
    PersistentRecord gntmlInstance;
    try {
      gntmlInstance = getInstance(node, regionStyle, null, null);
    } catch (MirrorNoSuchRecordException e) {
      throw new MirrorLogicException("Cannot resolve GNTML", e);
    }
    GntmlDocument gntmlDocument = gntmlInstance.opt(GNTMLDOCUMENT);
    JSONObject jGntml = new WritableJSONObject();
    jGntml.put(GNTML.getKeyName(), gntmlInstance.opt(GNTML));
    // GntmlParser parser = null;
    // try {
    // parser = getGntmlParser(__gntmlParserFactory, gntmlInstance);
    // } catch (MirrorNoSuchRecordException e) {
    // throw new MirrorLogicException("Cannot resolve parser for " + gntmlInstance, e);
    // }
    for (InlineTag inlineTag : getGntmlMgr().getInlineTags().values()) {
      inlineTag.saveJSON(jSaver, node, regionStyle, gntmlInstance, gntmlDocument, jGntml);
    }
    for (GntmlBlock gntmlBlock : gntmlDocument.getBlocks()) {
      gntmlBlock.saveJSON(jSaver, node, regionStyle, gntmlInstance, gntmlDocument, jGntml);
    }
    return jGntml;
  }

}
