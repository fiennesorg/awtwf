package org.awtwf.gntml;

import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.InternalSuccessOperation;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.view.DynamicAclAuthenticatedFactory;
import org.cord.util.NotImplementedException;
import org.webmacro.Context;

import com.google.common.base.Preconditions;

public class SourcePatternView
  extends DynamicAclAuthenticatedFactory
{
  private final SourcePatternMgr __mgr;

  public static final String NAME = "sourcePatternView";

  public static final String CGI_DEFINITIONNAME = "definitionName";

  public static final String WEBMACRO_SOURCEPATTERNDEFINITION = "sourcePatternDefinition";

  protected SourcePatternView(SourcePatternMgr mgr,
                              Integer aclId)
  {
    super(NAME,
          mgr.getNodeMgr(),
          InternalSuccessOperation.getInstance(),
          aclId,
          null);
    __mgr = Preconditions.checkNotNull(mgr, "mgr");
  }

  protected final SourcePatternMgr getMgr()
  {
    return __mgr;
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    String definitionName = nodeRequest.getString(CGI_DEFINITIONNAME);
    SourcePatternDefinition definition = getMgr().getDefinitions().get(definitionName);
    if (definition == null) {
      throw new FeedbackErrorException(null,
                                       node,
                                       "Cannot resolve SourcePatternDefinition named '%s'",
                                       definitionName);
    }
    context.put(WEBMACRO_SOURCEPATTERNDEFINITION, definition);

    throw new NotImplementedException();
  }
}
