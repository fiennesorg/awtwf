package org.awtwf.gntml;

import java.io.IOException;

import org.cord.mirror.TransientRecord;
import org.cord.util.HtmlUtils;
import org.cord.util.HtmlUtilsTheme;

public class GntmlBlockStyleError
  extends AbstractGntmlBlockStyle
{
  private final String __message;

  public GntmlBlockStyleError(String name,
                              String message)
  {
    super(name);
    __message = message;
  }

  @Override
  public boolean accepts(TransientRecord gntmlStyle)
  {
    return true;
  }

  @Override
  public void appendHeader(GntmlBlock gntmlBlock,
                           Appendable out)
      throws IOException
  {
    HtmlUtils.errorBlock(out, __message, DefinedGntmlBlockStyle.CSS_DEFAULTMARGINS);
    HtmlUtilsTheme.openDivOfClass(out, DefinedGntmlBlockStyle.CSS_DEFAULTMARGINS);
  }

  @Override
  public void appendFooter(GntmlBlock gntmlBlock,
                           Appendable out)
      throws IOException
  {
    HtmlUtilsTheme.closeDiv(out);
  }

  @Override
  public boolean outputsWebmacro()
  {
    return false;
  }

}
