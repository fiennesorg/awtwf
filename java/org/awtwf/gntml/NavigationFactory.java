package org.awtwf.gntml;

import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.ConstantDefinedFieldValueSupplier;
import org.cord.mirror.field.EnglishNamedMapField;
import org.cord.mirror.field.EnumField;
import org.cord.mirror.field.IntegerField;
import org.cord.mirror.field.StringField;
import org.cord.node.NodeManager;
import org.cord.util.DuplicateNamedException;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.NamespaceGettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

public class NavigationFactory
  extends TableBlockFactory
{
  public final static String NAME = "Navigation";

  public final static String TABLENAME = "GntmlNavigation";

  public final static String VIEWTEMPLATEPATH = "Gntml/navigation.view.wm.html";

  public final static String EDITTEMPLATEPATH = "Gntml/navigation.edit.wm.html";

  public NavigationFactory(NodeManager nodeMgr)
  {
    super(nodeMgr,
          NAME,
          "Nested Child Navigation",
          "|ChildPages:",
          "|",
          TABLENAME,
          VIEWTEMPLATEPATH,
          "gntml_navigation",
          "gntml_navigation_caption",
          EDITTEMPLATEPATH,
          null);
  }

  @Override
  protected TableBlock buildTableBlock(ListIterator<String> sourceLines,
                                       GntmlBlockStyle blockStyle,
                                       GntmlDocument gntmlDocument,
                                       int blockId)
      throws GntmlException
  {
    return new NavigationBlock(sourceLines, blockStyle, gntmlDocument, blockId);
  }

  public static final FieldKey<String> TITLE = FieldKey.create("title", String.class);
  public static final FieldKey<ValidChildren> VALIDCHILDREN = FieldKey.create("validChildren",
                                                                              ValidChildren.class);
  public static final FieldKey<NavigationStyle> NAVIGATIONSTYLE =
      FieldKey.create("navigationStyle", NavigationStyle.class);
  public static final FieldKey<Integer> PAGINATIONSIZE = FieldKey.create("paginationSize",
                                                                         Integer.class);
  public static final FieldKey<String> ITEMNAME = FieldKey.create("itemName", String.class);
  public static final FieldKey<String> ITEMSNAME = FieldKey.create("itemsName", String.class);

  private EnglishNamedMapField<NavigationStyle> _navigationStyle;

  public EnglishNamedMapField<NavigationStyle> getNavigationStyle()
  {
    return _navigationStyle;
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table table)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    table.addField(new StringField(TITLE, "Title"));
    table.addField(new EnumField<ValidChildren>(VALIDCHILDREN,
                                                "Child pages to be included",
                                                ValidChildren.class,
                                                ConstantDefinedFieldValueSupplier.wrap(ValidChildren.ALL)));
    NavigationStyle concise =
        new NavigationStyle("title", "List - Title", "Gntml/navigation/title.wm.html", false);
    _navigationStyle =
        new EnglishNamedMapField<NavigationStyle>(NAVIGATIONSTYLE,
                                                  "Presentation style",
                                                  NavigationStyle.class,
                                                  ConstantDefinedFieldValueSupplier.wrap(concise)) {
          @Override
          public boolean shouldReloadOnChange()
          {
            return true;
          }

        };
    _navigationStyle.addValue(concise);
    _navigationStyle.addValues(getNavigationStyles(getNodeManager().getGettable(), GETTABLE_NODE));
    _navigationStyle.addValues(getNavigationStyles(getNodeManager().getGettable(), GETTABLE_APP));
    table.addField(_navigationStyle);
    table.addField(new IntegerField(PAGINATIONSIZE, "Pagination size", 4));
    table.addField(new StringField(ITEMNAME,
                                   "Pagination item name",
                                   StringField.LENGTH_VARCHAR,
                                   "page"));
    table.addField(new StringField(ITEMSNAME,
                                   "Pagination item(s) name",
                                   StringField.LENGTH_VARCHAR,
                                   "pages"));
  }
  @Override
  public void shutdown()
  {
  }

  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
      throws GntmlException
  {
  }

  public enum ValidChildren {
    ALL("All published child pages") {
      @Override
      public RecordSource getChildren(PersistentRecord parentNode)
      {
        StringBuilder buf = new StringBuilder();
        buf.append("Node.parentNode_id=")
           .append(parentNode.getId())
           .append(" and Node.isPublished=1");
        String filter = buf.toString();
        return parentNode.getTable().getQuery(filter, filter, null);
      }
    },
    MENUITEMS("All published child menu pages") {
      @Override
      public RecordSource getChildren(PersistentRecord parentNode)
      {
        StringBuilder buf = new StringBuilder();
        buf.append("Node.parentNode_id=")
           .append(parentNode.getId())
           .append(" and Node.isPublished=1 and Node.isMenuItem=1");
        String filter = buf.toString();
        return parentNode.getTable().getQuery(filter, filter, null);
      }
    },
    NONMENUITEMS("All published child hidden pages") {
      @Override
      public RecordSource getChildren(PersistentRecord parentNode)
      {
        StringBuilder buf = new StringBuilder();
        buf.append("Node.parentNode_id=")
           .append(parentNode.getId())
           .append(" and Node.isPublished=1 and Node.isMenuItem=0");
        String filter = buf.toString();
        return parentNode.getTable().getQuery(filter, filter, null);
      }
    };

    private final String __toString;

    private ValidChildren(String toString)
    {
      __toString = toString;
    }

    @Override
    public String toString()
    {
      return __toString;
    }

    public abstract RecordSource getChildren(PersistentRecord parentNode);
  }

  public static final String GETTABLE_NAMES = "NavigationFactory.NavigationStyle.";
  public static final String GETTABLE_KEYS = ".keys";
  public static final String GETTABLE_NODE = "node";
  public static final String GETTABLE_APP = "app";

  public List<NavigationStyle> getNavigationStyles(Gettable source,
                                                   String namespace)
  {
    String keys = source.getString(GETTABLE_NAMES + namespace + GETTABLE_KEYS);
    if (keys == null) {
      return Collections.emptyList();
    }
    List<NavigationStyle> styles = Lists.newArrayList();
    for (String key : Splitter.on(',').trimResults().omitEmptyStrings().split(keys)) {
      styles.add(getNavigationStyle(source, key));
    }
    return styles;
  }

  public static final String GETTABLE_NAME_PREFIX = "NavigationFactory.NavigationStyle.";
  public static final String GETTABLE_ENGLISHNAME = "englishName";
  public static final String GETTABLE_TEMPLATEPATH = "templatePath";
  public static final String GETTABLE_HASPAGINATION = "hasPagination";

  public NavigationStyle getNavigationStyle(Gettable source,
                                            String name)
  {
    Preconditions.checkNotNull(name, "name");
    name = name.trim();
    Preconditions.checkState(name.length() > 0, "whitespace names are not permitted");
    NamespaceGettable gettable = new NamespaceGettable(source, GETTABLE_NAME_PREFIX + name + ".");
    try {
      return new NavigationStyle(name,
                                 gettable.getString(GETTABLE_ENGLISHNAME),
                                 gettable.getString(GETTABLE_TEMPLATEPATH),
                                 Objects.firstNonNull(gettable.getBoolean(GETTABLE_HASPAGINATION),
                                                      Boolean.FALSE));
    } catch (Exception e) {
      throw new IllegalArgumentException(String.format("Error resolving NavigationFactory named %s from %s",
                                                       name,
                                                       source),
                                         e);
    }
  }

  public static class NavigationStyle
    extends EnglishNamedImpl
  {
    private final String __templatePath;
    private final boolean __hasPagination;

    public NavigationStyle(String name,
                           String englishName,
                           String templatePath,
                           boolean hasPagination)
    {
      super(name,
            englishName);
      __templatePath = Preconditions.checkNotNull(templatePath, "templatePath");
      __hasPagination = hasPagination;
    }

    public String getTemplatePath()
    {
      return __templatePath;
    }

    public boolean hasPagination()
    {
      return __hasPagination;
    }
  }

  public class NavigationBlock
    extends TableBlock
  {

    protected NavigationBlock(ListIterator<String> sourceLines,
                              GntmlBlockStyle blockStyle,
                              GntmlDocument gntmlDocument,
                              int blockId) throws GntmlException
    {
      super(NAME,
            sourceLines,
            blockStyle,
            gntmlDocument,
            NavigationFactory.this,
            blockId);
    }

    @Override
    public void copyGntmlBlock(Settable settable,
                               PersistentRecord fromNode,
                               PersistentRecord fromRegionStyle,
                               PersistentRecord toRegionStyle,
                               PersistentRecord fromGntmlInstance,
                               GntmlDocument gntmlDocument,
                               PersistentRecord instance)
        throws SettableException
    {
      // TODO Auto-generated method stub
    }

    @Override
    protected void updateInstance(WritableRecord instance,
                                  Gettable params,
                                  Map<Object, Object> outputs)
        throws GntmlException, MirrorException
    {
      instance.setFieldIfDefined(TITLE, params.get(TITLE));
      instance.setFieldIfDefined(VALIDCHILDREN, params.get(VALIDCHILDREN));
      instance.setFieldIfDefined(NAVIGATIONSTYLE, params.get(NAVIGATIONSTYLE));
      instance.setFieldIfDefined(PAGINATIONSIZE, params.get(PAGINATIONSIZE));
      instance.setFieldIfDefined(ITEMNAME, params.get(ITEMNAME));
      instance.setFieldIfDefined(ITEMSNAME, params.get(ITEMSNAME));
    }
  }
}
