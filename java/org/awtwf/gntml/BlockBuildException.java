package org.awtwf.gntml;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * A GntmlException that includes the set of GntmlLines that were responsible for a compiler error
 * when parsing the document.
 * 
 * @author alex
 */
public class BlockBuildException
  extends GntmlException
{
  private static final long serialVersionUID = 1L;

  private List<String> __source = new ArrayList<String>();

  public BlockBuildException(String message,
                             Throwable cause,
                             List<GntmlLine> lines)
  {
    super(message,
          cause);
    for (GntmlLine line : lines) {
      __source.add(line.getSource());
    }
  }

  public ListIterator<String> getLineList()
  {
    return __source.listIterator();
  }
}
