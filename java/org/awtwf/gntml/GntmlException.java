package org.awtwf.gntml;

public class GntmlException
  extends Exception
{
  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public GntmlException(String message,
                        Throwable cause)
  {
    super(message,
          cause);
  }

  public GntmlException(String message)
  {
    super(message);
  }
}
