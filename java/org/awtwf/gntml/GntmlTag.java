package org.awtwf.gntml;

import org.cord.util.Named;

public interface GntmlTag
  extends Named
{
  public String getUsageExample();
}
