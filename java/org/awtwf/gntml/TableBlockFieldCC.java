package org.awtwf.gntml;

import org.cord.mirror.Field;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;

import com.google.common.base.Supplier;

public class TableBlockFieldCC<T>
  extends TableBlockCC<T>
{
  private final Field<T> __field;

  public TableBlockFieldCC(Field<T> field,
                           TableBlockFactory tableBlockFactory,
                           Supplier<T> defaultValue)
  {
    super(field.getName(),
          tableBlockFactory,
          defaultValue);
    __field = field;
  }

  @Override
  protected void init(String pwd,
                      Table table)
      throws MirrorTableLockedException
  {
    super.init(pwd, table);
    table.addField(__field);
  }

  @Override
  protected T getValue(TransientRecord instance)
  {
    return __field.getFieldValue(instance);
  }

  @Override
  protected boolean updateTransientInstance(TransientRecord instance,
                                            Gettable params)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException
  {
    instance.setFieldIfDefined(__field.getName(), params, __field.getName());
    return instance.getIsUpdated();
  }

}
