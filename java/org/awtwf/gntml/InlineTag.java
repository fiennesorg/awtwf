package org.awtwf.gntml;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.node.json.JSONLoader;
import org.cord.node.json.JSONSaver;
import org.cord.node.json.UnresolvedRecordException;
import org.cord.util.EnglishNamed;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.json.JSONException;
import org.json.JSONObject;

public interface InlineTag
  extends EnglishNamed
{
  public String parse(GntmlDocument document,
                      Transaction transaction,
                      String source,
                      Gettable params)
      throws GntmlException;

  public void preParse(GntmlDocument gntmlDocument,
                       Transaction transaction)
      throws GntmlException;

  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
      throws GntmlException;

  public boolean outputsWebmacro(String source);

  public boolean hasEditWidget();

  public String getEditTemplatePath();

  public void copyInlineTag(Settable settable,
                            PersistentRecord fromNode,
                            PersistentRecord fromRegionStyle,
                            PersistentRecord toRegionStyle,
                            PersistentRecord fromGntmlInstance,
                            GntmlDocument gntmlDocument)
      throws SettableException;

  /**
   * Does this InlineTag expect to receive parametric information when it is parsing the source in a
   * document? If yes then it cannot be parsed inside the context of a namespaced block because it
   * will not have a clear view of the non-namespaced Gettable.
   */
  public boolean isParametric();

  public void loadJSON(JSONLoader jLoader,
                       PersistentRecord node,
                       PersistentRecord regionStyle,
                       JSONObject jObj,
                       Settable settable)
      throws SettableException, UnresolvedRecordException, JSONException;

  public void saveJSON(JSONSaver jsonSaver,
                       PersistentRecord node,
                       PersistentRecord regionStyle,
                       PersistentRecord gntmlInstance,
                       GntmlDocument gntmlDocument,
                       JSONObject jGntml)
      throws JSONException;
}
