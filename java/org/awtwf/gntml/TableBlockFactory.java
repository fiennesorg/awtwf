package org.awtwf.gntml;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.Dependencies;
import org.cord.mirror.DirectCachingSimpleRecordOperation;
import org.cord.mirror.FieldKey;
import org.cord.mirror.HtmlGuiSupplier;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordStringFactory;
import org.cord.mirror.Table;
import org.cord.mirror.TableWrapper;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.trigger.ImmutableFieldTrigger;
import org.cord.mirror.trigger.PostTransactionDeleteTrigger;
import org.cord.node.CurrentUrlFactory;
import org.cord.node.MonoNodeQuestion;
import org.cord.node.Node;
import org.cord.node.NodeFilter;
import org.cord.node.NodeManager;
import org.cord.node.NodeQuestion;
import org.cord.node.NodeRequest;
import org.cord.node.RedirectionSuccessOperation;
import org.cord.node.RegionStyle;
import org.cord.node.field.NodeLinkField;
import org.cord.node.view.AskNodeQuestion;
import org.cord.sql.SqlUtil;
import org.cord.util.Assertions;
import org.cord.util.Casters;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.NamespaceGettable;
import org.cord.util.StringUtils;
import org.cord.webmacro.AppendableParametricMacro;
import org.webmacro.Context;
import org.webmacro.Macro;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

/**
 * Abstract base class that can be utilised for when a GntmlBlockFactory has named blocks each of
 * which map onto a single PersistentRecord in a backend mirror Table.
 * 
 * @author alex
 */
public abstract class TableBlockFactory
  extends NodeBlockFactory
  implements DbInitialiser, TableWrapper
{
  public static final FieldKey<Integer> GNTMLINSTANCE_ID =
      LinkField.getLinkFieldName(Gntml.TABLENAME);

  public static final RecordOperationKey<PersistentRecord> GNTMLINSTANCE =
      LinkField.getLinkName(GNTMLINSTANCE_ID);

  public static final FieldKey<String> BLOCKNAME = FieldKey.create("blockName", String.class);

  public static final FieldKey<Boolean> ISREFERENCED = FieldKey.create("isReferenced",
                                                                       Boolean.class);

  /**
   * The namePrefix that should be used to pass parameters into updates to Blocks produced by this
   * factory. The format is
   * "$tableBlock.regionStyle.name-$tableBlockFactory.Name-$tableBlock.xhtmlId"
   * 
   * @see RegionStyle#NAME
   * @see TableBlockFactory#getName()
   * @see #XHTMLID
   */
  public static final RecordOperationKey<String> NAMEPREFIX =
      RecordOperationKey.create("namePrefix", String.class);

  /** RecordOperation that calls {@link TableBlock#getXhtmlId()} */
  public static final RecordOperationKey<String> XHTMLID = RecordOperationKey.create("xhtmlId",
                                                                                     String.class);

  public static final RecordOperationKey<String> BLOCKID = RecordOperationKey.create("blockId",
                                                                                     String.class);

  public static final RecordOperationKey<String> URL = Node.URL;

  public static final MonoRecordOperationKey<String, Macro> ASFORMELEMENT =
      HtmlGuiSupplier.ASFORMELEMENT;
  public static final MonoRecordOperationKey<String, Macro> ASFORMVALUE =
      HtmlGuiSupplier.ASFORMVALUE;

  /**
   * RecordOperation on the instance table that resolves into the webmacro path that should be used
   * to render an instance.
   */
  public static final RecordOperationKey<String> VIEWTEMPLATEPATH =
      RecordOperationKey.create("viewTemplatePath", String.class);

  private final String __tableName;

  private Table _table;

  private final String __viewTemplatePath;

  private final String __editTemplatePath;

  private final String __instanceName;

  private final String __openTag;

  private final String __closeTag;

  private final String __blockCssClass;

  private final String __captionCssClass;

  private final List<TableBlockCC<?>> __tableBlockCcs = Lists.newArrayList();

  /**
   * @param nodeMgr
   *          The NodeManager that this TableBlockFactory is being embedded into. This will also be
   *          used as a souce of parameters for resolving optional configuration extensions via the
   *          contained Gettable. Parameters will be found in a NamespaceGettable with "name." as
   *          the prefix.
   * @param viewTemplatePath
   *          The path for the webmacro file that should be the default rendering method for
   *          instances. If viewTemplatePath is defined in the NamespaceGettable then this will take
   *          precedence.
   * @param instanceName
   *          The name that will be used for the variable in webmacro space that represents an
   *          instance of this block. If null then it will be the same as the tablename with a
   *          lowercase first letter.
   * @see NodeManager#getGettable()
   */
  public TableBlockFactory(NodeManager nodeMgr,
                           String name,
                           String englishName,
                           String openTag,
                           String closeTag,
                           String tableName,
                           String viewTemplatePath,
                           String blockCssClass,
                           String captionCssClass,
                           String editTemplatePath,
                           String instanceName)
  {
    super(nodeMgr,
          name,
          englishName);
    Gettable gettable = nodeMgr.getGettable();
    gettable = new NamespaceGettable(null, gettable, name + ".");
    Preconditions.checkNotNull(openTag, "openTag");
    Preconditions.checkNotNull(closeTag, "closeTag");
    openTag = openTag.trim();
    closeTag = closeTag.trim();
    Preconditions.checkState(openTag.length() > 0, "openTag");
    Preconditions.checkState(closeTag.length() > 0, "closeTag");
    __openTag = openTag.toLowerCase();
    __closeTag = closeTag.toLowerCase();
    __tableName = Assertions.notEmpty(tableName, "tableName");
    String customViewTemplatePath = gettable.getString(VIEWTEMPLATEPATH);
    viewTemplatePath = StringUtils.padEmpty(customViewTemplatePath, viewTemplatePath);
    __viewTemplatePath = Assertions.notEmpty(viewTemplatePath, "viewTemplatePath");
    __editTemplatePath = Assertions.notEmpty(editTemplatePath, "editTemplatePath");
    if (Strings.isNullOrEmpty(instanceName)) {
      __instanceName = Character.toLowerCase(tableName.charAt(0)) + tableName.substring(1);
    } else {
      __instanceName = instanceName;
    }
    __blockCssClass = blockCssClass;
    __captionCssClass =
        Strings.isNullOrEmpty(captionCssClass) ? "tableBlock_caption" : captionCssClass;
  }

  public List<TableBlockCC<?>> getTableBlockCcs()
  {
    return Collections.unmodifiableList(__tableBlockCcs);
  }

  protected void register(TableBlockCC<?> tableBlockCC,
                          String pwd,
                          Table table)
      throws MirrorTableLockedException
  {
    Preconditions.checkState(tableBlockCC != null
                                 && this.equals(tableBlockCC.getTableBlockFactory()),
                             "%s cannot be registered on %s",
                             tableBlockCC,
                             this);
    __tableBlockCcs.add(tableBlockCC);
    tableBlockCC.init(pwd, table);
  }

  public final String getInstanceName()
  {
    return __instanceName;
  }

  public final String getEditTemplatePath()
  {
    return __editTemplatePath;
  }

  public final String getViewTemplatePath()
  {
    return __viewTemplatePath;
  }

  @Override
  public final String getTableName()
  {
    return __tableName;
  }

  @Override
  public final Table getTable()
  {
    return _table;
  }

  /**
   * Does the firstSourceLine start with openTag and end with closeTag as defined in the constructor
   * and do these enclose a name that doesn't start or end with whitespace?
   */
  @Override
  public final boolean acceptsSource(GntmlDocument gntmlDocument,
                                     String firstSourceLine)
  {
    firstSourceLine = firstSourceLine.toLowerCase();
    if (firstSourceLine.startsWith(__openTag) && firstSourceLine.endsWith(__closeTag)
        && firstSourceLine.length() > __openTag.length() + __closeTag.length()) {
      return true;
    }
    return false;
  }

  /**
   * Calculate what is left from the blockLineSource after stripping off the openTag from the
   * beginning and the closeTag from the end.
   */
  public final String calculateBlockName(String blockLineSource)
  {
    return blockLineSource.substring(__openTag.length(),
                                     blockLineSource.length() - __closeTag.length()).toLowerCase();
  }

  /**
   * Create the common elements to the Table shared by all TableBlockFactories and then delegate to
   * the sublasses init method to complete the process with usage specific fields. The following
   * elements will be created:-
   * <ul>
   * <li>gntmlInstance_id as an immutable link to the containing GntmlInstance record.</li>
   * <li>blockName with immutable name of the block as extracted from calculateBlockName</li>
   * <li>isReferenced which is used to detect whether a given block is still referred to in a
   * document and therefore we can dump unused information once it is not longer required</li>
   * <li>StateTrigger on STATE_TRANSACTION_COMMITTED that does the deletion of all records that have
   * isReferenced set to false once an update transaction has been completed.</li>
   * </ul>
   * 
   * @see #GNTMLINSTANCE_ID
   * @see #BLOCKNAME
   * @see #ISREFERENCED
   * @see #init(Db, String, Table)
   */
  @Override
  public final void init(final Db db,
                         final String pwd)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    _table = db.addTable(this, getTableName());
    _table.addField(new LinkField(Gntml.TABLENAME,
                                  "Containing GNTML document",
                                  pwd,
                                  Dependencies.LINK_ENFORCED | Dependencies.BIT_S_ON_T_UPDATE).setMayHaveHtmlWidget(false));
    _table.addFieldTrigger(ImmutableFieldTrigger.create(GNTMLINSTANCE_ID));
    _table.addField(new StringField(BLOCKNAME, "Block name").setMayHaveHtmlWidget(false));
    _table.addFieldTrigger(ImmutableFieldTrigger.create(BLOCKNAME));
    _table.addField(new BooleanField(ISREFERENCED, "Is referenced in document").setMayHaveHtmlWidget(false));
    init(db, pwd, _table);
    PostTransactionDeleteTrigger.register(_table, ISREFERENCED, pwd, getName()
                                                                     + ".isReferenced cleanup");
    _table.addRecordOperation(new DirectCachingSimpleRecordOperation<String>(XHTMLID) {
      @Override
      public String calculate(TransientRecord instance,
                              Viewpoint viewpoint)
      {
        return TableBlock.getXhtmlId(instance);
      }
    });
    _table.addRecordOperation(new DirectCachingSimpleRecordOperation<String>(NAMEPREFIX) {
      @Override
      public String calculate(TransientRecord instance,
                              Viewpoint viewpoint)
      {
        StringBuilder buf = new StringBuilder();
        PersistentRecord gntmlInstance = instance.comp(GNTMLINSTANCE);
        PersistentRecord regionStyle = gntmlInstance.comp(Gntml.REGIONSTYLE);
        buf.append(regionStyle.opt(RegionStyle.NAME)).append('-');
        buf.append(TableBlockFactory.this.getName()).append('-');
        buf.append(instance.opt(XHTMLID)).append('-');
        return buf.toString();
      }
    });
    _table.addRecordOperation(new MonoRecordOperation<String, Macro>(ASFORMELEMENT,
                                                                     Casters.TOSTRING) {
      @Override
      public Macro calculate(final TransientRecord instance,
                             final Viewpoint viewpoint,
                             final String name)
      {
        return new AppendableParametricMacro() {
          @Override
          public void append(Appendable out,
                             Map<Object, Object> context)
              throws IOException
          {
            instance.appendFormElement(instance.opt(NAMEPREFIX),
                                       name,
                                       out,
                                       NodeRequest.asGettable(context));
          }
        };
      }
    });
    _table.addRecordOperation(new MonoRecordOperation<String, Macro>(ASFORMVALUE,
                                                                     Casters.TOSTRING) {
      @Override
      public Macro calculate(final TransientRecord instance,
                             final Viewpoint viewpoint,
                             final String name)
      {
        return new AppendableParametricMacro() {
          @Override
          public void append(Appendable out,
                             Map<Object, Object> context)
              throws IOException
          {
            instance.appendFormValue(instance.opt(NAMEPREFIX),
                                     name,
                                     out,
                                     NodeRequest.asGettable(context));
          }
        };
      }
    });
    _table.addRecordOperation(new IsActive());
    _table.addRecordOperation(new SimpleRecordOperation<String>(VIEWTEMPLATEPATH) {
      @Override
      public String getOperation(TransientRecord instance,
                                 Viewpoint viewpoint)
      {
        return getViewTemplatePath();
      }
    });
    _table.addRecordOperation(new DirectCachingSimpleRecordOperation<String>(BLOCKID) {
      @Override
      public String calculate(TransientRecord targetRecord,
                              Viewpoint viewpoint)
      {
        return targetRecord.getTableName() + targetRecord.getId();
      }
    });
    _table.addRecordOperation(new SimpleRecordOperation<String>(URL) {
      @Override
      public String getOperation(TransientRecord targetRecord,
                                 Viewpoint viewpoint)
      {
        PersistentRecord gntml = targetRecord.comp(GNTMLINSTANCE);
        PersistentRecord node = gntml.comp(Gntml.NODE);
        return node.opt(Node.URL) + "#" + targetRecord.opt(BLOCKID);
      }
    });
  }

  /**
   * RecordOperation that provides a hidden link to state that there is information being supplied
   * about a given TableBlock during an updated procedure ($tableBlock.asFormElement.isActive).
   * 
   * @author alex
   */
  public class IsActive
    extends SimpleRecordOperation<Boolean>
    implements HtmlGuiSupplier
  {
    public IsActive()
    {
      super(TableBlock.ISACTIVE);
    }

    @Override
    public Boolean getOperation(TransientRecord targetRecord,
                                Viewpoint viewpoint)
    {
      return true;
    }

    @Override
    public void appendHtmlValue(TransientRecord tableBlock,
                                Viewpoint viewpoint,
                                String namePrefix,
                                Appendable buf,
                                Gettable context)
        throws IOException
    {
      buf.append(Boolean.TRUE.toString());
    }

    @Override
    public void appendHtmlWidget(TransientRecord tableBlock,
                                 Viewpoint viewpoint,
                                 String namePrefix,
                                 Appendable buf,
                                 Gettable context)
        throws IOException
    {
      HtmlUtilsTheme.hiddenInput(buf, Boolean.TRUE, namePrefix, TableBlock.ISACTIVE);
    }

    @Override
    public void appendFormElement(TransientRecord record,
                                  Viewpoint viewpoint,
                                  String namePrefix,
                                  Appendable buf,
                                  Gettable context)
        throws IOException
    {
      appendHtmlWidget(record, viewpoint, namePrefix, buf, context);
    }

    @Override
    public boolean hasHtmlWidget(TransientRecord record)
    {
      return true;
    }

    @Override
    public String getHtmlLabel()
    {
      return null;
    }

    @Override
    public boolean shouldReloadOnChange()
    {
      return false;
    }
  }

  /**
   * Initialise the fields that are required on the data table after gntmlInstance_id, blockName and
   * isReferenced. This will be invoked from init(db,pwd) after the initial fields are setup.
   * 
   * @see #init(Db, String)
   */
  protected abstract void init(Db db,
                               String pwd,
                               Table table)
      throws MirrorTableLockedException, DuplicateNamedException;

  /**
   * Scan through all of the instances in the associated table that reference this gntmlDocument and
   * set the value of isReferenced to false. As each block that actually exists in the document is
   * processed, it will set the isReferenced field back to true so that we will have an accurate
   * picture of which records are no longer required.
   */
  @Override
  public final void preParse(GntmlDocument gntmlDocument,
                             Transaction transaction)
      throws GntmlException
  {
    try {
      PersistentRecord gntmlInstance = gntmlDocument.getGntmlInstance();
      for (PersistentRecord instance : gntmlInstance.opt(getTable().getReferencesName(),
                                                         transaction)) {
        instance.setField(ISREFERENCED, Boolean.FALSE);
      }
    } catch (MirrorException mEx) {
      throw new GntmlException("Unable to preparse " + gntmlDocument + " with " + this, mEx);
    }
  }

  /**
   * Invoke and return buildTableBlock and if there is a BlockBuildException then return an
   * ErrorBlock in it's place. The ErrorBlock will have the GntmlBlockFactory set to this, and the
   * BlockName set to "Error".
   * 
   * @see ErrorBlock
   * @see #buildTableBlock(ListIterator, GntmlBlockStyle, GntmlDocument, int)
   */
  @Override
  public final GntmlBlock buildBlock(ListIterator<String> sourceLines,
                                     GntmlBlockStyle blockStyle,
                                     GntmlDocument gntmlDocument,
                                     int blockId)
      throws GntmlException
  {
    try {
      return buildTableBlock(sourceLines, blockStyle, gntmlDocument, blockId);
    } catch (BlockBuildException bbEx) {
      System.err.println(bbEx);
      return new ErrorBlock(bbEx.getLineList(),
                            blockStyle,
                            gntmlDocument,
                            this,
                            "Error",
                            blockId,
                            bbEx.getMessage());
    }
  }

  protected abstract TableBlock buildTableBlock(ListIterator<String> sourceLines,
                                                GntmlBlockStyle blockStyle,
                                                GntmlDocument gntmlDocument,
                                                int blockId)
      throws GntmlException;

  /**
   * Get the Query that resolves to a given named block within a document. This resolves against
   * gntmlInstance_id and blockName. In a well-behaved system then this Query should be of length 0
   * or 1.
   * 
   * @param documentId
   * @param blockName
   */
  protected final Query getInstanceQuery(Integer documentId,
                                         String blockName)
  {
    StringBuilder buf = new StringBuilder();
    buf.append(GNTMLINSTANCE_ID)
       .append('=')
       .append(documentId.intValue())
       .append(" and ")
       .append(BLOCKNAME)
       .append('=');
    SqlUtil.toSafeSqlString(buf, blockName);
    String filter = buf.toString();
    Query query = getTable().getQuery(filter, filter, null);
    return query;
  }

  /**
   * Either create or resolve the record for a given named block within a document. The record will
   * be locked by viewpoint if defined.
   * 
   * @return A PersistentRecord or WritableRecord depending on the state of viewpoint
   */
  protected final PersistentRecord getOrCreateInstanceRecord(GntmlDocument gntmlDocument,
                                                             String blockName,
                                                             GntmlBlockStyle gntmlBlockStyle,
                                                             Transaction transaction)
      throws MirrorInstantiationException, MirrorFieldException, MirrorTransactionTimeoutException
  {
    Integer documentId = gntmlDocument.getDocumentId();
    Query query = getInstanceQuery(documentId, blockName);
    PersistentRecord pInstance = RecordSources.getOptOnlyRecord(query, transaction);
    if (pInstance == null) {
      TransientRecord tInstance = getTable().createTransientRecord();
      tInstance.setField(GNTMLINSTANCE_ID, documentId);
      tInstance.setField(BLOCKNAME, blockName);
      initialiseInstance(gntmlDocument, tInstance, gntmlBlockStyle);
      pInstance = tInstance.makePersistent(null);
    }
    if (transaction == null) {
      return pInstance;
    } else {
      WritableRecord wBookList = transaction.edit(pInstance);
      return wBookList;
    }
  }

  protected void initialiseInstance(GntmlDocument gntmlDocument,
                                    TransientRecord instance,
                                    GntmlBlockStyle gntmlBlockStyle)
      throws MirrorFieldException
  {
  }

  public String getCaptionCssClass()
  {
    return __captionCssClass;
  }

  public String getBlockCssClass()
  {
    return __blockCssClass;
  }

  public void addNodeLinkField(final Table table,
                               final FieldKey<Integer> fieldName,
                               final String label,
                               final RecordOperationKey<RecordSource> referencesName,
                               final String pwd,
                               final NodeFilter acceptableTargets,
                               final String questionTarget)
      throws MirrorTableLockedException
  {
    final String browseView = "browse" + getName() + fieldName;
    RecordStringFactory browseParams = new RecordStringFactory() {
      @Override
      public String getValue(TransientRecord record)
      {
        TransientRecord.assertIsTable(record, table);
        StringBuilder buf = new StringBuilder();
        buf.append("view=").append(browseView).append("&id=").append(record.getId());
        return buf.toString();
      }
    };
    table.addField(new NodeLinkField(fieldName,
                                     label,
                                     null,
                                     LinkField.getLinkName(fieldName),
                                     referencesName,
                                     null,
                                     pwd,
                                     Dependencies.LINK_ENFORCED | Dependencies.BIT_OPTIONAL,
                                     acceptableTargets,
                                     browseParams));
    AskNodeQuestion askNodeQuestion =
        new AskNodeQuestion(browseView,
                            getNodeManager(),
                            new RedirectionSuccessOperation(getNodeManager(),
                                                            CurrentUrlFactory.getInstance()),
                            Node.EDITACL,
                            "You don't have permission to select the page.") {
          @Override
          protected NodeQuestion createNodeQuestion(NodeRequest nodeRequest,
                                                    final PersistentRecord node,
                                                    Context context)
              throws MirrorNoSuchRecordException
          {
            final String id = nodeRequest.getString("id");
            PersistentRecord instance = getTable().getRecord(id);
            String question =
                String.format("Select %s named \"%s\" in \"%s\" found on \"%s\"",
                              questionTarget,
                              instance.opt(BLOCKNAME),
                              instance.comp(GNTMLINSTANCE)
                                      .comp(Gntml.REGIONSTYLE)
                                      .opt(RegionStyle.NAME),
                              node.opt(Node.TITLE));
            final String encodedNamespace = HtmlUtilsTheme.urlEncode(instance.opt(NAMEPREFIX));
            Transaction transaction = getTransaction(nodeRequest, node);
            NodeQuestion nodeQuestion = new MonoNodeQuestion(getName() + instance.getId(),
                                                             question,
                                                             "GNTML Image Link Target",
                                                             node,
                                                             null,
                                                             transaction) {
              @Override
              public String getAnswerUrl()
              {
                StringBuilder url = new StringBuilder();
                url.append(node.opt(Node.URL));
                url.append("?view=update");
                url.append('&')
                   .append(encodedNamespace)
                   .append(fieldName)
                   .append("=")
                   .append(getSelectedNodeId())
                   .append('&')
                   .append(encodedNamespace)
                   .append(TableBlock.ISACTIVE)
                   .append("=true");
                return url.toString();
              }
            };
            return nodeQuestion;
          }
        };
    getNodeManager().getNodeRequestSegmentManager().register(Transaction.TRANSACTION_UPDATE,
                                                             askNodeQuestion);
  }

}
