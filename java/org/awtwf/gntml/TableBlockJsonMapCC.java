package org.awtwf.gntml;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.JSONObjectField;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;

public class TableBlockJsonMapCC<T>
  extends TableBlockJsonCC<T>
{
  private final Map<String, T> __values;

  public TableBlockJsonMapCC(TableBlockFactory tableBlockFactory,
                             JSONObjectField jsonObjectField,
                             String label,
                             RecordOperationKey<T> jsonKey,
                             Map<String, T> values,
                             Supplier<T> defaultValue)
  {
    super(tableBlockFactory,
          jsonObjectField,
          label,
          jsonKey,
          defaultValue);
    __values = Preconditions.checkNotNull(values, "values");
  }

  public TableBlockJsonMapCC(TableBlockFactory tableBlockFactory,
                             JSONObjectField jsonObjectField,
                             String label,
                             RecordOperationKey<T> jsonKey,
                             Map<String, T> values,
                             String defaultValue)
  {
    this(tableBlockFactory,
         jsonObjectField,
         label,
         jsonKey,
         values,
         Suppliers.ofInstance(Preconditions.checkNotNull(values.get(defaultValue),
                                                         "Unable to resolve default value of %s in %s",
                                                         defaultValue,
                                                         values)));
  }
  public Map<String, T> getValues()
  {
    return Collections.unmodifiableMap(__values);
  }

  @Override
  protected T getValue(TransientRecord instance)
  {
    String jsonValue = asJsonObject(instance).optString(getJSONKey().getKeyName());
    if (jsonValue != null) {
      T value = __values.get(jsonValue);
      if (value != null) {
        return value;
      }
    }
    return getDefaultValue();
  }

  @Override
  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Gettable context)
      throws IOException
  {
    T value = getValue(record);
    HtmlUtils.openSelect(buf, false, false, false, namePrefix, getJSONKey());
    for (Map.Entry<String, T> entry : __values.entrySet()) {
      HtmlUtils.option(buf,
                       entry.getKey(),
                       entry.getValue(),
                       Objects.equal(value, entry.getValue()));
    }
    HtmlUtils.closeSelect(buf, null);
  }

}
