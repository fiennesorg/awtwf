package org.awtwf.gntml;

import java.io.IOException;
import java.util.ListIterator;
import java.util.Map;
import java.util.Stack;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;
import org.cord.util.Settable;
import org.cord.util.SettableException;

public class ListBlockFactory
  extends EnglishNamedImpl
  implements GntmlBlockFactory
{
  public final static String UL_STRING = "-";

  public final static String OL_STRING = "#";

  public final static String NAME = "List";

  private final static String[] LIST_HTML = { "ul", "ol" };

  public ListBlockFactory()
  {
    super(NAME);
  }

  @Override
  public boolean acceptsSource(GntmlDocument gntmlDocument,
                               String firstLine)
  {
    return GntmlListLine.getListType(firstLine) != GntmlListLine.LIST_NOT;
  }

  @Override
  public GntmlBlock buildBlock(ListIterator<String> sourceLines,
                               GntmlBlockStyle blockStyle,
                               GntmlDocument gntmlDocument,
                               int blockId)
  {
    return new ListBlock(sourceLines, blockStyle, gntmlDocument, blockId);
  }

  private int currentListType(Stack<Integer> lists)
  {
    return lists.peek().intValue();
  }

  private void openList(GntmlListLine listLine,
                        Stack<Integer> lists,
                        Appendable out)
      throws IOException
  {
    out.append("\n<").append(LIST_HTML[listLine.getListType()]).append("><li>");
    lists.push(listLine.getListType());
  }

  private void closeList(Stack<Integer> lists,
                         Appendable out)
      throws IOException
  {
    out.append("</li></").append(LIST_HTML[currentListType(lists)]).append(">");
    lists.pop();
  }

  private void closeAllLists(Stack<Integer> lists,
                             Appendable out)
      throws IOException
  {
    if (lists.size() > 0) {
      while (lists.size() > 0) {
        closeList(lists, out);
      }
      out.append('\n');
    }
  }

  private void processTrailingLines(ListIterator<GntmlLine> gntmlListLines,
                                    Appendable out,
                                    Gettable params)
      throws GntmlException, IOException
  {
    if (gntmlListLines.hasNext()) {
      while (gntmlListLines.hasNext()) {
        GntmlListLine listLine = (GntmlListLine) gntmlListLines.next();
        if (listLine.getListType() == GntmlListLine.LIST_NOT) {
          out.append("<br />\n");
          listLine.parse(out, params, true);
        } else {
          gntmlListLines.previous();
          break;
        }
      }
    }
  }

  private void processLine(ListIterator<GntmlLine> gntmlListLines,
                           Stack<Integer> lists,
                           Appendable out,
                           Gettable params)
      throws GntmlException, IOException
  {
    if (gntmlListLines.hasNext()) {
      GntmlListLine listLine = (GntmlListLine) gntmlListLines.next();
      switch (NumberUtil.compare(lists.size(), listLine.getListDepth())) {
        case -1:
          openList(listLine, lists, out);
          listLine.parse(out, params, true);
          processTrailingLines(gntmlListLines, out, params);
          processLine(gntmlListLines, lists, out, params);
          break;
        case 0:
          if (currentListType(lists) != listLine.getListType()) {
            closeList(lists, out);
            openList(listLine, lists, out);
          } else {
            out.append("</li>\n<li>");
          }
          listLine.parse(out, params, true);
          processTrailingLines(gntmlListLines, out, params);
          processLine(gntmlListLines, lists, out, params);
          break;
        case 1:
          closeList(lists, out);
          gntmlListLines.previous();
          processLine(gntmlListLines, lists, out, params);
          break;
      }
    }
  }

  /**
   * Do nothing
   */
  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
  {
  }

  /**
   * Do nothing
   */
  @Override
  public void preParse(GntmlDocument gntmlDocument,
                       Transaction transaction)
  {
  }

  public class ListBlock
    extends GntmlBlock
  {
    protected ListBlock(ListIterator<String> sourceLines,
                        GntmlBlockStyle blockStyle,
                        GntmlDocument gntmlDocument,
                        int blockId)
    {
      super(NAME,
            sourceLines,
            blockStyle,
            gntmlDocument,
            ListBlockFactory.this,
            blockId);
    }

    @Override
    public Appendable parse(Appendable out,
                            Gettable documentParams,
                            Gettable blockParams,
                            Map<Object, Object> outputs)
        throws GntmlException, IOException
    {
      openDivs(out);
      Stack<Integer> lists = new Stack<Integer>();
      ListIterator<GntmlLine> gntmlListLines = getLineList();
      processLine(gntmlListLines, lists, out, blockParams);
      closeAllLists(lists, out);
      closeDivs(out);
      return out;
    }

    /**
     * Do nothing
     */
    @Override
    public void copyGntmlBlock(Settable settable,
                               PersistentRecord fromNode,
                               PersistentRecord fromRegionStyle,
                               PersistentRecord toRegionStyle,
                               PersistentRecord fromGntmlInstance,
                               GntmlDocument gntmlDocument)
        throws SettableException
    {
    }

    @Override
    protected GntmlLine createGntmlLine(String source)
    {
      return new GntmlListLine(source, this);
    }
  }
}
