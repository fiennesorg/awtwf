package org.awtwf.gntml;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.cord.mirror.PersistentRecord;
import org.cord.node.Acl;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequestSegmentManager;

import com.google.common.base.Preconditions;

public class SourcePatternMgr
{
  private final NodeManager __nodeMgr;

  private final Integer __aclId;

  private final Map<String, SourcePatternDefinition> __definitions;

  private SourcePatternMgr(NodeManager nodeMgr,
                           Integer aclId,
                           Map<String, SourcePatternDefinition> definitions)
  {
    __nodeMgr = nodeMgr;
    __aclId = aclId;
    __definitions = Collections.unmodifiableMap(definitions);
    for (SourcePatternDefinition definition : __definitions.values()) {
      definition.setMgr(this);
    }
    NodeRequestSegmentManager nrsm = getNodeMgr().getNodeRequestSegmentManager();
    nrsm.register(new SourcePatternView(this, getAclId()));
  }

  public final NodeManager getNodeMgr()
  {
    return __nodeMgr;
  }

  public final Integer getAclId()
  {
    return __aclId;
  }

  public final PersistentRecord getAcl()
  {
    return getNodeMgr().getDb().getTable(Acl.TABLENAME).getCompRecord(getAclId());
  }

  public final Map<String, SourcePatternDefinition> getDefinitions()
  {
    return __definitions;
  }

  public static Builder getBuilder(NodeManager nodeMgr,
                                   Integer aclId)
  {
    return new Builder(Preconditions.checkNotNull(nodeMgr, "nodeMgr"),
                       aclId,
                       new HashMap<String, SourcePatternDefinition>());
  }

  private static class Builder
  {
    private final NodeManager __nodeMgr;

    private final Integer __aclId;

    private final Map<String, SourcePatternDefinition> __definitions;

    private Builder(NodeManager nodeMgr,
                    Integer aclId,
                    Map<String, SourcePatternDefinition> definitions)
    {
      __nodeMgr = nodeMgr;
      __aclId = aclId;
      __definitions = definitions;
    }

    public SourcePatternMgr build()
    {
      return new SourcePatternMgr(__nodeMgr, __aclId, __definitions);
    }

    public Builder add(String name,
                       String englishName,
                       String rlike)
    {
      __definitions.put(name, new SourcePatternDefinition(name, englishName, rlike));
      return this;
    }
  }
}
