package org.awtwf.gntml;

import java.io.IOException;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.TransientRecord;
import org.cord.util.HtmlUtilsTheme;

import com.google.common.base.Preconditions;

public class DefaultGntmlBlockStyle
  extends AbstractGntmlBlockStyle
{
  public static DefaultGntmlBlockStyle getInstance(TransientRecord gntmlStyle)
  {
    return gntmlStyle.opt(GntmlStyle.DEFAULTGNTMLBLOCKSTYLE);
  }

  private final GntmlMgr __gntmlMgr;
  private final Integer __gntmlStyleId;
  private final String __cssClass;

  protected DefaultGntmlBlockStyle(GntmlMgr gntmlMgr,
                                   TransientRecord gntmlStyle)
  {
    super("DEFAULT");
    __gntmlMgr = Preconditions.checkNotNull(gntmlMgr, "gntmlMgr");
    __gntmlStyleId = gntmlStyle.getId();
    __cssClass = "g_def g_def_" + __gntmlStyleId;
    set(WIDTH, gntmlStyle.opt(GntmlStyle.GRIDWIDTH));
  }

  public final GntmlMgr getGntmlMgr()
  {
    return __gntmlMgr;
  }

  public final PersistentRecord getGntmlStyle()
  {
    return getGntmlMgr().getGntmlStyle().getTable().getCompRecord(__gntmlStyleId);
  }

  @Override
  public boolean accepts(TransientRecord gntmlStyle)
  {
    return __gntmlStyleId.equals(gntmlStyle.getId());
  }

  public String getCssClass(GntmlBlock gntmlBlock)
  {
    return GntmlMgr.addCssClass(DefinedGntmlBlockStyle.CSS_DEFAULTMARGINS, gntmlBlock.getCssClass());
    // String cssClass = null;
    // cssClass = GntmlMgr.addCssClass(cssClass, gntmlBlock.getCssClass());
    // PersistentRecord gntmlStyle = getGntmlStyle();
    // cssClass =
    // GntmlMgr.addCssClass(cssClass, getGntmlMgr().getGntmlStyle().getCssMarginLeft(gntmlStyle));
    // cssClass =
    // GntmlMgr.addCssClass(cssClass, getGntmlMgr().getGntmlStyle().getCssMarginRight(gntmlStyle));
    // return cssClass;
  }

  @Override
  public void appendHeader(GntmlBlock gntmlBlock,
                           Appendable out)
      throws IOException
  {
    String cssClass = getCssClass(gntmlBlock);
    String cssId = gntmlBlock.getCssId();
    if (cssClass == null & cssId == null) {
      return;
    }
    HtmlUtilsTheme.openDivOfIdClass(out, cssId, GntmlMgr.addCssClass(__cssClass, cssClass));
    GntmlMgr.appendOpenInnerDiv(out);
  }

  @Override
  public void appendFooter(GntmlBlock gntmlBlock,
                           Appendable out)
      throws IOException
  {
    String cssClass = getCssClass(gntmlBlock);
    String cssId = gntmlBlock.getCssId();
    if (cssClass == null & cssId == null) {
      return;
    }
    HtmlUtilsTheme.closeDivs(out, 2);
  }

  /**
   * @return false
   */
  @Override
  public boolean outputsWebmacro()
  {
    return false;
  }
}
