package org.awtwf.gntml;

import java.io.IOException;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;

public class CodeBlockFactory
  extends EnglishNamedImpl
  implements GntmlBlockFactory
{
  public final static String PREBLOCK_STARTTAG = "<code>";

  public final static String NAME = "Pre";

  public CodeBlockFactory()
  {
    super(NAME);
  }

  @Override
  public GntmlBlock buildBlock(ListIterator<String> sourceLines,
                               GntmlBlockStyle blockStyle,
                               GntmlDocument gntmlDocument,
                               int blockId)
  {
    return new CodeBlock(sourceLines, blockStyle, gntmlDocument, blockId);
  }

  @Override
  public boolean acceptsSource(GntmlDocument gntmlDocument,
                               String firstSourceLine)
  {
    return PREBLOCK_STARTTAG.equals(firstSourceLine.toLowerCase().trim());
  }

  /**
   * Do nothing
   */
  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
  {
  }

  /**
   * Do nothing
   */
  @Override
  public void preParse(GntmlDocument gntmlDocument,
                       Transaction transaction)
  {
  }

  public class CodeBlock
    extends GntmlBlock
  {
    protected CodeBlock(ListIterator<String> sourceLines,
                        GntmlBlockStyle blockStyle,
                        GntmlDocument gntmlDocument,
                        int blockId)
    {
      super(NAME,
            sourceLines,
            blockStyle,
            gntmlDocument,
            CodeBlockFactory.this,
            blockId);
    }

    @Override
    public Appendable parse(Appendable xhtml,
                            Gettable documentParams,
                            Gettable blockParams,
                            Map<Object, Object> outputs)
        throws GntmlException, IOException
    {
      xhtml.append("<pre class=\"code_block\">");
      Iterator<GntmlLine> lineList = getLineList();
      if (lineList.hasNext()) {
        lineList.next();
      }
      while (lineList.hasNext()) {
        GntmlLine line = lineList.next();
        xhtml.append(line.getSource()).append('\n');
      }
      xhtml.append("</pre>\n");
      return xhtml;
    }

    /**
     * Do nothing as code blocks are implicitly copyable
     */
    @Override
    public void copyGntmlBlock(Settable settable,
                               PersistentRecord fromNode,
                               PersistentRecord fromRegionStyle,
                               PersistentRecord toRegionStyle,
                               PersistentRecord fromGntmlInstance,
                               GntmlDocument gntmlDocument)
        throws SettableException
    {
    }
  }
}
