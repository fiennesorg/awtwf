package org.awtwf.gntml;

import org.cord.mirror.AbstractTableWrapper;
import org.cord.mirror.Db;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.field.IntegerField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.operation.ManyToManyReference;
import org.cord.node.img.ImgOriginal;

public class CyclingImage_ImgOriginal
  extends AbstractTableWrapper
{
  public static final String TABLENAME = "GntmlCyclingImage_ImgOriginal";

  public static final FieldKey<Integer> GNTMLCYCLINGIMAGE_ID =
      LinkField.getLinkFieldName(CyclingImage.TABLENAME);
  public static final RecordOperationKey<PersistentRecord> GNTMLCYCLINGIMAGE =
      LinkField.getLinkName(GNTMLCYCLINGIMAGE_ID);
  public static final RecordOperationKey<RecordSource> GNTMLCYCLINGIMAGE_IMGORIGINALSLINKS =
      RecordOperationKey.create("imgOriginalsLinks", RecordSource.class);

  public static final FieldKey<Integer> IMGORIGINAL_ID =
      LinkField.getLinkFieldName(ImgOriginal.TABLENAME);
  public static final RecordOperationKey<PersistentRecord> IMGORIGINAL =
      LinkField.getLinkName(IMGORIGINAL_ID);
  public static final RecordOperationKey<RecordSource> IMGORIGINAL__GNTMLCYCLINGIMAGESLINKS =
      RecordOperationKey.create("gntmlCyclingImagesLinks", RecordSource.class);

  public static final FieldKey<Boolean> ISDEFINED = ManyToManyReference.ISDEFINED.copy();

  public static final FieldKey<Integer> RANK = FieldKey.create("rank", Integer.class);

  public CyclingImage_ImgOriginal()
  {
    super(TABLENAME);
  }

  @Override
  protected void initTable(Db db,
                           String pwd,
                           Table table)
      throws MirrorTableLockedException
  {
    ManyToManyReference.registerComponents(db,
                                           pwd,
                                           "Images",
                                           TABLENAME,
                                           TABLENAME + "." + RANK,
                                           CyclingImage.TABLENAME,
                                           null,
                                           null,
                                           true,
                                           ImgOriginal.TABLENAME,
                                           null,
                                           null,
                                           false,
                                           false);
    table.addField(new IntegerField(RANK, "Rank"));
    table.setDefaultOrdering(TABLENAME + "." + RANK);
  }

}
