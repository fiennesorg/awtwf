package org.awtwf.gntml;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.cord.mirror.TransientRecord;
import org.cord.util.DebugConstants;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class GntmlBlockStyleProvider
{
  public static final String CONF_PLUSBLOCKSTYLE = Gntml.CONF_GNTML + ".plusBlockStyle";
  public static final String CONF_BLOCKSTYLE = Gntml.CONF_GNTML + ".blockStyle";

  private final Map<String, DefinedGntmlBlockStyle> __styles = Maps.newHashMap();
  private final Map<String, DefinedGntmlBlockStyle> __plusStyles = Maps.newHashMap();
  private final GntmlMgr __gntmlMgr;

  private final ImmutableList<String> __compPlusFields;
  private final ImmutableList<String> __optPlusFields;
  private final ImmutableList<String> __compFields;
  private final ImmutableList<String> __optFields;

  public GntmlBlockStyleProvider(GntmlMgr gntmlMgr)
  {
    __gntmlMgr = Preconditions.checkNotNull(gntmlMgr, "gntmlMgr");
    Gettable gettable = gntmlMgr.getNodeMgr().getGettable();
    Splitter splitter = Splitter.on(",").omitEmptyStrings().trimResults();
    DebugConstants.variable(CONF_BLOCKSTYLE + ".fields.comp",
                            gettable.get(CONF_BLOCKSTYLE + ".fields.comp"));
    __compFields =
        ImmutableList.copyOf(splitter.split(gettable.getString(CONF_BLOCKSTYLE + ".fields.comp")));
    __optFields =
        ImmutableList.copyOf(splitter.split(gettable.getString(CONF_BLOCKSTYLE + ".fields.opt")));
    __compPlusFields =
        ImmutableList.copyOf(splitter.split(gettable.getString(CONF_PLUSBLOCKSTYLE + ".fields.comp")));
    __optPlusFields =
        ImmutableList.copyOf(splitter.split(gettable.getString(CONF_PLUSBLOCKSTYLE + ".fields.opt")));
    addStyles(gntmlMgr.getNodeMgr().getGettable());
  }

  public GntmlMgr getGntmlMgr()
  {
    return __gntmlMgr;
  }

  public void addStyles(Map<String, DefinedGntmlBlockStyle> styles,
                        Gettable gettable,
                        String namespace,
                        String keys,
                        Iterable<String> compFields,
                        Iterable<String> optFields)
  {
    for (DefinedGntmlBlockStyle style : GettableUtils.getValues(gettable,
                                                                namespace,
                                                                keys,
                                                                DefinedGntmlBlockStyle.getSupplier(getGntmlMgr(),
                                                                                                   compFields,
                                                                                                   optFields))) {
      if (Boolean.FALSE.equals(GntmlBlockStyles.optBoolean(style, GntmlBlockStyle.ISACTIVE))) {
        styles.remove(style.getName());
        DebugConstants.DEBUG_OUT.println(String.format("%s.%s removed", namespace, style.getName()));
      } else {
        styles.put(style.getName(), style);
        DebugConstants.DEBUG_OUT.println(String.format("%s.%s initialised",
                                                       namespace,
                                                       style.getName()));
      }
    }
  }

  public void addStyles(Gettable gettable)
  {
    addStyles(__styles,
              gettable,
              GntmlBlockStyleProvider.CONF_BLOCKSTYLE,
              "node.keys",
              __compFields,
              __optFields);
    addStyles(__styles,
              gettable,
              GntmlBlockStyleProvider.CONF_BLOCKSTYLE,
              "app.keys",
              __compFields,
              __optFields);
    addStyles(__plusStyles,
              gettable,
              GntmlBlockStyleProvider.CONF_PLUSBLOCKSTYLE,
              "node.keys",
              __compPlusFields,
              __optPlusFields);
    addStyles(__plusStyles,
              gettable,
              GntmlBlockStyleProvider.CONF_PLUSBLOCKSTYLE,
              "app.keys",
              __compPlusFields,
              __optPlusFields);
  }

  public DefinedGntmlBlockStyle getBlockStyle(Map<String, DefinedGntmlBlockStyle> styles,
                                              String styleType,
                                              TransientRecord gntmlStyle,
                                              String name)
      throws GntmlBlockStyleException
  {
    DefinedGntmlBlockStyle style = styles.get(name);
    if (style == null) {
      throw new GntmlBlockStyleException(String.format("Unknown %s named %s", styleType, name),
                                         name);
    }
    if (!style.accepts(gntmlStyle)) {
      throw new GntmlBlockStyleException(String.format("%s is not a supported %s in %s",
                                                       name,
                                                       styleType,
                                                       gntmlStyle.opt(GntmlStyle.NAME)), name);
    }
    return style;
  }

  private final Splitter __splitter = Splitter.on('+').omitEmptyStrings().trimResults();

  public GntmlBlockStyle getBlockStyle(TransientRecord gntmlStyle,
                                       String name)
      throws GntmlBlockStyleException
  {
    if (Strings.isNullOrEmpty(name)) {
      return DefaultGntmlBlockStyle.getInstance(gntmlStyle);
    }
    int index = name.indexOf('+');
    if (index == -1) {
      try {
        return getBlockStyle(__styles, "block style", gntmlStyle, name);
      } catch (GntmlBlockStyleException e) {
        if (__plusStyles.containsKey(name)) {
          throw new GntmlBlockStyleException(String.format("{%s} is an extended block style "
                                                           + "- did you mean {+%s}?", name, name),
                                             name);
        }
        throw e;
      }
    }
    Iterator<String> i = __splitter.split(name).iterator();
    DefinedGntmlBlockStyle primaryBlock = null;
    if (name.charAt(0) != '+') {
      primaryBlock = getBlockStyle(__styles, "block style", gntmlStyle, i.next());
    }
    List<DefinedGntmlBlockStyle> plusBlocks = Lists.newArrayList();
    while (i.hasNext()) {
      plusBlocks.add(getBlockStyle(__plusStyles, "extended block style", gntmlStyle, i.next()));
    }
    return CompoundGntmlBlockStyle.create(getGntmlMgr(), gntmlStyle, name, primaryBlock, plusBlocks);
  }
}
