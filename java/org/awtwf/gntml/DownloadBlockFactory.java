package org.awtwf.gntml;

import java.io.File;
import java.math.BigDecimal;
import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.node.NodeManager;
import org.cord.node.cc.BinaryAssetContentCompiler;
import org.cord.node.cc.BinaryAssetStyle;
import org.cord.node.cc.TableContentCompiler;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;

public class DownloadBlockFactory
  extends TableBlockFactory
{
  public final static String NAME = "GntmlDownload";

  public final static String ENGLISHNAME = "Downloadable file";

  public final static String NAMEPREFIX_VALUE = "dld";

  public final static String TABLENAME = "GntmlDownloadInstance";

  public final static String VIEWTEMPLATEPATH = "Gntml/download.view.wm.html";

  public final static String EDITTEMPLATEPATH = "Gntml/download.edit.wm.html";

  public final static String BLOCKCSSCLASS = "gntml_download";

  public final static String CAPTIONCSSCLASS = "gntml_download_caption";

  public final static FieldKey<Integer> STYLE_ID = TableContentCompiler.STYLE_ID.copy();

  public final static RecordOperationKey<PersistentRecord> STYLE = TableContentCompiler.STYLE;

  public final static FieldKey<Boolean> ISUPLOADED = BinaryAssetContentCompiler.ISUPLOADED;

  public final static FieldKey<String> PATH = BinaryAssetContentCompiler.PATH;

  public final static FieldKey<String> FOLDER = BinaryAssetContentCompiler.FOLDER;

  public final static FieldKey<String> FILENAME = BinaryAssetContentCompiler.FILENAME;

  public final static FieldKey<Integer> FORMAT_ID = BinaryAssetContentCompiler.FORMAT_ID;

  public final static RecordOperationKey<PersistentRecord> FORMAT =
      BinaryAssetContentCompiler.FORMAT;

  public final static FieldKey<Integer> SIZEKB = BinaryAssetContentCompiler.SIZEKB;

  public final static FieldKey<String> TITLE = BinaryAssetContentCompiler.TITLE;

  public final static FieldKey<String> DESCRIPTION = BinaryAssetContentCompiler.DESCRIPTION;

  public final static RecordOperationKey<File> JAVAFILE = BinaryAssetContentCompiler.JAVAFILE;

  public final static RecordOperationKey<String> FORMATTEDSIZE =
      RecordOperationKey.create("formattedSize", String.class);

  public DownloadBlockFactory(NodeManager nodeManager)
  {
    super(nodeManager,
          NAME,
          ENGLISHNAME,
          "|file:",
          "|",
          TABLENAME,
          VIEWTEMPLATEPATH,
          BLOCKCSSCLASS,
          CAPTIONCSSCLASS,
          EDITTEMPLATEPATH,
          null);
  }

  @Override
  protected void init(final Db db,
                      final String transactionPassword,
                      final Table gntmlDownloadTable)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    gntmlDownloadTable.addField(new LinkField(STYLE_ID,
                                              "Download style",
                                              null,
                                              STYLE,
                                              BinaryAssetStyle.TABLENAME,
                                              null,
                                              null,
                                              transactionPassword,
                                              Dependencies.LINK_ENFORCED));
    BinaryAssetContentCompiler.initialiseInstanceTable(getNodeManager(),
                                                       gntmlDownloadTable,
                                                       transactionPassword,
                                                       null);
    gntmlDownloadTable.addRecordOperation(new SimpleRecordOperation<String>(FORMATTEDSIZE) {
      private final BigDecimal __kilo = new BigDecimal("1024");

      @Override
      public String getOperation(TransientRecord targetRecord,
                                 Viewpoint viewpoint)
      {
        int sizeKb = targetRecord.comp(SIZEKB);
        if (sizeKb < 1024) {
          return sizeKb + "Kb";
        }
        BigDecimal m = new BigDecimal(sizeKb).divide(__kilo, 2, BigDecimal.ROUND_HALF_UP);
        return m + "Mb";
      }
    });
  }

  @Override
  protected void initialiseInstance(GntmlDocument gntmlDocument,
                                    TransientRecord instance,
                                    GntmlBlockStyle gntmlBlockStyle)
      throws MirrorFieldException
  {
    instance.setField(STYLE_ID,
                      gntmlDocument.getGntmlStyle().opt(GntmlStyle.DEFAULTDOWNLOADSTYLE_ID));
  }

  @Override
  protected TableBlock buildTableBlock(ListIterator<String> sourceLines,
                                       GntmlBlockStyle blockStyle,
                                       GntmlDocument gntmlDocument,
                                       int blockId)
      throws GntmlException
  {
    return new DownloadBlock(sourceLines, blockStyle, gntmlDocument, blockId);
  }

  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
  {
  }

  /**
   * @return true
   */
  public boolean outputsWebmacro()
  {
    return true;
  }

  public class DownloadBlock
    extends TableBlock
  {
    protected DownloadBlock(ListIterator<String> sourceLines,
                            GntmlBlockStyle blockStyle,
                            GntmlDocument gntmlDocument,
                            int blockId) throws GntmlException
    {
      super(NAME,
            sourceLines,
            blockStyle,
            gntmlDocument,
            DownloadBlockFactory.this,
            blockId);
    }

    @Override
    public void copyGntmlBlock(Settable settable,
                               PersistentRecord fromNode,
                               PersistentRecord fromRegionStyle,
                               PersistentRecord toRegionStyle,
                               PersistentRecord fromGntmlInstance,
                               GntmlDocument gntmlDocument,
                               PersistentRecord instance)
        throws SettableException
    {
      settable.set(TITLE, instance.opt(TITLE));
      settable.set(DESCRIPTION, instance.opt(DESCRIPTION));
      BinaryAssetContentCompiler.copy(settable, instance);
    }

    @Override
    protected void updateInstance(WritableRecord instance,
                                  Gettable params,
                                  Map<Object, Object> outputs)
        throws GntmlException, MirrorException
    {
      BinaryAssetContentCompiler.updateInstance(getNodeManager(),
                                                instance,
                                                instance.comp(STYLE),
                                                null,
                                                getTransaction(),
                                                params,
                                                false);
    }
  }

  @Override
  public void shutdown()
  {
  }
}
