package org.awtwf.gntml;

import org.cord.node.NodeManager;
import org.cord.util.EnglishNamedImpl;

/**
 * Abstract base class that can be utilised for when a GntmlBlockFactory wants to have visibility of
 * the node framework that the GntmlDocument is being utilised within.
 * 
 * @author alex
 */
public abstract class NodeBlockFactory
  extends EnglishNamedImpl
  implements GntmlBlockFactory
{
  private final NodeManager __nodeManager;

  public NodeBlockFactory(NodeManager nodeManager,
                          String name,
                          String englishName)
  {
    super(name,
          englishName);
    __nodeManager = nodeManager;
  }

  protected final NodeManager getNodeManager()
  {
    return __nodeManager;
  }
}
