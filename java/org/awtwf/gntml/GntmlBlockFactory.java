package org.awtwf.gntml;

import java.util.ListIterator;

import org.cord.mirror.Transaction;
import org.cord.util.EnglishNamed;

/**
 * The GntmlBlockFactory describes Objects that are registered with a GntmlParser and are able to
 * generate GntmlBlocks inside a GntmlDocument produced by the parser.
 * 
 * @author alex
 */
public interface GntmlBlockFactory
  extends EnglishNamed
{
  public GntmlBlock buildBlock(ListIterator<String> sourceLines,
                               GntmlBlockStyle gntmlBlockStyle,
                               GntmlDocument gntmlDocument,
                               int blockId)
      throws GntmlException;

  public boolean acceptsSource(GntmlDocument gntmlDocument,
                               String firstSourceLine);

  /**
   * Perform any work necessary to tidy up instances associated with the GntmlDocument after it has
   * been parsed.
   */
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
      throws GntmlException;

  /**
   * Perform any work necessary to prepare any instances associated with the given GntmlDocument
   * before it is parsed.
   */
  public void preParse(GntmlDocument gntmlDocument,
                       Transaction transaction)
      throws GntmlException;
}
