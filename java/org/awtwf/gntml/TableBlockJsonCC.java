package org.awtwf.gntml;

import java.io.IOException;

import org.cord.mirror.HtmlGuiSupplier;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.JSONObjectField;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;

public abstract class TableBlockJsonCC<T>
  extends TableBlockCC<T>
{
  private final JSONObjectField __jsonObjectField;
  private final String __label;
  private final RecordOperationKey<T> __jsonKey;

  public TableBlockJsonCC(TableBlockFactory tableBlockFactory,
                          JSONObjectField jsonObjectField,
                          String label,
                          RecordOperationKey<T> jsonKey,
                          Supplier<T> defaultValue)
  {
    super(jsonObjectField.getAsJSONName() + "_" + jsonKey,
          tableBlockFactory,
          defaultValue);
    __jsonObjectField = Preconditions.checkNotNull(jsonObjectField, "jsonObjectField");
    __label = Preconditions.checkNotNull(label, "label");
    __jsonKey = Preconditions.checkNotNull(jsonKey, "jsonKey");
  }

  public final JSONObjectField getJSONObjectField()
  {
    return __jsonObjectField;
  }

  public final RecordOperationKey<T> getJSONKey()
  {
    return __jsonKey;
  }

  protected final JSONObject asJsonObject(TransientRecord instance)
  {
    return instance.opt(getJSONObjectField().getAsJSONName());
  }

  /**
   * Get the value for this item as encoded in the params.
   * 
   * @return the result of calling {@link Gettable#get(Object)} with {@link #getJSONKey()} as the
   *         key. subclasses should override this if necessary.
   */
  protected Object getJsonValue(Gettable params)
  {
    return params.get(getJSONKey());
  }

  /**
   * Does the given Gettable actually define a value of relevance for this item? If false, then the
   * default implementation of {@link #updateJsonObject(TransientRecord, Gettable, JSONObject)} will
   * do nothing.
   * 
   * @return true if params contains a key of {@link #getJSONKey()} - subclasses should override
   *         this if there is a more accuate picture required.
   */
  protected boolean hasJsonValue(Gettable params)
  {
    return params.containsKey(getJSONKey());
  }

  protected boolean updateJsonObject(TransientRecord instance,
                                     Gettable params,
                                     JSONObject jObj)
      throws JSONException
  {
    if (hasJsonValue(params)) {
      Object value = getJsonValue(params);
      if (value == null) {
        if (params.containsKey(getJSONKey())) {
          jObj.put(getJSONKey().getKeyName(), JSONObject.NULL);
          return true;
        }
      } else {
        jObj.put(getJSONKey().getKeyName(), value);
        return true;
      }
    }
    return false;
  }

  @Override
  protected final boolean updateTransientInstance(TransientRecord instance,
                                                  Gettable params)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException
  {
    JSONObject jObj = asJsonObject(instance);
    try {
      if (updateJsonObject(instance, params, jObj)) {
        instance.setField(getJSONObjectField().getName(), jObj.toString());
      }
    } catch (JSONException e) {
      throw new MirrorFieldException(String.format("JSON error setting %s.%s",
                                                   getJSONObjectField().getName(),
                                                   getJSONKey()),
                                     e,
                                     instance,
                                     getJSONObjectField().getName());
    }
    return instance.getIsUpdated();
  }

  // public String getEditTemplate(PersistentRecord instance,
  // String webmacroInstanceName,
  // PersistentRecord user)
  // {
  // StringBuilder buf = new StringBuilder();
  // try {
  // HtmlUtils.text(buf,
  // __label,
  // StringUtils.toString(getValue(instance)),
  // false,
  // instance.getString(AppCC.NAMEPREFIX),
  // getName());
  // } catch (IOException e) {
  // throw new StringBuilderIOException(buf, e);
  // }
  // return buf.toString();
  // }

  public final String getLabel()
  {
    return __label;
  }

  @Override
  protected void init(String pwd,
                      Table table)
      throws MirrorTableLockedException
  {
    super.init(pwd, table);
    table.addRecordOperation(new Value());
  }

  public void appendFormElement(TransientRecord record,
                                Viewpoint viewpoint,
                                String namePrefix,
                                Appendable buf,
                                Gettable context)
      throws IOException
  {
    HtmlUtils.openLabelWidget(buf,
                              __label,
                              HtmlUtilsTheme.CSS_W_TEXT,
                              false,
                              namePrefix,
                              getJSONKey());
    appendHtmlWidget(record, viewpoint, namePrefix, buf, context);
    HtmlUtilsTheme.closeDiv(buf);
  }

  public void appendHtmlValue(TransientRecord record,
                              Viewpoint viewpoint,
                              String namePrefix,
                              Appendable buf,
                              Gettable context)
      throws IOException
  {
    buf.append(StringUtils.toString(getValue(record)));
  }

  public void appendHtmlWidget(TransientRecord record,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Gettable context)
      throws IOException
  {
    HtmlUtils.textWidget(buf, StringUtils.toString(getValue(record)), namePrefix, getJSONKey());
  }

  class Value
    extends SimpleRecordOperation<T>
    implements HtmlGuiSupplier
  {
    protected Value()
    {
      super(getJSONKey());
    }

    @Override
    public T getOperation(TransientRecord record,
                          Viewpoint viewpoint)
    {
      return getValue(record);
    }

    @Override
    public void appendFormElement(TransientRecord record,
                                  Viewpoint viewpoint,
                                  String namePrefix,
                                  Appendable buf,
                                  Gettable context)
        throws IOException
    {
      TableBlockJsonCC.this.appendFormElement(record, viewpoint, namePrefix, buf, context);
    }

    @Override
    public void appendHtmlValue(TransientRecord record,
                                Viewpoint viewpoint,
                                String namePrefix,
                                Appendable buf,
                                Gettable context)
        throws IOException
    {
      TableBlockJsonCC.this.appendHtmlValue(record, viewpoint, namePrefix, buf, context);
    }

    @Override
    public void appendHtmlWidget(TransientRecord record,
                                 Viewpoint viewpoint,
                                 String namePrefix,
                                 Appendable buf,
                                 Gettable context)
        throws IOException
    {
      TableBlockJsonCC.this.appendHtmlWidget(record, viewpoint, namePrefix, buf, context);
    }

    @Override
    public String getHtmlLabel()
    {
      return null;
    }

    @Override
    public boolean hasHtmlWidget(TransientRecord record)
    {
      return true;
    }

    @Override
    public boolean shouldReloadOnChange()
    {
      return false;
    }
  }

}
