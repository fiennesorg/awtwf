package org.awtwf.gntml;

import java.util.ListIterator;
import java.util.Map;

import org.awtwf.gntml.NavigationFactory.ValidChildren;
import org.cord.mirror.Db;
import org.cord.mirror.FieldKey;
import org.cord.mirror.IdList;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.ConstantDefinedFieldValueSupplier;
import org.cord.mirror.field.EnglishNamedMapField;
import org.cord.mirror.field.EnumField;
import org.cord.mirror.field.IntegerField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.AbstractRecordSource;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.util.Casters;
import org.cord.util.DuplicateNamedException;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.KeyGettableSupplier;
import org.cord.util.LogicException;
import org.cord.util.NumberUtil;
import org.cord.util.Settable;
import org.cord.util.SettableException;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

public class SiteMapFactory
  extends TableBlockFactory
{
  public final static String NAME = "SiteMap";
  public static final String TABLENAME = "GntmlSiteMap";

  public static final FieldKey<String> TITLE = FieldKey.create("title", String.class);
  public static final FieldKey<Integer> ROOTNODE_ID = FieldKey.create("rootNode_id", Integer.class);
  public static final RecordOperationKey<PersistentRecord> ROOTNODE =
      LinkField.getLinkName(ROOTNODE_ID);
  public static final FieldKey<RootNodePolicy> ROOTNODEPOLICY =
      FieldKey.create("rootNodePolicy", RootNodePolicy.class);
  public static final FieldKey<ValidChildren> VALIDCHILDREN = FieldKey.create("validChildren",
                                                                              ValidChildren.class);
  public static final FieldKey<PresentationStyle> NAVIGATIONSTYLE =
      FieldKey.create("navigationStyle", PresentationStyle.class);
  public static final FieldKey<Integer> MAXDEPTH = FieldKey.create("maxDepth", Integer.class);

  public static final RecordOperationKey<RecordSource> ROOTNODECHILDREN =
      RecordOperationKey.create("rootNodeChildren", RecordSource.class);
  public static final MonoRecordOperationKey<PersistentRecord, RecordSource> CHILDNODECHILDREN =
      MonoRecordOperationKey.create("childNodeChildren", PersistentRecord.class, RecordSource.class);

  public static final String GETTABLE_NODE = "node";
  public static final String GETTABLE_APP = "app";

  private EnglishNamedMapField<PresentationStyle> _navigationStyle;

  public enum RootNodePolicy {
    HIDE("Hide page"),
    ASPARENT("Show page as parent"),
    ASSIBLING("Show page as sibling");

    private final String __toString;

    private RootNodePolicy(String toString)
    {
      __toString = toString;
    }

    @Override
    public String toString()
    {
      return __toString;
    }
  }

  public SiteMapFactory(NodeManager nodeMgr)
  {
    super(nodeMgr,
          NAME,
          "Nested Site Map",
          "|" + NAME + ":",
          "|",
          TABLENAME,
          "Gntml/SiteMap/SiteMap.view.wm.html",
          "g_sitemap",
          "g_sitemap_caption",
          "Gntml/SiteMap/SiteMap.edit.wm.html",
          null);
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table table)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    table.addField(new StringField(TITLE, "Title", StringField.LENGTH_VARCHAR, null, ""));
    addNodeLinkField(table,
                     ROOTNODE_ID,
                     "Top-level page in sitemap",
                     null,
                     pwd,
                     null,
                     "top level page for embedded sitemap");
    table.addField(new EnumField<RootNodePolicy>(ROOTNODEPOLICY,
                                                 "Top-level page display",
                                                 RootNodePolicy.class,
                                                 ConstantDefinedFieldValueSupplier.wrap(RootNodePolicy.ASPARENT)));
    PresentationStyle concise =
        new PresentationStyle("title", "Title only", "Gntml/SiteMap/title.wm.html");
    _navigationStyle =
        new EnglishNamedMapField<PresentationStyle>(NAVIGATIONSTYLE,
                                                    "Presentation style",
                                                    PresentationStyle.class,
                                                    ConstantDefinedFieldValueSupplier.wrap(concise));
    _navigationStyle.addValue(concise);
    _navigationStyle.addValues(getPresentationStyles(getNodeManager().getGettable(), "node.keys"));
    _navigationStyle.addValues(getPresentationStyles(getNodeManager().getGettable(), "app.keys"));
    table.addField(_navigationStyle);
    table.addField(new EnumField<ValidChildren>(VALIDCHILDREN,
                                                "Child pages to be included",
                                                ValidChildren.class,
                                                ConstantDefinedFieldValueSupplier.wrap(ValidChildren.MENUITEMS)));
    table.addField(new IntegerField(MAXDEPTH, "Maximum generation depth", 4));
    table.addRecordOperation(new SimpleRecordOperation<RecordSource>(ROOTNODECHILDREN) {
      @Override
      public RecordSource getOperation(TransientRecord gntmlSiteMap,
                                       Viewpoint viewpoint)
      {
        final PersistentRecord rootNode = gntmlSiteMap.opt(ROOTNODE, viewpoint);
        if (rootNode == null) {
          return getNodeManager().getNode().getTable().getEmptyRecordSource();
        }
        RootNodePolicy rootNodePolicy = gntmlSiteMap.opt(ROOTNODEPOLICY);
        ValidChildren validChildren = gntmlSiteMap.opt(VALIDCHILDREN);
        switch (rootNodePolicy) {
          case HIDE:
            return validChildren.getChildren(rootNode);
          case ASPARENT:
            return rootNode.asRecordSource();
          case ASSIBLING:
            final RecordSource childrenNodes = validChildren.getChildren(rootNode);
            return new AbstractRecordSource(getNodeManager().getNode(),
                                            null) {
              @Override
              protected IdList buildIdList(Viewpoint viewpoint)
              {
                IdList childrenNodeIds = childrenNodes.getIdList(viewpoint);
                ArrayIdList ids =
                    new ArrayIdList(getNodeManager().getNode(), childrenNodeIds.size() + 1);
                ids.add(rootNode.getId());
                ids.addAll(childrenNodeIds);
                return ids;
              }

              @Override
              public long getLastChangeTime()
              {
                return childrenNodes.getLastChangeTime();
              }

              @Override
              public boolean shouldRebuild(Viewpoint viewpoint)
              {
                return childrenNodes.shouldRebuild(viewpoint);
              }
            };
          default:
            throw new LogicException(String.format("Unknown RootNodePolicy of %s", rootNodePolicy));
        }
      }
    });
    table.addRecordOperation(new MonoRecordOperation<PersistentRecord, RecordSource>(CHILDNODECHILDREN,
                                                                                     Casters.<PersistentRecord> CAST()) {
      @Override
      public RecordSource calculate(TransientRecord gntmlSiteMap,
                                    Viewpoint viewpoint,
                                    PersistentRecord childNode)
      {
        childNode.assertIsTableNamed(Node.TABLENAME);
        ValidChildren validChildren = gntmlSiteMap.opt(VALIDCHILDREN);
        if (!gntmlSiteMap.opt(ROOTNODE_ID).equals(childNode.getId())) {
          return validChildren.getChildren(childNode);
        }
        RootNodePolicy rootNodePolicy = gntmlSiteMap.opt(ROOTNODEPOLICY);
        switch (rootNodePolicy) {
          case ASPARENT:
            return validChildren.getChildren(childNode);
          case ASSIBLING:
            return getNodeManager().getNode().getTable().getEmptyRecordSource();
          default:
            throw new LogicException(String.format("Canno invoke childNodeChildren with %s and a rootNodePolicy of %s",
                                                   childNode,
                                                   rootNodePolicy));
        }
      }
    });
  }

  private final KeyGettableSupplier<PresentationStyle> __presentationStyleSupplier =
      new KeyGettableSupplier<PresentationStyle>() {
        @Override
        public PresentationStyle get(String key,
                                     Gettable gettable)
        {
          return new PresentationStyle(key,
                                       gettable.getString("title"),
                                       gettable.getString("templatePath"));
        }
      };

  private ImmutableList<PresentationStyle> getPresentationStyles(Gettable gettable,
                                                                 String keys)
  {
    return GettableUtils.getValues(gettable,
                                   "Gntml.SiteMapFactory",
                                   keys,
                                   __presentationStyleSupplier);
  }

  @Override
  protected TableBlock buildTableBlock(ListIterator<String> sourceLines,
                                       GntmlBlockStyle blockStyle,
                                       GntmlDocument gntmlDocument,
                                       int blockId)
      throws GntmlException
  {
    return new TableBlock(NAME,
                          sourceLines,
                          blockStyle,
                          gntmlDocument,
                          this,
                          blockId) {

      @Override
      public void copyGntmlBlock(Settable settable,
                                 PersistentRecord fromNode,
                                 PersistentRecord fromRegionStyle,
                                 PersistentRecord toRegionStyle,
                                 PersistentRecord fromGntmlInstance,
                                 GntmlDocument gntmlDocument,
                                 PersistentRecord instance)
          throws SettableException
      {
        // TODO Auto-generated method stub
      }

      @Override
      protected void updateInstance(WritableRecord instance,
                                    Gettable params,
                                    Map<Object, Object> outputs)
          throws GntmlException, MirrorException
      {
        if (Boolean.TRUE.equals(params.getBoolean(ISACTIVE))) {
          instance.setField(TITLE, params.get(TITLE));
          instance.setFieldIfDefined(ROOTNODE_ID, params.get(ROOTNODE_ID));
          instance.setField(ROOTNODEPOLICY, params.get(ROOTNODEPOLICY));
          instance.setField(NAVIGATIONSTYLE, params.get(NAVIGATIONSTYLE));
          instance.setField(VALIDCHILDREN, params.get(VALIDCHILDREN));
          Integer maxDepth = NumberUtil.toInteger(params.get(MAXDEPTH), null);
          if (maxDepth != null) {
            instance.setField(MAXDEPTH, NumberUtil.constrain(maxDepth, 1, 100));
          }
        }
      }
    };
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
      throws GntmlException
  {
  }

  public static class PresentationStyle
    extends EnglishNamedImpl
  {
    private final String __templatePath;

    public PresentationStyle(String name,
                             String englishName,
                             String templatePath)
    {
      super(name,
            englishName);
      __templatePath = Preconditions.checkNotNull(templatePath, "templatePath");
    }

    public String getTemplatePath()
    {
      return __templatePath;
    }
  }

}
