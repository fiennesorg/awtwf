package org.awtwf.gntml;

public class GntmlListLine
  extends GntmlLine
{
  private final static String[] LIST_TAGS = { "-", "#" };

  public final static int LIST_NOT = -1;

  public final static int LIST_UL = 0;

  public final static int LIST_OL = 1;

  private int _listType;

  private int _listDepth;

  public GntmlListLine(String source,
                       GntmlBlock gntmlBlock)
  {
    super(source,
          gntmlBlock);
    _listType = getListType(getSource());
    _listDepth = getListDepth(getSource(), _listType);
    setSource(getSource().substring(_listDepth), true);
  }

  public final static int getListType(String source)
  {
    for (int i = 0; i < LIST_TAGS.length; i++) {
      if (source.startsWith(LIST_TAGS[i])) {
        return i;
      }
    }
    return LIST_NOT;
  }

  public final int getListType()
  {
    return _listType;
  }

  private int getListDepth(String source,
                           int listType)
  {
    if (listType == LIST_NOT) {
      return 0;
    }
    char listChar = LIST_TAGS[listType].charAt(0);
    int depth = 0;
    while (depth < source.length() && source.charAt(depth) == listChar) {
      depth++;
    }
    return depth;
  }

  public final int getListDepth()
  {
    return _listDepth;
  }
}
