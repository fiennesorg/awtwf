package org.awtwf.gntml;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.TemplateSuccessOperation;
import org.cord.node.view.DynamicAclAuthenticatedFactory;
import org.webmacro.Context;

@Deprecated
public class ZoomImageBlock
  extends DynamicAclAuthenticatedFactory
{
  public static final String NAME = "zoomImageBlock";

  public static final String CGI_ID = "id";

  public ZoomImageBlock(NodeManager nodeManager,
                        String templatePath,
                        String authenticationError)
  {
    super(NAME,
          nodeManager,
          new TemplateSuccessOperation(nodeManager, templatePath),
          Node.VIEWACL,
          authenticationError);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    String id = nodeRequest.getString(CGI_ID);
    PersistentRecord zoomedImage = null;
    try {
      zoomedImage =
          getNodeManager().getDb().getTable(ImageBlockFactory.GNTMLZOOMEDIMAGE).getRecord(id);
    } catch (MirrorNoSuchRecordException e) {
      throw new FeedbackErrorException(null,
                                       "The image you have requested is not found",
                                       node,
                                       e,
                                       false);
    }
    PersistentRecord normalImage =
        zoomedImage.followEnforcedLink(ImageBlockFactory.GNTMLZOOMEDIMAGE_GNTMLIMAGEINSTANCE);
    PersistentRecord normalImageNode =
        normalImage.followEnforcedLink(ImageBlockFactory.GNTMLINSTANCE)
                   .followEnforcedLink(Gntml.NODE);
    getNodeManager().getNode().assertIsViewable(normalImageNode, nodeRequest.getUserId());
    context.put("controllingImage", normalImage);
    context.put("controlledImage", zoomedImage);
    return handleSuccess(nodeRequest, node, context, null);
  }
}
