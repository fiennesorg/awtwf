package org.awtwf.gntml;

import java.io.IOException;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.regex.Pattern;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;

public class ParagraphBlockFactory
  extends EnglishNamedImpl
  implements GntmlBlockFactory
{
  public final static String NAME = "Paragraph";

  public final static Pattern headerTags = Pattern.compile("([hH][1-6]|blockquote)(\\.(.+))?");

  public ParagraphBlockFactory()
  {
    super(NAME);
  }

  @Override
  public boolean acceptsSource(GntmlDocument gntmlDocument,
                               String firstLine)
  {
    return true;
  }

  @Override
  public GntmlBlock buildBlock(ListIterator<String> sourceLines,
                               GntmlBlockStyle blockStyle,
                               GntmlDocument gntmlDocument,
                               int blockId)
  {
    return new ParagraphBlock(sourceLines, blockStyle, gntmlDocument, blockId);
  }

  /**
   * Do nothing
   */
  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
  {
  }

  /**
   * Do nothing
   */
  @Override
  public void preParse(GntmlDocument gntmlDocument,
                       Transaction transaction)
  {
  }

  public class ParagraphBlock
    extends GntmlBlock
  {
    protected ParagraphBlock(ListIterator<String> sourceLines,
                             GntmlBlockStyle blockStyle,
                             GntmlDocument gntmlDocument,
                             int blockId)
    {
      super(NAME,
            sourceLines,
            blockStyle,
            gntmlDocument,
            ParagraphBlockFactory.this,
            blockId);
    }

    @Override
    public String getCssClass()
    {
      return "p";
    }

    @Override
    public Appendable parse(Appendable xhtml,
                            Gettable documentParams,
                            Gettable blockParams,
                            Map<Object, Object> outputs)
        throws GntmlException, IOException
    {
      openDivs(xhtml);
      String openTag = "<p>";
      String closeTag = "</p>";
      Iterator<GntmlLine> lineList = getLineList();
      parseBodyLines(xhtml, lineList, blockParams, true, openTag, closeTag);
      closeDivs(xhtml);
      return xhtml;
    }

    /**
     * Do nothing.
     */
    @Override
    public void copyGntmlBlock(Settable settable,
                               PersistentRecord fromNode,
                               PersistentRecord fromRegionStyle,
                               PersistentRecord toRegionStyle,
                               PersistentRecord fromGntmlInstance,
                               GntmlDocument gntmlDocument)
        throws SettableException
    {
    }
  }
}
