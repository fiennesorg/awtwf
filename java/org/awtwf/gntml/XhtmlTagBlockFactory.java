package org.awtwf.gntml;

import java.io.IOException;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.util.Assertions;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;

/**
 * GntmlBlockFactory that produces Blocks that are wrapped in a given XHTML open and close sequence,
 * normally a single tag. These block honour the leading {blockstyle} declerations thereby enabling
 * you to provide alternative themings of the blocks as required.
 * 
 * @author alex
 */
public class XhtmlTagBlockFactory
  extends EnglishNamedImpl
  implements GntmlBlockFactory
{
  private final String __firstLineMatch;

  private final String __openingTag;

  private final String __closingTag;

  public XhtmlTagBlockFactory(String name,
                              String englishName,
                              String firstLineMatch,
                              String openingTag,
                              String closingTag)
  {
    super(name,
          englishName);
    __firstLineMatch = Assertions.notEmpty(firstLineMatch, "firstLineMatch");
    __openingTag = Assertions.notEmpty(openingTag, "openingTag");
    __closingTag = Assertions.notEmpty(closingTag, "closingTag");
  }

  @Override
  public GntmlBlock buildBlock(ListIterator<String> sourceLines,
                               GntmlBlockStyle blockStyle,
                               GntmlDocument gntmlDocument,
                               int blockId)
      throws GntmlException
  {
    return new XhtmlTagBlock(getName(), sourceLines, blockStyle, gntmlDocument, blockId);
  }

  @Override
  public boolean acceptsSource(GntmlDocument gntmlDocument,
                               String firstSourceLine)
  {
    return __firstLineMatch.equals(firstSourceLine);
  }

  /**
   * do nothing
   */
  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
      throws GntmlException
  {
  }

  /**
   * do nothing
   */
  @Override
  public void preParse(GntmlDocument gntmlDocument,
                       Transaction transaction)
      throws GntmlException
  {
  }

  public class XhtmlTagBlock
    extends GntmlBlock
  {
    protected XhtmlTagBlock(String name,
                            ListIterator<String> sourceLines,
                            GntmlBlockStyle blockStyle,
                            GntmlDocument gntmlDocument,
                            int blockId)
    {
      super(name,
            sourceLines,
            blockStyle,
            gntmlDocument,
            XhtmlTagBlockFactory.this,
            blockId);
    }

    @Override
    public String getCssClass()
    {
      return XhtmlTagBlockFactory.this.getName();
    }

    @Override
    public Appendable parse(Appendable out,
                            Gettable documentParams,
                            Gettable blockParams,
                            Map<Object, Object> outputs)
        throws GntmlException, IOException
    {
      openDivs(out);
      Iterator<GntmlLine> lineList = getLineList();
      lineList.next();
      parseBodyLines(out, lineList, blockParams, true, __openingTag, __closingTag);
      closeDivs(out);
      return out;
    }

    @Override
    public void copyGntmlBlock(Settable settable,
                               PersistentRecord fromNode,
                               PersistentRecord fromRegionStyle,
                               PersistentRecord toRegionStyle,
                               PersistentRecord fromGntmlInstance,
                               GntmlDocument gntmlDocument)
        throws SettableException
    {
    }
  }
}
