package org.awtwf.gntml;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.Dependencies;
import org.cord.mirror.DirectCachingSimpleRecordOperation;
import org.cord.mirror.FieldKey;
import org.cord.mirror.HtmlGuiSupplier;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Query;
import org.cord.mirror.RecordList;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordReference;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TableWrapper;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.EnumeratedIntegerField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.trigger.PostTransactionDeleteTrigger;
import org.cord.mirror.trigger.PreInstantiationTriggerImpl;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeQuestion;
import org.cord.node.NodeRequest;
import org.cord.node.RedirectionSuccessOperation;
import org.cord.node.RegionStyle;
import org.cord.node.ValueFactory;
import org.cord.node.json.IdMappers;
import org.cord.node.json.JSONLoader;
import org.cord.node.json.JSONSaver;
import org.cord.node.json.UnresolvedRecordException;
import org.cord.node.links.OrderedNodeLinks;
import org.cord.node.links.RelatedPagesSupplier;
import org.cord.node.view.AskNodeQuestion;
import org.cord.sql.SqlUtil;
import org.cord.util.Casters;
import org.cord.util.DuplicateNamedException;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.NamespaceGettable;
import org.cord.util.ObjectUtil;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.cord.util.XmlUtils;
import org.cord.webmacro.AppendableParametricMacro;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONObject;
import org.webmacro.Context;
import org.webmacro.Macro;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

public class EmbeddedLink
  extends EnglishNamedImpl
  implements InlineTag, DbInitialiser, TableWrapper, RelatedPagesSupplier
{
  public static final String TABLENAME = "GntmlEmbeddedLink";

  public static final RecordOperationKey<RecordSource> REFERENCES =
      RecordOperationKey.create("gntmlEmbeddedLinks", RecordSource.class);

  public static final FieldKey<Integer> GNTMLINSTANCE_ID =
      LinkField.getLinkFieldName(Gntml.TABLENAME);

  public static final RecordOperationKey<PersistentRecord> GNTMLINSTANCE =
      LinkField.getLinkName(GNTMLINSTANCE_ID);

  public static final FieldKey<String> NAME = FieldKey.create("name", String.class);

  public static final FieldKey<Boolean> ISREFERENCED = FieldKey.create("isReferenced",
                                                                       Boolean.class);

  public static final FieldKey<Integer> NODE_ID = LinkField.getLinkFieldName(Node.TABLENAME);

  public static final RecordOperationKey<PersistentRecord> NODE = LinkField.getLinkName(NODE_ID);

  public static final FieldKey<String> CONTENTS = FieldKey.create("contents", String.class);

  public static final FieldKey<Boolean> HASBACKLINKS = FieldKey.create("hasBackLinks",
                                                                       Boolean.class);

  public static final FieldKey<String> BACKLINKTITLE = FieldKey.create("backLinkTitle",
                                                                       String.class);

  public static final String NODEPATH = "nodePath";

  public static final FieldKey<Integer> TITLEMODE = FieldKey.create("titleMode", Integer.class);

  public static final RecordOperationKey<Map<Integer, String>> TITLEMODES =
      RecordOperationKey.create("titleModes",
                                ObjectUtil.<Map<Integer, String>> castClass(Map.class));

  public static final int TITLEMODE_NONE = 0;

  public static final int TITLEMODE_TITLE = 1;

  public static final int TITLEMODE_DESCRIPTION = 2;

  public static final int TITLEMODE_TITLEDESCRIPTION = 3;

  public static final int TITLEMODE_EXPLICIT = 4;

  public static final FieldKey<String> EXPLICITTITLE = FieldKey.create("explicitTitle",
                                                                       String.class);

  /**
   * RecordOperation that returns a Boolean true if the titleMode currently chosen is using the
   * explicitTitle field.
   * 
   * @see #TITLEMODE
   * @see #EXPLICITTITLE
   */
  public static final RecordOperationKey<Boolean> HASEXPLICITTITLE =
      RecordOperationKey.create("hasExplicitTitle", Boolean.class);

  /**
   * RecordOperation that returns the String title of this embedded link as derived from titleMode
   * and explicitTitle
   * 
   * @see #TITLEMODE
   * @see #EXPLICITTITLE
   */
  public static final RecordOperationKey<String> TITLE = RecordOperationKey.create("title",
                                                                                   String.class);

  /**
   * Default webmacro path to the edit interface for a single EmbeddedLink
   */
  public static final String EDITTEMPLATEPATH = "Gntml/embeddedLinks.edit.wm.html";

  public static final RecordOperationKey<RecordSource> NODE_GNTMLBACKLINKS =
      RecordOperationKey.create("gntmlBackLinks", RecordSource.class);

  /**
   * RecordOperation that returns a String that should be prefixed to parameters when sending
   * variables to this link
   */
  public static final RecordOperationKey<String> NAMEPREFIX =
      RecordOperationKey.create("namePrefix", String.class);

  public static final MonoRecordOperationKey<String, Macro> ASFORMELEMENT =
      HtmlGuiSupplier.ASFORMELEMENT;

  /**
   * RecordOperation that returns the namespace of the GntmlEmbeddedLink that it is invoked on. This
   * will be "embeddedLinks-$gntmlEmbeddedLink.id-"
   */
  public static final RecordOperationKey<String> NAMESPACE =
      RecordOperationKey.create("namespace", String.class);

  /**
   * RecordOperation that returns a String containing the functional XHTML version of this link
   * ($gntmlEmbeddedLink.parse)
   */
  public static final RecordOperationKey<String> PARSE = RecordOperationKey.create("parse",
                                                                                   String.class);

  private static final Pattern __pattern = Pattern.compile("\\|\\|(.+?)\\|\\|");

  private final NodeManager __nodeManager;

  private final Db __db;

  private Table _gntmlEmbeddedLinks;

  private final ValueFactory __openNodeQuestionUrl;

  /**
   * Create a new EmbeddedLink item and register it as a DbInitialiser and RelatedPagesSupplier
   * against the supplied Db
   * 
   * @param openNodeQuestionUrl
   *          The ValueFactory that is going to supply the location that the user should be taken to
   *          when a NodeQuestion is opened to find the target of this EmbeddedLink.
   */
  public EmbeddedLink(NodeManager nodeManager,
                      ValueFactory openNodeQuestionUrl) throws MirrorTableLockedException
  {
    super("embeddedLink",
          "Internal Page Link");
    __nodeManager = nodeManager;
    __db = nodeManager.getDb();
    __openNodeQuestionUrl = Preconditions.checkNotNull(openNodeQuestionUrl);
  }

  public final NodeManager getNodeManager()
  {
    return __nodeManager;
  }

  public PersistentRecord getGntmlInstance(GntmlDocument gntmlDocument,
                                           Viewpoint viewpoint)
      throws MirrorNoSuchRecordException
  {
    return __db.getTable(Gntml.TABLENAME).getRecord(gntmlDocument.getDocumentId(), viewpoint);
  }

  protected Matcher getMatcher(String source)
  {
    return __pattern.matcher(source);
  }

  @Override
  public void init(final Db db,
                   final String pwd)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    _gntmlEmbeddedLinks = db.addTable(this, TABLENAME);
    _gntmlEmbeddedLinks.addField(new LinkField(Gntml.TABLENAME,
                                               "Containing GNTML document",
                                               pwd,
                                               Dependencies.LINK_ENFORCED
                                                   | Dependencies.BIT_S_ON_T_UPDATE));
    _gntmlEmbeddedLinks.addField(new StringField(NAME, "Name"));
    _gntmlEmbeddedLinks.addField(new BooleanField(ISREFERENCED,
                                                  "Is currently referenced in document?"));
    _gntmlEmbeddedLinks.addField(new LinkField(Node.TABLENAME,
                                               "Linked to Node",
                                               pwd,
                                               Dependencies.LINK_ENFORCED
                                                   | Dependencies.BIT_OPTIONAL));
    _gntmlEmbeddedLinks.addField(new StringField(CONTENTS, "Visible contents of link"));
    _gntmlEmbeddedLinks.addField(new BooleanField(HASBACKLINKS,
                                                  "Link has backlink on target page?",
                                                  Boolean.FALSE) {
      @Override
      public boolean shouldReloadOnChange()
      {
        return true;
      }
    });
    _gntmlEmbeddedLinks.addField(new StringField(BACKLINKTITLE, "Visible contents of backlink"));
    EnumeratedIntegerField titleMode = new EnumeratedIntegerField(TITLEMODE,
                                                                  "Link title (tooltip)",
                                                                  TITLEMODES,
                                                                  null) {
      @Override
      public boolean shouldReloadOnChange()
      {
        return true;
      }
    };
    titleMode.addValueName(TITLEMODE_NONE, "No title", true);
    titleMode.addValueName(TITLEMODE_TITLE, "Title of target page", false);
    titleMode.addValueName(TITLEMODE_DESCRIPTION, "Description of target page", false);
    titleMode.addValueName(TITLEMODE_TITLEDESCRIPTION,
                           "Title and description of target page",
                           false);
    titleMode.addValueName(TITLEMODE_EXPLICIT, "Explicit title", false);
    _gntmlEmbeddedLinks.addField(titleMode);
    _gntmlEmbeddedLinks.addField(new StringField(EXPLICITTITLE, "Explicit link title"));
    _gntmlEmbeddedLinks.addRecordOperation(new SimpleRecordOperation<String>(PARSE) {
      @Override
      public String getOperation(TransientRecord gntmlEmbeddedLink,
                                 Viewpoint viewpoint)
      {
        if (!gntmlEmbeddedLink.mayFollowLink(NODE_ID, viewpoint)) {
          return gntmlEmbeddedLink.opt(CONTENTS);
        }
        PersistentRecord node = gntmlEmbeddedLink.comp(NODE);
        return Node.constructHref(node,
                                  gntmlEmbeddedLink.opt(CONTENTS),
                                  Node.URL,
                                  null,
                                  gntmlEmbeddedLink.opt(TITLE),
                                  node.comp(Node.ISPUBLISHED));
      }
    });
    _gntmlEmbeddedLinks.addRecordOperation(new SimpleRecordOperation<Boolean>(HASEXPLICITTITLE) {
      @Override
      public Boolean getOperation(TransientRecord gntmlEmbeddedLink,
                                  Viewpoint viewpoint)
      {
        return Boolean.valueOf(gntmlEmbeddedLink.comp(TITLEMODE) == TITLEMODE_EXPLICIT);
      }
    });
    _gntmlEmbeddedLinks.addRecordOperation(new SimpleRecordOperation<String>(TITLE) {
      @Override
      public String getOperation(TransientRecord gntmlEmbeddedLink,
                                 Viewpoint viewpoint)
      {
        String title = "";
        PersistentRecord node = gntmlEmbeddedLink.comp(NODE);
        switch (gntmlEmbeddedLink.comp(TITLEMODE)) {
          case TITLEMODE_TITLE:
            title = node.opt(Node.TITLE);
            break;
          case TITLEMODE_DESCRIPTION:
            title = node.opt(Node.DESCRIPTION);
            break;
          case TITLEMODE_TITLEDESCRIPTION:
            title = node.opt(Node.TITLE) + ": " + node.opt(Node.DESCRIPTION);
            break;
          case TITLEMODE_EXPLICIT:
            title = gntmlEmbeddedLink.opt(EXPLICITTITLE);
            break;
        }
        return title;
      }
    });
    _gntmlEmbeddedLinks.addRecordOperation(new DirectCachingSimpleRecordOperation<String>(NAMESPACE) {
      @Override
      public String calculate(TransientRecord gntmlEmbeddedLink,
                              Viewpoint viewpoint)
      {
        return namespace(gntmlEmbeddedLink);
      }
    });
    _gntmlEmbeddedLinks.addPreInstantiationTrigger(new PreInstantiationTriggerImpl() {
      @Override
      public void triggerPre(TransientRecord instance,
                             Gettable params)
          throws MirrorFieldException
      {
        if (Strings.isNullOrEmpty(instance.opt(CONTENTS))) {
          instance.setField(CONTENTS, instance.opt(NAME));
        }
        if (Strings.isNullOrEmpty(instance.opt(BACKLINKTITLE))) {
          instance.setField(BACKLINKTITLE,
                            instance.comp(GNTMLINSTANCE).comp(Gntml.NODE).opt(Node.TITLE));
        }
      }
    });
    PostTransactionDeleteTrigger.register(_gntmlEmbeddedLinks,
                                          ISREFERENCED,
                                          pwd,
                                          "EmbeddedLink.isReferenced cleanup");
    _gntmlEmbeddedLinks.addRecordOperation(new DirectCachingSimpleRecordOperation<String>(NAMEPREFIX) {
      @Override
      public String calculate(TransientRecord gntmlEmbeddedLink,
                              Viewpoint viewpoint)
      {
        StringBuilder buf = new StringBuilder();
        buf.append(gntmlEmbeddedLink.comp(GNTMLINSTANCE).opt(Gntml.NAMEPREFIX));
        buf.append(gntmlEmbeddedLink.opt(NAMESPACE));
        return buf.toString();
      }
    });
    _gntmlEmbeddedLinks.addRecordOperation(new MonoRecordOperation<String, Macro>(ASFORMELEMENT,
                                                                                  Casters.TOSTRING) {
      @Override
      public Macro calculate(final TransientRecord gntmlInstance,
                             final Viewpoint viewpoint,
                             final String name)
      {
        return new AppendableParametricMacro() {
          @Override
          public void append(Appendable out,
                             Map<Object, Object> context)
              throws IOException
          {
            gntmlInstance.appendFormElement(gntmlInstance.opt(NAMEPREFIX),
                                            name,
                                            out,
                                            NodeRequest.asGettable(context));
          }
        };
      }
    });
    db.getTable(Gntml.TABLENAME)
      .addRecordOperation(new MonoRecordOperation<String, PersistentRecord>(MonoRecordOperationKey.create(getName(),
                                                                                                          String.class,
                                                                                                          PersistentRecord.class),
                                                                            Casters.TOSTRING) {
        @Override
        public PersistentRecord calculate(TransientRecord gntmlInstance,
                                          Viewpoint viewpoint,
                                          String name)
        {
          try {
            return getOrCreateInstance(gntmlInstance.getId(), name, null, viewpoint);
          } catch (MirrorException e) {
            throw new LogicException("Error resolving " + getName() + " named " + name + " on "
                                     + gntmlInstance, e);
          }
        }
      });
    db.getTable(Node.TABLENAME)
      .addRecordOperation(new SimpleRecordOperation<RecordSource>(NODE_GNTMLBACKLINKS) {
        @Override
        public RecordSource getOperation(TransientRecord node,
                                         Viewpoint viewpoint)
        {
          Table embeddedLinks = getNodeManager().getDb().getTable(EmbeddedLink.TABLENAME);
          StringBuilder buf = new StringBuilder();
          buf.append("(GntmlEmbeddedLink.node_id=")
             .append(node.getId().intValue())
             .append(") and (GntmlEmbeddedLink.isReferenced=1) "
                     + "and (GntmlEmbeddedLink.hasBackLinks=1) "
                     + "and (GntmlEmbeddedLink.gntmlInstance_id=GntmlInstance.id) "
                     + "and (GntmlInstance.isDefined=1)");
          String filter = buf.toString();
          Query allLinks =
              embeddedLinks.getQuery(filter,
                                     filter,
                                     "GntmlEmbeddedLink.backLinkTitle",
                                     getNodeManager().getDb().getTable(Gntml.TABLENAME));
          return RecordSources.viewedFrom(allLinks, viewpoint);
        }
      });
    AskNodeQuestion askNodeQuestion =
        new AskNodeQuestion(getName(),
                            getNodeManager(),
                            new RedirectionSuccessOperation(getNodeManager(), __openNodeQuestionUrl),
                            Node.EDITACL,
                            "You do not have permission to set the " + getName()) {
          @Override
          protected NodeQuestion createNodeQuestion(NodeRequest nodeRequest,
                                                    final PersistentRecord node,
                                                    Context context)
          {
            PersistentRecord gntmlEmbeddedLink;
            try {
              gntmlEmbeddedLink = getTable().getRecord(nodeRequest.get("id"));
            } catch (MirrorNoSuchRecordException e1) {
              throw new LogicException(String.format("Unable to resolve GntmlEmbeddedLink#%s",
                                                     nodeRequest.get("id")));
            }
            PersistentRecord gntmlInstance = gntmlEmbeddedLink.comp(GNTMLINSTANCE);
            PersistentRecord regionStyle = gntmlInstance.comp(Gntml.REGIONSTYLE);
            final String namePrefix = gntmlEmbeddedLink.opt(NAMEPREFIX);
            Transaction transaction = getTransaction(nodeRequest, node);
            String visibleQuestion =
                String.format("Select target for the embedded link <em>%s</em> in <em>%s</em> on the page %s:",
                              gntmlEmbeddedLink.opt(NAME),
                              regionStyle.opt(RegionStyle.TITLE),
                              node.opt(Node.HREF, Node.TITLE.getKeyName()));
            NodeQuestion question = new NodeQuestion(getName() + gntmlEmbeddedLink.getId(),
                                                     visibleQuestion,
                                                     "GNTML Embedded Link",
                                                     node,
                                                     1,
                                                     null,
                                                     transaction) {
              @Override
              public String getAnswerUrl()
              {
                StringBuilder url = new StringBuilder();
                url.append(node.opt(Node.URL));
                url.append("?view=update");
                url.append('&').append(namePrefix).append("node_id=").append(getSelectedNodeId());
                return url.toString();
              }

              @Override
              public boolean maySubmit()
              {
                return getSelectedNodeIds().size() == 1;
              }

              @Override
              public boolean shouldSubmit()
              {
                return maySubmit();
              }
            };
            return question;
          }
        };
    getNodeManager().getNodeRequestSegmentManager().register(Transaction.TRANSACTION_UPDATE,
                                                             askNodeQuestion);
    getNodeManager().getRelatedPagesManager().addSupplier(this);
  }
  protected Query getInstanceQuery(Integer documentId,
                                   String blockName)
  {
    StringBuilder filter = new StringBuilder();
    filter.append(GNTMLINSTANCE_ID)
          .append('=')
          .append(documentId.intValue())
          .append(" and ")
          .append(NAME)
          .append('=');
    SqlUtil.toSafeSqlString(filter, blockName);
    Query query = getTable().getQuery(null, filter.toString(), null);
    return query;
  }

  protected PersistentRecord getOrCreateInstance(Integer documentId,
                                                 String name,
                                                 String contents,
                                                 Viewpoint viewpoint)
      throws MirrorFieldException, MirrorInstantiationException, MirrorTransactionTimeoutException
  {
    Query instanceQuery = getInstanceQuery(documentId, name);
    PersistentRecord pInstance = RecordSources.getOptOnlyRecord(instanceQuery, viewpoint);
    if (pInstance == null) {
      TransientRecord tInstance = getTable().createTransientRecord();
      tInstance.setField(GNTMLINSTANCE_ID, documentId);
      tInstance.setField(NAME, name);
      if (contents != null) {
        tInstance.setField(CONTENTS, contents);
      }
      pInstance = tInstance.makePersistent(null);
    }
    if (viewpoint != null && viewpoint instanceof Transaction) {
      return ((Transaction) viewpoint).edit(pInstance);
    }
    return pInstance;
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public String getTableName()
  {
    return TABLENAME;
  }

  @Override
  public Table getTable()
  {
    return _gntmlEmbeddedLinks;
  }

  @Override
  public String parse(final GntmlDocument document,
                      final Transaction transaction,
                      final String source,
                      final Gettable gntmlParams)
      throws GntmlException
  {
    Matcher matcher = matcher(source);
    if (matcher == null) {
      return source;
    }
    StringBuffer result = new StringBuffer(source.length());
    boolean hasMatches = false;
    while (matcher.find()) {
      hasMatches = true;
      String contents = matcher.group(1);
      String name = contents;
      try {
        PersistentRecord pInstance =
            getOrCreateInstance(document.getDocumentId(), name, contents, transaction);
        if (transaction != null) {
          Gettable localParams = new NamespaceGettable(null, gntmlParams, pInstance.opt(NAMESPACE));
          pInstance.setField(ISREFERENCED, Boolean.TRUE);
          pInstance.setFieldIfDefined(CONTENTS, localParams.get("contents"));
          pInstance.setFieldIfDefined(NODE_ID,
                                      IdMappers.map(localParams,
                                                    Node.TABLENAME,
                                                    localParams.get("node_id")));
          String nodePath = localParams.getString(NODEPATH);
          if (nodePath != null) {
            PersistentRecord targetNode = pInstance.opt(NODE, transaction);
            String targetNodePath = targetNode == null ? null : (String) targetNode.opt(Node.PATH);
            if (!nodePath.equals(targetNodePath)) {
              try {
                targetNode = getNodeManager().getNode().getNodeByPath(nodePath, null);
                pInstance.setField(NODE_ID, targetNode.getId());
              } catch (MirrorNoSuchRecordException mnsrEx) {
                // do nothing as we don't have a valid target....
              }
            }
          }
          pInstance.setFieldIfDefined(HASBACKLINKS, localParams.getBoolean("hasBackLinks"));
          pInstance.setFieldIfDefined(BACKLINKTITLE, localParams.get("backLinkTitle"));
          pInstance.setFieldIfDefined(TITLEMODE, localParams.get(TITLEMODE));
          pInstance.setFieldIfDefined(EXPLICITTITLE, localParams.get(EXPLICITTITLE));
        }
        matcher.appendReplacement(result,
                                  String.format("\\$(db.GntmlEmbeddedLink.getRecord(%s).parse)",
                                                pInstance.getId()));
      } catch (MirrorException mEx) {
        throw new GntmlException("Error creating or resolving GntmlEmbeddedLink", mEx);
      }
    }
    matcher.appendTail(result);
    return hasMatches ? result.toString() : source;
  }

  @Override
  public void preParse(GntmlDocument gntmlDocument,
                       Transaction transaction)
      throws GntmlException
  {
    try {
      PersistentRecord gntmlInstance = getGntmlInstance(gntmlDocument, transaction);
      for (PersistentRecord gntmlEmbeddedLink : gntmlInstance.opt(getTable().getReferencesName(),
                                                                  transaction)) {
        gntmlEmbeddedLink.setField(ISREFERENCED, Boolean.FALSE);
      }
    } catch (MirrorException mEx) {
      throw new GntmlException("Unable to preparse " + gntmlDocument + " with " + this, mEx);
    }
  }

  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
      throws GntmlException
  {
  }

  private Matcher matcher(String source)
  {
    if (source.indexOf('|') == -1) {
      return null;
    }
    return __pattern.matcher(source);
  }

  @Override
  public boolean outputsWebmacro(String source)
  {
    Matcher matcher = matcher(source);
    return matcher == null ? false : matcher.find();
  }

  @Override
  public boolean hasEditWidget()
  {
    return true;
  }

  @Override
  public String getEditTemplatePath()
  {
    return EDITTEMPLATEPATH;
  }

  @Override
  public void addRelatedPages(PersistentRecord node,
                              OrderedNodeLinks nodeLinks)
  {
    for (PersistentRecord gntmlEmbeddedLink : node.opt(NODE_GNTMLBACKLINKS)) {
      PersistentRecord targetGntmlInstance = gntmlEmbeddedLink.comp(EmbeddedLink.GNTMLINSTANCE);
      if (targetGntmlInstance.comp(Gntml.ISDEFINED)) {
        PersistentRecord targetNode = targetGntmlInstance.comp(Gntml.NODE);
        String targetTitle = gntmlEmbeddedLink.opt(EmbeddedLink.BACKLINKTITLE);
        if (Strings.isNullOrEmpty(targetTitle)) {
          targetTitle = targetNode.opt(Node.HTMLTITLE);
        }
        nodeLinks.addNodeLabel(targetNode, targetTitle);
      }
    }
  }

  private static String namespace(TransientRecord gntmlEmbeddedLink)
  {
    return String.format("embeddedLinks-%s-", XmlUtils.encodeName(gntmlEmbeddedLink.opt(NAME)));
  }

  @Override
  public void copyInlineTag(Settable params,
                            PersistentRecord fromNode,
                            PersistentRecord fromRegionStyle,
                            PersistentRecord toRegionStyle,
                            PersistentRecord fromGntmlInstance,
                            GntmlDocument gntmlDocument)
      throws SettableException
  {
    for (PersistentRecord link : fromGntmlInstance.opt(EmbeddedLink.REFERENCES)) {
      if (link.comp(ISREFERENCED)) {
        String linkName = namespace(link);
        params.set(linkName + NODE_ID, link.opt(NODE_ID));
        params.set(linkName + CONTENTS, link.opt(CONTENTS));
        params.set(linkName + HASBACKLINKS, link.opt(HASBACKLINKS));
        params.set(linkName + BACKLINKTITLE, link.opt(BACKLINKTITLE));
        params.set(linkName + TITLEMODE, link.opt(TITLEMODE));
        params.set(linkName + EXPLICITTITLE, link.opt(EXPLICITTITLE));
      }
    }
  }

  @Override
  public void saveJSON(JSONSaver jsonSaver,
                       PersistentRecord node,
                       PersistentRecord regionStyle,
                       PersistentRecord gntmlInstance,
                       GntmlDocument gntmlDocument,
                       JSONObject jGntml)
      throws JSONException
  {
    RecordList rLinks = gntmlInstance.opt(EmbeddedLink.REFERENCES).getRecordList();
    if (rLinks.size() > 0) {
      JSONObject jLinks = new WritableJSONObject();
      for (PersistentRecord rLink : rLinks) {
        if (Boolean.TRUE.equals(rLink.opt(ISREFERENCED))) {
          JSONObject jLink = new WritableJSONObject();
          jLink.put(NODE_ID.getKeyName(), rLink.opt(NODE_ID));
          jLink.put(CONTENTS.getKeyName(), rLink.opt(CONTENTS));
          jLink.put(HASBACKLINKS.getKeyName(), rLink.opt(HASBACKLINKS));
          jLink.put(BACKLINKTITLE.getKeyName(), rLink.opt(BACKLINKTITLE));
          jLink.put(TITLEMODE.getKeyName(), rLink.opt(TITLEMODE));
          jLink.put(EXPLICITTITLE.getKeyName(), rLink.opt(EXPLICITTITLE));
          jLinks.put(rLink.opt(NAME), jLink);
        }
      }
      if (jLinks.length() > 0) {
        jGntml.put("embeddedLinks", jLinks);
      }
    }
  }

  /**
   * @return true
   */
  @Override
  public boolean isParametric()
  {
    return true;
  }

  @Override
  public void loadJSON(JSONLoader jLoader,
                       PersistentRecord node,
                       PersistentRecord regionStyle,
                       JSONObject jGntml,
                       Settable settable)
      throws SettableException, UnresolvedRecordException, JSONException
  {
    JSONObject jLinks = jGntml.optJSONObject("embeddedLinks");
    if (jLinks != null) {
      for (Iterator<String> i = jLinks.keys(); i.hasNext();) {
        String key = i.next();
        JSONObject jLink = jLinks.getJSONObject(key);
        String nameHeader = "embeddedLinks-" + XmlUtils.encodeName(key) + '-';
        Integer node_id = jLink.optInt(NODE_ID.getKeyName());
        if (node_id != null && node_id > 0) {
          Integer live_node_id = jLoader.getIdMapper().getMapping(Node.TABLENAME, node_id);
          if (live_node_id == null) {
            throw new UnresolvedRecordException(node,
                                                regionStyle,
                                                RecordReference.getInstance(getNodeManager().getNode(),
                                                                            node_id),
                                                null);
          }
          settable.set(nameHeader + NODE_ID, live_node_id);
        }
        settable.set(nameHeader + CONTENTS, jLink.opt(CONTENTS.getKeyName()));
        settable.set(nameHeader + HASBACKLINKS, jLink.opt(HASBACKLINKS.getKeyName()));
        settable.set(nameHeader + BACKLINKTITLE, jLink.opt(BACKLINKTITLE.getKeyName()));
      }
    }
  }
}
