package org.awtwf.gntml;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;
import org.cord.util.NamedImpl;

import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;

public abstract class TableBlockCC<T>
  extends NamedImpl
{
  private final TableBlockFactory __tableBlockFactory;
  private final Supplier<T> __defaultValue;

  public TableBlockCC(String name,
                      TableBlockFactory tableBlockFactory,
                      Supplier<T> defaultValue)
  {
    super(name);
    __tableBlockFactory = Preconditions.checkNotNull(tableBlockFactory, "tableBlockFactory");
    __defaultValue = Preconditions.checkNotNull(defaultValue, "defaultValue");
  }

  /**
   * Perform any initialisation work necessary on the table for the containing TableBlockFactory.
   * This will be invoked when this TableBlockCC is registered on the TableBlockFactory.
   */
  protected void init(String pwd,
                      Table table)
      throws MirrorTableLockedException
  {
  }

  protected final TableBlockFactory getTableBlockFactory()
  {
    return __tableBlockFactory;
  }

  public final T getDefaultValue()
  {
    return __defaultValue.get();
  }

  protected abstract T getValue(TransientRecord instance);

  protected abstract boolean updateTransientInstance(TransientRecord instance,
                                                     Gettable params)
      throws MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException;
}
