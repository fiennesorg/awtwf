package org.awtwf.gntml;

import java.util.ListIterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cord.mirror.Db;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.IntegerField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.MonoRecordOperation;
import org.cord.mirror.operation.MonoRecordOperationKey;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.trigger.AbstractFieldTrigger;
import org.cord.node.NodeManager;
import org.cord.util.Casters;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;

import com.google.common.base.Strings;

public class EmbedVideoFactory
  extends TableBlockFactory
{
  public static final String NAME = "EmbedVideo";

  public final static String TABLENAME = "GntmlEmbedVideo";
  public final static String REFERENCES = "gntmlEmbedVideos";

  public final static String VIEWTEMPLATEPATH = "Gntml/embedVideo.view.wm.html";
  public final static String EDITTEMPLATEPATH = "Gntml/embedVideo.edit.wm.html";

  public EmbedVideoFactory(NodeManager nodeMgr)
  {
    super(nodeMgr,
          NAME,
          "Embed Video",
          "|Video:",
          "|",
          TABLENAME,
          VIEWTEMPLATEPATH,
          "gntml_video",
          "gntml_video_caption",
          EDITTEMPLATEPATH,
          null);
  }

  @Override
  protected TableBlock buildTableBlock(ListIterator<String> sourceLines,
                                       GntmlBlockStyle blockStyle,
                                       GntmlDocument gntmlDocument,
                                       int blockId)
      throws GntmlException
  {
    return new EmbedVideoBlock(sourceLines, blockStyle, gntmlDocument, blockId);
  }

  // <object width="640" height="385"><param name="movie"
  // value="http://www.youtube.com/v/tlnMVb2XVVk?fs=1&amp;hl=en_US"></param><param
  // name="allowFullScreen" value="true"></param><param
  // name="allowscriptaccess" value="always"></param><embed
  // src="http://www.youtube.com/v/tlnMVb2XVVk?fs=1&amp;hl=en_US"
  // type="application/x-shockwave-flash" allowscriptaccess="always"
  // allowfullscreen="true" width="640" height="385"></embed></object>

  // <object width="400" height="225"><param name="allowfullscreen"
  // value="true" /><param name="allowscriptaccess" value="always" /><param
  // name="movie"
  // value="http://vimeo.com/moogaloop.swf?clip_id=18073096&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=1&amp;color=00ADEF&amp;fullscreen=1&amp;autoplay=0&amp;loop=0"
  // /><embed
  // src="http://vimeo.com/moogaloop.swf?clip_id=18073096&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=1&amp;color=00ADEF&amp;fullscreen=1&amp;autoplay=0&amp;loop=0"
  // type="application/x-shockwave-flash" allowfullscreen="true"
  // allowscriptaccess="always" width="400" height="225"></embed></object><p><a
  // href="http://vimeo.com/18073096">"A Verse Before Dying"</a> from <a
  // href="http://vimeo.com/user1098580">Proof, Inc.</a> on <a
  // href="http://vimeo.com">Vimeo</a>.</p>

  public final static FieldKey<String> TITLE = FieldKey.create("title", String.class);
  public final static FieldKey<String> HTML = FieldKey.create("html", String.class);
  public final static FieldKey<Integer> WIDTH = FieldKey.create("width", Integer.class);
  public final static FieldKey<Integer> HEIGHT = FieldKey.create("height", Integer.class);

  public final static RecordOperationKey<Boolean> ISVALID =
      RecordOperationKey.create("isValid", Boolean.class);
  public final static RecordOperationKey<String> ERRORMESSAGE =
      RecordOperationKey.create("errorMessage", String.class);

  public final static MonoRecordOperationKey<Integer, String> ATWIDTH =
      MonoRecordOperationKey.create("atWidth", Integer.class, String.class);

  private static final Pattern WIDTH_PATTERN = Pattern.compile("width=\"(\\d+)\"");

  private static final Pattern HEIGHT_PATTERN = Pattern.compile("height=\"(\\d+)\"");

  /**
   * @return The appropriate value or null if it wasn't possible to match the given pattern inside
   *         code.
   */
  private String getValue(String code,
                          Pattern pattern)
  {
    Matcher m = pattern.matcher(code);
    if (m.find()) {
      return m.group(1);
    }
    return null;
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table table)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    table.addField(new StringField(TITLE, "Video Title"));
    table.addField(new StringField(HTML, "External Embed Code", StringField.LENGTH_TEXT, "").setMayBeTextArea(true));
    table.addField(new IntegerField(WIDTH, "Original width"));
    table.addField(new IntegerField(HEIGHT, "Original height"));
    table.addFieldTrigger(new AbstractFieldTrigger<String>(HTML,
                                                           true,
                                                           false) {
      @Override
      protected Object triggerField(TransientRecord record,
                                    String fieldName,
                                    Transaction transaction)
          throws MirrorFieldException
      {
        String code = record.opt(HTML);
        record.setField(WIDTH, getValue(code, WIDTH_PATTERN));
        record.setField(HEIGHT, getValue(code, HEIGHT_PATTERN));
        return null;
      }
    });
    table.addRecordOperation(new MonoRecordOperation<Integer, String>(ATWIDTH,
                                                                      Casters.TOINT) {
      @Override
      public String calculate(TransientRecord targetRecord,
                              Viewpoint viewpoint,
                              Integer width)
      {
        return scaleEmbedCode(targetRecord, width);
      }
    });
    table.addRecordOperation(new SimpleRecordOperation<Boolean>(ISVALID) {
      @Override
      public Boolean getOperation(TransientRecord targetRecord,
                                  Viewpoint viewpoint)
      {
        return targetRecord.comp(WIDTH) > 0 && targetRecord.comp(HEIGHT) > 0;
      }
    });
    table.addRecordOperation(new SimpleRecordOperation<String>(ERRORMESSAGE) {
      @Override
      public String getOperation(TransientRecord targetRecord,
                                 Viewpoint viewpoint)
      {
        if (targetRecord.opt(ISVALID)) {
          return null;
        }
        if (Strings.isNullOrEmpty(targetRecord.opt(HTML))) {
          return "You must copy the embed code from youtube or vimeo "
                 + "into the External Embed Code field";
        }
        if (targetRecord.comp(WIDTH) <= 0) {
          return "It has not been possible to extract the width from your embed code.  "
                 + "Please double check that you have copied it without changing it.";
        }
        return "There has been an unknown error while trying to process your embed code";
      }

    });
  }

  private String attribute(String name,
                           int value)
  {
    return name + "=\"" + value + "\"";
  }

  private String css(String name,
                     int value)
  {
    return name + ": " + value;
  }

  public String scaleEmbedCode(TransientRecord instance,
                               int newWidth)
  {
    String code = instance.opt(HTML);
    int oldWidth = instance.comp(WIDTH);
    int oldHeight = instance.comp(HEIGHT);
    float zoom = ((float) newWidth) / ((float) oldWidth);
    int newHeight = (int) (oldHeight * zoom);
    code = code.replace(attribute("width", oldWidth), attribute("width", newWidth));
    code = code.replace(css("width", oldWidth), css("width", newWidth));
    code = code.replace(attribute("height", oldHeight), attribute("height", newHeight));
    code = code.replace(css("height", oldHeight), css("height", newHeight));
    return code;
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
      throws GntmlException
  {
  }

  public class EmbedVideoBlock
    extends TableBlock
  {
    protected EmbedVideoBlock(ListIterator<String> sourceLines,
                              GntmlBlockStyle blockStyle,
                              GntmlDocument gntmlDocument,
                              int blockId) throws GntmlException
    {
      super(EmbedVideoFactory.this.getName(),
            sourceLines,
            blockStyle,
            gntmlDocument,
            EmbedVideoFactory.this,
            blockId);
    }

    @Override
    public void copyGntmlBlock(Settable settable,
                               PersistentRecord fromNode,
                               PersistentRecord fromRegionStyle,
                               PersistentRecord toRegionStyle,
                               PersistentRecord fromGntmlInstance,
                               GntmlDocument gntmlDocument,
                               PersistentRecord instance)
        throws SettableException
    {
      settable.set(HTML, instance.opt(HTML));
    }

    @Override
    protected void updateInstance(WritableRecord instance,
                                  Gettable params,
                                  Map<Object, Object> outputs)
        throws GntmlException, MirrorException
    {
      instance.setFieldIfDefined(TITLE, params.get(TITLE));
      instance.setFieldIfDefined(HTML, params.get(HTML));
    }
  }
}
