package org.awtwf.gntml;

import org.cord.image.ImageStyleTable;
import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.DirectCachingSimpleRecordOperation;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.Table;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.JSONObjectField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.operation.TableStringMatcher;
import org.cord.node.cc.BinaryAssetStyle;
import org.cord.node.cc.StyleTableWrapper;
import org.cord.util.LogicException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONObject;

import com.google.common.base.Preconditions;

public class GntmlStyle
  extends StyleTableWrapper
{
  /**
   * "GntmlStyle"
   */
  public final static String TABLENAME = "GntmlStyle";

  public final static FieldKey<String> EDITCSS = FieldKey.create("editCss", String.class);

  public final static FieldKey<String> VIEWCSS = FieldKey.create("viewCss", String.class);

  public final static FieldKey<Integer> DEFAULTIMAGESTYLE_ID =
      FieldKey.create("defaultImageStyle_id", Integer.class);

  public final static RecordOperationKey<PersistentRecord> DEFAULTIMAGESTYLE =
      LinkField.getLinkName(DEFAULTIMAGESTYLE_ID);

  public final static RecordOperationKey<RecordSource> SINGLEIMAGESTYLE_DEFAULTGNTMLSTYLES =
      RecordOperationKey.create("defaultGntmlStyles", RecordSource.class);

  public final static FieldKey<Integer> DEFAULTZOOMEDIMAGESTYLE_ID =
      FieldKey.create("defaultZoomedImageStyle_id", Integer.class);

  public final static RecordOperationKey<PersistentRecord> DEFAULTZOOMEDIMAGESTYLE =
      LinkField.getLinkName(DEFAULTZOOMEDIMAGESTYLE_ID);

  public final static RecordOperationKey<RecordSource> SINGLEIMAGESTYLE_DEFAULTZOOMEDGNTMLSTYLES =
      RecordOperationKey.create("defaultZoomedGntmlStyles", RecordSource.class);

  public final static FieldKey<Integer> DEFAULTDOWNLOADSTYLE_ID =
      FieldKey.create("defaultDownloadStyle_id", Integer.class);

  public final static RecordOperationKey<PersistentRecord> DEFAULTDOWNLOADSTYLE =
      LinkField.getLinkName(DEFAULTDOWNLOADSTYLE_ID);

  public final static RecordOperationKey<RecordSource> BINARYASSETSTYLE_DEFAULTGNTMLSTYLES =
      RecordOperationKey.create("defaultGntmlStyles", RecordSource.class);

  public final static RecordOperationKey<RecordSource> REFERENCES =
      RecordOperationKey.create("gntmlStyles", RecordSource.class);

  public final static String GNTMLSTYLESINGLEIMAGESTYLE_NAME = "name";

  public final static RecordOperationKey<RecordSource> GNTMLDOCUMENTS = Gntml.REFERENCES;

  public final static FieldKey<String> CONFIGVALUE = FieldKey.create("configValue", String.class);
  public final static RecordOperationKey<JSONObject> CONFIG =
      RecordOperationKey.create("config", JSONObject.class);

  /**
   * RecordOperation that returns the {@link DefaultGntmlBlockStyle} for this GntmlStyle. The result
   * is cached so can be invoked frequently if required.
   */
  public final static RecordOperationKey<DefaultGntmlBlockStyle> DEFAULTGNTMLBLOCKSTYLE =
      RecordOperationKey.create("defaultGntmlBlockStyle", DefaultGntmlBlockStyle.class);

  /**
   * RecordOperation that returns the number of grid units wide that GntmlStyle represents.
   * 
   * @see #getGridWidth(TransientRecord)
   */
  public static final RecordOperationKey<Integer> GRIDWIDTH =
      RecordOperationKey.create("gridWidth", Integer.class);

  /**
   * @see #getGridTotalWidth(TransientRecord)
   */
  public static final RecordOperationKey<Integer> GRIDTOTALWIDTH =
      RecordOperationKey.create("gridTotalWidth", Integer.class);

  private final GntmlMgr __gntmlMgr;

  public GntmlStyle(GntmlMgr gntmlMgr)
  {
    super(TABLENAME);
    __gntmlMgr = Preconditions.checkNotNull(gntmlMgr, "gntmlMgr");
  }

  protected final GntmlMgr getGntmlMgr()
  {
    return __gntmlMgr;
  }

  @Override
  public void shutdown()
  {
  }

  /**
   * @return The appropriately named GntmlStyle or null if there is no style with that name.
   */
  public PersistentRecord getInstance(String name)
  {
    return TableStringMatcher.optOnlyMatch(getTable(), NAME, name, null);
  }

  @Override
  protected void initTableSub(Db db,
                              String pwd,
                              Table styleTable)
      throws MirrorTableLockedException
  {
    styleTable.addField(new StringField(EDITCSS, "Edit CSS class"));
    styleTable.addField(new StringField(VIEWCSS, "View CSS class"));
    styleTable.addField(new LinkField(DEFAULTIMAGESTYLE_ID,
                                      "Default image style",
                                      null,
                                      DEFAULTIMAGESTYLE,
                                      ImageStyleTable.SINGLEIMAGESTYLE,
                                      SINGLEIMAGESTYLE_DEFAULTGNTMLSTYLES,
                                      null,
                                      pwd,
                                      Dependencies.LINK_ENFORCED | Dependencies.BIT_OPTIONAL));
    styleTable.addField(new LinkField(DEFAULTZOOMEDIMAGESTYLE_ID,
                                      "Default zoomed image style",
                                      null,
                                      DEFAULTZOOMEDIMAGESTYLE,
                                      ImageStyleTable.SINGLEIMAGESTYLE,
                                      SINGLEIMAGESTYLE_DEFAULTZOOMEDGNTMLSTYLES,
                                      null,
                                      pwd,
                                      Dependencies.LINK_ENFORCED | Dependencies.BIT_OPTIONAL));
    styleTable.addField(new LinkField(DEFAULTDOWNLOADSTYLE_ID,
                                      "Default download style",
                                      null,
                                      DEFAULTDOWNLOADSTYLE,
                                      BinaryAssetStyle.TABLENAME,
                                      BINARYASSETSTYLE_DEFAULTGNTMLSTYLES,
                                      null,
                                      pwd,
                                      Dependencies.LINK_ENFORCED));
    styleTable.addField(new JSONObjectField(CONFIGVALUE,
                                            "GNTML Style Configuration",
                                            CONFIG,
                                            new WritableJSONObject()));
    styleTable.addRecordOperation(new SimpleRecordOperation<Integer>(GRIDWIDTH) {
      @Override
      public Integer getOperation(TransientRecord gntmlStyle,
                                  Viewpoint viewpoint)
      {
        return getGridWidth(gntmlStyle);
      }
    });
    styleTable.addRecordOperation(new SimpleRecordOperation<Integer>(GRIDTOTALWIDTH) {
      @Override
      public Integer getOperation(TransientRecord gntmlStyle,
                                  Viewpoint viewpoint)
      {
        return getGridTotalWidth(gntmlStyle);
      }
    });
    styleTable.addRecordOperation(new DirectCachingSimpleRecordOperation<DefaultGntmlBlockStyle>(DEFAULTGNTMLBLOCKSTYLE) {
      @Override
      public DefaultGntmlBlockStyle calculate(TransientRecord gntmlStyle,
                                              Viewpoint viewpoint)
      {
        return new DefaultGntmlBlockStyle(getGntmlMgr(), gntmlStyle);
      }
    });
  }

  public static JSONObject getConfig(TransientRecord gntmlStyle)
  {
    return gntmlStyle.opt(CONFIG);
  }

  public static String CONFIG_GRIDWIDTH = "gridWidth";
  public static String CONFIG_GRIDMARGINLEFT = "gridMarginLeft";
  public static String CONFIG_GRIDMARGINRIGHT = "gridMarginRight";

  /**
   * The width of the body component of this GntmlStyle as defined in {@link #CONFIG_GRIDWIDTH}
   */
  public static int getGridWidth(TransientRecord gntmlStyle)
  {
    return getConfig(gntmlStyle).optInt(CONFIG_GRIDWIDTH, 0);
  }

  public static String getCssMarginLeft(TransientRecord gntmlStyle)
  {
    int i = getGridMarginLeft(gntmlStyle);
    if (i == 0) {
      return null;
    }
    return "grid_i_l_" + i;
  }

  public static String getCssMarginRight(TransientRecord gntmlStyle)
  {
    int i = getGridMarginRight(gntmlStyle);
    if (i == 0) {
      return null;
    }
    return "grid_i_r_" + i;
  }

  /**
   * Look up the number of grid cells that are assigned as the left margin of this document style or
   * return 0 if not defined.
   */
  public static int getGridMarginLeft(TransientRecord gntmlStyle)
  {
    return getConfig(gntmlStyle).optInt(CONFIG_GRIDMARGINLEFT, 0);
  }

  /**
   * Loop up the number of grid cells that are assigned as the right margin of this document style
   * or return 0 if not defined.
   */
  public static int getGridMarginRight(TransientRecord gntmlStyle)
  {
    return getConfig(gntmlStyle).optInt(CONFIG_GRIDMARGINRIGHT, 0);
  }

  /**
   * Return the width of the GntmlStyle including both margins in grid units.
   */
  public static int getGridTotalWidth(TransientRecord gntmlStyle)
  {
    return getGridMarginLeft(gntmlStyle) + getGridWidth(gntmlStyle)
           + getGridMarginRight(gntmlStyle);
  }

  public PersistentRecord createStyle(String name,
                                      boolean isOptional,
                                      boolean isDefined,
                                      String editCss,
                                      String viewCss,
                                      int marginLeft,
                                      int gridWidth,
                                      int marginRight)
      throws MirrorInstantiationException, MirrorFieldException
  {
    PersistentRecord existingStyle = getInstance(name);
    if (existingStyle != null) {
      throw new IllegalArgumentException(String.format("Cannot create a GntmlStyle named %s because it conflicts with %s",
                                                       name,
                                                       existingStyle));
    }
    TransientRecord newStyle = getTable().createTransientRecord();
    newStyle.setField(NAME, name);
    newStyle.setField(ISOPTIONAL, isOptional);
    newStyle.setField(ISDEFINED, isDefined);
    newStyle.setField(EDITCSS, editCss);
    newStyle.setField(VIEWCSS, viewCss);
    newStyle.setField(DEFAULTDOWNLOADSTYLE_ID, 1);
    JSONObject config = new WritableJSONObject();
    try {
      config.putOpt(CONFIG_GRIDMARGINLEFT, marginLeft);
      config.putOpt(CONFIG_GRIDWIDTH, gridWidth);
      config.putOpt(CONFIG_GRIDMARGINRIGHT, marginRight);
    } catch (JSONException e) {
      throw new LogicException("Unexpected", e);
    }
    newStyle.setField(CONFIGVALUE, config);
    return newStyle.makePersistent(null);
  }
}