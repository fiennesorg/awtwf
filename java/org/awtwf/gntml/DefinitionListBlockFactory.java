package org.awtwf.gntml;

import java.io.IOException;
import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;

public class DefinitionListBlockFactory
  extends EnglishNamedImpl
  implements GntmlBlockFactory
{
  public final static String NAME = "Definition List";

  public DefinitionListBlockFactory()
  {
    super(NAME);
  }

  @Override
  public boolean acceptsSource(GntmlDocument gntmlDocument,
                               String firstLine)
  {
    return firstLine.indexOf("::") > 0;
  }

  @Override
  public GntmlBlock buildBlock(ListIterator<String> sourceLines,
                               GntmlBlockStyle blockStyle,
                               GntmlDocument gntmlDocument,
                               int blockId)
  {
    return new DefinitionListBlock(sourceLines, blockStyle, gntmlDocument, blockId);
  }

  /**
   * Do nothing
   */
  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
  {
  }

  /**
   * Do nothing
   */
  @Override
  public void preParse(GntmlDocument gntmlDocument,
                       Transaction transaction)
  {
  }

  public class DefinitionListBlock
    extends GntmlBlock
  {
    protected DefinitionListBlock(ListIterator<String> sourceLines,
                                  GntmlBlockStyle blockStyle,
                                  GntmlDocument gntmlDocument,
                                  int blockId)
    {
      super(NAME,
            sourceLines,
            blockStyle,
            gntmlDocument,
            DefinitionListBlockFactory.this,
            blockId);
    }

    @Override
    public Appendable parse(Appendable xhtml,
                            Gettable documentParams,
                            Gettable blockParams,
                            Map<Object, Object> outputs)
        throws GntmlException, IOException
    {
      openDivs(xhtml);
      xhtml.append("<dl>\n");
      ListIterator<GntmlLine> lines = getLineList();
      boolean hasListLine = false;
      while (lines.hasNext()) {
        DefinitionListLine line = (DefinitionListLine) lines.next();
        if (line.isListLine()) {
          if (hasListLine) {
            xhtml.append("</dd>\n");
          }
          hasListLine = true;
        } else {
          xhtml.append("<br />\n");
        }
        line.parse(xhtml, blockParams);
      }
      xhtml.append("</dd>\n</dl>\n");
      closeDivs(xhtml);
      return xhtml;
    }

    /**
     * Do nothing
     */
    @Override
    public void copyGntmlBlock(Settable settable,
                               PersistentRecord fromNode,
                               PersistentRecord fromRegionStyle,
                               PersistentRecord toRegionStyle,
                               PersistentRecord fromGntmlInstance,
                               GntmlDocument gntmlDocument)
        throws SettableException
    {
    }

    /**
     * @return a DefinitionListLine bound to this block
     * @see DefinitionListLine
     */
    @Override
    protected GntmlLine createGntmlLine(String source)
    {
      return new DefinitionListLine(source, this);
    }
  }
}
