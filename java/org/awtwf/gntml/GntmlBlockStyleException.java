package org.awtwf.gntml;

public class GntmlBlockStyleException
  extends GntmlException
{
  private static final long serialVersionUID = 1L;

  private final String __blockStyleDeclaration;

  /**
   * @param blockStyleDeclaration
   *          The contents of the {} declaration that caused the problem. We don't need the { & }
   *          characters.
   */
  public GntmlBlockStyleException(String message,
                                  String blockStyleDeclaration,
                                  Throwable cause)
  {
    super(message,
          cause);
    __blockStyleDeclaration = blockStyleDeclaration;
  }

  public GntmlBlockStyleException(String message,
                                  String blockStyleDeclaration)
  {
    this(message,
         blockStyleDeclaration,
         null);
  }

  /**
   * Get the value inside the {} characters that caused the GntmlBlockStyleException to be thrown.
   */
  public String getBlockStyleDeclaration()
  {
    return __blockStyleDeclaration;
  }
}
