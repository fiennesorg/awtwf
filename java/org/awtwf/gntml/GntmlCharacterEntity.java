package org.awtwf.gntml;

import org.cord.sql.SqlUtil;
import org.cord.util.NamedImpl;

/**
 * A mapping between a single char and a target String thereby enabling html extended encodings of
 * problematic characters.
 * 
 * @author alex
 */
public class GntmlCharacterEntity
  extends NamedImpl
  implements GntmlTag
{
  private final char _sourceChar;

  private final String _objectString;

  private final String _usageExample;

  public GntmlCharacterEntity(String name,
                              char sourceChar,
                              String objectString)
  {
    super(name);
    _sourceChar = sourceChar;
    _objectString = objectString;
    _usageExample = calculateUsageExample();
  }

  private final String calculateUsageExample()
  {
    return getName() + ":" + _sourceChar + "-->" + _objectString;
  }

  @Override
  public final String getUsageExample()
  {
    return _usageExample;
  }

  @Override
  public final String toString()
  {
    return getUsageExample();
  }

  public final String parse(String sourceString)
  {
    return SqlUtil.replaceChar(sourceString, _sourceChar, _objectString);
  }
}
