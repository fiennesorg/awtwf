package org.awtwf.gntml;

import java.util.NoSuchElementException;

import org.cord.mirror.Db;
import org.cord.mirror.MirrorLogicException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.node.Node;
import org.cord.util.ObjectUtil;
import org.cord.util.StringUtils;
import org.webmacro.InitException;
import org.webmacro.NotFoundException;
import org.webmacro.Provider;
import org.webmacro.ResourceException;
import org.webmacro.Template;
import org.webmacro.WebMacro;
import org.webmacro.engine.StringTemplate;
import org.webmacro.resource.AbstractTemplateLoader;
import org.webmacro.resource.CacheElement;
import org.webmacro.resource.CacheReloadContext;
import org.webmacro.resource.DelegatingTemplateProvider;
import org.webmacro.resource.TemplateLoader;

import com.google.common.base.Preconditions;

public class GntmlTemplateLoader
  extends AbstractTemplateLoader
{
  public static void init(WebMacro webmacro,
                          Db db)
      throws NotFoundException
  {
    if (webmacro != null) {
      Table gntml = db.getTable(Gntml.TABLENAME);
      Table node = db.getTable(Node.TABLENAME);
      Preconditions.checkState(gntml != null && Gntml.TABLENAME.equals(gntml.getTableName()),
                               "%s is not a GntmlInstance table",
                               gntml);
      Provider provider = webmacro.getBroker().getProvider("template");
      DelegatingTemplateProvider delegatingTemplateProvider =
          ObjectUtil.castTo(provider, DelegatingTemplateProvider.class);
      for (TemplateLoader templateLoader : delegatingTemplateProvider.getTemplateLoaders()) {
        if (templateLoader instanceof GntmlTemplateLoader) {
          ((GntmlTemplateLoader) templateLoader)._gntml = gntml;
          ((GntmlTemplateLoader) templateLoader)._node = node;
          return;
        }
      }
      throw new NoSuchElementException("No GntmlTemplateLoader registered");
    }
  }

  public static final String PROTOCOL = "gntml:";

  private final Template __emptyTemplate;

  public GntmlTemplateLoader()
  {
    __emptyTemplate = new StringTemplate(this.broker, "", "EMPTY");
  }

  private Table _gntml;
  private Table _node;

  public String getGntmlInstanceId(String query)
  {
    return query.substring(PROTOCOL.length());
  }

  @Override
  public Template load(String query,
                       CacheElement ce)
      throws ResourceException
  {
    if (!query.startsWith(PROTOCOL)) {
      return null;
    }
    // ce.setReloadContext(new TableReloadContext(_gntml));
    try {
      PersistentRecord gntmlInstance = _gntml.getRecord(getGntmlInstanceId(query));
      ce.setReloadContext(new NodeReloadContext(_node,
                                                gntmlInstance.opt(Gntml.NODE_ID),
                                                System.currentTimeMillis()));
      String template = gntmlInstance.opt(Gntml.HTML);
      if (StringUtils.isWhitespace(template)) {
        return __emptyTemplate;
      }
      return new StringTemplate(this.broker, gntmlInstance.opt(Gntml.HTML), query);
    } catch (MirrorNoSuchRecordException e) {
      throw new ResourceException("Unable to resolve GNTML record from " + query, e);
    }
  }
  @Override
  public void setConfig(String config)
      throws InitException
  {
  }

  static class NodeReloadContext
    extends CacheReloadContext
  {
    private final Table __nodes;
    private final Integer __nodeId;
    private final long __timestamp;

    NodeReloadContext(Table nodes,
                      Integer nodeId,
                      long timestamp)
    {
      __nodes = nodes;
      __nodeId = nodeId;
      __timestamp = timestamp;
    }

    @Override
    public boolean shouldReload()
    {
      try {
        return __nodes.getRecord(__nodeId).opt(Node.MODIFICATIONTIME).getTime() > __timestamp;
      } catch (MirrorNoSuchRecordException e) {
        throw new MirrorLogicException(String.format("Unable to resolve Node#", __nodeId), e);
      }
    }
  }
}
