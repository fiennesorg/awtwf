package org.awtwf.gntml;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.DbInitialiser;
import org.cord.mirror.DbInitialisers;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.node.CurrentUrlFactory;
import org.cord.node.NodeManager;
import org.cord.util.DuplicateNamedException;
import org.cord.util.NotImplementedException;
import org.cord.webmacro.AppendableConstantMacro;
import org.webmacro.Macro;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class GntmlMgr
{
  public static GntmlMgr createAndRegister(NodeManager nodeMgr,
                                           String pwd)
      throws MirrorTableLockedException
  {
    GntmlMgr gntmlMgr = new GntmlMgr(nodeMgr, pwd);
    nodeMgr.getContentCompilerFactory().register(gntmlMgr.getGntml());
    return gntmlMgr;
  }

  private final NodeManager __nodeMgr;
  private final List<GntmlCharacterEntity> __characterEntities = Lists.newArrayList();
  private final Map<String, InlineTag> __inlineTags = Maps.newLinkedHashMap();
  private final Map<String, GntmlBlockFactory> __blockFactories = Maps.newLinkedHashMap();
  private final GntmlBlockFactory __defaultBlockFactory;

  private final Gntml __gntml;
  private final GntmlStyle __gntmlStyle;

  private final GntmlBlockStyleProvider __gntmlBlockStyleProvider;

  protected GntmlMgr(NodeManager nodeMgr,
                     String pwd)
  {
    __nodeMgr = Preconditions.checkNotNull(nodeMgr, "nodeMgr");
    __defaultBlockFactory = new ParagraphBlockFactory();
    __gntmlStyle = new GntmlStyle(this);
    __gntml = new Gntml(this, pwd, __gntmlStyle);
    __gntmlBlockStyleProvider = new GntmlBlockStyleProvider(this);
  }

  public final GntmlBlockStyleProvider getGntmlBlockStyleProvider()
  {
    return __gntmlBlockStyleProvider;
  }

  public final Gntml getGntml()
  {
    return __gntml;
  }

  public final GntmlStyle getGntmlStyle()
  {
    return __gntmlStyle;
  }

  public GntmlDocument getDocument(Integer documentId,
                                   String source,
                                   String downloadNameSpace,
                                   Transaction transaction)
      throws GntmlException
  {
    return new GntmlDocument(this, documentId, source, downloadNameSpace, transaction);
  }

  public final NodeManager getNodeMgr()
  {
    return __nodeMgr;
  }

  public void registerCharacterEntity(GntmlCharacterEntity characterEntity)
  {
    assertIsNotLocked();
    if (characterEntity != null && !__characterEntities.contains(characterEntity)) {
      __characterEntities.add(characterEntity);
    }
  }

  public List<GntmlCharacterEntity> getCharacterEntities()
  {
    return Collections.unmodifiableList(__characterEntities);
  }

  /**
   * @return The InlineTag that was previously registered under the same name and which will have
   *         been replaced with inlineTag, or null if inlineTag was registered without replacing
   *         anything.
   */
  public final InlineTag registerInlineTag(InlineTag inlineTag)
  {
    assertIsNotLocked();
    Preconditions.checkNotNull(inlineTag, "inlineTag");
    return __inlineTags.put(inlineTag.getName(), inlineTag);
  }

  /**
   * Get an unmodifiable List of all the registered InlineTags that this Parser supports.
   */
  public Map<String, InlineTag> getInlineTags()
  {
    return Collections.unmodifiableMap(__inlineTags);
  }

  public Map<String, GntmlBlockFactory> getGntmlBlockFactories()
  {
    return Collections.unmodifiableMap(__blockFactories);
  }

  public GntmlBlockFactory getDefaultBlockFactory()
  {
    return __defaultBlockFactory;
  }

  /**
   * Register the appropriate GntmlBlockFactory with this GntmlParser thereby making it available in
   * GntmlDocuments. If the GntmlBlockFactory is a DbInitialiser or DbInitialisers then register it
   * with the Db as well.
   * 
   * @throws MirrorTableLockedException
   *           If there is some problem with registering the DbInitialisers. This will normally only
   *           occur if you try and register factories after the Db has been locked.
   * @throws DuplicateNamedException
   *           if there is already a block registered with the same name.
   * @see DbInitialiser
   * @see DbInitialisers
   */
  public final GntmlBlockFactory registerBlock(GntmlBlockFactory blockFactory)
      throws MirrorTableLockedException
  {
    assertIsNotLocked();
    Preconditions.checkNotNull(blockFactory, "blockFactory");
    return __blockFactories.put(blockFactory.getName(), blockFactory);
  }

  private void assertIsNotLocked()
  {
    if (_isLocked) {
      throw new IllegalStateException("GntmlMgr is already locked");
    }
  }

  private volatile boolean _isLocked = false;

  public void lock()
      throws MirrorTableLockedException
  {
    if (_isLocked) {
      throw new IllegalStateException("Cannot lock() a GntmlMgr more than once");
    }
    _isLocked = true;
    for (GntmlBlockFactory blockFactory : getGntmlBlockFactories().values()) {
      addDbInitialisers(blockFactory);
    }
    for (InlineTag inlineTag : getInlineTags().values()) {
      addDbInitialisers(inlineTag);
    }
  }

  private void addDbInitialisers(Object o)
      throws MirrorTableLockedException
  {
    Db db = getNodeMgr().getDb();
    if (o instanceof DbInitialiser) {
      db.add((DbInitialiser) o);
    }
    if (o instanceof DbInitialisers) {
      db.add((DbInitialisers) o);
    }
  }

  /**
   * Register default blocks onto the parser
   * 
   * @see ListBlockFactory
   * @see DownloadBlockFactory
   * @see ImageBlockFactory
   */
  public GntmlMgr registerDefaults()
      throws MirrorTableLockedException
  {
    registerBlock(new BlockquoteBlockFactory());
    registerBlock(new CodeBlockFactory());
    registerBlock(new DefinitionListBlockFactory());
    registerBlock(new DownloadBlockFactory(getNodeMgr()));
    registerBlock(new ScalableImageFactory(getNodeMgr()));
    registerBlock(new NavigationFactory(getNodeMgr()));
    registerBlock(new SiteMapFactory(getNodeMgr()));
    // registerBlock(new ImageBlockFactory(getNodeMgr(), 0));
    // registerBlock(new LegacyHtmlBlockFactory());
    registerBlock(new ListBlockFactory());
    registerBlock(new HtmlBlockFactory(getNodeMgr()));
    registerBlock(new EmbedVideoFactory(getNodeMgr()));
    // registerBlock(new XhtmlTagBlockFactory("h1",
    // "h1 heading",
    // "|h1|",
    // "<h1>",
    // "</h1>"));
    registerBlock(new XhtmlTagBlockFactory("h2", "h2 heading", "|h2|", "<h2>", "</h2>"));
    registerBlock(new XhtmlTagBlockFactory("h3", "h3 heading", "|h3|", "<h3>", "</h3>"));
    registerBlock(new XhtmlTagBlockFactory("h4", "h4 heading", "|h4|", "<h4>", "</h4>"));
    registerBlock(new ConstantBlockFactory("HR",
                                           "====",
                                           "<hr />\n",
                                           "Content is not permitted inside a horizontal rule.<br/> "
                                               + "Leave a blank line after the \"====\""));
    registerBlock(new PageList(getNodeMgr()));
    registerDefaultInlineTags();
    registerBlock(new CyclingImage(getNodeMgr()));
    return this;
  }

  public void registerDefaultInlineTags()
      throws MirrorTableLockedException
  {
    registerInlineTag(new StaticInlineTag("bold",
                                          "Bold",
                                          "**",
                                          "\\*\\*(.*?)\\*\\*",
                                          "<strong>$1</strong>",
                                          null,
                                          null,
                                          null,
                                          null,
                                          false));
    registerInlineTag(new StaticInlineTag("italic",
                                          "Italic",
                                          "\\\\",
                                          "\\\\\\\\(.*?)\\\\\\\\",
                                          "<em>$1</em>",
                                          null,
                                          null,
                                          null,
                                          null,
                                          false));
    registerInlineTag(new StaticInlineTag("underline",
                                          "Underline",
                                          "__",
                                          "__(.*?)__",
                                          "<span style=\"text-decoration: underline\">$1</span>",
                                          null,
                                          null,
                                          null,
                                          null,
                                          false));
    registerInlineTag(new StaticInlineTag("nobreak",
                                          "No Line Break",
                                          "~~",
                                          "~~(.*?)~~",
                                          "<nobr>$1</nobr>",
                                          null,
                                          null,
                                          null,
                                          null,
                                          false));
    registerInlineTag(new StaticInlineTag("externalLink",
                                          "External HTTP link",
                                          "http",
                                          "(.?)(https?://[^\\p{Space}\"\\(]+)(?:\\(([^\"\\)]+)\\))?(.?)",
                                          null,
                                          null,
                                          null,
                                          null,
                                          "$1\n"
                                              + "#eval \\$ga_trackedlink using { \"href\":\"$2\", \"contents\":\"$3\", \"rel\":\"external\" }"
                                              + "\n$4",
                                          true));
    registerInlineTag(new StaticInlineTag("emailLink",
                                          "Email link",
                                          "mailto",
                                          "(mailto:([^\\p{Space}(]+))(?:\\(([^)]+)\\))?",
                                          "<a href=\"$0\">$2</a>",
                                          null,
                                          null,
                                          "<a href=\"$1\">$3</a>",
                                          null,
                                          false));
    registerInlineTag(new StaticInlineTag("ftpLink",
                                          "FTP link",
                                          "ftp://",
                                          "(ftp://[^\\p{Space}(]+)(?:\\(([^)]+)\\))?",
                                          "<a href=\"$0\">$0</a>",
                                          null,
                                          "<a href=\"$1\">$2</a>",
                                          null,
                                          null,
                                          false));
    registerInlineTag(new StaticInlineTag("internalLink",
                                          "Internal Link",
                                          "page:",
                                          "page:([^\\p{Space}(]+)(?:\\(([^)]+)\\))?",
                                          "<a href=\"$1\">$1</a>",
                                          null,
                                          "<a href=\"$1\">$2</a>",
                                          null,
                                          null,
                                          false));
    registerInlineTag(new EmbeddedLink(getNodeMgr(), CurrentUrlFactory.getInstance()));
    registerInlineTag(StaticInlineTag.createHtmlTag("sup", "superscript"));
    registerInlineTag(StaticInlineTag.createHtmlTag("sub", "subscript"));
  }

  /**
   * Utility method to take an CSS class and add a new class onto it with a separating space. It is
   * quite efficient and avoids creating objects if necessary. If both arguments are null then an
   * empty String is returned.
   */
  public static String addCssClass(String existingCssClass,
                                   String newCssClass)
  {
    if (existingCssClass == null) {
      if (newCssClass == null) {
        return "";
      }
      return newCssClass;
    }
    if (newCssClass == null) {
      return existingCssClass;
    }
    return existingCssClass + " " + newCssClass;
  }

  public static void appendOpenInnerDiv(Appendable out)
      throws IOException
  {
    out.append("<div class=\"" + GntmlBlockStyle.INNERCLASS + "\">\n");
  }

  public Macro render(final TransientRecord node,
                      final String regionStyleName)
  {
    return new AppendableConstantMacro() {

      @Override
      public void append(Appendable out)
          throws IOException
      {
        try {
          getNodeMgr().getWebMacro().getTemplate(null);
        } catch (Exception e) {
          e.printStackTrace();
        }
        throw new NotImplementedException();
      }
    };
  }
}
