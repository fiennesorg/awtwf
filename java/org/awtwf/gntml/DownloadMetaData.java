package org.awtwf.gntml;

import java.io.File;

public class DownloadMetaData
{
  private final String __title;

  private final String __urlPath;

  private final String __filename;

  private final int __sizeKb;

  private final String __formatName;

  private final String __description;

  private final File __file;

  public DownloadMetaData(String urlPath,
                          String filename,
                          int sizeKb,
                          String formatName,
                          String title,
                          String description,
                          File file)
  {
    __urlPath = urlPath;
    __filename = filename;
    __sizeKb = sizeKb;
    __formatName = formatName;
    __title = title;
    __description = description;
    __file = file;
  }

  public File getFile()
  {
    return __file;
  }

  public String getTitle()
  {
    return __title;
  }

  public String getUrlPath()
  {
    return __urlPath;
  }

  public String getFilename()
  {
    return __filename;
  }

  public int getSizeKb()
  {
    return __sizeKb;
  }

  public String getFormattedSize()
  {
    if (__sizeKb < 1024) {
      return __sizeKb + "Kb";
    }
    return (__sizeKb / 1024.0) + "Mb";
  }

  public String getFormatName()
  {
    return __formatName;
  }

  public String getDescription()
  {
    return __description;
  }
}