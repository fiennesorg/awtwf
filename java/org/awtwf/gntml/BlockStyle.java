package org.awtwf.gntml;

import java.io.IOException;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cord.util.NumberUtil;
import org.cord.util.StringUtils;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

/**
 * Representation of the core and plus block styles that a GntmlBlock should honour
 * 
 * @author alex
 */
@Deprecated
public final class BlockStyle
{
  private final static Pattern __validStyleDef =
      Pattern.compile("\\s*\\{\\s*(\\w*)(?:\\s*\\+\\s*(\\w*))?\\s*\\}\\s*");

  /**
   * Parse the first line and if it represents a block style declaration then return the appropriate
   * BlockStyle.
   * 
   * @param firstLine
   *          The first line of source text of the GntmlBlock that may or may not be a block style
   *          decleration
   * @return The appropriate BlockStyle or null if it was not a block style declaration
   */
  public static BlockStyle getInstance(GntmlDocument gntmlDocument,
                                       String firstLine)
  {
    Matcher m = __validStyleDef.matcher(firstLine);
    if (m.matches()) {
      return new BlockStyle(gntmlDocument, m.group(1), m.group(2));
    }
    return new BlockStyle(gntmlDocument, null, null);
  }

  public static BlockStyle getInstance(GntmlDocument gntmlDocument,
                                       ListIterator<String> sourceLines)
  {
    String firstLine = null;
    while (sourceLines.hasNext() && Strings.isNullOrEmpty(firstLine)) {
      firstLine = sourceLines.next();
    }
    if (firstLine == null) {
      return null;
    }
    BlockStyle blockStyle = getInstance(gntmlDocument, firstLine);
    if (!blockStyle.hasBlockNames()) {
      sourceLines.previous();
    }
    return blockStyle;
  }

  private final String __blockName;

  private final String __plusBlockName;

  private final GntmlDocument __gntmlDocument;

  // private final Integer __maximumContentWidth;
  //
  // private final Integer __maximumContentHeight;

  private final Integer __cols;
  private final Integer __hBorder;
  private final boolean __isGridX;

  protected BlockStyle(final GntmlDocument gntmlDocument,
                       final String blockName,
                       final String plusBlockName)
  {
    __gntmlDocument = Preconditions.checkNotNull(gntmlDocument, "gntmlDocument");
    __blockName = StringUtils.padEmpty(blockName, null);
    __plusBlockName = StringUtils.padEmpty(plusBlockName, null);
    // PersistentRecord gntmlStyle = gntmlInstance.followEnforcedLink(Gntml.STYLE);
    // int width = 0;
    // int height = 0;
    Integer cols = null;
    Integer hBorder = 0;
    // PersistentRecord singleImageStyle = null;
    // PersistentRecord gntmlImageStyle =
    // gntmlManager.getGntmlImageStyle().getOptionalInstance(gntmlStyle, blockName, null);
    // if (gntmlImageStyle != null) {
    // singleImageStyle = gntmlImageStyle.followEnforcedLink(GntmlImageStyle.SINGLEIMAGESTYLE);
    // }
    // if (singleImageStyle == null) {
    // singleImageStyle = gntmlStyle.followOptionalLink(GntmlStyle.DEFAULTIMAGESTYLE);
    // }
    // if (singleImageStyle != null) {
    // String name = singleImageStyle.getString(SingleImageStyle.NAME);
    // if (name.startsWith("grid_")) {
    // cols = singleImageStyle.getId() - 100;
    // }
    // width = singleImageStyle.getIntField(ImageStyleTable.TARGETWIDTH);
    // height = singleImageStyle.getIntField(ImageStyleTable.TARGETHEIGHT);
    // PersistentRecord gntmlPlusBlockStyle =
    // gntmlManager.getGntmlPlusBlockStyle()
    // .getOptionalInstance(gntmlStyle, plusBlockName, null);
    // if (gntmlPlusBlockStyle != null) {
    // hBorder = gntmlPlusBlockStyle.getIntField(GntmlPlusBlockStyle.HORIZONTALBORDER);
    // width -= gntmlPlusBlockStyle.getIntField(GntmlPlusBlockStyle.HORIZONTALBORDER);
    // height -= gntmlPlusBlockStyle.getIntField(GntmlPlusBlockStyle.VERTICALBORDER);
    // }
    // }
    // if (width == 0) {
    // __maximumContentWidth = null;
    // __maximumContentHeight = null;
    // } else {
    // __maximumContentWidth = Integer.valueOf(width);
    // __maximumContentHeight = Integer.valueOf(height);
    // }
    __cols = cols;
    __hBorder = hBorder;
    __newGridWidth = getGridWidth(getBlockName(), gntmlDocument.getGridWidth());
    __isGridX = isGridX(getBlockName());
  }

  protected GntmlDocument getGntmlDocument()
  {
    return __gntmlDocument;
  }

  protected GntmlMgr getGntmlMgr()
  {
    return __gntmlDocument.getGntmlMgr();
  }

  private final int __newGridWidth;

  public Integer getNewGridWidth()
  {
    return __newGridWidth;
  }

  public boolean outputsWebmacro()
  {
    return getNewGridWidth() != null;
  }

  /**
   * Extract the width of the blockStyle in grid units or the default width of the document if not
   * defined. This will look for grid_x, left_x, right_x and center_x definitions.
   */
  public static int getGridWidth(String blockName,
                                 int defaultGridWidth)
  {
    if (blockName != null
        && blockName.length() > 0
        && (blockName.startsWith("grid_") || blockName.startsWith("left_")
            || blockName.startsWith("right_") || blockName.startsWith("center_"))) {
      return NumberUtil.toInteger(blockName.substring(blockName.indexOf('_') + 1), defaultGridWidth);
    }
    return defaultGridWidth;
  }

  public final boolean isGridX()
  {
    return __isGridX;
  }

  /**
   * Check to see whether this blockName is a grid_x type by looking to see if it starts with
   * "grid_"
   */
  public static boolean isGridX(String blockName)
  {
    return blockName != null && blockName.startsWith("grid_");
  }

  /**
   * Get the width of this BlockStyle in grid units if defined.
   * 
   * @return The width in grid units or null if it is not clear what it is.
   */
  public Integer getGridWidth()
  {
    return __cols;
  }

  public void appendBlockStyle_width(Appendable out)
      throws IOException
  {
    TableBlock.appendSet(out,
                         "blockStyle_width",
                         String.format("$Grid.getWidth(%s,%s)", __newGridWidth, __hBorder));
  }

  public boolean hasBlockNames()
  {
    return __blockName != null || __plusBlockName != null;
  }

  // protected Gntml getGntmlManager()
  // {
  // return __gntmlManager;
  // }

  // /**
  // * Get the maximum width of this element in pixels if known.
  // *
  // * @return The width in pixels or null if the width is not known.
  // */
  // @Deprecated
  // public Integer getMaximumContentWidth()
  // {
  // return __maximumContentWidth;
  // }
  //
  // /**
  // * Get the maximum height of this element in pixels if known
  // *
  // * @return The height in pixels or null if the height is not known.
  // */
  // @Deprecated
  // public Integer getMaximumContentHeight()
  // {
  // return __maximumContentHeight;
  // }

  public String getBlockName()
  {
    return __blockName;
  }

  public String getPlusBlockName()
  {
    return __plusBlockName;
  }

  private int _divCount = 0;

  private void spacer(Appendable out,
                      boolean needsSpacer)
      throws IOException
  {
    if (needsSpacer) {
      out.append(" ");
    }
  }

  public void appendOpenDiv(Appendable out,
                            String baseClass,
                            String id)
      throws IOException
  {
    boolean needsSpacer = false;
    String blockName =
        __blockName == null && StringUtils.notEmpty(baseClass) ? "default" : __blockName;
    if (blockName != null || __plusBlockName != null || StringUtils.notEmpty(baseClass)
        || StringUtils.notEmpty(id)) {
      out.append("<div class=\"");
      if (blockName != null) {
        out.append("gntml_").append(blockName);
        needsSpacer = true;
      }
      if (__plusBlockName != null) {
        spacer(out, needsSpacer);
        out.append("gntml_plus_").append(__plusBlockName);
        needsSpacer = true;
      }
      if (StringUtils.notEmpty(baseClass)) {
        spacer(out, needsSpacer);
        out.append(baseClass);
      }
      out.append("\"");
      if (StringUtils.notEmpty(id)) {
        out.append(" id=\"").append(id).append("\"");
      }
      out.append(">");
      _divCount++;
    }
    if (blockName != null || __plusBlockName != null) {
      needsSpacer = false;
      out.append("<div class=\"");
      if (blockName != null) {
        out.append("gntml_").append(blockName).append("_i");
        needsSpacer = true;
      }
      if (__plusBlockName != null) {
        spacer(out, needsSpacer);
        out.append("gntml_plus_").append(__plusBlockName).append("_i");
        needsSpacer = true;
      }
      out.append("\">");
      _divCount++;
    }
    if (getNewGridWidth() != null) {
      out.append("#set $grid_width_outer = $grid_width\n");
      out.append("#set $grid_width = ").append(getNewGridWidth().toString()).append("\n");
    }
    if (_divCount > 0) {
      out.append('\n');
    }
  }

  public void appendCloseDiv(Appendable out)
      throws IOException
  {
    for (int i = 0; i < _divCount; i++) {
      out.append("</div>");
    }
    if (_divCount > 0) {
      out.append('\n');
    }
    if (getNewGridWidth() != null) {
      out.append("#set $grid_width = $grid_width_outer\n");
    }
    out.append('\n');
  }

  @Override
  public String toString()
  {
    return String.format("BlockStyle(%s,%s)", __blockName, __plusBlockName);
  }
}
