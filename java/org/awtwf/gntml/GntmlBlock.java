package org.awtwf.gntml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.node.FeedbackErrorException;
import org.cord.node.json.JSONLoader;
import org.cord.node.json.JSONSaver;
import org.cord.node.json.UnresolvedRecordException;
import org.cord.util.Gettable;
import org.cord.util.NamedImpl;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.cord.webmacro.TagBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

/**
 * GntmlBlock is the base class for all blocks that make up the Gntml system. It is responsible for
 * extracting the GntmlLines that make up the block from the source string and keeping track of any
 * global css tags for the block.
 */
public abstract class GntmlBlock
  extends NamedImpl
{
  private final GntmlDocument __gntmlDocument;

  private final GntmlBlockFactory __gntmlBlockFactory;

  private List<GntmlLine> _lines;

  private final int __blockId;

  private final GntmlBlockStyle __blockStyle;

  protected GntmlBlock(String name,
                       ListIterator<String> sourceLines,
                       GntmlBlockStyle gntmlBlockStyle,
                       GntmlDocument gntmlDocument,
                       GntmlBlockFactory gntmlBlockFactory,
                       int blockId)
  {
    super(name);
    __gntmlDocument = gntmlDocument;
    __gntmlBlockFactory = gntmlBlockFactory;
    _lines = new ArrayList<GntmlLine>();
    __blockStyle = Preconditions.checkNotNull(gntmlBlockStyle, "blockStyle");
    __blockId = blockId;
    while (sourceLines.hasNext()) {
      GntmlLine nextLine = createGntmlLine(sourceLines.next());
      if (nextLine.isSeparator()) {
        return;
      }
      addLine(nextLine);
    }
  }

  public final GntmlBlockFactory getGntmlBlockFactory()
  {
    return __gntmlBlockFactory;
  }

  /**
   * Check to see if this block should be interpretted as static text or a webmacro template. The
   * default implementation is just to invoke containsWebmacroLine() thereby checking for embedded
   * template elements inside the lines that make up the block. If your implementation of GntmlBlock
   * always returns webmacro then you can override this and return true. If it is conditional then
   * you should remembed to take into account the embedded lines as part of the result.
   * 
   * @return true if there is a GntmlLine that outputs webmacro
   * @see #containsWebmacroLine()
   */
  public boolean outputsWebmacro()
  {
    return containsWebmacroLine();
  }

  /**
   * Scan through all the currently registered GntmlLines that make up this GntmlBlock and return
   * true if any of them declare that they output webmacro. This is useful when implementing
   * outputsWebmacro in the GntmlBlock.
   * 
   * @see #getLineList()
   * @see #outputsWebmacro()
   * @see GntmlLine#outputsWebmacro()
   * @return true if any of the contained GntmlLines declare that they return webmacro code.
   */
  public boolean containsWebmacroLine()
  {
    Iterator<GntmlLine> lines = getLineList();
    while (lines.hasNext()) {
      if (lines.next().outputsWebmacro()) {
        return true;
      }
    }
    return false;
  }

  /**
   * @return false
   */
  public boolean hasEditWidget()
  {
    return false;
  }

  /**
   * @return null
   */
  public String getEditTemplatePath()
  {
    return null;
  };

  /**
   * Get the identifier for this parsed GntmlBlock if defined.
   * 
   * @return The default implementation doesn't support named blocks so it returns ""
   */
  public String getBlockName()
  {
    return "";
  }

  /**
   * Get the XHTML identifier for this parsed GntmlBlock if defined. This should not contain any
   * characters that are illegal when constructing an XHTML id or name field.
   * 
   * @return The default implementation doesn't support HtmlIds so it returns ""
   */
  public String getXhtmlId()
  {
    return "";
  }

  /**
   * Take the information contained in the source of the GNTML and any subsidiary information
   * defined in the Gettable params and use it to generate the compiled output for this block. This
   * will be invoked for each GntmlBlock in turn when parsing a GntmlDocument.
   * 
   * @param out
   *          The StringBuilder to which the compiled version of the GNTML should be attached. This
   *          may be XHTML or XHTML + webmacro depending on the configuration of the GntmlBLock
   * @param documentParams
   *          The params that are supplied to the document as a whole. If you need access to
   *          non-namespaced values then they will be contained in this Gettable. If the block
   *          doesn't have a block name then it will be the same as documentParams.
   * @param blockParams
   *          The additional parameters that have been supplied as part of this parse request. If
   *          this GntmlBlock has a block name then the params will automatically have been setup as
   *          a NamespaceGettable so that you are guaranteed to get the correct items out of the
   *          parameter block that are specific to this GntmlBlock.
   * @see GntmlDocument#parse(Gettable, Transaction)
   */
  public abstract Appendable parse(Appendable out,
                                   Gettable documentParams,
                                   Gettable blockParams,
                                   Map<Object, Object> outputs)
      throws GntmlException, IOException;

  /**
   * Get the transactional viewpoint as defined in the GntmlDocument that this block is contained
   * within.
   * 
   * @see GntmlDocument#getTransaction()
   */
  protected final Transaction getTransaction()
  {
    return getGntmlDocument().getTransaction();
  }

  /**
   * Get the index of this block inside its containing GntmlDocument. This is initialised during the
   * GntmlDocument parsing process and may be useful for identifying non-named blocks by their
   * position within the document. However it should not be utilised as any kind of persistent
   * reference point because it may change as documents are re-parsed due to the fact that the block
   * may be moved around inside the document, or additional content may be added or removed above
   * the block. Hence it should only be used for performing operations on an individually
   * GntmlDocument within the lifecycle of the document itself.
   * 
   * @return &gt;= 0
   */
  public final int getBlockIndex()
  {
    return __blockId;
  }

  /**
   * Get the XHTML id that should be used to identify this block as an anchor for linking to.
   * 
   * @return null by default because most blocks don't support this. Override this as appropriate.
   */
  public String getBlockId()
  {
    return null;
  }

  public final GntmlBlockStyle getBlockStyle()
  {
    return __blockStyle;
  }

  /**
   * Get the GntmlDocument that contains this GntmlBlock
   */
  public final GntmlDocument getGntmlDocument()
  {
    return __gntmlDocument;
  }

  protected GntmlLine createGntmlLine(String source)
  {
    return new GntmlLine(source, this);
  }

  public final ListIterator<GntmlLine> getLineList()
  {
    return _lines.listIterator();
  }

  /**
   * @return Unmodifiable view of the List of GntmlLines that make up this GntmlBlock.
   */
  protected final List<GntmlLine> getLines()
  {
    return Collections.unmodifiableList(_lines);
  }

  private void addLine(GntmlLine line)
  {
    _lines.add(line);
  }

  public Appendable parseBodyLines(Appendable out,
                                   Iterator<GntmlLine> lineList,
                                   Gettable params,
                                   boolean permitsParametricTags,
                                   String openingTag,
                                   String closingTag)
      throws GntmlException, IOException
  {
    if (lineList.hasNext()) {
      out.append(openingTag);
      lineList.next().parse(out, params, permitsParametricTags);
      while (lineList.hasNext()) {
        out.append("<br />\n");
        lineList.next().parse(out, params, permitsParametricTags);
      }
      out.append(closingTag).append("\n");
    }
    return out;
  }

  public Appendable parseBodyLinesAsParagraph(Appendable out,
                                              Iterator<GntmlLine> lineList,
                                              Gettable params,
                                              boolean permitsParametricTags,
                                              String paragraphClass)
      throws GntmlException, IOException
  {
    return parseBodyLines(out,
                          lineList,
                          params,
                          permitsParametricTags,
                          Strings.isNullOrEmpty(paragraphClass)
                              ? "<p>"
                              : new TagBuilder("p").attr("class", paragraphClass).build(true),
                          "</p>");
  }

  @Override
  public String toString()
  {
    return getClass().getName() + "(" + getBlockIndex() + "," + getName() + "," + getBlockStyle()
           + "," + getBlockName() + ")";
  }

  /**
   * Invoke the appropriate {@link BlockStyle#appendOpenDiv(Appendable, String, String)} using
   * {@link #getCssClass()} and {@link #getCssId()} to get any instance specific CSS behaviour
   * required.
   */
  protected void openDivs(Appendable buffer)
      throws IOException
  {
    __blockStyle.appendHeader(this, buffer);
  }

  protected final void closeDivs(Appendable result)
      throws IOException
  {
    __blockStyle.appendFooter(this, result);
    result.append('\n');
  }

  /**
   * Get the optional CSS class that this Block requires for the wrapper div.
   * 
   * @return null by default - subclasses should override this if a value is known.
   */
  public String getCssClass()
  {
    return null;
  }

  /**
   * Get the optional CSS id that this Block requires for the wrapper div.
   * 
   * @return null by default - subclasses should override this if a value is known.
   */
  public String getCssId()
  {
    return null;
  }

  /**
   * Copy any configuration of this block that isn't contained in the source to the Settable to
   * enable cloning the document. If your block contains all of the configuration inside the GNTML
   * source then this method will not have to actually do anything as the compilation of the copied
   * document will restore the state.
   * 
   * @param namespacedSettable
   *          The Settable that the state of the GntmlBlock should be placed into. If
   *          {@link #getXhtmlId()} is defined then the Settable will be automatically namespaced so
   *          you don't have to worry what the block id is, just put the values into it.
   */
  public abstract void copyGntmlBlock(Settable namespacedSettable,
                                      PersistentRecord fromNode,
                                      PersistentRecord fromRegionStyle,
                                      PersistentRecord toRegionStyle,
                                      PersistentRecord fromGntmlInstance,
                                      GntmlDocument gntmlDocument)
      throws SettableException;

  public void saveJSON(JSONSaver jSaver,
                       PersistentRecord node,
                       PersistentRecord regionStyle,
                       PersistentRecord gntmlInstance,
                       GntmlDocument gntmlDocument,
                       JSONObject jGntml)
      throws JSONException, IOException
  {
  }

  public void loadJSON(JSONLoader jLoader,
                       PersistentRecord node,
                       PersistentRecord regionStyle,
                       JSONObject jGntml,
                       Settable settable)
      throws SettableException, UnresolvedRecordException, JSONException,
      MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException, IOException, FeedbackErrorException
  {
  }
}
