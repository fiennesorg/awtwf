package org.awtwf.gntml;

import org.cord.mirror.Query;
import org.cord.sql.SqlUtil;
import org.cord.util.Assertions;
import org.cord.util.EnglishNamedImpl;

import com.google.common.base.Preconditions;

public class SourcePatternDefinition
  extends EnglishNamedImpl
{
  protected SourcePatternMgr _mgr;

  private final String __rlike;

  private final String __sqlFilter;

  private final String __sqlName;

  protected SourcePatternDefinition(String name,
                                    String englishName,
                                    String rlike)
  {
    super(name,
          englishName);
    __rlike = Assertions.notEmpty(rlike, "rlike");
    StringBuilder buf = new StringBuilder();
    buf.append(Gntml.TABLENAME + '.' + Gntml.GNTML + " rlike ");
    SqlUtil.toSafeSqlString(buf, rlike);
    __sqlFilter = buf.toString();
    __sqlName = "SourcePatternDefinition." + name;
  }

  protected void setMgr(SourcePatternMgr mgr)
  {
    Preconditions.checkNotNull(mgr, "mgr");
    Assertions.isTrue(_mgr == null, "Cannot invoke setMgr twice on %s", this);
    _mgr = mgr;
  }

  public final String getRlike()
  {
    return __rlike;
  }

  /**
   * @return null if the definition is not yet add to a SourcePatternMgr
   */
  public SourcePatternMgr getMgr()
  {
    return _mgr;
  }

  public Query searchGntmlInstances()
  {
    return getMgr().getNodeMgr()
                   .getDb()
                   .getTable(Gntml.TABLENAME)
                   .getQuery(__sqlName, __sqlFilter, Gntml.NODE_ID.getKeyName());
  }
}
