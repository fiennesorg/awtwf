package org.awtwf.gntml;

import org.cord.mirror.AbstractTableWrapper;
import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.field.IntegerField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.sql.SqlUtil;

import com.google.common.base.Strings;

public class GntmlPlusBlockStyle
  extends AbstractTableWrapper
{
  public static final String TABLENAME = "GntmlPlusBlockStyle";

  public static final FieldKey<Integer> GNTMLSTYLE_ID =
      LinkField.getLinkFieldName(GntmlStyle.TABLENAME);

  public static final FieldKey<String> NAME = FieldKey.create("name", String.class);

  /*
   * The total sum of the width that this plus block style changes the width of the containing div -
   * this should include the sum of both the left and right borders as appropriate.
   */
  public static final FieldKey<Integer> HORIZONTALBORDER = FieldKey.create("horizontalBorder",
                                                                           Integer.class);

  public static final FieldKey<Integer> VERTICALBORDER = FieldKey.create("verticalBorder",
                                                                         Integer.class);

  public GntmlPlusBlockStyle()
  {
    super(TABLENAME);
  }

  @Override
  protected void initTable(Db db,
                           String pwd,
                           Table table)
      throws MirrorTableLockedException
  {
    table.addField(new LinkField(GntmlStyle.TABLENAME,
                                 "Containing GNTML document style",
                                 pwd,
                                 Dependencies.LINK_ENFORCED));
    table.addField(new StringField(NAME, "Block name"));
    table.addField(new IntegerField(HORIZONTALBORDER, "Total horizontal borders"));
    table.addField(new IntegerField(VERTICALBORDER, "Total vertical borders"));
  }

  /**
   * @return The appropriate instance or null if name cannot be resolved to an instance inside
   *         GntmlStyle
   */
  public PersistentRecord getOptionalInstance(PersistentRecord gntmlStyle,
                                              String name,
                                              Transaction transaction)
  {
    TransientRecord.assertIsTableNamed(gntmlStyle, GntmlStyle.TABLENAME);
    if (Strings.isNullOrEmpty(name)) {
      return null;
    }
    StringBuilder filter = new StringBuilder();
    filter.append(TABLENAME + '.' + GNTMLSTYLE_ID + '=').append(gntmlStyle.getId());
    filter.append(" and " + TABLENAME + '.' + NAME + '=');
    SqlUtil.toSafeSqlString(filter, name);
    return RecordSources.getOptFirstRecord(getTable().getQuery(null, filter.toString(), null), null);
  }
}
