package org.awtwf.gntml;

import java.io.IOException;

import org.cord.mirror.TransientRecord;
import org.cord.util.Named;
import org.cord.util.SimpleGettable;

public interface GntmlBlockStyle
  extends Named, SimpleGettable
{
  public static final String INNERCLASS = "i";

  public static final String TITLE = "title";
  public static final String CSSCLASS = "cssClass";

  public static final String WIDTH = "width";
  public static final String ISACTIVE = "isActive";
  public static final String ISGRIDX = "isGridX";
  public static final String PADDING = "padding";
  public static final String MINWIDTH = "minWidth";
  public static final String MINCONTENTWIDTH = "minContentWidth";
  public static final String MAXWIDTH = "maxWidth";
  public static final String MAXCONTENTWIDTH = "maxContentWidth";

  /**
   * optional comma separated list of valid block names where all of the preceeding blocks before a
   * plus block style must be on the list if defined.
   */
  public static final String PLUS_ACCEPTSBLOCK = "acceptsBlock";

  /**
   * optional comma separate list of block names that must not appear in the list of preceeding
   * blocks before a plus block style if defined.
   */
  public static final String PLUS_REJECTSBLOCK = "rejectsBlock";

  public static final String BOTH_ACCEPTSDOC = "acceptsDoc";
  public static final String BOTH_REJECTSDOC = "rejectsDoc";

  public boolean accepts(TransientRecord gntmlStyle);

  public void appendHeader(GntmlBlock gntmlBlock,
                           Appendable out)
      throws IOException;

  public void appendFooter(GntmlBlock gntmlBlock,
                           Appendable out)
      throws IOException;

  public boolean outputsWebmacro();
}
