package org.awtwf.gntml;

import java.io.IOException;

import org.cord.util.Gettable;
import org.cord.util.StringUtils;

public class GntmlLine
{
  private String _source;

  private final GntmlBlock _gntmlBlock;

  public GntmlLine(String source,
                   GntmlBlock gntmlBlock)
  {
    this(source,
         gntmlBlock,
         true);
  }

  public GntmlLine(String source,
                   GntmlBlock gntmlBlock,
                   boolean parseCharacterEntities)
  {
    _gntmlBlock = gntmlBlock;
    setSource(source, parseCharacterEntities);
  }

  public boolean outputsWebmacro()
  {
    for (InlineTag inlineTag : getGntmlBlock().getGntmlDocument()
                                              .getGntmlMgr()
                                              .getInlineTags()
                                              .values()) {
      if (inlineTag.outputsWebmacro(_source)) {
        return true;
      }
    }
    return false;
  }

  public void setSource(String source,
                        boolean parseCharacterEntities)
  {
    if (source != null) {
      _source = source;
      if (parseCharacterEntities) {
        _source = parseCharacterEntities(_source);
      }
    } else {
      _source = "";
    }
  }

  private String parseCharacterEntities(String source)
  {
    if (StringUtils.notEmpty(source)) {
      for (GntmlCharacterEntity characterEntity : getGntmlBlock().getGntmlDocument()
                                                                 .getGntmlMgr()
                                                                 .getCharacterEntities()) {
        source = characterEntity.parse(source);
      }
    }
    return source;
  }

  public final GntmlBlock getGntmlBlock()
  {
    return _gntmlBlock;
  }

  private String parseInlineBlocks(String source,
                                   Gettable params,
                                   boolean permitsParametricTags)
      throws GntmlException
  {
    GntmlDocument gntmlDocument = getGntmlBlock().getGntmlDocument();
    for (InlineTag inlineTag : gntmlDocument.getGntmlMgr().getInlineTags().values()) {
      if (permitsParametricTags || !inlineTag.isParametric()) {
        source = inlineTag.parse(gntmlDocument, gntmlDocument.getTransaction(), source, params);
      }
    }
    return source;
  }

  @Deprecated
  public boolean isEmpty()
  {
    return _source.length() == 0;
  }

  /**
   * Is this line signifying the gap between two blocks, ie completely whitespace?
   */
  public boolean isSeparator()
  {
    return StringUtils.isWhitespace(_source);
  }

  protected Appendable parse(Appendable result,
                             Gettable params,
                             String source,
                             boolean permitsParametricTags)
      throws GntmlException, IOException
  {
    source = StringUtils.noHtml(source);
    if (_gntmlBlock.getGntmlDocument().outputsWebmacro()) {
      source = StringUtils.noWebmacro(source);
    }
    source = parseInlineBlocks(source, params, permitsParametricTags);
    result.append(source);
    return result;
  }

  public Appendable parse(Appendable result,
                          Gettable params,
                          boolean permitsParametricTags)
      throws GntmlException, IOException
  {
    return parse(result, params, _source, permitsParametricTags);
  }

  public String getSource()
  {
    return _source;
  }

  @Override
  public String toString()
  {
    return _source;
  }
}
