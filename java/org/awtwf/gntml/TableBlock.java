package org.awtwf.gntml;

import java.io.IOException;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.WritableRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.json.JSONLoader;
import org.cord.node.json.JSONSaver;
import org.cord.node.json.UnresolvedRecordException;
import org.cord.util.Gettable;
import org.cord.util.LogicException;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.cord.util.XmlUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.WritableJSONObject;

public abstract class TableBlock
  extends GntmlBlock
{
  /**
   * RecordOperation that provides a hidden link to state that there is information being supplied
   * about a given TableBlock during an updated procedure ($tableBlock.asFormElement.isActive).
   * 
   * @see TableBlockFactory.IsActive
   */
  public static final RecordOperationKey<Boolean> ISACTIVE =
      RecordOperationKey.create("isActive", Boolean.class);

  /**
   * enum representing the way that decisions as to whether a trailing caption for a TableBlock is
   * rendered.
   */
  public enum CaptionPolicy {
    /**
     * The caption will always be shown if defined.
     */
    ALWAYS,
    /**
     * The caption will never be shown and including a caption will raise an error in the rendered
     * document.
     */
    NEVER,
    /**
     * The caption will be rendered to a templet named $t_caption and the webmacro template that
     * renders this TableBlock can take a decision as to whether or not to include it.
     */
    WEBMACRO;
  }

  private final TableBlockFactory __tableBlockFactory;

  private final Integer __instanceId;

  private final String __blockName;

  protected TableBlock(String name,
                       ListIterator<String> sourceLines,
                       GntmlBlockStyle gntmlBlockStyle,
                       GntmlDocument gntmlDocument,
                       TableBlockFactory tableBlockFactory,
                       int blockId) throws GntmlException
  {
    super(name,
          sourceLines,
          gntmlBlockStyle,
          gntmlDocument,
          tableBlockFactory,
          blockId);
    __tableBlockFactory = tableBlockFactory;
    String source = getLines().get(0).getSource();
    __blockName = tableBlockFactory.calculateBlockName(source);
    if (gntmlDocument.getBlock(tableBlockFactory.getName(), getBlockName()) != null) {
      throw new BlockBuildException("Duplicate " + getName() + " block named \"" + getBlockName()
                                    + "\"", null, getLines());
    }
    try {
      PersistentRecord instance =
          tableBlockFactory.getOrCreateInstanceRecord(gntmlDocument,
                                                      getBlockName(),
                                                      gntmlBlockStyle,
                                                      getTransaction());
      __instanceId = instance.getId();
    } catch (MirrorException e) {
      throw new GntmlException("Unable to create record for " + name + " named " + getBlockName(),
                               e);
    }
  }

  /**
   * Get the policy that this TableBlock should utilise in deciding whether and how to render the
   * optional trailing caption.
   * 
   * @return {@link CaptionPolicy#ALWAYS} by default. Subclasses should override this if an
   *         alternative behaviour is required.
   */
  public CaptionPolicy getCaptionPolicy()
  {
    return CaptionPolicy.ALWAYS;
  }

  /**
   * @return a unique tablename + id string that can be used to identify the block that is under the
   *         control of the referenced record.
   */
  @Override
  public String getBlockId()
  {
    return getInstance().opt(TableBlockFactory.BLOCKID);
  }

  @Override
  public final String getBlockName()
  {
    return __blockName;
  }

  static String getXhtmlId(TransientRecord instance)
  {
    return XmlUtils.encodeName(instance.comp(TableBlockFactory.BLOCKNAME));
  }

  @Override
  public final String getXhtmlId()
  {
    return XmlUtils.encodeName(__blockName);
  }

  public final TableBlockFactory getTableBlockFactory()
  {
    return __tableBlockFactory;
  }

  public final Integer getInstanceId()
  {
    return __instanceId;
  }

  public final PersistentRecord getInstance()
  {
    try {
      return getTableBlockFactory().getTable().getRecord(__instanceId, getTransaction());
    } catch (MirrorNoSuchRecordException e) {
      throw new LogicException("Lost instance for " + this, e);
    }
  }

  /**
   * Get the edit interface as defined in the constructor to the controlling TableBlockFactory
   * 
   * @see TableBlockFactory#getEditTemplatePath()
   */
  @Override
  public final String getEditTemplatePath()
  {
    return getTableBlockFactory().getEditTemplatePath();
  }

  /**
   * @return true
   */
  @Override
  public final boolean hasEditWidget()
  {
    return true;
  }

  /**
   * @return true
   */
  @Override
  public final boolean outputsWebmacro()
  {
    return true;
  }

  @Override
  public final void copyGntmlBlock(Settable settable,
                                   PersistentRecord fromNode,
                                   PersistentRecord fromRegionStyle,
                                   PersistentRecord toRegionStyle,
                                   PersistentRecord fromGntmlInstance,
                                   GntmlDocument gntmlDocument)
      throws SettableException
  {
    PersistentRecord instance = getInstance();
    settable.set(ISACTIVE, Boolean.TRUE);
    copyGntmlBlock(settable,
                   fromNode,
                   fromRegionStyle,
                   toRegionStyle,
                   fromGntmlInstance,
                   gntmlDocument,
                   instance);
  }

  public abstract void copyGntmlBlock(Settable settable,
                                      PersistentRecord fromNode,
                                      PersistentRecord fromRegionStyle,
                                      PersistentRecord toRegionStyle,
                                      PersistentRecord fromGntmlInstance,
                                      GntmlDocument gntmlDocument,
                                      PersistentRecord instance)
      throws SettableException;

  @Override
  public final void loadJSON(JSONLoader jLoader,
                             PersistentRecord node,
                             PersistentRecord regionStyle,
                             JSONObject jGntml,
                             Settable settable)
      throws SettableException, UnresolvedRecordException, JSONException,
      MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException, IOException, FeedbackErrorException
  {
    JSONObject jBlocks = jGntml.optJSONObject(getGntmlBlockFactory().getName());
    if (jBlocks == null) {
      return;
    }
    JSONObject jBlock = jBlocks.optJSONObject(getBlockName());
    if (jBlock == null) {
      return;
    }
    loadJSON(jLoader, node, regionStyle, jGntml, jBlock, settable);
  }

  public void loadJSON(JSONLoader jLoader,
                       PersistentRecord node,
                       PersistentRecord regionStyle,
                       JSONObject jGntml,
                       JSONObject jBlock,
                       Settable settable)
      throws SettableException, UnresolvedRecordException, JSONException,
      MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
      MirrorNoSuchRecordException, IOException, FeedbackErrorException
  {

  }

  @Override
  public final void saveJSON(JSONSaver jSaver,
                             PersistentRecord node,
                             PersistentRecord regionStyle,
                             PersistentRecord gntmlInstance,
                             GntmlDocument gntmlDocument,
                             JSONObject jGntml)
      throws JSONException, IOException
  {
    JSONObject jBlocks = jGntml.optJSONObject(getGntmlBlockFactory().getName());
    if (jBlocks == null) {
      jBlocks = new WritableJSONObject();
      jGntml.put(getGntmlBlockFactory().getName(), jBlocks);
    }
    JSONObject jBlock = new WritableJSONObject();
    jBlocks.put(getBlockName(), jBlock);
    saveJSON(jSaver, node, regionStyle, gntmlInstance, gntmlDocument, jGntml, jBlock);
  }

  public void saveJSON(JSONSaver jSaver,
                       PersistentRecord node,
                       PersistentRecord regionStyle,
                       PersistentRecord gntmlInstance,
                       GntmlDocument gntmlDocument,
                       JSONObject jGntml,
                       JSONObject jBlock)
      throws JSONException, IOException
  {
  }

  /**
   * If the GUI is active then invoke the sub-class updateInstance method and then rebuild the
   * public template for the TableBlock in the parsed GNTML regardless.
   * 
   * @see #updateInstance(WritableRecord, Gettable)
   * @see TableBlockFactory#getName()
   * @see TableBlockFactory#getTableName()
   * @see TableBlockFactory#getViewTemplatePath()
   */
  @Override
  public final Appendable parse(Appendable out,
                                Gettable documentParams,
                                Gettable blockParams,
                                Map<Object, Object> outputs)
      throws GntmlException, IOException
  {
    final TableBlockFactory factory = getTableBlockFactory();
    PersistentRecord instance = getInstance();
    Transaction transaction = getTransaction();
    if (transaction != null && blockParams != null) {
      try {
        WritableRecord rwInstance = transaction.edit(instance);
        rwInstance.setField(TableBlockFactory.ISREFERENCED, Boolean.TRUE);
        if (Boolean.TRUE.equals(blockParams.getBoolean(ISACTIVE))) {
          for (TableBlockCC<?> tableBlockCc : getTableBlockFactory().getTableBlockCcs()) {
            tableBlockCc.updateTransientInstance(instance, blockParams);
          }
          updateInstance(rwInstance, blockParams, outputs);
        }
      } catch (MirrorException e) {
        throw new GntmlException("Error updating " + getName() + " on " + instance, e);
      }
    }
    final CaptionPolicy captionPolicy = getCaptionPolicy();
    if (CaptionPolicy.WEBMACRO.equals(captionPolicy)) {
      out.append("\n#templet $t_caption {\n");
      parseCaption(out, documentParams);
      out.append("}\n");
    }
    openDivs(out);
    out.append("#set $")
       .append(factory.getInstanceName())
       .append(" = $db.")
       .append(factory.getTableName())
       .append(".getRecord(")
       .append(instance.getId().toString())
       .append(",$gntmlInstance)\n");
    // getBlockStyle().appendBlockStyle_width(out);
    out.append("#include as template \"$")
       .append(factory.getInstanceName())
       .append("." + TableBlockFactory.VIEWTEMPLATEPATH + "\"\n");
    switch (captionPolicy) {
      case ALWAYS:
        parseCaption(out, documentParams);
        break;
      case WEBMACRO:
        break;
      case NEVER:
        if (getLines().size() > 1) {
          out.append("<div class=\"error\">\n");
          parseCaption(out, documentParams);
          out.append("</div>\n");
        }
    }
    closeDivs(out);
    return out;
  }

  protected void parseCaption(Appendable out,
                              Gettable documentParams)
      throws GntmlException, IOException
  {
    Iterator<GntmlLine> lineList = getLineList();
    lineList.next();
    if (lineList.hasNext()) {
      parseBodyLinesAsParagraph(out,
                                lineList,
                                documentParams,
                                true,
                                getTableBlockFactory().getCaptionCssClass());
    }
  }

  @Override
  public String getCssClass()
  {
    return getTableBlockFactory().getBlockCssClass();
  }

  @Override
  public String getCssId()
  {
    return getInstance().opt(TableBlockFactory.BLOCKID);
  }

  public static void appendSet(Appendable out,
                               String name,
                               Object value)
      throws IOException
  {
    out.append("#set $").append(name).append(" = ").append(value.toString()).append('\n');
  }

  protected abstract void updateInstance(WritableRecord instance,
                                         Gettable params,
                                         Map<Object, Object> outputs)
      throws GntmlException, MirrorException;

  public static void main(String[] arts)
  {
    char c = ' ';
    System.out.println(String.format("%#02x", c));
  }
}
