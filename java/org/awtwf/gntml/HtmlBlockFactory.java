package org.awtwf.gntml;

import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.StringField;
import org.cord.node.NodeManager;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;

public class HtmlBlockFactory
  extends TableBlockFactory
{
  public static final FieldKey<String> HTML = FieldKey.create("html", String.class);

  public static final String TABLENAME = "HtmlBlock";

  public HtmlBlockFactory(NodeManager nodeManager)
  {
    super(nodeManager,
          TABLENAME,
          "Literal HTML Block",
          "|HTML:",
          "|",
          "HtmlBlock",
          "Gntml/htmlBlock.view.wm.html",
          "gntmlblock_html",
          "gntmlblock_html_caption",
          "Gntml/htmlBlock.edit.wm.html",
          null);
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table table)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    table.addField(new StringField(HTML, "HTML", StringField.LENGTH_TEXT, "").setMayBeTextArea(true));
  }

  /**
   * @return an HtmlBlockFactory.HtmlBlock
   * @see HtmlBlock
   */
  @Override
  protected TableBlock buildTableBlock(ListIterator<String> sourceLines,
                                       GntmlBlockStyle blockStyle,
                                       GntmlDocument gntmlDocument,
                                       int blockId)
      throws GntmlException
  {
    return new HtmlBlock(getName(), sourceLines, blockStyle, gntmlDocument, this, blockId);
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
      throws GntmlException
  {
  }

  public class HtmlBlock
    extends TableBlock
  {
    protected HtmlBlock(String name,
                        ListIterator<String> sourceLines,
                        GntmlBlockStyle blockStyle,
                        GntmlDocument gntmlDocument,
                        TableBlockFactory tableBlockFactory,
                        int blockId) throws GntmlException
    {
      super(name,
            sourceLines,
            blockStyle,
            gntmlDocument,
            tableBlockFactory,
            blockId);
    }

    @Override
    public void copyGntmlBlock(Settable settable,
                               PersistentRecord fromNode,
                               PersistentRecord fromRegionStyle,
                               PersistentRecord toRegionStyle,
                               PersistentRecord fromGntmlInstance,
                               GntmlDocument gntmlDocument,
                               PersistentRecord instance)
        throws SettableException
    {
      settable.set(HTML, instance.opt(HTML));
    }

    @Override
    protected void updateInstance(WritableRecord instance,
                                  Gettable params,
                                  Map<Object, Object> outputs)
        throws GntmlException, MirrorException
    {
      instance.setField(HTML, params.get(HTML));
    }
  }
}
