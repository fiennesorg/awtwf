package org.awtwf.gntml;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.cord.mirror.Db;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordOrdering;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordSourceOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.recordsource.AbstractRecordOrdering;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.operation.SimpleRecordSourceOperation;
import org.cord.node.Node;
import org.cord.node.NodeFilter;
import org.cord.node.NodeManager;
import org.cord.util.EnglishNamed;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.EnglishNamedOptions;
import org.cord.util.Gettable;
import org.cord.util.NamespaceGettable;

import com.google.common.base.Preconditions;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

public class PageList
  extends AbstractListBlock
{
  public static final String NAME = "Pages";

  public static final EnglishNamedOptions VIEWS;
  static {
    VIEWS = new EnglishNamedOptions(new EnglishNamedImpl("Gntml/pageList/title.wm.html", "Title"));
  }

  public static final FieldKey<Integer> GNTMLPAGELIST_ID = FieldKey.create("gntmlPageList_id",
                                                                           Integer.class);
  public static final FieldKey<Integer> NODE_ID = FieldKey.create("node_id", Integer.class);
  public static final RecordOperationKey<PersistentRecord> NODE = LinkField.getLinkName(NODE_ID);

  public static final RecordSourceOperationKey<RecordSource> SORTBYNUMBERTITLE =
      RecordSourceOperationKey.create("sortByNumberTitle", RecordSource.class);

  public static final Comparator<TransientRecord> SORTBYNUMBERTITLE_COMPARATOR =
      new Comparator<TransientRecord>() {
        @Override
        public int compare(TransientRecord link0,
                           TransientRecord link1)
        {
          if (link0.getId().equals(link1.getId())) {
            return 0;
          }
          int c = link0.comp(NUMBER).compareTo(link1.comp(NUMBER));
          if (c != 0) {
            return c;
          }
          PersistentRecord node0 = link0.comp(NODE);
          PersistentRecord node1 = link1.comp(NODE);
          c = node0.comp(Node.TITLE).compareTo(node1.comp(Node.TITLE));
          if (c != 0) {
            return c;
          }
          return node0.getId().compareTo(node1.getId());
        }
      };

  public static final RecordOrdering SORTBYNUMBERTITLE_RECORDORDERING =
      new AbstractRecordOrdering("sortByNumberTitle",
                                 "Sort by link numbering then page title",
                                 null,
                                 SORTBYNUMBERTITLE_COMPARATOR);

  public PageList(NodeManager nodeMgr) throws MirrorTableLockedException
  {
    super(nodeMgr,
          NAME,
          "Embedded Page List",
          "GntmlPageList",
          "GntmlPageListJoin",
          GNTMLPAGELIST_ID,
          NODE_ID,
          Node.TABLENAME,
          null,
          "Gntml/pageList.view.wm.html",
          "Gntml_Pages",
          null,
          null,
          NodeFilter.ALLNODES,
          VIEWS,
          "Select pages for Page List named ",
          "Select Pages");
    VIEWS.addOptions(getViews(nodeMgr.getGettable(), GETTABLE_NODE));
    VIEWS.addOptions(getViews(nodeMgr.getGettable(), GETTABLE_APP));
  }

  @Override
  protected void postInit(Db db,
                          String pwd,
                          Table table,
                          Table linkTable,
                          Table targetTable)
      throws MirrorTableLockedException
  {
    super.postInit(db, pwd, table, linkTable, targetTable);
    linkTable.addRecordSourceOperation(new SimpleRecordSourceOperation<RecordSource>(SORTBYNUMBERTITLE) {
      @Override
      public RecordSource invokeRecordSourceOperation(RecordSource gntmlPageListJoins,
                                                      Viewpoint viewpoint)
      {
        return RecordSources.applyRecordOrdering(gntmlPageListJoins,
                                                 SORTBYNUMBERTITLE_RECORDORDERING);
      }
    });
  }

  @Override
  protected PersistentRecord getTarget(PersistentRecord targetNode,
                                       Transaction transaction)
      throws MirrorNoSuchRecordException
  {
    return targetNode;
  }

  @Override
  protected PersistentRecord getTargetNode(PersistentRecord target,
                                           Transaction transaction)
      throws MirrorNoSuchRecordException
  {
    return target;
  }

  public static final String GETTABLE_PREFIX = "PageList.View.";
  public static final String GETTABLE_KEYS = ".keys";
  public static final String GETTABLE_TITLE = "title";
  public static final String GETTABLE_TEMPLATEPATH = "templatePath";
  public static final String GETTABLE_NODE = "node";
  public static final String GETTABLE_APP = "app";

  public EnglishNamed getView(Gettable source,
                              String key)
  {
    Preconditions.checkNotNull(key, "key");
    key = key.trim();
    Preconditions.checkState(key.length() > 0, "whitespace keys are not permitted");
    NamespaceGettable gettable = new NamespaceGettable(source, GETTABLE_PREFIX + key + ".");
    return new EnglishNamedImpl(Preconditions.checkNotNull(gettable.getString(GETTABLE_TEMPLATEPATH),
                                                           "%s%s.%s not defined",
                                                           GETTABLE_PREFIX,
                                                           key,
                                                           GETTABLE_TEMPLATEPATH)
                                             .trim(),
                                Preconditions.checkNotNull(gettable.getString(GETTABLE_TITLE),
                                                           "%s%s.%s not defined",
                                                           GETTABLE_PREFIX,
                                                           key,
                                                           GETTABLE_TITLE).trim());
  }

  public List<EnglishNamed> getViews(Gettable source,
                                     String namespace)
  {
    String keys = source.getString(GETTABLE_PREFIX + namespace + GETTABLE_KEYS);
    if (keys == null) {
      return Collections.emptyList();
    }
    List<EnglishNamed> views = Lists.newArrayList();
    for (String key : Splitter.on(',').trimResults().omitEmptyStrings().split(keys)) {
      views.add(getView(source, key));
    }
    return views;
  }
}
