package org.awtwf.gntml;

import java.io.IOException;
import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.PersistentRecord;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;

/**
 * A GntmlBlock that may be returned by a GntmlBlockFactory if it is unable to complete the
 * buildBlock request so that it can convey the issues to the editor. The block will appear as a
 * highlighted error with a copy of the source code in both the view and the edit environment so
 * that the editor can correct whatever problem.
 * 
 * @author alex
 */
public class ErrorBlock
  extends GntmlBlock
{
  public static final String NAME = "ErrorBlock";

  public static final String EDITTEMPLATEPATH = "Gntml/errorBlock.edit.wm.html";

  private final String __errorMessage;

  private final String __blockName;

  protected ErrorBlock(ListIterator<String> sourceLines,
                       GntmlBlockStyle gntmlBlockStyle,
                       GntmlDocument gntmlDocument,
                       GntmlBlockFactory originalBlockFactory,
                       String blockName,
                       int blockId,
                       String errorMessage)
  {
    super(NAME,
          sourceLines,
          gntmlBlockStyle,
          gntmlDocument,
          originalBlockFactory,
          blockId);
    __blockName = blockName;
    __errorMessage = errorMessage;
  }

  @Override
  public String getBlockName()
  {
    return __blockName;
  }

  public final String getErrorMessage()
  {
    return __errorMessage;
  }

  @Override
  public Appendable parse(Appendable out,
                          Gettable documentParams,
                          Gettable blockParams,
                          Map<Object, Object> outputs)
      throws GntmlException, IOException
  {
    out.append("<div class=\"gntml_error\">\n");
    out.append("<strong>").append(__errorMessage).append("</strong>\n");
    out.append("<pre>\n");
    ListIterator<GntmlLine> lines = getLineList();
    while (lines.hasNext()) {
      GntmlLine line = lines.next();
      out.append(line.getSource()).append("\n");
    }
    out.append("</pre>\n");
    out.append("</div>");
    return out;
  }

  /**
   * @see #EDITTEMPLATEPATH
   */
  @Override
  public String getEditTemplatePath()
  {
    return EDITTEMPLATEPATH;
  }

  /**
   * @return true
   */
  @Override
  public boolean hasEditWidget()
  {
    return true;
  }

  /**
   * Do nothing as an error will just be regenerated in the copied version of the document as it was
   * in this version of the document.
   */
  @Override
  public void copyGntmlBlock(Settable settable,
                             PersistentRecord fromNode,
                             PersistentRecord fromRegionStyle,
                             PersistentRecord toRegionStyle,
                             PersistentRecord fromGntmlInstance,
                             GntmlDocument gntmlDocument)
      throws SettableException
  {
  }
}
