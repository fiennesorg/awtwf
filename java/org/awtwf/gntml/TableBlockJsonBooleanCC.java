package org.awtwf.gntml;

import java.io.IOException;

import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.field.JSONObjectField;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtils;
import org.json.JSONObject;

import com.google.common.base.Supplier;

public class TableBlockJsonBooleanCC
  extends TableBlockJsonCC<Boolean>
{

  public TableBlockJsonBooleanCC(TableBlockFactory tableBlockFactory,
                                 JSONObjectField jsonObjectField,
                                 String label,
                                 RecordOperationKey<Boolean> jsonKey,
                                 Supplier<Boolean> defaultValue)
  {
    super(tableBlockFactory,
          jsonObjectField,
          label,
          jsonKey,
          defaultValue);
  }

  @Override
  protected Object getJsonValue(Gettable params)
  {
    return params.getBoolean(getJSONKey());
  }

  @Override
  protected Boolean getValue(TransientRecord instance)
  {
    JSONObject jObj = asJsonObject(instance);
    if (jObj.has(getJSONKey().getKeyName())) {
      return jObj.optBoolean(getJSONKey().getKeyName());
    }
    return getDefaultValue();
  }

  @Override
  public void appendHtmlWidget(TransientRecord instance,
                               Viewpoint viewpoint,
                               String namePrefix,
                               Appendable buf,
                               Gettable context)
      throws IOException
  {
    HtmlUtils.checkboxInput(buf, null, getValue(instance), false, namePrefix, getJSONKey());
  }

}
