package org.awtwf.gntml;

import java.io.IOException;

import org.cord.util.Booleans;
import org.cord.util.NumberUtil;

/**
 * Static utility methods for doing things with GntmlBlockStyle implementations.
 */
public class GntmlBlockStyles
{
  public static Integer optInteger(GntmlBlockStyle gntmlBlockStyle,
                                   String key)
  {
    return NumberUtil.toInteger(gntmlBlockStyle.get(key), true, null, false, null);
  }

  public static Boolean optBoolean(GntmlBlockStyle gntmlBlockStyle,
                                   String key)
  {
    return Booleans.valueOf(gntmlBlockStyle.get(key), null, Boolean.FALSE);
  }

  public static void appendOpenDimensions(GntmlBlockStyle gntmlBlockStyle,
                                          Appendable out)
      throws IOException
  {
    Object width = gntmlBlockStyle.get(GntmlBlockStyle.WIDTH);
    Object padding = gntmlBlockStyle.get(GntmlBlockStyle.PADDING);
    if (width == null) {
      if (padding == null) {
        return;
      }
      out.append("$Dimensions.openPadded(").append(padding.toString()).append(")\n");
    } else {
      if (padding == null) {
        out.append("$Dimensions.open(").append(width.toString()).append(")\n");
      } else {
        out.append("$Dimensions.openPadded(")
           .append(width.toString())
           .append(",")
           .append(padding.toString())
           .append(")\n");
      }
    }
  }

  public static void appendCloseDimensions(GntmlBlockStyle gntmlBlockStyle,
                                           Appendable out)
      throws IOException
  {
    Object width = gntmlBlockStyle.get(GntmlBlockStyle.WIDTH);
    Object padding = gntmlBlockStyle.get(GntmlBlockStyle.PADDING);
    if (width != null | padding != null) {
      out.append("$Dimensions.close()\n");
    }
  }

  /**
   * Check to see if there is a possibility that the given gntmlBlockStyle might output webmacro
   * code. This is currently defined as if it either has a width or a non-zero padding defined as
   * both of these would trigger a $Dimensions declaration. Implementations of GntmlBlockStyle can
   * utilise this method as a starting point for their implementations of
   * {@link GntmlBlockStyle#outputsWebmacro()} and extend it if they have further webmacro related
   * logic.
   */
  public static boolean outputsWebmacro(GntmlBlockStyle gntmlBlockStyle)
  {
    Object width = gntmlBlockStyle.get(GntmlBlockStyle.WIDTH);
    Object padding = gntmlBlockStyle.get(GntmlBlockStyle.PADDING);
    return width != null | padding != null;
  }

  /**
   * Resolve a boolean param on a GntmlBlockStyle where not being defined or not being parseable as
   * a param is taken as false.
   */
  public static boolean is(GntmlBlockStyle gntmlBlockStyle,
                           String flagName)
  {
    return Booleans.valueOf(gntmlBlockStyle.get(flagName), Boolean.FALSE, Boolean.FALSE);
  }
}
