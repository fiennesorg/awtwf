package org.awtwf.gntml;

import org.cord.util.NamedImpl;
import org.cord.util.SettableMap;

import com.google.common.base.Preconditions;

public abstract class AbstractGntmlBlockStyle
  extends NamedImpl
  implements GntmlBlockStyle
{
  private final SettableMap __settable = new SettableMap();

  public AbstractGntmlBlockStyle(String name)
  {
    super(name);
  }

  /**
   * Define a parameter with non-null key and value.
   * 
   * @return the previous value for the param if any
   */
  protected Object set(String key,
                       Object value)
  {
    Preconditions.checkNotNull(key, "key");
    Preconditions.checkNotNull(value, "value");
    return __settable.put(key, value);
  }

  /**
   * Define the parameter if value is not null
   * 
   * @return the previous value if any
   */
  protected Object setIfDefined(String key,
                                Object value)
  {
    return value == null ? null : set(key, value);
  }

  @Override
  public boolean containsKey(Object key)
  {
    return __settable.containsKey(key);
  }

  @Override
  public Object get(Object key)
  {
    return __settable.get(key);
  }
}
