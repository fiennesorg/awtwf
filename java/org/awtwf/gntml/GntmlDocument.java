package org.awtwf.gntml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.node.Node;
import org.cord.node.grid.Grid;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.LogicException;
import org.cord.util.NamespaceGettable;
import org.cord.util.NumberUtil;
import org.cord.util.StringBuilderIOException;
import org.cord.util.StringUtils;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

public class GntmlDocument
{
  private final GntmlMgr __gntmlMgr;

  private final String __source;

  private final List<GntmlBlock> __blocks = new ArrayList<GntmlBlock>();
  private final List<GntmlBlock> __roBlocks = Collections.unmodifiableList(__blocks);

  private final Map<String, Map<String, GntmlBlock>> __blockIndex =
      new HashMap<String, Map<String, GntmlBlock>>();

  // private final String __imageNameSpace;

  private final String __downloadNameSpace;

  private final Integer __documentId;

  private final Transaction __transaction;

  private boolean _outputsWebmacro = false;

  // private static final Pattern __blockDivider =
  // Pattern.compile("(?:\\{([^,\\}]*)(?:,([^,\\}]*)(?:,(.*?))?)?\\}\n)?(.*?)(?:\n{2,}|$)",
  // Pattern.DOTALL);

  private final Table __gntmlInstances;

  private final int __gridWidth;
  private final int __totalWidth;

  protected GntmlDocument(GntmlMgr gntmlMgr,
                          Integer documentId,
                          String source,
                          String downloadNameSpace,
                          Transaction transaction) throws GntmlException
  {
    __gntmlMgr = gntmlMgr;
    __documentId = Preconditions.checkNotNull(documentId, "documentId");
    __source = source;
    __gntmlInstances = getGntmlMgr().getNodeMgr().getDb().getTable(Gntml.TABLENAME);
    // __imageNameSpace = imageNameSpace;
    __downloadNameSpace = downloadNameSpace;
    __transaction = transaction;
    PersistentRecord gntmlStyle = getGntmlStyle();
    __gridWidth = GntmlStyle.getGridWidth(gntmlStyle);
    __totalWidth =
        GntmlStyle.getGridMarginLeft(gntmlStyle) + __gridWidth
            + GntmlStyle.getGridMarginRight(gntmlStyle);
    if (source.length() > 0) {
      List<String> sourceLines = Arrays.asList(StringUtils.splitByLines(source));
      ListIterator<String> i = sourceLines.listIterator();
      int blockId = 0;
      while (i.hasNext()) {
        GntmlBlock nextBlock = getGntmlBlock(i, blockId);
        if (nextBlock != null) {
          __blocks.add(nextBlock);
          indexBlock(nextBlock);
          blockId++;
          _outputsWebmacro =
              _outputsWebmacro || nextBlock.getBlockStyle().outputsWebmacro()
                  || nextBlock.outputsWebmacro();
        }
      }
    }
  }

  /**
   * Get the first GntmlBlock that has been registered that accepts the first source line. Each
   * block in the set is passed the first line in it's acceptsSource(firstSourceLine) method. The
   * first one that returns true will be returned. The order of the checking is the same as the
   * order of registration. If none of the blocks accepts the first line then the default block (as
   * defined in the initialiser) is returned.
   * 
   * @param firstSourceLine
   *          The first line of the block that should be parsed.
   * @return The first registered block that accepts the line or the default block if all the
   *         registered blocks reject the block.
   */
  private GntmlBlockFactory getGntmlBlockFactory(String firstSourceLine)
  {
    for (GntmlBlockFactory blockFactory : getGntmlMgr().getGntmlBlockFactories().values()) {
      if (blockFactory.acceptsSource(this, firstSourceLine)) {
        return blockFactory;
      }
    }
    return getGntmlMgr().getDefaultBlockFactory();
  }

  private GntmlBlockStyle getGntmlBlockStyle(ListIterator<String> sourceLines)
      throws GntmlBlockStyleException
  {
    String firstLine = null;
    while (sourceLines.hasNext() & StringUtils.isWhitespace(firstLine)) {
      firstLine = sourceLines.next();
    }
    if (firstLine == null) {
      return null;
    }
    firstLine = firstLine.trim();
    if (firstLine.startsWith("{") & firstLine.endsWith("}")) {
      String name = firstLine.substring(1, firstLine.length() - 1);
      return getGntmlMgr().getGntmlBlockStyleProvider().getBlockStyle(getGntmlStyle(), name);
    }
    sourceLines.previous();
    return DefaultGntmlBlockStyle.getInstance(getGntmlStyle());
  }

  /**
   * Get the first built GntmlBlock that has been registered that accepts the next non-null and
   * non-css line.
   * 
   * @throws GntmlException
   *           if the appropriate GntmlBlockFactory fails to build the requested block
   */
  private GntmlBlock getGntmlBlock(ListIterator<String> sourceLines,
                                   int blockId)
      throws GntmlException
  {
    GntmlBlockStyle blockStyle = null;
    try {
      blockStyle = getGntmlBlockStyle(sourceLines);
    } catch (GntmlBlockStyleException e) {
      System.err.println(String.format("%s(%s).getGntmlBlock(...) --> {%s} --> %s",
                                       this.getGntmlInstance().comp(Gntml.NODE).comp(Node.URL),
                                       getGntmlStyle().comp(GntmlStyle.NAME),
                                       e.getBlockStyleDeclaration(),
                                       e.getMessage()));
      blockStyle = new GntmlBlockStyleError("ERROR", e.getMessage());
    }
    GntmlBlockFactory blockFactory = null;
    if (sourceLines.hasNext()) {
      String sourceLine = sourceLines.next();
      blockFactory = getGntmlBlockFactory(sourceLine);
      sourceLines.previous();
    } else {
      blockFactory = getGntmlMgr().getDefaultBlockFactory();
    }
    return blockFactory.buildBlock(sourceLines, blockStyle, this, blockId);
  }

  public final GntmlMgr getGntmlMgr()
  {
    return __gntmlMgr;
  }

  public PersistentRecord getGntmlInstance()
  {
    try {
      return __gntmlInstances.getRecord(__documentId, __transaction);
    } catch (MirrorNoSuchRecordException e) {
      throw new LogicException("Lost GntmlInstance for " + this, e);
    }
  }

  public PersistentRecord getGntmlStyle()
  {
    return getGntmlInstance().comp(Gntml.STYLE);
  }

  /*
   * private void buildBlocks(String source) { Matcher m = __blockDivider.matcher(source); while
   * (m.find()) { String cssType = m.group(1); String blockName = m.group(2); String blockType =
   * m.group(3); String blockSource = m.group(4); } }
   */
  /**
   * Check to see whether the output of parsing this document is expected to be processed by the
   * webmacro template engine.
   * 
   * @see GntmlBlock#outputsWebmacro()
   */
  public final boolean outputsWebmacro()
  {
    return _outputsWebmacro;
  }

  protected Transaction getTransaction()
  {
    return __transaction;
  }

  public final Integer getDocumentId()
  {
    return __documentId;
  }

  public String debug()
  {
    StringBuilder result = new StringBuilder();
    result.append("GntmlDocument\n");
    result.append("All Blocks:").append(__blocks).append("\n");
    result.append("Named Index:").append(__blockIndex).append("\n");
    return result.toString();
  }

  public GntmlBlock getGntmlBlock(int blockId)
  {
    return __blocks.get(blockId);
  }

  public Map<String, GntmlBlock> getBlockIndex(String blockType)
  {
    Map<String, GntmlBlock> blocks = __blockIndex.get(blockType);
    if (blocks == null) {
      blocks = new HashMap<String, GntmlBlock>();
      __blockIndex.put(blockType, blocks);
    }
    return blocks;
  }

  public GntmlBlock getBlock(String blockType,
                             String blockName)
  {
    return getBlockIndex(blockType).get(blockName);
  }

  public boolean indexBlock(GntmlBlock block)
  {
    String blockType = block.getName();
    Map<String, GntmlBlock> blocks = getBlockIndex(blockType);
    String blockName = block.getBlockName();
    if (Strings.isNullOrEmpty(blockName) || blocks.containsKey(blockName)) {
      return false;
    }
    blocks.put(blockName, block);
    return true;
  }

  public String getSource()
  {
    return __source;
  }

  // public String getImageNameSpace()
  // {
  // return __imageNameSpace;
  // }

  public String getDownloadNameSpace()
  {
    return __downloadNameSpace;
  }

  // public final GntmlParser getGntmlParser()
  // {
  // return __gntmlParser;
  // }

  public int getGridWidth()
  {
    return __gridWidth;
  }

  private void openGridX(Appendable buf)
      throws IOException
  {
    HtmlUtilsTheme.openDivOfClass(buf, Grid.GNTML_GRID_X);
    HtmlUtilsTheme.openDivOfClass(buf, GntmlBlockStyle.INNERCLASS);
  }

  private void closeGridX(Appendable buf)
      throws IOException
  {
    HtmlUtilsTheme.closeDiv(buf);
    HtmlUtilsTheme.closeDiv(buf);
  }

  public String parse(final Gettable documentParams,
                      final Transaction transaction,
                      Map<Object, Object> outputs)
      throws GntmlException
  {
    StringBuilder buf = new StringBuilder((int) (__source.length() * 1.05));
    try {
      for (GntmlBlockFactory gntmlBlockFactory : getGntmlMgr().getGntmlBlockFactories().values()) {
        gntmlBlockFactory.preParse(this, transaction);
      }
      for (InlineTag inlineTag : getGntmlMgr().getInlineTags().values()) {
        inlineTag.preParse(this, transaction);
      }
      int gridX = 0;
      for (Iterator<GntmlBlock> i = __blocks.iterator(); i.hasNext();) {
        GntmlBlock block = i.next();
        GntmlBlockStyle blockStyle = block.getBlockStyle();
        if (GntmlBlockStyles.is(blockStyle, GntmlBlockStyle.ISGRIDX)) {
          int gridWidth = NumberUtil.toInt(blockStyle.get(GntmlBlockStyle.WIDTH));
          if (gridX == 0) {
            openGridX(buf);
            gridX += gridWidth;
          } else {
            gridX += gridWidth;
            if (gridX > __totalWidth) {
              closeGridX(buf);
              openGridX(buf);
              gridX = gridWidth;
            }
          }
        } else {
          if (gridX > 0) {
            closeGridX(buf);
            gridX = 0;
          }
        }
        String xhtmlId = block.getXhtmlId();
        Gettable blockParams =
            Strings.isNullOrEmpty(xhtmlId)
                ? documentParams
                : new NamespaceGettable(null, documentParams, block.getGntmlBlockFactory()
                                                                   .getName() + "-" + xhtmlId + "-");
        try {
          block.parse(buf, documentParams, blockParams, outputs);
        } catch (IOException ioEx) {
          throw new StringBuilderIOException(buf, ioEx);
        }
        _outputsWebmacro = _outputsWebmacro || block.outputsWebmacro();
      }
      if (gridX > 0) {
        closeGridX(buf);
      }
      for (GntmlBlockFactory gntmlBlockFactory : getGntmlMgr().getGntmlBlockFactories().values()) {
        gntmlBlockFactory.postParse(this, transaction);
      }
      for (InlineTag inlineTag : getGntmlMgr().getInlineTags().values()) {
        inlineTag.postParse(this, transaction);
      }
    } catch (IOException ioEx) {
      throw new StringBuilderIOException(buf, ioEx);
    }
    return buf.toString();
  }

  /**
   * Get an unmodifiable List of all the GntmlBlocks in this document in document order.
   */
  public List<GntmlBlock> getBlocks()
  {
    return __roBlocks;
  }
}
