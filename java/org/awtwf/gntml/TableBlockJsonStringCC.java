package org.awtwf.gntml;

import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.field.JSONObjectField;
import org.json.JSONObject;

import com.google.common.base.Supplier;

/**
 * Implementation of TableBlockJsonCC that stores a String variable in the JSONObjectField.
 */
public class TableBlockJsonStringCC
  extends TableBlockJsonCC<String>
{

  public TableBlockJsonStringCC(TableBlockFactory tableBlockFactory,
                                JSONObjectField jsonObjectField,
                                String label,
                                RecordOperationKey<String> jsonKey,
                                Supplier<String> defaultValue)
  {
    super(tableBlockFactory,
          jsonObjectField,
          label,
          jsonKey,
          defaultValue);
  }

  @Override
  protected String getValue(TransientRecord instance)
  {
    JSONObject jObj = asJsonObject(instance);
    if (jObj.has(getJSONKey().getKeyName())) {
      return jObj.optString(getJSONKey().getKeyName());
    }
    return getDefaultValue();
  }
}
