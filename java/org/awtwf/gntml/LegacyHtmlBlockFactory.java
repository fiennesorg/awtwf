package org.awtwf.gntml;

import java.io.IOException;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;

@Deprecated
public class LegacyHtmlBlockFactory
  extends EnglishNamedImpl
  implements GntmlBlockFactory
{
  public final static String HTMLBLOCK_STARTTAG = "<html>";

  public final static String NAME = "Html";

  public LegacyHtmlBlockFactory()
  {
    super(NAME);
  }

  @Override
  public GntmlBlock buildBlock(ListIterator<String> sourceLines,
                               GntmlBlockStyle blockStyle,
                               GntmlDocument gntmlDocument,
                               int blockId)
  {
    return new HtmlBlock(sourceLines, blockStyle, gntmlDocument, blockId);
  }

  @Override
  public boolean acceptsSource(GntmlDocument gntmlDocument,
                               String firstSourceLine)
  {
    return HTMLBLOCK_STARTTAG.equals(firstSourceLine.toLowerCase().trim());
  }

  protected class GntmlHtmlLine
    extends GntmlLine
  {
    public GntmlHtmlLine(String source,
                         GntmlBlock gntmlBlock)
    {
      super(source,
            gntmlBlock,
            false);
    }

    /**
     * append the source of the line directly with no processing at all.
     */
    public StringBuilder parse(StringBuilder result)
    {
      return result.append(getSource());
    }
  }

  /**
   * Do nothing
   */
  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
  {
  }

  /**
   * Do nothing
   */
  @Override
  public void preParse(GntmlDocument gntmlDocument,
                       Transaction transaction)
  {
  }

  public class HtmlBlock
    extends GntmlBlock
  {
    protected HtmlBlock(ListIterator<String> sourceLines,
                        GntmlBlockStyle blockStyle,
                        GntmlDocument gntmlDocument,
                        int blockId)
    {
      super(NAME,
            sourceLines,
            blockStyle,
            gntmlDocument,
            LegacyHtmlBlockFactory.this,
            blockId);
    }

    @Override
    public Appendable parse(Appendable xhtml,
                            Gettable documentParams,
                            Gettable blockParams,
                            Map<Object, Object> outputs)
        throws GntmlException, IOException
    {
      Iterator<GntmlLine> lineList = getLineList();
      if (lineList.hasNext()) {
        lineList.next();
        while (lineList.hasNext()) {
          xhtml.append(lineList.next().getSource()).append('\n');
        }
      }
      return xhtml;
    }

    /**
     * Do nothing
     */
    @Override
    public void copyGntmlBlock(Settable settable,
                               PersistentRecord fromNode,
                               PersistentRecord fromRegionStyle,
                               PersistentRecord toRegionStyle,
                               PersistentRecord fromGntmlInstance,
                               GntmlDocument gntmlDocument)
        throws SettableException
    {
    }

    @Override
    protected GntmlLine createGntmlLine(String source)
    {
      return new GntmlHtmlLine(source, this);
    }
  }
}
