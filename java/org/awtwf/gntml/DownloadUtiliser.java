package org.awtwf.gntml;

import java.util.Iterator;

import org.cord.util.NoSuchNamedException;

/**
 * Interface describing objects that use DownloadMetaData objects.
 */
@Deprecated
public interface DownloadUtiliser
{
  /**
   * Get a list of all the image names that are used by this DownloadUtiliser.
   * 
   * @return The names of the used images. The order is the same as the order of usage in the object
   *         and multiple instances are also preserved.
   */
  public Iterator<String> getUtilisedDownloadNames();

  /**
   * Get the number of images used by the DownloadUtiliser
   * 
   * @return The image count. Multiple instances are counted towards the total.
   */
  public int getDownloadCount();

  /**
   * Get the DownloadMetaDataProvider that this DownloadUtiliser is using to get get it's image
   * information.
   */
  public DownloadMetaDataProvider getDownloadMetaDataProvider();

  /**
   * Get the index based on the usage order within the object of a named image starting at a
   * specific in the listing. This enables you to target specific instances of specific downloads in
   * duplicate lists.
   * 
   * @param imageName
   *          The name of the image to search for.
   * @param startingIndex
   *          The index to start searching from.
   * @return The image index
   * @throws org.cord.util.NoSuchNamedException
   *           If the specific named image cannot be found after the startingIndex. Please note that
   *           the image may well still exist pre-startingIndex.
   */
  public int getDownloadIndex(String imageName,
                              int startingIndex)
      throws NoSuchNamedException;
}
