package org.awtwf.gntml;

import org.cord.mirror.Transaction;

/**
 * Interface describing an object that provides DownloadMetaData objects on request.
 */
public interface DownloadMetaDataProvider
{
  /**
   * Get the requested meta data about a named image.
   * 
   * @return the appropriate meta-data or null if the image cannot be found.
   */
  public DownloadMetaData getDownloadMetaData(String namespace,
                                              String imageName,
                                              Transaction transaction);
}
