package org.awtwf.gntml;

import java.io.File;
import java.util.ListIterator;
import java.util.Map;

import org.awtwf.node.GntmlImageStyle;
import org.cord.image.ImageStyleTable;
import org.cord.image.ImageTable;
import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordStringFactory;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.node.CurrentUrlFactory;
import org.cord.node.MonoNodeQuestion;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeQuestion;
import org.cord.node.NodeRequest;
import org.cord.node.RedirectionSuccessOperation;
import org.cord.node.RegionStyle;
import org.cord.node.cc.SingleImageContentCompiler;
import org.cord.node.cc.TableContentCompiler;
import org.cord.node.field.NodeLinkField;
import org.cord.node.json.IdMappers;
import org.cord.node.links.OrderedNodeLinks;
import org.cord.node.links.RelatedPagesSupplier;
import org.cord.node.view.AskNodeQuestion;
import org.cord.util.DuplicateNamedException;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.webmacro.Context;

import com.google.common.base.Strings;

@Deprecated
public class ImageBlockFactory
  extends TableBlockFactory
  implements RelatedPagesSupplier
{
  public final static String IMAGE_OPEN_TAG = "[";

  public final static String IMAGE_CLOSE_TAG = "]";

  public final static String TABLENAME = "GntmlImageInstance";

  public final static RecordOperationKey<RecordSource> REFERENCES =
      RecordOperationKey.create("gntmlImageInstances", RecordSource.class);

  public final static String NAME = "GntmlImage";

  public final static String ENGLISHNAME = "Image";

  public final static String NAMEPREFIX_VALUE = "img";

  public final static FieldKey<Integer> STYLE_ID = TableContentCompiler.STYLE_ID;

  public final static RecordOperationKey<PersistentRecord> STYLE = TableContentCompiler.STYLE;

  public final static FieldKey<String> TITLE = FieldKey.create("title", String.class);

  public final static FieldKey<String> CAPTION = FieldKey.create("caption", String.class);

  public final static FieldKey<Integer> CONTROLLINGIMAGE_ID =
      FieldKey.create("controllingImage_id", Integer.class);

  public final static RecordOperationKey<PersistentRecord> CONTROLLINGIMAGE =
      LinkField.getLinkName(CONTROLLINGIMAGE_ID);

  public final static RecordOperationKey<RecordSource> CONTROLLEDIMAGES =
      RecordOperationKey.create("controlledImages", RecordSource.class);

  public final static FieldKey<Boolean> HASZOOMEDVIEW = FieldKey.create("hasZoomedView",
                                                                        Boolean.class);

  public final static FieldKey<String> IMAGETARGET = FieldKey.create("imageTarget", String.class);

  public final static FieldKey<Boolean> HASNODETARGET = FieldKey.create("hasNodeTarget",
                                                                        Boolean.class);

  public final static FieldKey<Integer> TARGETNODE_ID = FieldKey.create("targetNode_id",
                                                                        Integer.class);

  public final static RecordOperationKey<PersistentRecord> TARGETNODE =
      RecordOperationKey.create("targetNode", PersistentRecord.class);

  public final static RecordOperationKey<RecordSource> NODE_GNTMLIMAGEINSTANCES =
      RecordOperationKey.create("gntmlImageInstances", RecordSource.class);

  public final static FieldKey<Boolean> HASBACKLINK = FieldKey.create("hasBackLink", Boolean.class);

  public final static FieldKey<String> BACKLINKTITLE = FieldKey.create("backLinkTitle",
                                                                       String.class);

  public final static FieldKey<Integer> GNTMLIMAGESTYLE_ID =
      LinkField.getLinkFieldName(GntmlImageStyle.TABLENAME);

  public final static RecordOperationKey<PersistentRecord> GNTMLIMAGESTYLE =
      LinkField.getLinkName(GNTMLIMAGESTYLE_ID);

  public final static String VIEWTEMPLATEPATH = "Gntml/image.view.wm.html";

  public final static String EDITTEMPLATEPATH = "Gntml/image.edit.wm.html";

  public final static RecordOperationKey<Boolean> MAYHAVEZOOM =
      RecordOperationKey.create("mayHaveZoom", Boolean.class);

  /**
   * RecordOperation that returns the id of the GntmlZoomedImage record that represents the zoomed
   * version of this image or 0 if there is no zoomed version found ($gntmlImageInstance.zoomedId)
   */
  public final static RecordOperationKey<Integer> ZOOMEDID =
      RecordOperationKey.create("zoomedId", Integer.class);

  public final static FieldKey<String> WEBPATH = ImageTable.WEBPATH;

  public final static FieldKey<Integer> WIDTH = ImageTable.WIDTH;

  public final static FieldKey<Integer> HEIGHT = ImageTable.HEIGHT;

  public final static FieldKey<String> ALT = ImageTable.ALT;

  public final static FieldKey<Integer> FORMAT_ID = ImageTable.FORMAT_ID;

  public final static RecordOperationKey<PersistentRecord> FORMAT = ImageTable.FORMAT;

  public final static FieldKey<Integer> SIZEKB = ImageTable.SIZEKB;

  public final static FieldKey<String> CSS = ImageTable.CSS;

  public final static RecordOperationKey<String> EXPLICITCSS = ImageTable.EXPLICITCSS;

  public final static RecordOperationKey<File> JAVAFILE = ImageTable.JAVAFILE;

  public final static String ZOOMEDIMAGEFILE = "zoomedImageFile";

  private final int __baselineGridSize;

  private Table _gntmlZoomedImages;

  public static final String GNTMLZOOMEDIMAGE = "GntmlZoomedImage";

  public static final RecordOperationKey<RecordSource> GNTMLZOOMEDIMAGES =
      RecordOperationKey.create("gntmlZoomedImages", RecordSource.class);

  public static final String VIEW_BROWSEIMAGETARGET = "browseImageTarget";

  private ImageBlockFactory(NodeManager nodeManager,
                            int baselineGridSize)
  {
    super(nodeManager,
          NAME,
          ENGLISHNAME,
          IMAGE_OPEN_TAG,
          IMAGE_CLOSE_TAG,
          TABLENAME,
          VIEWTEMPLATEPATH,
          "gntml_image",
          "gntml_image_caption",
          EDITTEMPLATEPATH,
          null);
    __baselineGridSize = baselineGridSize;
    nodeManager.getRelatedPagesManager().addSupplier(this);
    AskNodeQuestion askNodeQuestion =
        new AskNodeQuestion(VIEW_BROWSEIMAGETARGET,
                            nodeManager,
                            new RedirectionSuccessOperation(nodeManager,
                                                            CurrentUrlFactory.getInstance()),
                            Node.EDITACL,
                            "You don't have permission to browse the target") {
          @Override
          protected NodeQuestion createNodeQuestion(NodeRequest nodeRequest,
                                                    final PersistentRecord node,
                                                    Context context)
              throws MirrorNoSuchRecordException
          {
            final String id = nodeRequest.getString("id");
            PersistentRecord gntmlImageInstance = getTable().getRecord(id);
            String question =
                String.format("Select target for Image named \"%s\" in \"%s\" found on \"%s\"",
                              gntmlImageInstance.comp(BLOCKNAME),
                              gntmlImageInstance.comp(GNTMLINSTANCE)
                                                .comp(Gntml.REGIONSTYLE)
                                                .comp(RegionStyle.NAME),
                              node.comp(Node.TITLE));
            final String nameprefix = gntmlImageInstance.comp(NAMEPREFIX);
            final String encodedNamespace = HtmlUtilsTheme.urlEncode(nameprefix);
            Transaction transaction = getTransaction(nodeRequest, node);
            NodeQuestion nodeQuestion =
                new MonoNodeQuestion(getName() + gntmlImageInstance.getId(),
                                     question,
                                     "GNTML Image Link Target",
                                     node,
                                     null,
                                     transaction) {
                  @Override
                  public String getAnswerUrl()
                  {
                    StringBuilder url = new StringBuilder();
                    url.append(node.comp(Node.URL));
                    url.append("?view=update");
                    url.append('&')
                       .append(encodedNamespace)
                       .append("targetNode_id=")
                       .append(getSelectedNodeId())
                       .append('&')
                       .append(encodedNamespace)
                       .append(TableBlock.ISACTIVE)
                       .append("=true");
                    return url.toString();
                  }
                };
            return nodeQuestion;
          }
        };
    nodeManager.getNodeRequestSegmentManager().register(Transaction.TRANSACTION_UPDATE,
                                                        askNodeQuestion);
  }

  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
  {
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table gntmlImageInstances)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    gntmlImageInstances.addField(new LinkField(STYLE_ID,
                                               "Image style",
                                               null,
                                               STYLE,
                                               ImageStyleTable.SINGLEIMAGESTYLE,
                                               REFERENCES,
                                               null,
                                               pwd,
                                               Dependencies.LINK_ENFORCED));
    gntmlImageInstances.addField(new StringField(TITLE, "Title"));
    gntmlImageInstances.addField(new StringField(CAPTION, "Zoomed caption").setMayBeTextArea(true));
    gntmlImageInstances.addField(new LinkField(CONTROLLINGIMAGE_ID,
                                               "Controlling image",
                                               null,
                                               CONTROLLINGIMAGE,
                                               TABLENAME,
                                               CONTROLLEDIMAGES,
                                               null,
                                               pwd,
                                               Dependencies.LINK_ENFORCED
                                                   | Dependencies.BIT_OPTIONAL));
    gntmlImageInstances.addField(new BooleanField(HASZOOMEDVIEW,
                                                  "Has zoomed view?") {
      @Override
      public boolean shouldReloadOnChange()
      {
        return true;
      }

      @Override
      public boolean hasHtmlWidget(TransientRecord gntmlImageInstance)
      {
        return gntmlImageInstance.comp(MAYHAVEZOOM);
      }
    });
    gntmlImageInstances.addRecordOperation(new SimpleRecordOperation<Boolean>(MAYHAVEZOOM) {
      @Override
      public Boolean getOperation(TransientRecord gntmlImageInstance,
                                  Viewpoint viewpoint)
      {
        if (gntmlImageInstance.comp(STYLE_ID) == 0) {
          PersistentRecord gntmlStyle = gntmlImageInstance.comp(GNTMLINSTANCE).comp(Gntml.STYLE);
          if (gntmlStyle.comp(GntmlStyle.DEFAULTZOOMEDIMAGESTYLE_ID) != 0) {
            return Boolean.TRUE;
          }
        } else {
          PersistentRecord gntmlImageStyle = gntmlImageInstance.comp(GNTMLIMAGESTYLE);
          if (gntmlImageStyle.comp(GntmlImageStyle.ZOOMEDSINGLEIMAGESTYLE_ID) != 0) {
            return Boolean.TRUE;
          }
        }
        return Boolean.FALSE;
      }
    });
    /**
     * @see #ZOOMEDID
     */
    gntmlImageInstances.addRecordOperation(new SimpleRecordOperation<Integer>(ZOOMEDID) {
      @Override
      public Integer getOperation(TransientRecord gntmlImageInstance,
                                  Viewpoint viewpoint)
      {
        PersistentRecord controlledImage =
            RecordSources.getOptOnlyRecord(gntmlImageInstance.getRecordSource(GNTMLZOOMEDIMAGES,
                                                                              viewpoint), null);
        return controlledImage == null ? Integer.valueOf(0) : controlledImage.getId();
      }
    });
    gntmlImageInstances.addField(new StringField(IMAGETARGET, "Image link target"));
    gntmlImageInstances.addField(new BooleanField(HASNODETARGET,
                                                  "Image links to internal page?") {
      @Override
      public boolean shouldReloadOnChange()
      {
        return true;
      }
    });
    RecordStringFactory browseParams = new RecordStringFactory() {
      @Override
      public String getValue(TransientRecord gntmlImageInstance)
      {
        TransientRecord.assertIsTableNamed(gntmlImageInstance, TABLENAME);
        StringBuilder buf = new StringBuilder();
        buf.append("view=" + VIEW_BROWSEIMAGETARGET + "&id=").append(gntmlImageInstance.getId());
        return buf.toString();
      }
    };
    gntmlImageInstances.addField(new NodeLinkField(TARGETNODE_ID,
                                                   "Target page of image link",
                                                   null,
                                                   TARGETNODE,
                                                   NODE_GNTMLIMAGEINSTANCES,
                                                   null,
                                                   pwd,
                                                   Dependencies.LINK_ENFORCED
                                                       | Dependencies.BIT_OPTIONAL,
                                                   null,
                                                   browseParams));
    gntmlImageInstances.addField(new BooleanField(HASBACKLINK,
                                                  "Has backlink?") {
      @Override
      public boolean shouldReloadOnChange()
      {
        return true;
      }
    });
    gntmlImageInstances.addField(new StringField(BACKLINKTITLE, "Back link title"));
    gntmlImageInstances.addField(new LinkField(GntmlImageStyle.TABLENAME,
                                               "Image style",
                                               pwd,
                                               Dependencies.LINK_ENFORCED
                                                   | Dependencies.BIT_OPTIONAL));
    ImageTable.initialiseInstanceTable(getNodeManager(), pwd, gntmlImageInstances, REFERENCES);
    gntmlImageInstances.addRecordOperation(new SimpleRecordOperation<String>(EXPLICITCSS) {
      @Override
      public String getOperation(TransientRecord targetRecord,
                                 Viewpoint viewpoint)
      {
        if (__baselineGridSize > 0) {
          int paddingBottom =
              __baselineGridSize - (targetRecord.getIntField(HEIGHT) % __baselineGridSize);
          StringBuilder buf = new StringBuilder();
          buf.append("padding-bottom=").append(paddingBottom).append("px");
          return buf.toString();
        }
        return "";
      }
    });
    _gntmlZoomedImages = db.addTable(null, GNTMLZOOMEDIMAGE);
    _gntmlZoomedImages.addField(new LinkField(TABLENAME,
                                              "Controlling image",
                                              pwd,
                                              Dependencies.LINK_ENFORCED));
    _gntmlZoomedImages.addField(new LinkField(STYLE_ID,
                                              "SingleImageStyle constraints",
                                              null,
                                              STYLE,
                                              ImageStyleTable.SINGLEIMAGESTYLE,
                                              null,
                                              null,
                                              pwd,
                                              Dependencies.LINK_ENFORCED));
    ImageTable.initialiseInstanceTable(getNodeManager(), pwd, _gntmlZoomedImages, null);
  }

  public static final RecordOperationKey<PersistentRecord> GNTMLZOOMEDIMAGE_GNTMLIMAGEINSTANCE =
      RecordOperationKey.create("gntmlImageInstance", PersistentRecord.class);

  @Override
  protected TableBlock buildTableBlock(ListIterator<String> sourceLines,
                                       GntmlBlockStyle blockStyle,
                                       GntmlDocument gntmlDocument,
                                       int blockId)
      throws GntmlException
  {
    return new ImageBlock(sourceLines, blockStyle, gntmlDocument, blockId);
  }

  @Override
  public void shutdown()
  {
  }

  @Override
  protected void initialiseInstance(GntmlDocument gntmlDocument,
                                    TransientRecord gntmlImageInstance,
                                    GntmlBlockStyle gntmlBlockStyle)
      throws MirrorFieldException
  {
    PersistentRecord gntmlInstance = gntmlImageInstance.comp(GNTMLINSTANCE);
    PersistentRecord gntmlStyle = gntmlInstance.comp(Gntml.STYLE);
    gntmlImageInstance.setField(STYLE_ID, gntmlStyle.opt(GntmlStyle.DEFAULTIMAGESTYLE));
    PersistentRecord gntmlImageStyle = null; // getGntmlImageStyle(gntmlDocument, cssValue, null);
    updateStyles(gntmlImageInstance, gntmlImageStyle);
  }

  protected void updateStyles(TransientRecord gntmlImageInstance,
                              PersistentRecord gntmlImageStyle)
      throws MirrorFieldException
  {
    if (gntmlImageStyle == null) {
      gntmlImageInstance.setField(STYLE_ID,
                                  gntmlImageInstance.comp(GNTMLINSTANCE)
                                                    .comp(Gntml.STYLE)
                                                    .comp(GntmlStyle.DEFAULTIMAGESTYLE_ID));
      gntmlImageInstance.setField(GNTMLIMAGESTYLE_ID, Integer.valueOf(0));
    } else {
      gntmlImageInstance.setField(STYLE_ID,
                                  gntmlImageStyle.comp(GntmlImageStyle.SINGLEIMAGESTYLE_ID));
      gntmlImageInstance.setField(GNTMLIMAGESTYLE_ID, gntmlImageStyle);
    }
  }

  // protected PersistentRecord getGntmlImageStyle(GntmlDocument gntmlDocument,
  // String cssValue,
  // Viewpoint viewpoint)
  // {
  // return Gntml.getGntmlManager(getNodeManager())
  // .getGntmlImageStyle()
  // .getOptionalInstance(gntmlDocument.getGntmlStyle(), cssValue, viewpoint);
  // // return GntmlImageStyle.getRecord(getNodeManager().getDb()
  // // .getTable("GntmlImageStyle"),
  // // gntmlDocument.getGntmlStyle().getId(),
  // // cssValue,
  // // viewpoint);
  // }

  @Deprecated
  public class ImageBlock
    extends TableBlock
  {
    protected ImageBlock(ListIterator<String> sourceLines,
                         GntmlBlockStyle blockStyle,
                         GntmlDocument gntmlDocument,
                         int blockId) throws GntmlException
    {
      super(NAME,
            sourceLines,
            blockStyle,
            gntmlDocument,
            ImageBlockFactory.this,
            blockId);
    }

    @Override
    public void copyGntmlBlock(Settable settable,
                               PersistentRecord fromNode,
                               PersistentRecord fromRegionStyle,
                               PersistentRecord toRegionStyle,
                               PersistentRecord fromGntmlInstance,
                               GntmlDocument gntmlDocument,
                               PersistentRecord instance)
        throws SettableException
    {
      settable.set(TITLE, instance.comp(TITLE));
      settable.set(HASZOOMEDVIEW, instance.comp(HASZOOMEDVIEW));
      if (instance.comp(HASZOOMEDVIEW)) {
        PersistentRecord zoomedInstance;
        try {
          zoomedInstance = getOrCreateZoomedInstance(instance, getTransaction());
          settable.set(ZOOMEDIMAGEFILE, zoomedInstance.comp(JAVAFILE));
        } catch (MirrorException e) {
        }
      }
      settable.set(IMAGETARGET, instance.comp(IMAGETARGET));
      settable.set(HASNODETARGET, instance.comp(HASNODETARGET));
      settable.set(TARGETNODE_ID, instance.comp(TARGETNODE_ID));
      settable.set(HASBACKLINK, instance.comp(HASBACKLINK));
      settable.set(BACKLINKTITLE, instance.comp(BACKLINKTITLE));
      settable.set(STYLE_ID, instance.comp(STYLE_ID));
      settable.set(GNTMLIMAGESTYLE_ID, instance.comp(GNTMLIMAGESTYLE_ID));
      settable.set(CAPTION, instance.comp(CAPTION));
      ImageTable.copy(settable, instance);
    }

    @Override
    protected void updateInstance(WritableRecord gntmlImageInstance,
                                  Gettable params,
                                  Map<Object, Object> outputs)
        throws GntmlException, MirrorException
    {
      gntmlImageInstance.setFieldIfDefined(TITLE, params.get(TITLE));
      gntmlImageInstance.setFieldIfDefined(IMAGETARGET, params.get(IMAGETARGET));
      gntmlImageInstance.setFieldIfDefined(HASNODETARGET, params.getBoolean(HASNODETARGET));
      gntmlImageInstance.setFieldIfDefined(TARGETNODE_ID,
                                           IdMappers.map(params,
                                                         Node.TABLENAME,
                                                         params.get(TARGETNODE_ID)));
      gntmlImageInstance.setFieldIfDefined(HASBACKLINK, params.getBoolean(HASBACKLINK));
      gntmlImageInstance.setFieldIfDefined(BACKLINKTITLE, params.get(BACKLINKTITLE));
      PersistentRecord gntmlImageStyle = null;
      // getGntmlImageStyle(getGntmlDocument(), getBlockStyle().getBlockName(), getTransaction());
      updateStyles(gntmlImageInstance, gntmlImageStyle);
      SingleImageContentCompiler.updateInstance(getNodeManager(),
                                                gntmlImageInstance,
                                                gntmlImageInstance.followEnforcedLink(STYLE),
                                                null,
                                                getTransaction(),
                                                params,
                                                null);
      gntmlImageInstance.setFieldIfDefined(CAPTION, params.get(CAPTION));
      if (Boolean.TRUE.equals(gntmlImageInstance.opt(MAYHAVEZOOM))) {
        gntmlImageInstance.setFieldIfDefined(HASZOOMEDVIEW, params.getBoolean(HASZOOMEDVIEW));
        if (Boolean.TRUE.equals(gntmlImageInstance.opt(HASZOOMEDVIEW))) {
          PersistentRecord slave = getOrCreateZoomedInstance(gntmlImageInstance, getTransaction());
          slave = getTransaction().edit(slave);
          SingleImageContentCompiler.updateInstance(getNodeManager(),
                                                    slave,
                                                    slave.followEnforcedLink(STYLE),
                                                    null,
                                                    getTransaction(),
                                                    params,
                                                    params.getFile(ZOOMEDIMAGEFILE));
        }
      }
    }
  }

  protected PersistentRecord getOrCreateZoomedInstance(PersistentRecord master,
                                                       Viewpoint viewpoint)
      throws MirrorFieldException, MirrorInstantiationException
  {
    PersistentRecord instance =
        RecordSources.getOptOnlyRecord(master.getRecordSource(GNTMLZOOMEDIMAGES, viewpoint),
                                       viewpoint);
    if (instance != null) {
      return instance;
    }
    TransientRecord newInstance = _gntmlZoomedImages.createTransientRecord();
    if (master.getIntField(GNTMLIMAGESTYLE_ID) == 0) {
      PersistentRecord gntmlInstance = master.comp(GNTMLINSTANCE);
      PersistentRecord gntmlStyle = gntmlInstance.comp(Gntml.STYLE);
      newInstance.setField(STYLE_ID, gntmlStyle.comp(GntmlStyle.DEFAULTZOOMEDIMAGESTYLE_ID));
    } else {
      PersistentRecord gntmlImageStyle = master.comp(GNTMLIMAGESTYLE);
      newInstance.setField(STYLE_ID,
                           gntmlImageStyle.comp(GntmlImageStyle.ZOOMEDSINGLEIMAGESTYLE_ID));
    }
    newInstance.setField("gntmlImageInstance_id", master.getId());
    return newInstance.makePersistent(null);
  }

  @Override
  public void addRelatedPages(PersistentRecord node,
                              OrderedNodeLinks nodeLinks)
  {
    RecordSource gntmlImageInstances = node.getRecordSource(NODE_GNTMLIMAGEINSTANCES, null);
    for (PersistentRecord gntmlImageInstance : gntmlImageInstances) {
      if (gntmlImageInstance.getBooleanField(HASNODETARGET)
          && gntmlImageInstance.getBooleanField(HASBACKLINK)) {
        PersistentRecord targetGntmlInstance = gntmlImageInstance.followEnforcedLink(GNTMLINSTANCE);
        if (targetGntmlInstance.getBooleanField(Gntml.ISDEFINED)) {
          PersistentRecord targetNode = targetGntmlInstance.followEnforcedLink(Gntml.NODE);
          String targetTitle = gntmlImageInstance.getString(BACKLINKTITLE);
          if (Strings.isNullOrEmpty(targetTitle)) {
            targetTitle = targetNode.getString(Node.HTMLTITLE);
          }
          nodeLinks.addNodeLabel(targetNode, targetTitle);
        }
      }
    }
  }
}
