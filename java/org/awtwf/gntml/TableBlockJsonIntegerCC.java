package org.awtwf.gntml;

import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.field.JSONObjectField;
import org.cord.util.Gettable;
import org.cord.util.NumberUtil;
import org.json.JSONObject;

import com.google.common.base.Supplier;

public class TableBlockJsonIntegerCC
  extends TableBlockJsonCC<Integer>
{
  public TableBlockJsonIntegerCC(TableBlockFactory tableBlockFactory,
                                 JSONObjectField jsonObjectField,
                                 String label,
                                 RecordOperationKey<Integer> jsonKey,
                                 Supplier<Integer> defaultValue)
  {
    super(tableBlockFactory,
          jsonObjectField,
          label,
          jsonKey,
          defaultValue);
  }

  @Override
  protected Integer getValue(TransientRecord instance)
  {
    JSONObject jObj = asJsonObject(instance);
    if (jObj.has(getJSONKey().getKeyName())) {
      return jObj.optInteger(getJSONKey().getKeyName());
    }
    return getDefaultValue();
  }

  @Override
  protected Integer getJsonValue(Gettable params)
  {
    return NumberUtil.toInteger(params.get(getJSONKey()), null);
  }
}
