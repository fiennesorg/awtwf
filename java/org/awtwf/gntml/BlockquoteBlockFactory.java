package org.awtwf.gntml;

import java.io.IOException;
import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;

public class BlockquoteBlockFactory
  extends EnglishNamedImpl
  implements GntmlBlockFactory
{
  public static final String NAME = "Blockquote";

  public static final String MARKUP_TOKEN = "\"\"";

  public BlockquoteBlockFactory()
  {
    super(NAME);
  }

  @Override
  public GntmlBlock buildBlock(ListIterator<String> sourceLines,
                               GntmlBlockStyle blockStyle,
                               GntmlDocument gntmlDocument,
                               int blockId)
  {
    return new BlockquoteBlock(NAME, sourceLines, blockStyle, gntmlDocument, blockId);
  }

  @Override
  public boolean acceptsSource(GntmlDocument gntmlDocument,
                               String firstSourceLine)
  {
    return firstSourceLine.startsWith(MARKUP_TOKEN);
  }

  /**
   * Do nothing
   */
  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
      throws GntmlException
  {
  }

  /**
   * Do nothing
   */
  @Override
  public void preParse(GntmlDocument gntmlDocument,
                       Transaction transaction)
      throws GntmlException
  {
  }

  /**
   * Do nothing
   */
  public void copyGntmlBlock(Settable settable,
                             PersistentRecord fromNode,
                             PersistentRecord fromRegionStyle,
                             PersistentRecord toRegionStyle,
                             PersistentRecord fromGntmlInstance,
                             GntmlDocument gntmlDocument)
      throws SettableException
  {
  }

  public class BlockquoteBlock
    extends GntmlBlock
  {
    protected BlockquoteBlock(String name,
                              ListIterator<String> sourceLines,
                              GntmlBlockStyle blockStyle,
                              GntmlDocument gntmlDocument,
                              int blockId)
    {
      super(name,
            sourceLines,
            blockStyle,
            gntmlDocument,
            BlockquoteBlockFactory.this,
            blockId);
    }

    @Override
    protected GntmlLine createGntmlLine(String source)
    {
      if (source.startsWith(MARKUP_TOKEN)) {
        source = source.substring(MARKUP_TOKEN.length());
        if (source.trim().length() == 0) {
          source = "&nbsp;";
        }
      }
      if (source.endsWith(MARKUP_TOKEN)) {
        source = source.substring(0, source.length() - MARKUP_TOKEN.length());
      }
      return super.createGntmlLine(source);
    }

    @Override
    public Appendable parse(Appendable xhtml,
                            Gettable documentParams,
                            Gettable blockParams,
                            Map<Object, Object> outputs)
        throws GntmlException, IOException
    {
      openDivs(xhtml);
      xhtml.append("<blockquote><p>\n");
      ListIterator<GntmlLine> lines = getLineList();
      if (lines.hasNext()) {
        GntmlLine line = lines.next();
        line.parse(xhtml, blockParams, true);
        while (lines.hasNext()) {
          xhtml.append("<br />\n");
          line = lines.next();
          line.parse(xhtml, blockParams, true);
        }
      }
      xhtml.append("</p></blockquote>\n");
      closeDivs(xhtml);
      return xhtml;
    }

    @Override
    public void copyGntmlBlock(Settable settable,
                               PersistentRecord fromNode,
                               PersistentRecord fromRegionStyle,
                               PersistentRecord toRegionStyle,
                               PersistentRecord fromGntmlInstance,
                               GntmlDocument gntmlDocument)
        throws SettableException
    {
    }
  }
}
