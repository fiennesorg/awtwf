package org.awtwf.gntml;

import java.io.IOException;

import org.cord.mirror.TransientRecord;
import org.cord.util.Gettable;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.KeyGettableSupplier;
import org.cord.util.StringUtils;

import com.google.common.base.Preconditions;

public class DefinedGntmlBlockStyle
  extends AbstractGntmlBlockStyle
{
  public static final String HEADERSPANCSSCLASS = "headerSpanCssClass";
  public static final String HEADERSPANHASPRIMARYCLASS = "headerSpanHasPrimaryClass";

  public static final String HASDEFAULTMARGINS = "hasDefaultMargins";

  public static final String CSS_DEFAULTMARGINS = "content";

  public static final KeyGettableSupplier<DefinedGntmlBlockStyle> getSupplier(final GntmlMgr gntmlMgr,
                                                                              final Iterable<String> compFields,
                                                                              final Iterable<String> optFields)
  {
    Preconditions.checkNotNull(gntmlMgr, "gntmlMgr");
    return new KeyGettableSupplier<DefinedGntmlBlockStyle>() {
      @Override
      public DefinedGntmlBlockStyle get(String key,
                                        Gettable gettable)
      {
        DefinedGntmlBlockStyle style = new DefinedGntmlBlockStyle(gntmlMgr, key);
        for (String compField : compFields) {
          style.set(compField,
                    Preconditions.checkNotNull(gettable.get(compField),
                                               "Undefined compulsory GNTML block style field of %s on %s",
                                               compField,
                                               style));
        }
        for (String optField : optFields) {
          style.setIfDefined(optField, gettable.get(optField));
        }
        return style;
      }
    };
  }

  private final GntmlMgr __gntmlMgr;

  public DefinedGntmlBlockStyle(GntmlMgr gntmlMgr,
                                String name)
  {
    super(name);
    __gntmlMgr = Preconditions.checkNotNull(gntmlMgr, "gntmlMgr");
  }

  public boolean hasDefaultMargins()
  {
    return "true".equals(get(HASDEFAULTMARGINS));
  }

  public GntmlMgr getGntmlMgr()
  {
    return __gntmlMgr;
  }

  @Override
  public boolean accepts(final TransientRecord gntmlStyle)
  {
    int docTotalWidth = GntmlStyle.getGridTotalWidth(gntmlStyle);
    Integer minWidth = GntmlBlockStyles.optInteger(this, MINWIDTH);
    if (minWidth != null && minWidth > docTotalWidth) {
      return false;
    }
    Integer maxWidth = GntmlBlockStyles.optInteger(this, MAXWIDTH);
    if (maxWidth != null && maxWidth < docTotalWidth) {
      return false;
    }
    Integer minContentWidth = GntmlBlockStyles.optInteger(this, MINCONTENTWIDTH);
    int docContentWidth = GntmlStyle.getGridWidth(gntmlStyle);
    if (minContentWidth != null && minContentWidth > docContentWidth) {
      return false;

    }
    Integer maxContentWidth = GntmlBlockStyles.optInteger(this, MAXCONTENTWIDTH);
    if (maxContentWidth != null && maxContentWidth > docTotalWidth) {
      return false;
    }
    return true;
  }

  @Override
  public void appendHeader(GntmlBlock gntmlBlock,
                           Appendable out)
      throws IOException
  {
    String cssClass = StringUtils.toString(get(CSSCLASS));
    if (hasDefaultMargins()) {
      cssClass = GntmlMgr.addCssClass(cssClass, CSS_DEFAULTMARGINS);
    }
    cssClass = GntmlMgr.addCssClass(cssClass, gntmlBlock.getCssClass());
    HtmlUtilsTheme.openDivOfIdClass(out, gntmlBlock.getCssId(), cssClass);
    GntmlMgr.appendOpenInnerDiv(out);
    GntmlBlockStyles.appendOpenDimensions(this, out);
  }

  @Override
  public void appendFooter(GntmlBlock gntmlBlock,
                           Appendable out)
      throws IOException
  {
    GntmlBlockStyles.appendCloseDimensions(this, out);
    HtmlUtilsTheme.closeDivs(out, 2);
  }

  @Override
  public boolean outputsWebmacro()
  {
    return GntmlBlockStyles.outputsWebmacro(this);
  }
}
