package org.awtwf.gntml.tools;

import org.cord.node.NodeManager;
import org.cord.node.view.AlternativeTemplateFactory;

public class ContentCheck
  extends AlternativeTemplateFactory
{
  public final static String NAME = "contentCheck";

  /** Default template used if no special template is defined. */
  public final static String TEMPLATEPATH_DEFAULT = "Gntml/tools/contentCheck.view.wm.html";

  public ContentCheck(NodeManager nodeManager,
                      Integer aclId)
  {
    super(NAME,
          nodeManager,
          TEMPLATEPATH_DEFAULT,
          true,
          true,
          aclId,
          "You do not have permission to check the site content");
  }

  /**
   * Use the specified templatePath: isRenderReset and isRootTemplatePath are set to false
   */
  public ContentCheck(NodeManager nodeManager,
                      Integer aclId,
                      String templatePath)
  {
    super(NAME,
          nodeManager,
          templatePath,
          false,
          false,
          aclId,
          "You do not have permission to check the site content");
  }
}
