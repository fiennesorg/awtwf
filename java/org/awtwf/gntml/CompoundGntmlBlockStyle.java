package org.awtwf.gntml;

import java.io.IOException;
import java.util.Iterator;

import org.cord.mirror.TransientRecord;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.NamedImpl;
import org.cord.util.StringUtils;

import com.google.common.base.Strings;

public class CompoundGntmlBlockStyle
  extends NamedImpl
  implements GntmlBlockStyle
{
  private static StringBuilder addClass(StringBuilder buf,
                                        Object cssClass)
  {
    if (cssClass == null) {
      return buf;
    }
    String cssClassName = cssClass.toString();
    if (cssClassName.length() == 0) {
      return buf;
    }
    if (buf.length() != 0) {
      buf.append(' ');
    }
    buf.append(cssClassName);
    return buf;
  }

  /**
   * @return Either a ValidGntmlBlockStyle or a CompoundGntmlBlockStyle if all the parameters passed
   *         in are valid, or a GntmlBlockStyleError if there is a conflict of padding between the
   *         blocks.
   */
  public static GntmlBlockStyle create(GntmlMgr gntmlMgr,
                                       TransientRecord gntmlStyle,
                                       String name,
                                       DefinedGntmlBlockStyle primaryBlock,
                                       Iterable<DefinedGntmlBlockStyle> plusBlockStyles)
      throws GntmlBlockStyleException
  {
    if (plusBlockStyles == null) {
      if (primaryBlock == null) {
        throw new GntmlBlockStyleException(String.format("No styles defined in {%s}", name), name);
      }
    }
    Iterator<DefinedGntmlBlockStyle> i = plusBlockStyles.iterator();
    if (!i.hasNext()) {
      if (primaryBlock == null) {
        throw new GntmlBlockStyleException(String.format("No styles defined in {%s}", name), name);
      }
      return primaryBlock;
    }
    Object padding = primaryBlock == null ? null : primaryBlock.get(GntmlBlockStyle.PADDING);
    StringBuilder css = new StringBuilder();
    if (primaryBlock != null) {
      addClass(css, primaryBlock.get(GntmlBlockStyle.CSSCLASS));
      if (primaryBlock.hasDefaultMargins()) {
        addClass(css, DefinedGntmlBlockStyle.CSS_DEFAULTMARGINS);
      }
    } else {
      addClass(css, DefinedGntmlBlockStyle.CSS_DEFAULTMARGINS);
    }
    while (i.hasNext()) {
      DefinedGntmlBlockStyle plusBlockStyle = i.next();
      Object plusPadding = plusBlockStyle.get(GntmlBlockStyle.PADDING);
      if (plusPadding != null) {
        if (padding == null) {
          padding = plusPadding;
        } else if (!plusPadding.equals(padding)) {
          throw new GntmlBlockStyleException(String.format("%s defines a padding of %s which clashes with %s",
                                                           plusBlockStyle,
                                                           plusPadding,
                                                           padding),
                                             name);
        }
      }
      addClass(css, plusBlockStyle.get(GntmlBlockStyle.CSSCLASS));
    }
    return new CompoundGntmlBlockStyle(name, css.toString(), primaryBlock, plusBlockStyles);
  }

  private final String __cssClass;
  private final DefinedGntmlBlockStyle __primaryBlock;
  private final Iterable<DefinedGntmlBlockStyle> __plusBlockStyles;

  private CompoundGntmlBlockStyle(String name,
                                  String cssClass,
                                  DefinedGntmlBlockStyle primaryBlock,
                                  Iterable<DefinedGntmlBlockStyle> plusBlockStyles)
  {
    super(name);
    __cssClass = cssClass;
    __primaryBlock = primaryBlock;
    __plusBlockStyles = plusBlockStyles;
  }

  @Override
  public boolean accepts(TransientRecord gntmlStyle)
  {
    if (!__primaryBlock.accepts(gntmlStyle)) {
      return false;
    }
    for (GntmlBlockStyle plusStyle : __plusBlockStyles) {
      if (!plusStyle.accepts(gntmlStyle)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public void appendHeader(GntmlBlock gntmlBlock,
                           Appendable out)
      throws IOException
  {
    for (DefinedGntmlBlockStyle plusBlockStyle : __plusBlockStyles) {
      String headerSpanCssClass =
          StringUtils.toString(plusBlockStyle.get(DefinedGntmlBlockStyle.HEADERSPANCSSCLASS));
      if (!Strings.isNullOrEmpty(headerSpanCssClass)) {
        String headerSpanHasPrimaryClass =
            (String) plusBlockStyle.get(DefinedGntmlBlockStyle.HEADERSPANHASPRIMARYCLASS);
        if ("true".equals(headerSpanHasPrimaryClass)) {
          headerSpanCssClass =
              GntmlMgr.addCssClass(headerSpanCssClass, StringUtils.toString(get(CSSCLASS)));
        } else if ("all".equals(headerSpanHasPrimaryClass)) {
          headerSpanCssClass = GntmlMgr.addCssClass(headerSpanCssClass, __cssClass);
          headerSpanCssClass = GntmlMgr.addCssClass(headerSpanCssClass, gntmlBlock.getCssClass());
        }
        HtmlUtilsTheme.openSpanOfClass(out, headerSpanCssClass);
        HtmlUtilsTheme.closeSpan(out);
      }
    }
    HtmlUtilsTheme.openDivOfIdClass(out,
                                    gntmlBlock.getCssId(),
                                    GntmlMgr.addCssClass(__cssClass, gntmlBlock.getCssClass()));
    GntmlMgr.appendOpenInnerDiv(out);
    GntmlBlockStyles.appendOpenDimensions(this, out);
  }

  @Override
  public void appendFooter(GntmlBlock gntmlBlock,
                           Appendable out)
      throws IOException
  {
    GntmlBlockStyles.appendCloseDimensions(this, out);
    HtmlUtilsTheme.closeDivs(out, 2);
  }

  @Override
  public boolean outputsWebmacro()
  {
    return GntmlBlockStyles.outputsWebmacro(this);
  }

  /**
   * Check to see if either the primary or any of the plus blocks define a value for the key
   */
  @Override
  public boolean containsKey(Object key)
  {
    if (__primaryBlock != null && __primaryBlock.containsKey(key)) {
      return true;
    }
    for (GntmlBlockStyle plusBlockStyle : __plusBlockStyles) {
      if (plusBlockStyle.containsKey(key)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Return the first block out of primary block and then plus blocks that has a value for this key,
   * or null if nobody does.
   */
  @Override
  public Object get(Object key)
  {
    Object o = __primaryBlock != null ? __primaryBlock.get(key) : null;
    if (o != null) {
      return o;
    }
    for (GntmlBlockStyle plusBlockStyle : __plusBlockStyles) {
      o = plusBlockStyle.get(key);
      if (o != null) {
        return o;
      }
    }
    return null;
  }
}
