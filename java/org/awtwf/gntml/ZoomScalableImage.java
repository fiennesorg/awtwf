package org.awtwf.gntml;

import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.PersistentRecord;
import org.cord.node.FeedbackErrorException;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeRequest;
import org.cord.node.NodeRequestException;
import org.cord.node.NodeRequestSegment;
import org.cord.node.TemplateSuccessOperation;
import org.cord.node.view.DynamicAclAuthenticatedFactory;
import org.cord.util.GettableUtils;
import org.webmacro.Context;

public class ZoomScalableImage
  extends DynamicAclAuthenticatedFactory
{
  public static final String INIT_TEMPLATEPATH = "ZoomScalableImage.templatePath";
  public static final String DEFAULT_TEMPLATEPATH = "InForm/views/zoomScalableImage.wm.html";
  public static final String INIT_AUTHENTICATIONERROR = "ZoomScalableImage.authenticationError";

  public static ZoomScalableImage getInstance(NodeManager nodeMgr)
  {
    String templatePath = GettableUtils.get(nodeMgr, INIT_TEMPLATEPATH, DEFAULT_TEMPLATEPATH);
    String authenticationError =
        GettableUtils.get(nodeMgr,
                          INIT_AUTHENTICATIONERROR,
                          "you do not have permission to view this image");
    return new ZoomScalableImage(nodeMgr, templatePath, authenticationError);
  }

  public static final String NAME = "zoomScalableImage";

  public static final String CGI_ID = "id";

  public static final String WM_GNTMLSCALABLEIMAGE = "gntmlScalableImage";
  public static final String WM_ZOOMEDGNTMLSCALABLEIMAGE = "zoomedGntmlScalableImage";

  public ZoomScalableImage(NodeManager nodeMgr,
                           String templatePath,
                           String authenticationError)
  {
    super(NAME,
          nodeMgr,
          new TemplateSuccessOperation(nodeMgr, templatePath),
          Node.VIEWACL,
          authenticationError);
  }

  @Override
  protected NodeRequestSegment getInstance(NodeRequest nodeRequest,
                                           PersistentRecord node,
                                           Context context,
                                           PersistentRecord acl)
      throws NodeRequestException
  {
    PersistentRecord gntmlScalableImage = null;
    try {
      gntmlScalableImage =
          getNodeManager().getDb()
                          .getTable(ScalableImageFactory.TABLENAME)
                          .getRecord(nodeRequest.get(CGI_ID));
    } catch (MirrorNoSuchRecordException e) {
      throw new FeedbackErrorException(null,
                                       node,
                                       e,
                                       false,
                                       true,
                                       "The image you have requested is not found");
    }
    context.put(WM_GNTMLSCALABLEIMAGE, gntmlScalableImage);
    context.put(WM_ZOOMEDGNTMLSCALABLEIMAGE, gntmlScalableImage);
    return handleSuccess(nodeRequest, node, context, null);
  }
}
