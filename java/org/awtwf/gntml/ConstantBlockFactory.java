package org.awtwf.gntml;

import java.io.IOException;
import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Transaction;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.Settable;
import org.cord.util.SettableException;

import com.google.common.base.Preconditions;

/**
 * BlockFactory that maps a constant isolated line of GNTML into a constant line of XHTML. Content
 * is not permitted inside the block and an error message is raised in the document if one tries to
 * do so.
 * 
 * @author alex
 */
public class ConstantBlockFactory
  extends EnglishNamedImpl
  implements GntmlBlockFactory
{
  private final String __gntml;
  private final String __xhtml;
  private final String __errorMessage;

  public ConstantBlockFactory(String name,
                              String gntml,
                              String xhtml,
                              String errorMessage)
  {
    super(name);
    __gntml = Preconditions.checkNotNull(gntml, "gntml");
    __xhtml = Preconditions.checkNotNull(xhtml, "xhtml");
    __errorMessage = Preconditions.checkNotNull(errorMessage, "errorMessage");
  }

  @Override
  public boolean acceptsSource(GntmlDocument gntmlDocument,
                               String firstSourceLine)
  {
    return __gntml.equals(firstSourceLine);
  }

  @Override
  public GntmlBlock buildBlock(ListIterator<String> sourceLines,
                               GntmlBlockStyle gntmlBlockStyle,
                               GntmlDocument gntmlDocument,
                               int blockId)
      throws GntmlException
  {
    try {
      return new ConstantBlock(sourceLines, gntmlBlockStyle, gntmlDocument, blockId);
    } catch (BlockBuildException bbEx) {
      return new ErrorBlock(bbEx.getLineList(),
                            gntmlBlockStyle,
                            gntmlDocument,
                            this,
                            "Error",
                            blockId,
                            __errorMessage);
    }
  }

  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
      throws GntmlException
  {
  }

  @Override
  public void preParse(GntmlDocument gntmlDocument,
                       Transaction transaction)
      throws GntmlException
  {
  }

  public class ConstantBlock
    extends GntmlBlock
  {
    protected ConstantBlock(ListIterator<String> sourceLines,
                            GntmlBlockStyle gntmlBlockStyle,
                            GntmlDocument gntmlDocument,
                            int blockId) throws BlockBuildException
    {
      super(ConstantBlockFactory.this.getName(),
            sourceLines,
            gntmlBlockStyle,
            gntmlDocument,
            ConstantBlockFactory.this,
            blockId);
      if (getLines().size() > 1) {
        throw new BlockBuildException("Nested content is not permitted inside a ConstantBlock",
                                      null,
                                      getLines());
      }
    }

    @Override
    public void copyGntmlBlock(Settable settable,
                               PersistentRecord fromNode,
                               PersistentRecord fromRegionStyle,
                               PersistentRecord toRegionStyle,
                               PersistentRecord fromGntmlInstance,
                               GntmlDocument gntmlDocument)
        throws SettableException
    {
    }

    @Override
    public Appendable parse(Appendable out,
                            Gettable documentParams,
                            Gettable blockParams,
                            Map<Object, Object> outputs)
        throws GntmlException, IOException
    {
      openDivs(out);
      out.append(__xhtml);
      closeDivs(out);
      return out;
    }

  }

}
