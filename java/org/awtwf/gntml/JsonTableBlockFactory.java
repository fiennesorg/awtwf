package org.awtwf.gntml;

import org.cord.mirror.Db;
import org.cord.mirror.FieldKey;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.Table;
import org.cord.mirror.field.JSONObjectField;
import org.cord.node.NodeManager;
import org.cord.util.DuplicateNamedException;
import org.json.JSONObject;

public abstract class JsonTableBlockFactory
  extends TableBlockFactory
{
  /**
   * RecordOperation that gives you the {@link JSONObject} encoded version of the
   * {@value #CONFIGSRC} variable.
   */
  public static final RecordOperationKey<JSONObject> CONFIG =
      RecordOperationKey.create("config", JSONObject.class);

  /**
   * The text field that holds the JSON source of the {@link #CONFIG} variable.
   */
  public static final FieldKey<String> CONFIGSRC = FieldKey.create("configSrc", String.class);

  private JSONObjectField _configSrc = null;

  public JsonTableBlockFactory(NodeManager nodeMgr,
                               String name,
                               String englishName,
                               String openTag,
                               String closeTag,
                               String tableName,
                               String viewTemplatePath,
                               String blockCssClass,
                               String captionCssClass,
                               String editTemplatePath,
                               String instanceName)
  {
    super(nodeMgr,
          name,
          englishName,
          openTag,
          closeTag,
          tableName,
          viewTemplatePath,
          blockCssClass,
          captionCssClass,
          editTemplatePath,
          instanceName);
  }

  protected final JSONObjectField getConfigSrc()
  {
    return _configSrc;
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table table)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    _configSrc = new JSONObjectField(CONFIGSRC, "Configuration", CONFIG, null);
    table.addField(_configSrc);
  }
}
