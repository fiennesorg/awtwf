package org.awtwf.gntml;

import java.io.File;
import java.io.IOException;
import java.util.ListIterator;
import java.util.Map;

import org.cord.mirror.Db;
import org.cord.mirror.Dependencies;
import org.cord.mirror.FieldKey;
import org.cord.mirror.IdList;
import org.cord.mirror.MirrorException;
import org.cord.mirror.MirrorFieldException;
import org.cord.mirror.MirrorInstantiationException;
import org.cord.mirror.MirrorNoSuchRecordException;
import org.cord.mirror.MirrorTableLockedException;
import org.cord.mirror.MirrorTransactionTimeoutException;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.RecordOperationKey;
import org.cord.mirror.RecordSource;
import org.cord.mirror.RecordStringFactory;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.TransientRecord;
import org.cord.mirror.Viewpoint;
import org.cord.mirror.WritableRecord;
import org.cord.mirror.field.BooleanField;
import org.cord.mirror.field.ConstantFieldValueSupplier;
import org.cord.mirror.field.EnglishNamedMapField;
import org.cord.mirror.field.LinkField;
import org.cord.mirror.field.StringField;
import org.cord.mirror.operation.FileFormElement;
import org.cord.mirror.operation.SimpleRecordOperation;
import org.cord.mirror.recordsource.ArrayIdList;
import org.cord.mirror.recordsource.ConstantRecordSource;
import org.cord.mirror.recordsource.RecordSources;
import org.cord.mirror.recordsource.UnmodifiableIdList;
import org.cord.node.CurrentUrlFactory;
import org.cord.node.FeedbackErrorException;
import org.cord.node.Node;
import org.cord.node.NodeManager;
import org.cord.node.NodeQuestion;
import org.cord.node.NodeRequest;
import org.cord.node.RedirectionSuccessOperation;
import org.cord.node.RegionStyle;
import org.cord.node.field.NodeLinkField;
import org.cord.node.img.ImgFormat;
import org.cord.node.img.ImgOriginal;
import org.cord.node.json.IdMappers;
import org.cord.node.json.JSONLoader;
import org.cord.node.json.JSONSaver;
import org.cord.node.json.UnresolvedRecordException;
import org.cord.node.links.OrderedNodeLinks;
import org.cord.node.links.RelatedPagesSupplier;
import org.cord.node.view.AskNodeQuestion;
import org.cord.util.DuplicateNamedException;
import org.cord.util.EnglishNamedImpl;
import org.cord.util.Gettable;
import org.cord.util.GettableUtils;
import org.cord.util.HtmlUtilsTheme;
import org.cord.util.JSONUtils;
import org.cord.util.Named;
import org.cord.util.Settable;
import org.cord.util.SettableException;
import org.cord.util.XmlUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.webmacro.Context;

import com.google.common.base.Strings;

public class ScalableImageFactory
  extends TableBlockFactory
  implements RelatedPagesSupplier
{
  public final static String IMAGE_OPEN_TAG = "|image:";

  public final static String IMAGE_CLOSE_TAG = "|";

  public final static String TABLENAME = "GntmlScalableImage";

  public final static RecordOperationKey<RecordSource> REFERENCES =
      RecordOperationKey.create("gntmlScalableImages", RecordSource.class);

  public final static String NAME = "ScalableImage";

  public final static String ENGLISHNAME = "Scalable Image";

  public final static String NAMEPREFIX_VALUE = "img";

  public final static FieldKey<String> TITLE = FieldKey.create("title", String.class);

  public final static FieldKey<EnglishNamedImpl> FUNCTIONALITY =
      FieldKey.create("functionality", EnglishNamedImpl.class);
  public static final String FUNCTIONALITY_NONE = "NONE";
  public static final String FUNCTIONALITY_ZOOM = "ZOOM";
  public static final String FUNCTIONALITY_NODE = "NODE";
  public static final String FUNCTIONALITY_EXTERNAL = "EXTERNAL";
  public static final String FUNCTIONALITY_DOWNLOAD = "DOWNLOAD";

  public final static FieldKey<String> ZOOMCAPTION = FieldKey.create("zoomCaption", String.class);

  public final static FieldKey<String> IMAGETARGET = FieldKey.create("imageTarget", String.class);

  public final static FieldKey<Integer> TARGETNODE_ID = FieldKey.create("targetNode_id",
                                                                        Integer.class);

  public final static RecordOperationKey<PersistentRecord> TARGETNODE =
      LinkField.getLinkName(TARGETNODE_ID);

  public final static RecordOperationKey<RecordSource> NODE_GNTMLSCALABLEIMAGES =
      RecordOperationKey.create("gntmlScalableImages", RecordSource.class);

  public final static FieldKey<Boolean> HASBACKLINK = FieldKey.create("hasBackLink", Boolean.class);

  public final static FieldKey<String> BACKLINKTITLE = FieldKey.create("backLinkTitle",
                                                                       String.class);

  public final static String VIEWTEMPLATEPATH = "Gntml/scalableImage.view.wm.html";

  public final static String EDITTEMPLATEPATH = "Gntml/scalableImage.edit.wm.html";

  public final static FieldKey<String> ALT = FieldKey.create("alt", String.class);

  public final static FieldKey<Integer> IMGFORMAT_ID =
      LinkField.getLinkFieldName(ImgFormat.TABLENAME);
  public final static RecordOperationKey<PersistentRecord> IMGFORMAT =
      LinkField.getLinkName(IMGFORMAT_ID);

  public final static FieldKey<Integer> IMGORIGINAL_ID =
      LinkField.getLinkFieldName(ImgOriginal.TABLENAME);
  public final static RecordOperationKey<PersistentRecord> IMGORIGINAL =
      LinkField.getLinkName(IMGORIGINAL_ID);
  public final static RecordOperationKey<RecordSource> IMGORIGINAL_GNTMLSCALABLEIMAGES =
      LinkField.getReferencesKey(TABLENAME);
  public final static RecordOperationKey<RecordSource> IMGORIGINAL_GNTMLSCALABLEIMAGENODES =
      RecordOperationKey.create("gntmlScalableImageNodes", RecordSource.class);

  public final static FieldKey<Boolean> ISDOWNLOADABLE = FieldKey.create("isDownloadable",
                                                                         Boolean.class);

  public static final String VIEW_BROWSESCALABLEIMAGETARGET = "browseScalableImageTarget";

  public static final RecordOperationKey<Object> CGI_FILE = RecordOperationKey.create("file",
                                                                                      Object.class);

  public ScalableImageFactory(NodeManager nodeManager)
  {
    super(nodeManager,
          NAME,
          ENGLISHNAME,
          IMAGE_OPEN_TAG,
          IMAGE_CLOSE_TAG,
          TABLENAME,
          VIEWTEMPLATEPATH,
          "gntml_image",
          "gntml_image_caption",
          EDITTEMPLATEPATH,
          null);
    nodeManager.getRelatedPagesManager().addSupplier(this);
    AskNodeQuestion askNodeQuestion =
        new AskNodeQuestion(VIEW_BROWSESCALABLEIMAGETARGET,
                            nodeManager,
                            new RedirectionSuccessOperation(nodeManager,
                                                            CurrentUrlFactory.getInstance()),
                            Node.EDITACL,
                            "You don't have permission to browse the target") {
          @Override
          protected NodeQuestion createNodeQuestion(NodeRequest nodeRequest,
                                                    final PersistentRecord node,
                                                    Context context)
              throws MirrorNoSuchRecordException
          {
            final String id = nodeRequest.getString("id");
            PersistentRecord gntmlScalableImage = getTable().getRecord(id);
            String question =
                String.format("Select target for Image named \"%s\" in \"%s\" found on \"%s\"",
                              gntmlScalableImage.opt(BLOCKNAME),
                              gntmlScalableImage.comp(GNTMLINSTANCE)
                                                .comp(Gntml.REGIONSTYLE)
                                                .opt(RegionStyle.NAME),
                              node.opt(Node.TITLE));
            final String nameprefix = gntmlScalableImage.opt(NAMEPREFIX);
            final String encodedNamespace = HtmlUtilsTheme.urlEncode(nameprefix);
            Transaction transaction = getTransaction(nodeRequest, node);
            NodeQuestion nodeQuestion = new NodeQuestion(getName() + gntmlScalableImage.getId(),
                                                         question,
                                                         "GNTML Image Link Target",
                                                         node,
                                                         1,
                                                         null,
                                                         transaction) {
              @Override
              public String getAnswerUrl()
              {
                StringBuilder url = new StringBuilder();
                url.append(node.opt(Node.URL));
                url.append("?view=update");
                url.append('&')
                   .append(encodedNamespace)
                   .append("targetNode_id=")
                   .append(getSelectedNodeId())
                   .append('&')
                   .append(encodedNamespace)
                   .append(TableBlock.ISACTIVE)
                   .append("=true");
                return url.toString();
              }

              @Override
              public boolean maySubmit()
              {
                return getSelectedNodeIds().size() == 1;
              }

              @Override
              public boolean shouldSubmit()
              {
                return maySubmit();
              }
            };
            return nodeQuestion;
          }
        };
    nodeManager.getNodeRequestSegmentManager().register(Transaction.TRANSACTION_UPDATE,
                                                        askNodeQuestion);
  }

  @Override
  public void postParse(GntmlDocument gntmlDocument,
                        Transaction transaction)
  {
  }

  @Override
  protected void init(Db db,
                      String pwd,
                      Table gntmlScalableImages)
      throws MirrorTableLockedException, DuplicateNamedException
  {
    gntmlScalableImages.addField(new StringField(TITLE, "Title that appears above the image"));
    EnglishNamedImpl none = new EnglishNamedImpl(FUNCTIONALITY_NONE, "None");
    EnglishNamedMapField<EnglishNamedImpl> functionality =
        new EnglishNamedMapField<EnglishNamedImpl>(FUNCTIONALITY,
                                                   "Action on clicking image",
                                                   EnglishNamedImpl.class,
                                                   ConstantFieldValueSupplier.getInstance(none)) {
          /**
           * @return true
           */
          @Override
          public boolean shouldReloadOnChange()
          {
            return true;
          }
        };
    functionality.addValue(none);
    functionality.addValue(new EnglishNamedImpl(FUNCTIONALITY_ZOOM, "View zoomed image"));
    functionality.addValue(new EnglishNamedImpl(FUNCTIONALITY_NODE, "Go to a page on this site"));
    functionality.addValue(new EnglishNamedImpl(FUNCTIONALITY_EXTERNAL, "Go to an external page"));
    functionality.addValue(new EnglishNamedImpl(FUNCTIONALITY_DOWNLOAD,
                                                "Download the original image"));
    gntmlScalableImages.addField(functionality);
    gntmlScalableImages.addField(new StringField(ZOOMCAPTION,
                                                 "Caption text that appears below zoomed image").setMayBeTextArea(true));
    gntmlScalableImages.addField(new StringField(IMAGETARGET,
                                                 "URL of external page (include http://)"));
    RecordStringFactory browseParams = new RecordStringFactory() {
      @Override
      public String getValue(TransientRecord gntmlScalableImage)
      {
        TransientRecord.assertIsTableNamed(gntmlScalableImage, TABLENAME);
        StringBuilder buf = new StringBuilder();
        buf.append("view=" + VIEW_BROWSESCALABLEIMAGETARGET + "&id=")
           .append(gntmlScalableImage.getId());
        return buf.toString();
      }
    };
    gntmlScalableImages.addField(new NodeLinkField(TARGETNODE_ID,
                                                   "Select internal target page",
                                                   null,
                                                   TARGETNODE,
                                                   NODE_GNTMLSCALABLEIMAGES,
                                                   null,
                                                   pwd,
                                                   Dependencies.LINK_ENFORCED
                                                       | Dependencies.BIT_OPTIONAL,
                                                   null,
                                                   browseParams));
    gntmlScalableImages.addField(new BooleanField(HASBACKLINK,
                                                  "Generate a link from the target page back to this page?") {
      /**
       * @return true to force the interface to rebuild the dependent fields
       */
      @Override
      public boolean shouldReloadOnChange()
      {
        return true;
      }
    });
    gntmlScalableImages.addField(new StringField(BACKLINKTITLE,
                                                 "Clickable text that appears on target page"));
    gntmlScalableImages.addField(new StringField(ALT, "ALT - text description of image contents"));
    gntmlScalableImages.addField(new LinkField(ImgFormat.TABLENAME,
                                               "Scaled Image Format",
                                               pwd,
                                               Dependencies.BIT_ENFORCED
                                                   | Dependencies.BIT_OPTIONAL) {

      private final String __filter = ImgFormat.ISDEFINEDFORMAT + "=1";

      @Override
      protected RecordSource getValidTargets(TransientRecord gntmlScalableImage)
      {
        return getTargetTable().getQuery(__filter, __filter, null);
      }
    });
    gntmlScalableImages.addField(new LinkField(ImgOriginal.TABLENAME,
                                               "Previously uploaded images",
                                               pwd,
                                               Dependencies.BIT_ENFORCED
                                                   | Dependencies.BIT_OPTIONAL) {
      @Override
      public boolean shouldReloadOnChange()
      {
        return true;
      }
    });
    final ImgOriginal ImgOriginal_ = getNodeManager().getImgMgr().getImgOriginal();
    ImgOriginal_.getTable()
                .addRecordOperation(new SimpleRecordOperation<RecordSource>(IMGORIGINAL_GNTMLSCALABLEIMAGENODES) {
                  @Override
                  public RecordSource getOperation(TransientRecord imgOriginal,
                                                   Viewpoint viewpoint)
                  {
                    IdList nodeIds = new ArrayIdList(getNodeManager().getNode());
                    for (PersistentRecord gntmlScalableImage : imgOriginal.comp(IMGORIGINAL_GNTMLSCALABLEIMAGES,
                                                                                viewpoint)) {
                      Integer nodeId =
                          gntmlScalableImage.comp(GNTMLINSTANCE, viewpoint)
                                            .comp(Gntml.NODE, viewpoint)
                                            .getId();
                      if (!nodeIds.contains(nodeId)) {
                        nodeIds.add(nodeId);
                      }
                    }
                    return RecordSources.viewedFrom(new ConstantRecordSource(new UnmodifiableIdList(nodeIds),
                                                                             null),
                                                    viewpoint);
                  }
                });
    ImgOriginal_.registerNodeResolver(IMGORIGINAL_GNTMLSCALABLEIMAGENODES);
    gntmlScalableImages.addField(new BooleanField(ISDOWNLOADABLE, "Is original image downloadable?"));
    gntmlScalableImages.addRecordOperation(new FileFormElement(CGI_FILE, "Upload new image"));
  }

  @Override
  protected TableBlock buildTableBlock(ListIterator<String> sourceLines,
                                       GntmlBlockStyle blockStyle,
                                       GntmlDocument gntmlDocument,
                                       int blockId)
      throws GntmlException
  {
    return new ScalableImageBlock(sourceLines, blockStyle, gntmlDocument, blockId);
  }

  @Override
  public void shutdown()
  {
  }

  public class ScalableImageBlock
    extends TableBlock
  {
    protected ScalableImageBlock(ListIterator<String> sourceLines,
                                 GntmlBlockStyle blockStyle,
                                 GntmlDocument gntmlDocument,
                                 int blockId) throws GntmlException
    {
      super(NAME,
            sourceLines,
            blockStyle,
            gntmlDocument,
            ScalableImageFactory.this,
            blockId);
    }

    @Override
    public void copyGntmlBlock(Settable settable,
                               PersistentRecord fromNode,
                               PersistentRecord fromRegionStyle,
                               PersistentRecord toRegionStyle,
                               PersistentRecord fromGntmlInstance,
                               GntmlDocument gntmlDocument,
                               PersistentRecord gntmlScalableImage)
        throws SettableException
    {
      GettableUtils.set(settable,
                        gntmlScalableImage,
                        TITLE,
                        FUNCTIONALITY,
                        IMAGETARGET,
                        TARGETNODE_ID,
                        HASBACKLINK,
                        BACKLINKTITLE,
                        ZOOMCAPTION,
                        ALT,
                        IMGFORMAT_ID,
                        IMGORIGINAL_ID,
                        ISDOWNLOADABLE);
    }

    @Override
    public void saveJSON(JSONSaver jSaver,
                         PersistentRecord node,
                         PersistentRecord regionStyle,
                         PersistentRecord gntmlInstance,
                         GntmlDocument gntmlDocument,
                         JSONObject jGntml,
                         JSONObject jBlock)
        throws JSONException, IOException
    {
      PersistentRecord instance = getInstance();
      JSONUtils.put(jBlock,
                    instance,
                    TITLE,
                    FUNCTIONALITY,
                    IMAGETARGET,
                    TARGETNODE_ID,
                    HASBACKLINK,
                    BACKLINKTITLE,
                    ZOOMCAPTION,
                    ALT,
                    IMGFORMAT_ID,
                    IMGORIGINAL_ID,
                    ISDOWNLOADABLE);
      jSaver.save(instance.opt(IMGFORMAT));
      jSaver.save(instance.opt(IMGORIGINAL));
    }

    @Override
    public void loadJSON(JSONLoader jLoader,
                         PersistentRecord node,
                         PersistentRecord regionStyle,
                         JSONObject jGntml,
                         JSONObject jBlock,
                         Settable settable)
        throws SettableException, UnresolvedRecordException, JSONException,
        MirrorTransactionTimeoutException, MirrorFieldException, MirrorInstantiationException,
        MirrorNoSuchRecordException, IOException, FeedbackErrorException
    {
      String nameHeader =
          getGntmlBlockFactory().getName() + "-" + XmlUtils.encodeName(getBlockName()) + "-";
      settable.set(nameHeader + ISACTIVE, Boolean.TRUE);
      settable.set(nameHeader + TITLE, jBlock.opt(TITLE.getKeyName()));
      settable.set(nameHeader + FUNCTIONALITY, jBlock.opt(FUNCTIONALITY.getKeyName()));
      settable.set(nameHeader + TARGETNODE_ID,
                   jLoader.mapId(node,
                                 regionStyle,
                                 getNodeManager().getNode(),
                                 jBlock.optInt(TARGETNODE_ID.getKeyName())));
      settable.set(nameHeader + HASBACKLINK, jBlock.opt(HASBACKLINK.getKeyName()));
      settable.set(nameHeader + BACKLINKTITLE, jBlock.opt(BACKLINKTITLE.getKeyName()));
      settable.set(nameHeader + ZOOMCAPTION, jBlock.opt(ZOOMCAPTION.getKeyName()));
      settable.set(nameHeader + ALT, jBlock.opt(ALT.getKeyName()));
      // TODO: decide about whether we filter loading of ImgFormat #2929
      settable.set(nameHeader + IMGFORMAT_ID, jBlock.opt(IMGFORMAT_ID.getKeyName()));
      Integer saved_imgOriginal_id = jBlock.optInt(IMGORIGINAL_ID.getKeyName());
      if (saved_imgOriginal_id != null && saved_imgOriginal_id > 0) {
        Integer live_imgOriginal_id =
            jLoader.getIdMapper().getMapping(ImgOriginal.TABLENAME, saved_imgOriginal_id);
        if (live_imgOriginal_id == null) {
          PersistentRecord imgOriginal =
              jLoader.loadRecord(getNodeManager().getImgMgr().getImgOriginal(),
                                 saved_imgOriginal_id);
          if (imgOriginal == null) {
            throw new RuntimeException("Unable to load ImgOriginal - unsure why...");
          }
          live_imgOriginal_id = imgOriginal.getId();
        }
        settable.set(nameHeader + IMGORIGINAL_ID, live_imgOriginal_id);
      }
      settable.set(nameHeader + ISDOWNLOADABLE, jBlock.opt(ISDOWNLOADABLE.getKeyName()));
    }

    @Override
    protected void updateInstance(WritableRecord gntmlScalableImage,
                                  Gettable params,
                                  Map<Object, Object> outputs)
        throws GntmlException, MirrorException
    {
      gntmlScalableImage.setFieldIfDefined(TITLE, params.get(TITLE));
      gntmlScalableImage.setFieldIfDefined(FUNCTIONALITY, params.get(FUNCTIONALITY));
      gntmlScalableImage.setFieldIfDefined(IMAGETARGET, params.get(IMAGETARGET));
      gntmlScalableImage.setFieldIfDefined(TARGETNODE_ID,
                                           IdMappers.map(params,
                                                         Node.TABLENAME,
                                                         params.get(TARGETNODE_ID)));
      gntmlScalableImage.setFieldIfDefined(HASBACKLINK, params.getBoolean(HASBACKLINK));
      gntmlScalableImage.setFieldIfDefined(BACKLINKTITLE, params.get(BACKLINKTITLE));
      File file = params.getFile(CGI_FILE.getKeyName());
      if (file != null) {
        try {
          PersistentRecord imgOriginal =
              getNodeManager().getImgMgr().getImgOriginal().createInstance(file);
          gntmlScalableImage.setField(IMGORIGINAL_ID, imgOriginal);
        } catch (IOException e) {
          throw new GntmlException(String.format("It has not been possible to identify %s as an image file for |image:%s|",
                                                 file.getName(),
                                                 gntmlScalableImage.opt(BLOCKNAME)),
                                   e);
        }
      } else {
        gntmlScalableImage.setFieldIfDefined(IMGORIGINAL_ID, params.get(IMGORIGINAL_ID));
      }
      gntmlScalableImage.setFieldIfDefined(ZOOMCAPTION, params.get(ZOOMCAPTION));
      PersistentRecord imgFormat =
          getNodeManager().getImgMgr()
                          .getImgFormat()
                          .getTable()
                          .getOptRecord(params.get(IMGFORMAT_ID));
      if (imgFormat == null) {
        gntmlScalableImage.setField(IMGFORMAT_ID, getNodeManager().getImgMgr()
                                                                  .getImgScaled()
                                                                  .getDefaultImgFormat());
      } else {
        gntmlScalableImage.setField(IMGFORMAT_ID, imgFormat);
      }
      gntmlScalableImage.setFieldIfDefined(ALT, params.get(ALT));
      gntmlScalableImage.setFieldIfDefined(ISDOWNLOADABLE, params.getBoolean(ISDOWNLOADABLE));
    }
  }

  @Override
  public void addRelatedPages(PersistentRecord node,
                              OrderedNodeLinks nodeLinks)
  {
    for (PersistentRecord gntmlImageInstance : node.opt(NODE_GNTMLSCALABLEIMAGES)) {
      Named functionality = gntmlImageInstance.opt(FUNCTIONALITY);
      if (FUNCTIONALITY_NODE.equals(functionality.getName())
          && gntmlImageInstance.comp(HASBACKLINK)) {
        PersistentRecord targetNode = gntmlImageInstance.comp(GNTMLINSTANCE).comp(Gntml.NODE);
        String targetTitle = gntmlImageInstance.opt(BACKLINKTITLE);
        if (Strings.isNullOrEmpty(targetTitle)) {
          targetTitle = targetNode.opt(Node.TITLE);
        }
        nodeLinks.addNodeLabel(targetNode, targetTitle);
      }
    }
  }
}
