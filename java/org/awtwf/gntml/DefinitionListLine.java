package org.awtwf.gntml;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cord.util.Gettable;
import org.cord.util.StringUtils;

public class DefinitionListLine
  extends GntmlLine
{
  public static final Pattern DEFINITIONLIST = Pattern.compile("(.*)::(.*)");

  private final Matcher __matcher;

  public DefinitionListLine(String source,
                            GntmlBlock gntmlBlock)
  {
    super(source,
          gntmlBlock);
    __matcher = DEFINITIONLIST.matcher(getSource());
  }

  public boolean isListLine()
  {
    return __matcher.matches();
  }

  public Appendable parse(Appendable buf,
                          Gettable params)
      throws GntmlException, IOException
  {
    if (isListLine()) {
      String type = __matcher.group(1).trim();
      if (StringUtils.notEmpty(type)) {
        buf.append("<dt>");
        parse(buf, params, type, true);
        buf.append("</dt>\n");
      }
      String definition = __matcher.group(2).trim();
      if (StringUtils.notEmpty(definition)) {
        buf.append("<dd>");
        parse(buf, params, definition, true);
      }
    } else {
      super.parse(buf, params, true);
    }
    return buf;
  }
}
