package org.awtwf.gntml2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test
{
  private final Pattern __styleBlocksPattern = Pattern.compile("\n{2,}", Pattern.DOTALL);

  public String[] splitStyleBlocks(CharSequence source)
  {
    return __styleBlocksPattern.split(source);
  }

  private final Pattern __styleHeaderPattern = Pattern.compile("(?:\\{([^\\}]*)\\}\n)?(.*)",
                                                               Pattern.DOTALL);

  private void parseStyleBlock(String styleBlock,
                               StringBuffer out)
  {
    Matcher m = __styleHeaderPattern.matcher(styleBlock);
    if (m.find()) {
      String style = m.group(1);
      if (style != null) {
        out.append("<div class=\"gntml_").append(style).append("\">\n\n");
      }
      String[] primitiveBlocks = splitPrimitiveBlocks(m.group(2));
      for (int i = 0; i < primitiveBlocks.length; i++) {
        parsePrimitiveBlock(primitiveBlocks[i], out);
      }
      if (style != null) {
        out.append("</div>\n\n");
      }
    }
  }

  private final Pattern __primitiveBlocksPattern = Pattern.compile("\n\\.\\.\\.\n", Pattern.DOTALL);

  private String[] splitPrimitiveBlocks(CharSequence source)
  {
    return __primitiveBlocksPattern.split(source);
  }

  // private void parsePrimitiveBlock(String primitiveBlock, StringBuffer out)
  // {
  // out.append("<p>").append(__linesPattern.matcher(primitiveBlock).replaceAll("<br
  // />\n")).append("</p>\n\n");
  // }
  private void parsePrimitiveBlock(String primitiveBlock,
                                   StringBuffer out)
  {
    String[] lines = __linesPattern.split(primitiveBlock);
    out.append("<ul>\n");
    for (int i = 0; i < lines.length; i++) {
      out.append("<li>").append(lines[i]).append("</li>\n");
    }
    out.append("</ul>\n\n");
  }

  private final Pattern __linesPattern = Pattern.compile("\n", Pattern.DOTALL);

  public void parseSource(String source,
                          StringBuffer out)
  {
    String[] styleBlocks = splitStyleBlocks(source);
    for (int i = 0; i < styleBlocks.length; i++) {
      parseStyleBlock(styleBlocks[i], out);
    }
  }

  public Test()
  {
    String source = "{style}\n123\n456\n...\n789\n\n\n{random}\nABC\nDEF\n\nGHI";
    StringBuffer out = new StringBuffer();
    parseSource(source, out);
    System.out.println(source);
    System.out.println("---");
    System.out.println(out);
  }

  public static void main(String[] args)
  {
    new Test();
  }
}
